//
//  Place.h
//  Qlumi
//
//  Created by William Smillie on 10/30/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>


typedef enum : NSUInteger {
    GPlaceTypeFood,
    GPlaceTypeLiquor,
    GPlaceTypeCar,
    GPlaceTypeTransportation,
    GPlaceTypeShopping,
    GPlaceTypeWorship,
    GPlaceTypeEstablishment,
    GPlaceTypeGovernment,
    GPlaceTypeEmergencyService,
    GPlaceTypeHealth,
    GPlaceTypeMoney,
    GPlaceTypeParking,
    GPlaceTypeLodging,
    GPlaceTypeFilm,
    GPlaceTypeOutdoors,
    GPlaceTypeFestival,
    GPlaceTypeCasino,
    GPlaceTypeBowling,
    GPlaceTypeStadium,
    GPlaceTypeRVPark,
    GPlaceTypeGym,
    GPlaceTypeNote,
    GPlaceTypeStartPoint,
    GPlaceTypeAd
} GPlaceType;

@interface Place : NSObject

@property (nonatomic, strong) NSString *placeId;

@property (nonatomic, strong) UIImage *thumbnail;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *details;
@property float rating;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSURL *website;
@property (nonatomic, strong) NSURL *phoneNumber;
@property (nonatomic, strong) NSURL *photoURL;

@property BOOL enabled;

@property CLLocationCoordinate2D location;
@property id annotation;
@property GPlaceType type;


-(id)initWithDictionary:(NSDictionary *)dict;
-(id)initWithPredicationDict:(NSDictionary *)dict;

+(GPlaceType)typeForPlace:(NSString *)typeString;
+(UIImage *)iconForType:(GPlaceType)type;
+(UIColor *)colorForType:(GPlaceType)type;

@end

