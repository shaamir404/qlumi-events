//
//  Place.m
//  Qlumi
//
//  Created by William Smillie on 10/30/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import "Place.h"
#import "QMapAnnotation.h"

@implementation Place

-(id)initWithDictionary:(NSDictionary *)dict{
    self = [super init];
    if(self) {
        
        NSString *phoneString = dict[@"international_phone_number"];
        phoneString = [phoneString stringByReplacingOccurrencesOfString:@" " withString:@""];
        phoneString = [phoneString stringByReplacingOccurrencesOfString:@"-" withString:@""];

        //Meta
        self.placeId = dict[@"place_id"];
        self.title = dict[@"name"];
        self.rating = [dict[@"rating"] floatValue];
        self.address = dict[@"vicinity"];
        self.website = [NSURL URLWithString:dict[@"website"]];
        self.phoneNumber = [NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", phoneString]];
        self.location = CLLocationCoordinate2DMake([dict[@"geometry"][@"location"][@"lat"] floatValue], [dict[@"geometry"][@"location"][@"lng"] floatValue]);
        self.photoURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=%@&key=AIzaSyC67qOLCwRi1_6Z8g1zKA6OQkJekYIBDz8", dict[@"photos"][0][@"photo_reference"]]];
        self.type = [Place typeForPlace:dict[@"types"][0]];
        
        
        QMapAnnotation *annotation = [[QMapAnnotation alloc] init];
        [annotation setCoordinate:self.location];
        [annotation setTitle:self.title];
        [annotation setSubtitle:self.address];
        annotation.place = self;
        self.annotation = annotation;
    }
    return self;
}

-(id)initWithPredicationDict:(NSDictionary *)dict{
    if (self = [super init]) {
        self.placeId = dict[@"place_id"];
        self.title = dict[@"description"];
        self.address = dict[@"description"];
        self.type = [Place typeForPlace:dict[@"types"][0]];
    }
    return self;
}

#pragma mark - Class Methods

+(GPlaceType)typeForPlace:(NSString *)typeString{
    NSSet *foodSet = [NSSet setWithObjects:@"bakery", @"cafe", @"food", @"restaurant", @"meal_delivery", @"meal_takeaway", nil];
    NSSet *liquordSet = [NSSet setWithObjects:@"bar", @"liquor_store", @"night_club", nil];
    NSSet *carSet = [NSSet setWithObjects:@"car_dealer", @"car_rental", @"car_repair", @"car_wash", @"gas_station", nil];
    NSSet *transportationSet = [NSSet setWithObjects:@"bus_station", @"airport", @"taxi_stand", @"train_station", @"transit_station", @"subway_station", nil];
    NSSet *shoppingSet = [NSSet setWithObjects:@"clothing_store", @"convenience_store", @"department_store", @"electronics_store", @"store", @"furniture_store",
                          @"pet_store", @"book_store", @"jewelry_store", @"hardware_store", @"home_goods_store", @"shoe_store", @"shopping_mall", @"grocery_or_supermarket", nil];
    NSSet *worshipSet = [NSSet setWithObjects:@"mosque", @"church", @"hindu_temple", @"place_of_worship", @"synagogue", nil];
    NSSet *establishmentsSet = [NSSet setWithObjects:@"establishment", @"beauty_salon", @"finance", @"general_contractor", @"locksmith", @"florist", @"lawyer", @"electrician", @"storage", @"travel_agency", @"real_estate_agency", @"roofing_contractor", @"insurance_agency", @"moving_company", @"hair_care", @"laundry", @"bicycle_store", @"plumber", @"art_gallery", @"funeral_home", @"painter", @"aquarium", @"spa", nil];
    NSSet *governmentSet = [NSSet setWithObjects:@"city_hall", @"local_government_office", @"embassy", @"courthouse", @"school", @"museum", @"post_office", @"* university", nil];
    NSSet *emergencySet = [NSSet setWithObjects:@"fire_station", @"police", nil];
    NSSet *healthSet = [NSSet setWithObjects:@"dentist", @"doctor", @"health", @"hospital", @"pharmacy", @"veterinary_care", @"physiotherapist", nil];
    NSSet *moneySet = [NSSet setWithObjects:@"accounting", @"atm", @"bank", nil];
    NSSet *parkingSet = [NSSet setWithObjects:@"parking", nil];
    NSSet *lodgingSet = [NSSet setWithObjects:@"lodging", nil];
    NSSet *filmSet = [NSSet setWithObjects:@"movie_theater", @"movie_rental", nil];
    NSSet *outdoorsSet = [NSSet setWithObjects:@"park", @"campground", @"cemetery", nil];
    NSSet *festivalSet = [NSSet setWithObjects:@"amusement_park", nil];
    NSSet *casinoSet = [NSSet setWithObjects:@"casino", nil];
    NSSet *bowlingSet = [NSSet setWithObjects:@"bowling", nil];
    NSSet *stadiumSet = [NSSet setWithObjects:@"stadium", @"zoo", nil];
    NSSet *rvSet = [NSSet setWithObjects:@"rv_park", nil];
    NSSet *gymSet = [NSSet setWithObjects:@"gym", nil];

    
    if ([foodSet containsObject:typeString]) {
        return GPlaceTypeFood;
    }else if ([liquordSet containsObject:typeString]){
        return GPlaceTypeLiquor;
    }else if ([carSet containsObject:typeString]){
        return GPlaceTypeCar;
    }else if ([transportationSet containsObject:typeString]){
        return GPlaceTypeTransportation;
    }else if ([shoppingSet containsObject:typeString]){
        return GPlaceTypeShopping;
    }else if ([worshipSet containsObject:typeString]){
        return GPlaceTypeWorship;
    }else if ([establishmentsSet containsObject:typeString]){
        return GPlaceTypeEstablishment;
    }else if ([governmentSet containsObject:typeString]){
        return GPlaceTypeGovernment;
    }else if ([emergencySet containsObject:typeString]){
        return GPlaceTypeEmergencyService;
    }else if ([healthSet containsObject:typeString]){
        return GPlaceTypeHealth;
    }else if ([moneySet containsObject:typeString]){
        return GPlaceTypeMoney;
    }else if ([parkingSet containsObject:typeString]){
        return GPlaceTypeParking;
    }else if ([lodgingSet containsObject:typeString]){
        return GPlaceTypeLodging;
    }else if ([filmSet containsObject:typeString]){
        return GPlaceTypeFilm;
    }else if ([outdoorsSet containsObject:typeString]){
        return GPlaceTypeOutdoors;
    }else if ([festivalSet containsObject:typeString]){
        return GPlaceTypeFestival;
    }else if ([casinoSet containsObject:typeString]){
        return GPlaceTypeCasino;
    }else if ([bowlingSet containsObject:typeString]){
        return GPlaceTypeBowling;
    }else if ([stadiumSet containsObject:typeString]){
        return GPlaceTypeStadium;
    }else if ([rvSet containsObject:typeString]){
        return GPlaceTypeRVPark;
    }else if ([gymSet containsObject:typeString]){
        return GPlaceTypeGym;
    }else{
        return GPlaceTypeEstablishment;
    }
}

+(UIImage *)iconForType:(GPlaceType)type{
    NSString *imageName;
    switch (type) {
        case GPlaceTypeFood:
            imageName = @"food-and-drink";
            break;
        case GPlaceTypeLiquor:
            imageName = @"liquor";
            break;
        case GPlaceTypeCar:
            imageName = @"car";
            break;
        case GPlaceTypeTransportation:
            imageName = @"public-transportation";
            break;
        case GPlaceTypeShopping:
            imageName = @"shopping-bag";
            break;
        case GPlaceTypeWorship:
            imageName = @"worship";
            break;
        case GPlaceTypeEstablishment:
            imageName = @"establishment";
            break;
        case GPlaceTypeGovernment:
            imageName = @"government";
            break;
        case GPlaceTypeEmergencyService:
            imageName = @"emergency";
            break;
        case GPlaceTypeHealth:
            imageName = @"health";
            break;
        case GPlaceTypeMoney:
            imageName = @"money";
            break;
        case GPlaceTypeParking:
            imageName = @"parking";
            break;
        case GPlaceTypeLodging:
            imageName = @"lodging";
            break;
        case GPlaceTypeFilm:
            imageName = @"film";
            break;
        case GPlaceTypeOutdoors:
            imageName = @"outdoors";
            break;
        case GPlaceTypeFestival:
            imageName = @"amusement-park";
            break;
        case GPlaceTypeCasino:
            imageName = @"casino";
            break;
        case GPlaceTypeBowling:
            imageName = @"bowling";
            break;
        case GPlaceTypeStadium:
            imageName = @"stadium";
            break;
        case GPlaceTypeRVPark:
            imageName = @"rv";
            break;
        case GPlaceTypeGym:
            imageName = @"gym";
            break;
        case GPlaceTypeNote:
            imageName = @"note";
            break;
        case GPlaceTypeStartPoint:
            imageName = @"local-pin";
            break;
        case GPlaceTypeAd:
            imageName = @"star";
            break;

        default:
            imageName = @"food-and-drink";
            break;
    }
    return [UIImage imageNamed:imageName];
}

+(UIColor *)colorForType:(GPlaceType)type{
    UIColor *color;
    switch (type) {
        case GPlaceTypeFood:
            color = [UIColor colorWithRed:0.96 green:0.36 blue:0.36 alpha:1.0];
            break;
        case GPlaceTypeLiquor:
            color = [UIColor colorWithRed:0.80 green:0.30 blue:0.92 alpha:1.0];
            break;
        case GPlaceTypeCar:
            color = [UIColor colorWithRed:0.92 green:0.71 blue:0.30 alpha:1.0];
            break;
        case GPlaceTypeTransportation:
            color = [UIColor colorWithRed:0.96 green:0.86 blue:0.36 alpha:1.0];
            break;
        case GPlaceTypeShopping:
            color = [UIColor colorWithRed:0.45 green:0.28 blue:0.66 alpha:1.0];
            break;
        case GPlaceTypeWorship:
            color = [UIColor colorWithRed:0.28 green:0.47 blue:0.66 alpha:1.0];
            break;
        case GPlaceTypeEstablishment:
            color = [UIColor colorWithRed:0.30 green:0.70 blue:0.92 alpha:1.0];
            break;
        case GPlaceTypeGovernment:
            color = [UIColor colorWithRed:0.66 green:0.48 blue:0.28 alpha:1.0];
            break;
        case GPlaceTypeEmergencyService:
            color = [UIColor colorWithRed:0.82 green:0.16 blue:0.16 alpha:1.0];
            break;
        case GPlaceTypeHealth:
            color = [UIColor colorWithRed:0.82 green:0.16 blue:0.16 alpha:1.0];
            break;
        case GPlaceTypeMoney:
            color = [UIColor colorWithRed:0.20 green:0.54 blue:0.17 alpha:1.0];
            break;
        case GPlaceTypeParking:
            color = [UIColor colorWithRed:0.05 green:0.45 blue:0.74 alpha:1.0];
            break;
        case GPlaceTypeLodging:
            color = [UIColor colorWithRed:0.05 green:0.45 blue:0.74 alpha:1.0];
            break;
        case GPlaceTypeFilm:
            color = [UIColor colorWithRed:1.00 green:0.00 blue:0.60 alpha:1.0];
            break;
        case GPlaceTypeOutdoors:
            color = [UIColor colorWithRed:0.31 green:0.66 blue:0.28 alpha:1.0];
            break;
        case GPlaceTypeFestival:
            color = [UIColor colorWithRed:1.00 green:0.00 blue:0.60 alpha:1.0];
            break;
        case GPlaceTypeCasino:
            color = [UIColor colorWithRed:0.20 green:0.54 blue:0.17 alpha:1.0];
            break;
        case GPlaceTypeBowling:
            color = [UIColor colorWithRed:0.20 green:0.54 blue:0.17 alpha:1.0];
            break;
        case GPlaceTypeStadium:
            color = [UIColor colorWithRed:0.20 green:0.54 blue:0.17 alpha:1.0];
            break;
        case GPlaceTypeRVPark:
            color = [UIColor colorWithRed:0.20 green:0.54 blue:0.17 alpha:1.0];
            break;
        case GPlaceTypeGym:
            color = [UIColor colorWithRed:0.20 green:0.54 blue:0.17 alpha:1.0];
            break;
        case GPlaceTypeNote:
            color = [UIColor colorWithRed:0.96 green:0.86 blue:0.36 alpha:1.0];
            break;
        case GPlaceTypeStartPoint:
            color = [UIColor colorWithRed:0.30 green:0.70 blue:0.92 alpha:1.0];
            break;
        case GPlaceTypeAd:
            color = [UIColor colorWithRed:1.00 green:0.78 blue:0.00 alpha:1.0];
            break;

        default:
            color = [UIColor greenColor];
            break;
    }
    return color;
}


@end
