//
//  searchProfile.h
//  Qlumi
//
//  Created by William Smillie on 11/30/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface searchProfile : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSMutableDictionary *filters;

-(instancetype)initWithName:(NSString *)name;

@end
