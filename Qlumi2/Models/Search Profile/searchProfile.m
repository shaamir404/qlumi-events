//
//  searchProfile.m
//  Qlumi
//
//  Created by William Smillie on 11/30/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import "searchProfile.h"

@implementation searchProfile
@synthesize filters, name;

-(instancetype)initWithName:(NSString *)profileName{
    self = [super init];
    if (self) {
        filters = [[NSMutableDictionary alloc] init];
        name = [profileName copy];
        
        filters[@"radius"] = @"32190";
//        filters[@"keyword"] = @"";
//        filters[@"language"] = @"";
//        filters[@"minprice"] = @"";
//        filters[@"maxprice"] = @"";
//        filters[@"opennow"] = @"NO";
//        filters[@"rankby"] = @"distance";//@"prominence";
    }
    return self;
}

@end
