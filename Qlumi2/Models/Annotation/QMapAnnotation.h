//
//  MyAnnotation.h
//  Qlumi2
//
//  Created by William Smillie on 12/18/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import <MapKit/MapKit.h>
#include "Place.h"

@interface QMapAnnotation : NSObject<MKAnnotation>
{
    CLLocationCoordinate2D coordinate;
    NSString *title;
    NSString *subtitle;
    Place *place;
}

@property (nonatomic, strong) Place *place;
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;
@property BOOL enabled;
@property BOOL isNote;

@end

