//
//  Event.h
//  Qlumi2
//
//  Created by William Smillie on 12/11/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import <Parse/Parse.h>

@interface Event : PFObject <PFSubclassing>

+ (NSString *)parseClassName;

@property (retain) NSString *title;
@property (retain) NSString *about;
@property (retain) NSDate *date;
@property (retain) NSString *address;
@property (retain) NSString *venueName;
@property (retain) NSArray *locations;
@property (retain) PFUser *user;
@property (retain) PFFile *image;
@property (retain) NSArray *schedule;
@property (retain) NSArray *guests;
@property (retain) PFRelation *photos;
@property (retain) NSNumber *photoCount;
@property (retain) NSString *featureTitle;
@property (retain) NSString *featureBody;

@end
