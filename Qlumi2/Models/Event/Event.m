//
//  Event.m
//  Qlumi2
//
//  Created by William Smillie on 12/11/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import "Event.h"

@implementation Event

+ (void)load{
    [self registerSubclass];
}

+ (NSString *)parseClassName {
    return @"Event";
}


/*+ (Event *)eventWithObject:(PFObject *)object{
    Event *event = [[Event alloc] init];
    
    event.objectId = object.objectId;
    event.title = object[@"title"];
    event.description = object[@"description"];
    event.date = object[@"date"];
    event.user = object[@"user"];
    event.venueName = object[@"venueName"];
    event.location = object[@"venueName"];
    event.image = object[@"image"];
    event.schedule = object[@"schedule"];
    
    return event;
}*/

@end
