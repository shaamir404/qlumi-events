//
//  QResponder.m
//  Qlumi
//
//  Created by William Smillie on 12/21/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import "QResponder.h"

@implementation QResponder

-(id)initWithView:(id)view keyboardType:(UIKeyboardType)keyboard andPlaceholder:(NSString *)placeholder{
    if (self = [super init]) {
        self.view = view;
        self.view.userInteractionEnabled = YES;
        self.keyboardType = keyboard;
        self.placeholder = placeholder;
    }
    return self;
}

-(id)initWithView:(id)view placeholder:(NSString *)placeholder andInputSource:(UIView *)inputSource{
    if (self = [super init]) {
        self.view = view;
        self.view.userInteractionEnabled = YES;
        self.inputSource = inputSource;
        self.placeholder = placeholder;
    }
    self.keyboardType = nil;

    return self;
}

@end
