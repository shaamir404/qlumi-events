//
//  QResponder.h
//  Qlumi
//
//  Created by William Smillie on 12/21/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface QResponder : NSObject

-(id)initWithView:(id)view keyboardType:(UIKeyboardType)keyboard andPlaceholder:(NSString *)placeholder;
-(id)initWithView:(id)view placeholder:(NSString *)placeholder andInputSource:(UIView *)inputSource;


@property (nonatomic, strong) UIView *view;
@property (nonatomic, strong) UIView *inputSource;
@property (nonatomic, strong) NSString *placeholder;
@property UIKeyboardType keyboardType;

@end
