//
//  ScheduledObject.h
//  Qlumi
//
//  Created by William Smillie on 10/30/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ScheduledObject : NSObject

-(instancetype)initWithTitle:(NSString *)title startTime:(NSString *)start endTime:(NSString *)end;
@property NSString *title;
@property NSDate *date;
@property NSString *startTime;
@property NSString *endTime;

@end
