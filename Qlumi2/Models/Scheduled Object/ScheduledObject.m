//
//  ScheduledObject.m
//  Qlumi
//
//  Created by William Smillie on 10/30/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import "ScheduledObject.h"

@implementation ScheduledObject

-(instancetype)initWithTitle:(NSString *)title startTime:(NSString *)start endTime:(NSString *)end{
    self = [super init];
    if(self){
        self.title = [title copy];
        self.startTime = [start copy];
        self.endTime = [end copy];
    }
    return self;
}

@end
