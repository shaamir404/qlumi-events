//
//  ApiManager.swift
//  Qlumi
//
//  Created by iMEDHealth_Dev on 12/11/2019.
//  Copyright © 2019 Red Door Endeavors. All rights reserved.
//

import UIKit
import Alamofire

class ApiManager: NSObject {
    
    static let shared = ApiManager()
    
    private override init() {
        super.init()
    }
    
    func getPlaces(searchString: String, completion: @escaping (_ places: [Place]) -> Void) {
        let newString = NSString(string: searchString).addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? ""
        Alamofire.request(URL(string: "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=\(newString)&key=AIzaSyC67qOLCwRi1_6Z8g1zKA6OQkJekYIBDz8")!, method: .get, parameters: [:], encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            var isSuccess = false
            if response.result.isSuccess {
                if let responseData = response.result.value as? [String: Any] {
                    if let status = responseData["status"] as? String {
                        if status == "OK" {
                            if let results = responseData["predictions"] as? [[String: Any]] {
                                isSuccess = true
                                let places: [Place] = results.map({ Place(predicationDict: $0) })
                                completion(places)
                            }
                        }
                    }
                }
            }
            if !isSuccess {
                completion([])
            }
        }
    }
    
    func getPlace(with placeId: String, completion: @escaping (_ place: Place?) -> Void) {
        Alamofire.request(URL(string: "https://maps.googleapis.com/maps/api/place/details/json?key=AIzaSyC67qOLCwRi1_6Z8g1zKA6OQkJekYIBDz8&placeid=\(placeId)")!, method: .get, parameters: [:], encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            var isSuccess = false
            if response.result.isSuccess {
                if let responseDict = response.result.value as? [String: Any] {
                    if let placeDict = responseDict["result"] as? [String: Any] {
                        isSuccess = true
                        let place = Place(dictionary: placeDict)
                        completion(place)
                    }
                }
            }
            if !isSuccess {
                completion(nil)
            }
        }
    }
    
    func getImages(with tag: String, completion: @escaping (_ images: [String]) -> Void) {
        Alamofire.request(URL(string: "https://api.unsplash.com/search/photos?client_id=5d4592e505f01a9c0a941a1eebb68b5d1e838ee5c3fae3e4b98d984e1334be2e&page=1&query=\(tag)")!, method: .get, parameters: [:], encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            var isSuccess = false
            if response.result.isSuccess {
                if let responseDict = response.result.value as? [String: Any] {
                    if let results = responseDict["results"] as? [[String: Any]] {
                        isSuccess = true
                        var imagesURLs = [String]()
                        for result in results {
                            if let urls = result["urls"] as? [String: Any] {
                                if let regularURL = urls["regular"] as? String {
                                    imagesURLs.append(regularURL)
                                }
                            }
                        }
                        completion(imagesURLs)
                    }
                }
            }
            if !isSuccess {
                completion([])
            }
        }
    }
}
