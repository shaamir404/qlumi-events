//
//  EPContactsPicker.swift
//  EPContacts
//
//  Created by Prabaharan Elangovan on 12/10/15.
//  Copyright © 2015 Prabaharan Elangovan. All rights reserved.
//

import UIKit
import Contacts
import libPhoneNumber_iOS

@objc public protocol EPPickerDelegate: class {
	func epContactPicker(contactPicker: EPContactsPicker, didContactFetchFailed error: NSError)
    func epContactPicker(contactPicker: EPContactsPicker, didCancel error: NSError)
    func epContactPicker(contactPicker: EPContactsPicker, didSelectContact contact: Any)
	func epContactPicker(contactPicker: EPContactsPicker, didSelectMultipleContacts contacts: [Any])
}

public extension EPPickerDelegate {
	func epContactPicker(contactPicker: EPContactsPicker, didContactFetchFailed error: NSError) { }
	func epContactPicker(contactPicker: EPContactsPicker, didCancel error: NSError) { }
	func epContactPicker(contactPicker: EPContactsPicker, didSelectContact contact: Any) { }
	func epContactPicker(contactPicker: EPContactsPicker, didSelectMultipleContacts contacts: [Any]) { }
}

typealias ContactsHandler = (_ contacts : [CNContact] , _ error : NSError?) -> Void

public enum SubtitleCellValue{
    case phoneNumber
    case email
    case birthday
    case organization
}
@objc
open class EPContactsPicker: UITableViewController, UISearchResultsUpdating, UISearchBarDelegate, ChangeNumAlertDelegate {
    
    // MARK: - Properties
    @objc
    open weak var contactDelegate: EPPickerDelegate?
    var contactsStore: CNContactStore?
    var resultSearchController = UISearchController()
    var orderedContacts = [String: [CNContact]]() //Contacts ordered in dicitonary alphabetically
    var sortedContactKeys = [String]()
    
    var selectedContacts = [EPContact]()
    var filteredContacts = [CNContact]()
     let phoneUtil = NBPhoneNumberUtil()
    var subtitleCellValue = SubtitleCellValue.phoneNumber
    var multiSelectEnabled: Bool = false //Default is single selection contact
    
    // MARK: - Lifecycle Methods
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        self.title = EPGlobalConstants.Strings.contactsTitle

        registerContactCell()
        inititlizeBarButtons()
        initializeSearchBar()
        reloadContacts()
    }
    
    func initializeSearchBar() {
        self.resultSearchController = ( {
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.dimsBackgroundDuringPresentation = false
            controller.hidesNavigationBarDuringPresentation = false
            controller.searchBar.sizeToFit()
            controller.searchBar.delegate = self
            self.tableView.tableHeaderView = controller.searchBar
            return controller
        })()
    }
    
    func inititlizeBarButtons() {
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(onTouchCancelButton))
        self.navigationItem.leftBarButtonItem = cancelButton
        
        if multiSelectEnabled {
            let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(onTouchDoneButton))
            self.navigationItem.rightBarButtonItem = doneButton
            
        }
    }
    
    fileprivate func registerContactCell() {
        
        let podBundle = Bundle(for: self.classForCoder)
        if let bundleURL = podBundle.url(forResource: EPGlobalConstants.Strings.bundleIdentifier, withExtension: "bundle") {
            
            if let bundle = Bundle(url: bundleURL) {
                
                let cellNib = UINib(nibName: EPGlobalConstants.Strings.cellNibIdentifier, bundle: bundle)
                tableView.register(cellNib, forCellReuseIdentifier: "Cell")
            }
            else {
                assertionFailure("Could not load bundle")
            }
        }
        else {
            
            let cellNib = UINib(nibName: EPGlobalConstants.Strings.cellNibIdentifier, bundle: nil)
            tableView.register(cellNib, forCellReuseIdentifier: "Cell")
        }
    }

    override open func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Initializers
  
    convenience public init(delegate: EPPickerDelegate?) {
        self.init(delegate: delegate, multiSelection: false)
    }
    
    convenience public init(delegate: EPPickerDelegate?, multiSelection : Bool) {
        self.init(style: .plain)
        self.multiSelectEnabled = multiSelection
        contactDelegate = delegate
    }

    convenience public init(delegate: EPPickerDelegate?, multiSelection : Bool, subtitleCellType: SubtitleCellValue) {
        self.init(style: .plain)
        self.multiSelectEnabled = multiSelection
        contactDelegate = delegate
        subtitleCellValue = subtitleCellType
    }
    
    
    // MARK: - Contact Operations
  
      open func reloadContacts() {
        getContacts( {(contacts, error) in
            if (error == nil) {
                DispatchQueue.main.async(execute: {
                    self.tableView.reloadData()
                })
            }
        })
      }
  
    func getContacts(_ completion:  @escaping ContactsHandler) {
        if contactsStore == nil {
            //ContactStore is control for accessing the Contacts
            contactsStore = CNContactStore()
        }
        let error = NSError(domain: "EPContactPickerErrorDomain", code: 1, userInfo: [NSLocalizedDescriptionKey: "No Contacts Access"])
        
        switch CNContactStore.authorizationStatus(for: CNEntityType.contacts) {
            case CNAuthorizationStatus.denied, CNAuthorizationStatus.restricted:
                //User has denied the current app to access the contacts.
                
                let productName = Bundle.main.infoDictionary!["CFBundleName"]!
                
                let alert = UIAlertController(title: "Unable to access contacts", message: "\(productName) does not have access to contacts. Kindly enable it in privacy settings ", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Ok", style: .default, handler: {  action in
                    completion([], error)
                    self.dismiss(animated: true, completion: {
                        self.contactDelegate?.epContactPicker(contactPicker: self, didContactFetchFailed: error)//epContactPicker(self, didContactFetchFailed: error)
                    })
                })
                alert.addAction(okAction)
                self.present(alert, animated: true, completion: nil)
            
            case CNAuthorizationStatus.notDetermined:
                //This case means the user is prompted for the first time for allowing contacts
                contactsStore?.requestAccess(for: CNEntityType.contacts, completionHandler: { (granted, error) -> Void in
                    //At this point an alert is provided to the user to provide access to contacts. This will get invoked if a user responds to the alert
                    if  (!granted ){
                        DispatchQueue.main.async(execute: { () -> Void in
                            completion([], error! as NSError?)
                        })
                    }
                    else{
                        self.getContacts(completion)
                    }
                })
            
            case  CNAuthorizationStatus.authorized:
                //Authorization granted by user for this app.
                var contactsArray = [CNContact]()
                
                let contactFetchRequest = CNContactFetchRequest(keysToFetch: allowedContactKeys())
                
                do {
                    try contactsStore?.enumerateContacts(with: contactFetchRequest, usingBlock: { (contact, stop) -> Void in
                        //Ordering contacts based on alphabets in firstname
                        contactsArray.append(contact)
                        var key: String = "#"
                        //If ordering has to be happening via family name change it here.
                        if let firstLetter = contact.givenName[0..<1] , firstLetter.containsAlphabets() {
                            key = firstLetter.uppercased()
                        }
                        var contacts = [CNContact]()
                        
                        if let segregatedContact = self.orderedContacts[key] {
                            contacts = segregatedContact
                        }
                        contacts.append(contact)
                        self.orderedContacts[key] = contacts

                    })
                    self.sortedContactKeys = Array(self.orderedContacts.keys).sorted(by: <)
                    if self.sortedContactKeys.first == "#" {
                        self.sortedContactKeys.removeFirst()
                        self.sortedContactKeys.append("#")
                    }
                    completion(contactsArray, nil)
                }
                //Catching exception as enumerateContactsWithFetchRequest can throw errors
                catch let error as NSError {
                    print(error.localizedDescription)
                }
            
        }
    }
    
    func allowedContactKeys() -> [CNKeyDescriptor]{
        //We have to provide only the keys which we have to access. We should avoid unnecessary keys when fetching the contact. Reducing the keys means faster the access.
        return [CNContactNamePrefixKey as CNKeyDescriptor,
            CNContactGivenNameKey as CNKeyDescriptor,
            CNContactFamilyNameKey as CNKeyDescriptor,
            CNContactOrganizationNameKey as CNKeyDescriptor,
            CNContactBirthdayKey as CNKeyDescriptor,
            CNContactImageDataKey as CNKeyDescriptor,
            CNContactThumbnailImageDataKey as CNKeyDescriptor,
            CNContactImageDataAvailableKey as CNKeyDescriptor,
            CNContactPhoneNumbersKey as CNKeyDescriptor,
            CNContactEmailAddressesKey as CNKeyDescriptor,
        ]
    }
    
    // MARK: - Table View DataSource
    
    override open func numberOfSections(in tableView: UITableView) -> Int {
        if resultSearchController.isActive { return 1 }
        return sortedContactKeys.count
    }
    
    override open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if resultSearchController.isActive { return filteredContacts.count }
        if let contactsForSection = orderedContacts[sortedContactKeys[section]] {
            return contactsForSection.count
        }
        return 0
    }

    // MARK: - Table View Delegates

    override open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! EPContactCell
        cell.accessoryType = .none
        //Convert CNContact to EPContact
		let contact: EPContact
        
        if resultSearchController.isActive {
            contact = EPContact(contact: filteredContacts[(indexPath as NSIndexPath).row])
        } else {
			guard let contactsForSection = orderedContacts[sortedContactKeys[(indexPath as NSIndexPath).section]] else {
				assertionFailure()
				return UITableViewCell()
			}

			contact = EPContact(contact: contactsForSection[(indexPath as NSIndexPath).row])
        }
		
        if multiSelectEnabled  && selectedContacts.contains(where: { $0.contactId == contact.contactId }) {
            cell.accessoryType = .checkmark
        }
		
        cell.updateContactsinUI(contact, indexPath: indexPath, subtitleType: subtitleCellValue)
        return cell
    }
    
    override open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! EPContactCell
        let selectedContact =  cell.contact!
        if multiSelectEnabled {
            //Keeps track of enable=ing and disabling contacts
            if cell.accessoryType == .checkmark {
                cell.accessoryType = .none
                selectedContacts = selectedContacts.filter(){
                    return selectedContact.contactId != $0.contactId
                }
            }
            else {
                cell.accessoryType = .checkmark
                if (selectedContact.phoneNumbers.count > 1){
                    self.presentNumberSelectionForConect(selectedContact) { (contact) in
                        
                        
                        var nsDictionary: NSDictionary?
                        if let path = Bundle.main.path(forResource: "DiallingCodes", ofType: "plist") {
                            nsDictionary = NSDictionary(contentsOfFile: path)
                        }
                        let countryCode = NSLocale.current.regionCode
                        var phoneNum:String = selectedContact.phoneNumber!
                        if (phoneNum.hasPrefix("00")){
                            phoneNum = String(phoneNum.dropFirst(2))
                        }
                        if self.CheckValidityNum(number: phoneNum){
                            if phoneNum.hasPrefix("0"){
                                phoneNum = String(phoneNum.dropFirst(1))
                                for key in nsDictionary!{
                                    if countryCode?.lowercased() == key.key as? String{
                                        phoneNum = "+\(key.value) \(phoneNum)"
                                    }
                                }
                                if self.CheckValidityNum(number: phoneNum){
                                    contact.phoneNumber = phoneNum
                                    self.selectedContacts.append(contact);
                                    return
                                }
                                else{
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let controller : ChangeNumAlert = storyboard.instantiateViewController(withIdentifier: "ChangeNumAlert") as! ChangeNumAlert
                                    controller.modalPresentationStyle = .overCurrentContext
                                    controller.delegate = self
                                    controller.titleLabelString = "The number [\(selectedContact.phoneNumber ?? "000")] is invalid. Qlumi needs a country and area code with this number in order to successfully send out your invitation. Please select the guest's country from dropdown menu and then fill in the area code and number."
                                    controller.selectedContactsAlert = selectedContact
                                    self.presentedViewController?.present(controller, animated: true, completion: nil)
                                    
                                }
                            }
                            else{
                                contact.phoneNumber = phoneNum
                                self.selectedContacts.append(contact);
                                return
                            }
                        }
                        else if self.CheckValidityNum(number: "+\(phoneNum)"){
                            contact.phoneNumber = "+\(phoneNum)"
                            self.selectedContacts.append(contact);
                            return
                        }
                        
                        for key in nsDictionary!{
                            if countryCode?.lowercased() == key.key as? String{
                                phoneNum = "+\(key.value) \(phoneNum)"
                            }
                        }
                        if self.CheckValidityNum(number: phoneNum){
                            contact.phoneNumber = phoneNum
                            self.selectedContacts.append(contact);
                            return
                        }
                        else{
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let controller : ChangeNumAlert = storyboard.instantiateViewController(withIdentifier: "ChangeNumAlert") as! ChangeNumAlert
                            controller.modalPresentationStyle = .overCurrentContext
                            controller.delegate = self
                            controller.titleLabelString = "The number [\(selectedContact.phoneNumber ?? "000")] is invalid. Qlumi needs a country and area code with this number in order to successfully send out your invitation. Please select the guest's country from dropdown menu and then fill in the area code and number."
                            controller.selectedContactsAlert = contact
                            
                            
                            UIApplication.shared.keyWindow?.rootViewController?.presentedViewController?.present(controller, animated: true, completion: nil)
                            
                        }
                    };
                }else{
                    
                    var nsDictionary: NSDictionary?
                    if let path = Bundle.main.path(forResource: "DiallingCodes", ofType: "plist") {
                        nsDictionary = NSDictionary(contentsOfFile: path)
                    }
                    let countryCode = NSLocale.current.regionCode
                    var phoneNum:String = selectedContact.phoneNumber!
                    if (phoneNum.hasPrefix("00")){
                        phoneNum = String(phoneNum.dropFirst(2))
                    }
                    if CheckValidityNum(number: phoneNum){
                        if phoneNum.hasPrefix("0"){
                            phoneNum = String(phoneNum.dropFirst(1))
                            for key in nsDictionary!{
                                if countryCode?.lowercased() == key.key as? String{
                                    phoneNum = "+\(key.value) \(phoneNum)"
                                }
                            }
                            if CheckValidityNum(number: phoneNum){
                                selectedContact.phoneNumber = phoneNum
                                self.selectedContacts.append(selectedContact);
                                return
                            }
                            else{
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let controller : ChangeNumAlert = storyboard.instantiateViewController(withIdentifier: "ChangeNumAlert") as! ChangeNumAlert
                                controller.modalPresentationStyle = .overCurrentContext
                                controller.delegate = self
                                controller.titleLabelString = "The number [\(selectedContact.phoneNumber ?? "000")] is invalid. Qlumi needs a country and area code with this number in order to successfully send out your invitation. Please select the guest's country from dropdown menu and then fill in the area code and number."
                                controller.selectedContactsAlert = selectedContact
                                presentedViewController?.present(controller, animated: true, completion: nil)
                                
                            }
                        }
                        else{
                            selectedContact.phoneNumber = phoneNum
                            self.selectedContacts.append(selectedContact);
                            return
                        }
                    }
                    else if CheckValidityNum(number: "+\(phoneNum)"){
                        selectedContact.phoneNumber = "+\(phoneNum)"
                        self.selectedContacts.append(selectedContact);
                        return
                    }
                    
                    for key in nsDictionary!{
                        if countryCode?.lowercased() == key.key as? String{
                            phoneNum = "+\(key.value) \(phoneNum)"
                        }
                    }
                    if CheckValidityNum(number: phoneNum){
                        selectedContact.phoneNumber = phoneNum
                        self.selectedContacts.append(selectedContact);
                        return
                    }
                    else{
                        

                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let controller : ChangeNumAlert = storyboard.instantiateViewController(withIdentifier: "ChangeNumAlert") as! ChangeNumAlert
                        controller.modalPresentationStyle = .overCurrentContext
                        controller.delegate = self
                        controller.titleLabelString = "The number [\(selectedContact.phoneNumber ?? "000")] is invalid. Qlumi needs a country and area code with this number in order to successfully send out your invitation. Please select the guest's country from dropdown menu and then fill in the area code and number."
                        controller.selectedContactsAlert = selectedContact
                        
                        UIApplication.shared.keyWindow?.rootViewController?.presentedViewController?.present(controller, animated: true, completion: nil)
                    }
                    
                    
                   // selectedContacts.append(selectedContact)
                }
            }
        }
        else {
            //Single selection code
			resultSearchController.isActive = false
			self.dismiss(animated: true, completion: {
				DispatchQueue.main.async {
                    self.contactDelegate?.epContactPicker(contactPicker: self, didSelectContact: selectedContact)
				}
			})
        }
    }
    
    public func CheckValidityNum(number: String)->Bool{
        var checkNum:Bool = false
        let countryCode = NSLocale.current.regionCode
        do {
            let phoneNumber: NBPhoneNumber = try phoneUtil.parse(number, defaultRegion: countryCode)
            checkNum = phoneUtil.isValidNumber(phoneNumber)
            
        }
        catch let error as NSError {
            print(error.localizedDescription)
        }
        
        return checkNum
    }
    
    
    public func didDoneBtnPressing(contact: EPContact) {
        selectedContacts.append(contact)
    }
    
    public func didCancelBtnPressing() {
        
    }
    
    
    func presentNumberSelectionForConect(_ contact: EPContact, completion: @escaping (_ contact: EPContact) -> Void){
        
        let alert = UIAlertController.init(title: "Select Number", message: nil, preferredStyle: .actionSheet);
        
        for number in contact.phoneNumbers {            
            var numberLabel = number.phoneLabel.replacingOccurrences(of: "_$!<", with: "");
            numberLabel = numberLabel.replacingOccurrences(of: ">!$_", with: "");
            let label = String.init(format: "%@ - %@", numberLabel, number.phoneNumber);
            alert.addAction(UIAlertAction.init(title: label, style: .default, handler: { (UIAlertAction) in
                contact.phoneNumber = number.phoneNumber;
                completion(contact);
            }));
        }
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil));
        self.present(alert, animated: true, completion: nil);

    }
    
    override open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    override open func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        if resultSearchController.isActive { return 0 }
        tableView.scrollToRow(at: IndexPath(row: 0, section: index), at: .top , animated: false)
        return sortedContactKeys.index(of: title)!
    }
    
    override  open func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        if resultSearchController.isActive { return nil }
        return sortedContactKeys
    }

    override open func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if resultSearchController.isActive { return nil }
        return sortedContactKeys[section]
    }
    
    // MARK: - Button Actions
    
     @objc func onTouchCancelButton() {
        dismiss(animated: true, completion: {
            self.contactDelegate?.epContactPicker(contactPicker: self, didCancel: NSError(domain: "EPContactPickerErrorDomain", code: 2, userInfo: [ NSLocalizedDescriptionKey: "User Canceled Selection"]))
        })
    }
    
    @objc func onTouchDoneButton() {
        dismiss(animated: true, completion: {
            self.contactDelegate?.epContactPicker(contactPicker: self, didSelectMultipleContacts: self.selectedContacts)
        })
    }
    
    // MARK: - Search Actions
    
    open func updateSearchResults(for searchController: UISearchController)
    {
        if let searchText = resultSearchController.searchBar.text , searchController.isActive {
            
            let predicate: NSPredicate
            if searchText.characters.count > 0 {
                predicate = CNContact.predicateForContacts(matchingName: searchText)
            } else {
                predicate = CNContact.predicateForContactsInContainer(withIdentifier: contactsStore!.defaultContainerIdentifier())
            }
            
            let store = CNContactStore()
            do {
                filteredContacts = try store.unifiedContacts(matching: predicate,
                    keysToFetch: allowedContactKeys())
                //print("\(filteredContacts.count) count")
                
                self.tableView.reloadData()
                
            }
            catch {
                print("Error!")
            }
        }
    }
    
    open func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        DispatchQueue.main.async(execute: {
            self.tableView.reloadData()
        })
    }
    
}
