//
//  GuestCollectionCell.swift
//  Qlumi
//
//  Created by iMEDHealth_Dev on 11/11/2019.
//  Copyright © 2019 Red Door Endeavors. All rights reserved.
//

import UIKit

class GuestCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var userInitialsLAbel: UILabel!
    @IBOutlet weak var checkImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var rsvpImageView: UIImageView!
    
}
