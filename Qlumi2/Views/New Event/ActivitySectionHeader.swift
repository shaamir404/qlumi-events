//
//  ActivitySectionHeader.swift
//  Qlumi
//
//  Created by iMEDHealth_Dev on 11/11/2019.
//  Copyright © 2019 Red Door Endeavors. All rights reserved.
//

import UIKit

class ActivitySectionHeader: UICollectionReusableView {
        
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var addButton: UIButton!
    var onAddActivity: (() -> ())?
    
    @IBAction func addActivityAction(_ sender: UIButton) {
        onAddActivity?()
    }
    
}
