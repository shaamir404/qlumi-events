//
//  ActivityCollectionCell.swift
//  Qlumi
//
//  Created by iMEDHealth_Dev on 11/11/2019.
//  Copyright © 2019 Red Door Endeavors. All rights reserved.
//

import UIKit

class ActivityCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    
}
