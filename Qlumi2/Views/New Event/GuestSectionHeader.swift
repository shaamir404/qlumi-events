//
//  GuestSectionHeader.swift
//  Qlumi
//
//  Created by iMEDHealth_Dev on 11/11/2019.
//  Copyright © 2019 Red Door Endeavors. All rights reserved.
//

import UIKit

class GuestSectionHeader: UICollectionReusableView {
        
    @IBOutlet weak var addButton: UIButton!
    var onAddGuest: (() -> ())?
    
    @IBAction func addGuestAction(_ sender: UIButton) {
        onAddGuest?()
    }
    
}
