//
//  BackgroundSectionHeader.swift
//  Qlumi
//
//  Created by iMEDHealth_Dev on 11/11/2019.
//  Copyright © 2019 Red Door Endeavors. All rights reserved.
//

import UIKit

class BackgroundSectionHeader: UICollectionReusableView {
    
    @IBOutlet weak var pickImageButton: UIButton!
    var onPickImageAction: (() -> ())?
    
    @IBAction func pickImageAction(_ sender: UIButton) {
        onPickImageAction?()
    }
}
