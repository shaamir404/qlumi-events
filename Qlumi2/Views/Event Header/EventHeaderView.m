//
//  EventHeaderView.m
//  Qlumi
//
//  Created by William Smillie on 9/25/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import "EventHeaderView.h"
#import <INTUAnimationEngine/INTUInterpolationFunctions.h>

@implementation EventHeaderView
@synthesize effectView, imageView, closeButton, parentVC, eventLabel, gradientImageView;

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = [UIColor clearColor];
        [self setupSubviews];
        self.userInteractionEnabled = YES;
    }
    return self;
}

- (void)setupSubviews {
    imageView = [[UIImageView alloc] initWithFrame:self.bounds];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.layer.cornerRadius = 10;
    [self addSubview:imageView];
    
    gradientImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, self.frame.size.height-64, self.frame.size.width, 64)];
    gradientImageView.image = [UIImage imageNamed:@"gradient"];
    gradientImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self addSubview:gradientImageView];
    
    
    UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleRegular];
    effectView = [[UIVisualEffectView alloc] initWithEffect:blur];
    effectView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    [self addSubview:effectView];
    effectView.alpha = 0;
    
    
    eventLabel = [[UILabel alloc] initWithFrame:CGRectMake(32, self.frame.size.height-64, self.frame.size.width-64, 64)];
    eventLabel.textColor = [UIColor whiteColor];
    eventLabel.font = [UIFont boldSystemFontOfSize:28];
    eventLabel.lineBreakMode = NSLineBreakByWordWrapping;
    eventLabel.numberOfLines = 0;
    eventLabel.userInteractionEnabled = YES;
    [eventLabel setFont:[UIFont fontWithName:@"JosefinSans-Regular" size:16.0]];
    [self addSubview:eventLabel];
    
    eventLabel.layer.shadowColor = [UIColor blackColor].CGColor;
    eventLabel.layer.shadowOffset = CGSizeMake(0, 0);
    eventLabel.layer.shadowRadius = 10.0f;
    eventLabel.layer.shadowOpacity = 1.0f;
}


- (void)didChangeStretchFactor:(CGFloat)stretchFactor {
    [super didChangeStretchFactor:stretchFactor];
    
    if(([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone) && (int)[[UIScreen mainScreen] nativeBounds].size.height == 2436) {
        float perc = (self.contentView.frame.size.height/self.maximumContentHeight)*100;
        CGFloat const inMin = 44; CGFloat const inMax = 100; CGFloat const outMin = 0.0; CGFloat const outMax = 1.0; CGFloat in = perc;
        CGFloat percentage = outMin + (outMax - outMin) * (in - inMin) / (inMax - inMin);
        
        effectView.alpha = INTUInterpolateCGFloat(1, 0, percentage);
        imageView.frame = INTUInterpolateCGRect(self.bounds, self.bounds, percentage);
        gradientImageView.frame = INTUInterpolateCGRect(CGRectMake(0, self.frame.size.height-64, self.frame.size.width, 64), CGRectMake(0, self.frame.size.height-64, self.frame.size.width, 64), percentage);
        
        eventLabel.frame = INTUInterpolateCGRect(CGRectMake(44, 44, self.frame.size.width-110, 36), CGRectMake(16, 44, self.frame.size.width/2, self.frame.size.height-88), percentage);
        eventLabel.numberOfLines = INTUInterpolateCGFloat(1, 0, percentage);
    }else{
        float perc = (self.contentView.frame.size.height/self.maximumContentHeight)*100;
        CGFloat const inMin = 32.0; CGFloat const inMax = 100; CGFloat const outMin = 0.0; CGFloat const outMax = 1.0; CGFloat in = perc;
        CGFloat percentage = outMin + (outMax - outMin) * (in - inMin) / (inMax - inMin);
        
        effectView.alpha = INTUInterpolateCGFloat(1, 0, percentage);
        imageView.frame = INTUInterpolateCGRect(self.bounds, self.bounds, percentage);
        gradientImageView.frame = INTUInterpolateCGRect(CGRectMake(0, self.frame.size.height-64, self.frame.size.width, 64), CGRectMake(0, self.frame.size.height-64, self.frame.size.width, 64), percentage);
        
        eventLabel.frame = INTUInterpolateCGRect(CGRectMake(44, 22, self.frame.size.width-100, 34), CGRectMake(16, 44, self.frame.size.width/2, self.frame.size.height-88), percentage);
        eventLabel.numberOfLines = INTUInterpolateCGFloat(1, 0, percentage);

//        eventLabel.frame = CGRectMake(16, 0, self.frame.size.width/2, 100);
//        eventLabel.center = CGPointMake(eventLabel.center.x, (-self.center.y));
        
    }
    [eventLabel setAdjustsFontSizeToFitWidth:YES];
    [eventLabel adjustsFontSizeToFitWidth];
    [eventLabel setNeedsDisplay];
    [eventLabel layoutSubviews];
}

-(IBAction)dismiss:(id)sender{
    [(UIViewController *)[[[self superview] superview] nextResponder] dismissViewControllerAnimated:YES completion:nil];
}

@end
