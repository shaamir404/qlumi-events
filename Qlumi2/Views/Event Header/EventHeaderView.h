//
//  EventHeaderView.h
//  Qlumi
//
//  Created by William Smillie on 9/25/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import <GSKStretchyHeaderView/GSKStretchyHeaderView.h>

@interface EventHeaderView : GSKStretchyHeaderView

@property UIImageView *imageView;
@property UIImageView *gradientImageView;
@property UILabel *eventLabel;

@property UIButton *closeButton;
@property (strong, nonatomic) UIVisualEffectView *effectView;

@property UIViewController *parentVC;


@end
