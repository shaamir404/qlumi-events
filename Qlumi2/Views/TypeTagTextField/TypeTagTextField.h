//
//  TypeTagTextField.h
//  Qlumi2
//
//  Created by William Smillie on 12/14/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TypeTagTextField;
@protocol TypeTagTextFieldDelegate <NSObject>
- (void)TypeTagTextField:(TypeTagTextField *)textField textDidChange:(NSString *)text;
- (void)TypeTagTextField:(TypeTagTextField *)textField didSelectTag:(NSString *)tag;
@end

@interface TypeTagTextField : UITextField
@property (nonatomic, weak) id <TypeTagTextFieldDelegate> typeTagDelegate;

@property NSMutableArray *buttonsArray;

-(void)showTags:(BOOL)show;

@end
