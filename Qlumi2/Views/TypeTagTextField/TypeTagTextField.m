//
//  TypeTagTextField.m
//  Qlumi2
//
//  Created by William Smillie on 12/14/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import "TypeTagTextField.h"
#import "AppDelegate.h"

@interface TypeTagTextField () {
    UIButton *selectedTag;
    
    BOOL showTags;
}
@end

@implementation TypeTagTextField
@synthesize typeTagDelegate, buttonsArray;

-(void)awakeFromNib{
    [super awakeFromNib];
    
    [self addTarget:self action:@selector(textChanged:) forControlEvents:UIControlEventEditingChanged];
    buttonsArray = [[NSMutableArray alloc] init];
}

-(void)showTags:(BOOL)show{
    showTags = show;
    
    [self removeTags];
    if (show) {
        [self performSelector:@selector(processTextForKeywords:) withObject:nil afterDelay:0];
    }
}


-(void)textChanged:(TypeTagTextField *)textField{
    [self removeTags];
    
    if (showTags) {
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(processTextForKeywords:) object:nil];
        [self performSelector:@selector(processTextForKeywords:) withObject:nil afterDelay:0.5];
    }
    
    [self.typeTagDelegate TypeTagTextField:self textDidChange:self.text];
}

-(IBAction)tappedTag:(UIButton *)sender{
    selectedTag = sender;
    
    for (UIButton *button in buttonsArray) {
        UIColor *targetColor;
        if (button == selectedTag) {
            targetColor = [AppDelegate QOrange]; //Qlumi Orange
        }else{
            targetColor = [AppDelegate QBlue]; //Light blue
        }
        [UIView animateWithDuration:0.3 animations:^{
            button.backgroundColor = targetColor;
        }];
    }
    
    [self.typeTagDelegate TypeTagTextField:self didSelectTag:sender.titleLabel.text];
}


#pragma mark — word processor

-(void)removeTags{
    for (UIButton *button in buttonsArray) {
        [button removeFromSuperview];
    }
}

-(void)processTextForKeywords:(id)sender{

    /*NSLinguisticTagger *tagger = [[NSLinguisticTagger alloc] initWithTagSchemes:[NSArray arrayWithObject:NSLinguisticTagSchemeLexicalClass] options:~NSLinguisticTaggerOmitWords];
    [tagger setString:self.text];
    
    [tagger enumerateTagsInRange:NSMakeRange(0, [self.text length]) scheme:NSLinguisticTagSchemeLexicalClass options:~NSLinguisticTaggerOmitWords usingBlock:^(NSString *tag, NSRange tokenRange, NSRange sentenceRange, BOOL *stop) {

        if ([tag isEqualToString:@"Verb"] | [tag isEqualToString:@"Noun"]) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button addTarget:self action:@selector(tappedTag:) forControlEvents:UIControlEventTouchUpInside];
            [button setTitle:[self.text substringWithRange:tokenRange] forState:UIControlStateNormal];
            button.frame = [self frameOfTextRange:tokenRange];
            button.backgroundColor = [UIColor colorWithRed:0.36 green:0.67 blue:0.93 alpha:1.0];
            button.layer.cornerRadius = 5;
            button.titleLabel.font = self.font;
            
            CGRect targetFrame = button.frame;
            button.frame = CGRectMake(button.center.x, button.center.y, 0, 0);
            [self addSubview:button];

            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:0.3];
            button.frame = targetFrame;
            [UIView commitAnimations];

            [buttonsArray addObject:button];
            
            if (i == 0) {
                [button sendActionsForControlEvents:UIControlEventTouchUpInside];
            }
            i++;
        }
    }];*/
    
    
    __block int i = 0;

    NSLinguisticTagger *tagger = [[NSLinguisticTagger alloc] initWithTagSchemes:[NSArray arrayWithObjects:NSLinguisticTagSchemeNameTypeOrLexicalClass, NSLinguisticTagSchemeLemma, nil] options:(NSLinguisticTaggerOmitWhitespace | NSLinguisticTaggerOmitPunctuation)];
    [tagger setString:self.text];
    [tagger enumerateTagsInRange:NSMakeRange(0, [self.text length]) scheme:NSLinguisticTagSchemeNameTypeOrLexicalClass options:(NSLinguisticTaggerOmitWhitespace | NSLinguisticTaggerOmitPunctuation) usingBlock:^(NSString *tag, NSRange tokenRange, NSRange sentenceRange, BOOL *stop) {
//        if ([tag isEqualToString:@"Verb"] | [tag isEqualToString:@"Noun"]) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button addTarget:self action:@selector(tappedTag:) forControlEvents:UIControlEventTouchUpInside];
            [button setTitle:[self.text substringWithRange:tokenRange] forState:UIControlStateNormal];
            button.frame = [self frameOfTextRange:tokenRange];
            button.backgroundColor = [UIColor colorWithRed:0.36 green:0.67 blue:0.93 alpha:1.0];
            button.layer.cornerRadius = 5;
            button.titleLabel.font = self.font;
            
            CGRect targetFrame = button.frame;
            button.frame = CGRectMake(button.center.x, button.center.y, 0, 0);
            [self addSubview:button];
            
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:0.3];
            button.frame = targetFrame;
            [UIView commitAnimations];
            
            [buttonsArray addObject:button];
            
            if (i == 0) {
                [button sendActionsForControlEvents:UIControlEventTouchUpInside];
            }
            i++;
//        }
    }];

}


#pragma mark — helpers

- (CGRect)frameOfTextRange:(NSRange)range
{
    UITextPosition *beginning = self.beginningOfDocument;
    UITextPosition *start = [self positionFromPosition:beginning offset:range.location];
    UITextPosition *end = [self positionFromPosition:start offset:range.length];
    UITextRange *textRange = [self textRangeFromPosition:start toPosition:end];
    CGRect rect = [self firstRectForRange:textRange];
    
    float padding = 1;
    rect = CGRectMake(rect.origin.x-padding, rect.origin.y-padding, rect.size.width+(padding*2), rect.size.height+(padding*2));
    
    return [self convertRect:rect fromView:self.textInputView];
}




@end
