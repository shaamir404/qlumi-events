//
//  MapTableView.m
//  Qlumi2
//
//  Created by William Smillie on 12/15/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import "MapTableView.h"

@implementation MapTableView

-(instancetype)init{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    return point.y >= 0 && [super pointInside:point withEvent:event];
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    UIView *hitView = [super hitTest:point withEvent:event];
    return hitView == self ? nil : hitView;
}

@end
