//
//  MapTableView.h
//  Qlumi2
//
//  Created by William Smillie on 12/15/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BVReorderTableView.h"

@interface MapTableView : BVReorderTableView

@end
