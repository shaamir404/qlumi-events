//
//  PlaceAnnotationView.h
//  Qlumi2
//
//  Created by William Smillie on 12/18/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import <MapKit/MapKit.h>
#import <UIKit/UIKit.h>

#import "QMapAnnotation.h"
#import "QMapAnnotationViewCallout.h"

@interface QMapAnnotationView : MKAnnotationView

@property (nonatomic, strong) IBOutlet UIImageView *typeImageView;
@property (nonatomic, strong) IBOutlet UIView *pinView;

@end
