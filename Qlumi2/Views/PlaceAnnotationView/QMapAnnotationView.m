//
//  PlaceAnnotationView.m
//  Qlumi2
//
//  Created by William Smillie on 12/18/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import "QMapAnnotationView.h"

@implementation QMapAnnotationView

-(instancetype)initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier]) {
        
        self = [[[NSBundle mainBundle] loadNibNamed:@"QMapAnnotationView" owner:self options:nil] objectAtIndex:0];
    }
    return self;
}

-(void)awakeFromNib{
    [super awakeFromNib];
    
    self.backgroundColor = [UIColor clearColor];
}


-(void)layoutSubviews{
    CALayer *maskLayer = [CALayer layer];
    UIImage *maskImage = [UIImage imageNamed:@"pin-mask"];
    maskLayer.contents = (id)maskImage.CGImage;
    maskLayer.frame = (CGRect){CGPointZero, maskImage.size};
    self.pinView.layer.mask = maskLayer;
    
    self.pinView.layer.shadowOpacity = 1;
    self.pinView.layer.shadowRadius = 8.0f;
    self.pinView.layer.shadowOffset = CGSizeMake(0, 0);
    self.pinView.layer.shadowColor = [UIColor whiteColor].CGColor;

    
    self.typeImageView.image = [self.typeImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.typeImageView setTintColor:[UIColor whiteColor]];
    
    self.centerOffset = CGPointMake(0.0, -self.pinView.frame.size.height/2);
}

@end
