//
//  QButton.h
//  Qlumi2
//
//  Created by William Smillie on 12/12/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import <LGPlusButtonsView/LGPlusButtonsView.h>

// My Classes
#import "NewEventTableViewController.h"
#import "ProfileViewController.h"
#import "ScheduleTableViewController.h"
#import "MapViewController.h"
#import "ConversationsTableViewController.h"

@interface QButton : LGPlusButtonsView <LGPlusButtonsViewDelegate>

@end
