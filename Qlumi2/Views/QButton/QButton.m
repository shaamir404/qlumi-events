//
//  QButton.m
//  Qlumi2
//
//  Created by William Smillie on 12/12/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import "QButton.h"

@implementation QButton



- (void)drawRect:(CGRect)rect {
    self.delegate = self;
    
    //    self.observedScrollView = self.collectionView;
    self.coverColor = [UIColor colorWithWhite:1.f alpha:0.7];
    self.position = LGPlusButtonsViewPositionBottomRight;
    self.plusButtonAnimationType = LGPlusButtonAnimationTypeRotate;
    
    [self setButtonsTitles:@[@"", @"", @"", @"", @"", @""] forState:UIControlStateNormal];
    [self setDescriptionsTexts:@[@"", @"New Event", @"My Events", @"Search", @"Messages", @"My Profile"]];
    [self setButtonsImages:@[[UIImage imageNamed:@"actionButtonLogo"], [UIImage imageNamed:@"plus"], [UIImage imageNamed:@"calendar"], [UIImage imageNamed:@"search"], [UIImage imageNamed:@"messages"], [UIImage imageNamed:@"profile"]] forState:UIControlStateNormal forOrientation:LGPlusButtonsViewOrientationAll];
    
    [self setButtonsAdjustsImageWhenHighlighted:NO];
    [self setButtonsBackgroundColor:[UIColor colorWithRed:0.f green:0.5 blue:1.f alpha:1.f] forState:UIControlStateNormal];
    [self setButtonsBackgroundColor:[UIColor colorWithRed:0.2 green:0.6 blue:1.f alpha:1.f] forState:UIControlStateHighlighted];
    [self setButtonsBackgroundColor:[UIColor colorWithRed:0.2 green:0.6 blue:1.f alpha:1.f] forState:UIControlStateHighlighted|UIControlStateSelected];
    [self setButtonsSize:CGSizeMake(44.f, 44.f) forOrientation:LGPlusButtonsViewOrientationAll];
    [self setButtonsLayerCornerRadius:44.f/2.f forOrientation:LGPlusButtonsViewOrientationAll];
    [self setButtonsTitleFont:[UIFont boldSystemFontOfSize:24.f] forOrientation:LGPlusButtonsViewOrientationAll];
    [self setButtonsLayerShadowColor:[UIColor colorWithRed:0.1 green:0.1 blue:0.1 alpha:1.f]];
    [self setButtonsLayerShadowOpacity:0.5];
    [self setButtonsLayerShadowRadius:3.f];
    [self setButtonsLayerShadowOffset:CGSizeMake(0.f, 2.f)];
    
    [self setButtonAtIndex:0 size:CGSizeMake(56.f, 56.f) forOrientation:(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? LGPlusButtonsViewOrientationPortrait : LGPlusButtonsViewOrientationAll)];
    [self setButtonAtIndex:0 layerCornerRadius:56.f/2.f forOrientation:(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? LGPlusButtonsViewOrientationPortrait : LGPlusButtonsViewOrientationAll)];
    [self setButtonAtIndex:0 titleFont:[UIFont systemFontOfSize:40.f] forOrientation:(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? LGPlusButtonsViewOrientationPortrait : LGPlusButtonsViewOrientationAll)];
    
    [self setButtonAtIndex:0 titleOffset:CGPointMake(0.f, -3.f) forOrientation:LGPlusButtonsViewOrientationAll];
    [self setButtonAtIndex:0 backgroundColor:[UIColor colorWithRed:0.75 green:0.44 blue:0.54 alpha:0] forState:UIControlStateNormal];
    [self setButtonAtIndex:0 backgroundColor:[UIColor colorWithRed:0.75 green:0.44 blue:0.54 alpha:0] forState:UIControlStateHighlighted];
    [self setButtonAtIndex:1 backgroundColor:[UIColor colorWithRed:0.75 green:0.44 blue:0.54 alpha:1.0] forState:UIControlStateNormal];
    [self setButtonAtIndex:1 backgroundColor:[UIColor colorWithRed:0.75 green:0.44 blue:0.54 alpha:.75] forState:UIControlStateHighlighted];
    [self setButtonAtIndex:2 backgroundColor:[UIColor colorWithRed:0.79 green:0.44 blue:0.33 alpha:1.0] forState:UIControlStateNormal];
    [self setButtonAtIndex:2 backgroundColor:[UIColor colorWithRed:0.79 green:0.44 blue:0.33 alpha:.75] forState:UIControlStateHighlighted];
    [self setButtonAtIndex:3 backgroundColor:[UIColor colorWithRed:0.64 green:0.69 blue:0.69 alpha:1.0] forState:UIControlStateNormal];
    [self setButtonAtIndex:3 backgroundColor:[UIColor colorWithRed:0.64 green:0.69 blue:0.69 alpha:.75] forState:UIControlStateHighlighted];
    [self setButtonAtIndex:4 backgroundColor:[UIColor colorWithRed:0.47 green:0.70 blue:0.64 alpha:1.0] forState:UIControlStateNormal];
    [self setButtonAtIndex:4 backgroundColor:[UIColor colorWithRed:0.47 green:0.70 blue:0.64 alpha:.75] forState:UIControlStateHighlighted];
    [self setButtonAtIndex:5 backgroundColor:[UIColor colorWithRed:0.44 green:0.61 blue:0.73 alpha:1.0] forState:UIControlStateNormal];
    [self setButtonAtIndex:5 backgroundColor:[UIColor colorWithRed:0.44 green:0.61 blue:0.73 alpha:.75] forState:UIControlStateHighlighted];
    
    [self setDescriptionsBackgroundColor:[UIColor whiteColor]];
    [self setDescriptionsTextColor:[UIColor blackColor]];
    [self setDescriptionsLayerShadowColor:[UIColor colorWithRed:0.1 green:0.1 blue:0.1 alpha:1.f]];
    [self setDescriptionsLayerShadowOpacity:0.25];
    [self setDescriptionsLayerShadowRadius:1.f];
    [self setDescriptionsLayerShadowOffset:CGSizeMake(0.f, 1.f)];
    [self setDescriptionsLayerCornerRadius:6.f forOrientation:LGPlusButtonsViewOrientationAll];
    [self setDescriptionsContentEdgeInsets:UIEdgeInsetsMake(4.f, 8.f, 4.f, 8.f) forOrientation:LGPlusButtonsViewOrientationAll];
    
    for (NSUInteger i=1; i<=5; i++)
        [self setButtonAtIndex:i offset:CGPointMake(-6.f, 0.f)
                        forOrientation:(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? LGPlusButtonsViewOrientationPortrait : LGPlusButtonsViewOrientationAll)];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        [self setButtonAtIndex:0 titleOffset:CGPointMake(0.f, -2.f) forOrientation:LGPlusButtonsViewOrientationLandscape];
        [self setButtonAtIndex:0 titleFont:[UIFont systemFontOfSize:32.f] forOrientation:LGPlusButtonsViewOrientationLandscape];
    }
}

#pragma mark — button delegate

-(void)plusButtonsView:(LGPlusButtonsView *)plusButtonsView buttonPressedWithTitle:(NSString *)title description:(NSString *)description index:(NSUInteger)index{
    if (index == 1) {
        
        NewEventViewController *eventVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"NewEventViewController"];
        [eventVC setHierarchy:NewEventHierarchyNEW];
//        NewEventTableViewController *formVC = [[NewEventTableViewController alloc] init];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:eventVC];
        [nav setModalPresentationStyle:UIModalPresentationOverCurrentContext];
        [[self topViewController] presentViewController:nav animated:YES completion:nil];
    }else if (index == 2){
        ScheduleTableViewController *vc = [[self topViewController].storyboard instantiateViewControllerWithIdentifier:@"ScheduleTableViewController"];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
        [[self topViewController] presentViewController:nav animated:YES completion:nil];
    }else if (index == 3){
        MapViewController *vc = [[MapViewController alloc] initWithEditingEnabled:NO andSearchingEnabled:YES];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
        [[self topViewController] presentViewController:nav animated:YES completion:nil];
    }else if (index == 4){
        ConversationsTableViewController *vc = [[ConversationsTableViewController alloc] init];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
        [[self topViewController] presentViewController:nav animated:YES completion:nil];
    }else if (index == 5){
        ProfileViewController *vc = [[ProfileViewController alloc] init];//[[self topViewController].storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
        [[self topViewController] presentViewController:nav animated:YES completion:nil];
    }
    
    if (index !=0) {
        [self hideButtonsAnimated:YES completionHandler:nil];
    }
}



- (UIViewController *)topViewController{
    return [self topViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

- (UIViewController *)topViewController:(UIViewController *)rootViewController
{
    if (rootViewController.presentedViewController == nil) {
        return rootViewController;
    }
    
    if ([rootViewController.presentedViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController *navigationController = (UINavigationController *)rootViewController.presentedViewController;
        UIViewController *lastViewController = [[navigationController viewControllers] lastObject];
        return [self topViewController:lastViewController];
    }
    
    UIViewController *presentedViewController = (UIViewController *)rootViewController.presentedViewController;
    return [self topViewController:presentedViewController];
}




@end
