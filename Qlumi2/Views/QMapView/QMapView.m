//
//  QMapView.m
//  Qlumi
//
//  Created by William Smillie on 12/19/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import "QMapView.h"

#define MERCATOR_RADIUS 85445659.44705395
#define MAX_GOOGLE_LEVELS 20


@interface QMapView (){
    CGPoint touchPoint;
    BOOL editingEnabled;
}
@end

@implementation QMapView

-(void)awakeFromNib{
    [super awakeFromNib];
    [self setup];
}

-(instancetype)init{
    if (self = [super init]) {
        [self setup];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setup];
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame andEditingEnabled:(BOOL)editing{
    if (self = [super initWithFrame:frame]) {
        editingEnabled = editing;
        [self setup];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]) {
        [self setup];
    }
    return self;
}

-(void)setup{
    self.contextualMenu = [BAMContextualMenu addContextualMenuToView:self delegate:self dataSource:self activateOption:kBAMContextualMenuActivateOptionLongPress];
    
    self.researchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.researchButton.frame = CGRectMake(0, 0, 160, 44);
    [self.researchButton setTitle:@"Search This Area" forState:UIControlStateNormal];
    self.researchButton.layer.shadowOpacity = 0.5;
    self.researchButton.layer.shadowRadius = 5;
    self.researchButton.layer.shadowOffset = CGSizeMake(0, 0);
    self.researchButton.backgroundColor = [UIColor whiteColor];
    [self.researchButton setTitleColor:[UIColor colorWithRed:0.25 green:0.62 blue:0.88 alpha:1.0] forState:UIControlStateNormal];
    [self.researchButton setTitleColor:[UIColor colorWithRed:0.11 green:0.54 blue:0.78 alpha:1.0] forState:UIControlStateHighlighted];
    [self addSubview:self.researchButton];
    [self.researchButton addTarget:self action:@selector(research:) forControlEvents:UIControlEventTouchUpInside];
    self.researchButton.alpha = 0;
    
    [self.researchButton.layer setCornerRadius:12.0f];
    [self.researchButton.layer setShadowPath:
     [[UIBezierPath bezierPathWithRoundedRect:[self.researchButton bounds]
                                 cornerRadius:12.0f] CGPath]];

    
    self.canAddNote = YES;
    self.firstLoad = YES;
    self.delegate = self;
    self.showsUserLocation = YES;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    
    self.researchButton.center = CGPointMake(self.center.x, 64+22+32+28+10);
    CGFloat aspectRatio = [[UIScreen mainScreen] bounds].size.height / [[UIScreen mainScreen] bounds].size.width;
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone) {
        if (aspectRatio > 1.8) {
            self.researchButton.center = CGPointMake(self.center.x, 64+22+32+28+10+40);
        }
    }


    [self zoomToPins];
}

-(void)zoomToPins{
    NSMutableArray *annotations = [[NSMutableArray alloc] init];
    for (id annotation in self.annotations) {
        if (![annotation isKindOfClass:[MKUserLocation class]]){
            [annotations addObject:annotation];
        }
    }
    [self showAnnotations:annotations animated:YES];
}

#pragma mark - ContextMenu

-(NSUInteger)numberOfContextualMenuItems{
    if (editingEnabled && self.canAddNote) {
        return 3;
    }else{
        return 2;
    }
}

-(UIView *)contextualMenu:(BAMContextualMenu *)contextualMenu viewForMenuItemAtIndex:(NSUInteger)index{
    UIImageView *imgView;
    switch (index) {
        case 0:
            imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"myLocation"]];
            break;
        case 1:
            imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"userLocation"]];
            break;
        case 2:
            imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"addNote"]];
            break;
    }
    
    imgView.layer.cornerRadius = imgView.frame.size.width/2;
    imgView.clipsToBounds = YES;
    
    return imgView;
}


-(NSString *)contextualMenu:(BAMContextualMenu *)contextualMenu titleForMenuItemAtIndex:(NSUInteger)index{
    switch (index) {
        case 0:
            return @"Show Current Location";
            break;
        case 1:
            return @"Plant Me Here";
            break;
        case 2:
            return @"Add Note";
            break;
    }
    return @"";
}

-(void)contextualMenu:(BAMContextualMenu *)contextualMenu didSelectItemAtIndex:(NSUInteger)index{
    if (index == 0) {
        MKCoordinateRegion mapRegion;
        mapRegion.center = self.userLocation.coordinate;
        mapRegion.span.latitudeDelta = 0.2;
        mapRegion.span.longitudeDelta = 0.2;
        [self setRegion:mapRegion animated:YES];
    }else if (index == 1) {
        CLLocationCoordinate2D coords = [self convertPoint:contextualMenu.startingLocation toCoordinateFromView:contextualMenu];
        [self.placesDelegate mapView:self drawRouteFromLocation:coords];
    }else{
        CLLocationCoordinate2D coords = [self convertPoint:contextualMenu.startingLocation toCoordinateFromView:contextualMenu];
        [self.placesDelegate mapView:self shouldAtNoteForLocation:coords];
    }
}


#pragma mark - MKMapViewDelegate


- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[QMapAnnotation class]])
    {
        QMapAnnotation *qannotation = (QMapAnnotation *)annotation;
        
        QMapAnnotationView *pinView = (QMapAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"MyAnnotation"];
        if (!pinView){
            pinView = [[QMapAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"MyAnnotation"];
            pinView.canShowCallout = NO;
            pinView.calloutOffset = CGPointMake(0, 0);
        }
        
        UIImage *image = [Place iconForType:qannotation.place.type];
        pinView.typeImageView.image = image;
        pinView.pinView.backgroundColor = [Place colorForType:qannotation.place.type];
        
        return pinView;
    }
    return nil;
}

-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)annotationView{
    
    [self.placesDelegate mapView:self didSelectAnnotation:(QMapAnnotationView *)annotationView];
        
    MKCoordinateRegion mapRegion;
    mapRegion.center = annotationView.annotation.coordinate;
    mapRegion.span.latitudeDelta = 0.005;
    mapRegion.span.longitudeDelta = 0.005;
    [self setRegion:mapRegion animated:YES];
}


-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation{
    if (self.firstLoad && self.annotations.count <= 1) {
        MKCoordinateRegion mapRegion;
        mapRegion.center = mapView.userLocation.coordinate;
        mapRegion.span.latitudeDelta = 0.25;
        mapRegion.span.longitudeDelta = 0.25;
        [mapView setRegion:mapRegion animated: YES];
        self.firstLoad = NO;
    }
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id)overlay
{
    if ([overlay isKindOfClass:[MKPolyline class]]) {
        MKPolylineRenderer *renderer = [[MKPolylineRenderer alloc] initWithOverlay:overlay];
        
        CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
        CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
        CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
        UIColor *color = [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];
        
        
        [renderer setStrokeColor:color];
        [renderer setLineWidth:3.0];
        return renderer;
    }
    if ([overlay isKindOfClass:[MKCircle class]])
    {
        MKCircleRenderer *renderer = [[MKCircleRenderer alloc] initWithCircle:overlay];
        
        renderer.fillColor   = [[UIColor redColor] colorWithAlphaComponent:0.2];
        renderer.lineWidth   = 3;
        
        return renderer;
    }

    return nil;
}

-(void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated{
    [self.placesDelegate mapViewShouldUpdateZoomSlider:self];
    if (self.shouldShowResearch) {
        [UIView animateWithDuration:0.3 animations:^{
            self.researchButton.alpha = 1;
        }];
    }else{
        [UIView animateWithDuration:0.3 animations:^{
            self.researchButton.alpha = 0;
        }];
    }
}



-(IBAction)research:(id)sender{
    [self.placesDelegate researchForMapView:self];
//    [UIView animateWithDuration:0.3 animations:^{
//        self.researchButton.alpha = 0;
//    }];
}


#pragma mark - Helpers

- (double)getZoomLevel
{
    CLLocationDegrees longitudeDelta = self.region.span.longitudeDelta;
    CGFloat mapWidthInPixels = self.bounds.size.width;
    double zoomScale = longitudeDelta * MERCATOR_RADIUS * M_PI / (180.0 * mapWidthInPixels);
    double zoomer = MAX_GOOGLE_LEVELS - log2( zoomScale );
    if ( zoomer < 0 ) zoomer = 0;
    //  zoomer = round(zoomer);
    return zoomer;
}


@end
