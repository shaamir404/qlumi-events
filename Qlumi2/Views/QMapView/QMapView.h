//
//  QMapView.h
//  Qlumi
//
//  Created by William Smillie on 12/19/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import <MapKit/MapKit.h>

#import "QMapAnnotationView.h"
#import "CalloutController.h"
#import "BAMContextualMenu.h"

@class QMapView;
@protocol QMapViewDelegate
-(void)mapView:(QMapView *)map didSelectAnnotation:(QMapAnnotationView *)annotationView;
-(void)mapView:(QMapView *)map shouldAtNoteForLocation:(CLLocationCoordinate2D)location;
-(void)mapView:(QMapView *)map drawRouteFromLocation:(CLLocationCoordinate2D)location;
-(void)researchForMapView:(QMapView *)map;
-(void)mapViewShouldUpdateZoomSlider:(QMapView *)map;
@end

@interface QMapView : MKMapView <MKMapViewDelegate, BAMContextualMenuDelegate, BAMContextualMenuDataSource>

-(id)initWithFrame:(CGRect)frame andEditingEnabled:(BOOL)editing;

@property (nonatomic, weak) id <QMapViewDelegate> placesDelegate;
@property (nonatomic, strong) UIButton *researchButton;
@property BAMContextualMenu *contextualMenu;

@property BOOL firstLoad;
@property BOOL shouldShowResearch;
@property BOOL canAddNote;

-(void)zoomToPins;
- (double)getZoomLevel;

@end
