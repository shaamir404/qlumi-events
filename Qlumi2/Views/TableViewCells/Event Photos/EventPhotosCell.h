//
//  photoCell.h
//  Qlumi
//
//  Created by William Smillie on 10/6/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import <UIKit/UIKit.h>

// My Class
#import "EventTableViewBaseCell.h"

@class EventPhotosCell;
@protocol EventPhotosSelectionDelegate <NSObject>
- (void) eventPhotosCell:(EventPhotosCell *)cell didSelectPhotoAtIndex:(NSInteger)index;
- (void) eventPhotosCell:(EventPhotosCell *)cell didDeletePhotoAtIndex:(NSInteger)index;
@end

@interface EventPhotosCell : EventTableViewBaseCell <UICollectionViewDelegate, UICollectionViewDataSource>
@property (nonatomic, weak) id <EventPhotosSelectionDelegate> selectionDelegate;

@property (nonatomic, strong) NSMutableArray *photosArray;
@property (nonatomic, strong) NSMutableArray *photoURLs;


@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;

-(void)setEditingEnabled:(BOOL)enabled;

@end
