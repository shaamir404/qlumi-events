//
//  EventAddPhotosTableViewCell.h
//  Qlumi
//
//  Created by Will on 1/19/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventTableViewBaseCell.h"


@class EventAddPhotosCell;
@protocol EventAddPhotosCellDelegate <NSObject>
- (void)AddPhotosCell:(EventAddPhotosCell *)cell didSelectPhoto:(UIImage *)image;
@end



@interface EventAddPhotosCell : EventTableViewBaseCell <UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (nonatomic, weak) id <EventAddPhotosCellDelegate>photoSelectionDelegate;
@property (strong, nonatomic) IBOutlet UIButton *addPhotosButton;
@property (nonatomic) BOOL shouldDisplaySectionHeader;


-(void)setTint:(UIColor *)color;
@end
