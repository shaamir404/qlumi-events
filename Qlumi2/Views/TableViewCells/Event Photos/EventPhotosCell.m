//
//  photoCell.m
//  Qlumi
//
//  Created by William Smillie on 10/6/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import "EventPhotosCell.h"
#import "photoCollectionViewCell.h"

#import <Parse/Parse.h>

@interface EventPhotosCell (){
    bool editingEnabled;
    UIWindow *modalWindow;
}

@end

@implementation EventPhotosCell
@synthesize photosArray = _photosArray;

-(void)awakeFromNib{
    [super awakeFromNib];

    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.pagingEnabled = YES;
    
    UIEdgeInsets insets = UIEdgeInsetsMake(0, 0, 0, 0);
    self.collectionView.contentInset = insets;

    [self.collectionView registerClass:[photoCollectionViewCell class] forCellWithReuseIdentifier:@"photoCell"];
    [self adjustContentInsets];
    
    
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
       initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.delaysTouchesBegan = YES;
    lpgr.cancelsTouchesInView = YES;
    lpgr.minimumPressDuration = 1;
    [self.collectionView addGestureRecognizer:lpgr];
}

- (void)adjustContentInsets {
    UIEdgeInsets insets = UIEdgeInsetsMake(0, 0, 0, 0);
    self.collectionView.contentInset = insets;
    self.collectionView.scrollIndicatorInsets = insets;
}

-(void)setEditingEnabled:(BOOL)enabled{
    editingEnabled = enabled;
    [self.collectionView reloadData];
}

-(void)setPhotosArray:(PFRelation *)photosArray{
    PFQuery *pQ = [photosArray query];
    [pQ includeKey:@"event"];
    [pQ findObjectsInBackgroundWithBlock:^(NSArray * _Nullable photos, NSError * _Nullable error) {
        _photosArray = [photos mutableCopy];
        
        NSMutableArray *urls = [[NSMutableArray alloc] init];
        for (PFObject *photo in photos) {
            [urls addObject:[(PFFile *)photo[@"image"] url]];
        }
        self.photoURLs = urls;
        
        [self.collectionView reloadData];
    }];
}


#pragma mark - edit

-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        CGPoint p = [gestureRecognizer locationInView:self.collectionView];
        NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:p];
        
        if (([[self.photosArray[indexPath.row][@"user"] objectId] isEqualToString:[PFUser currentUser].objectId]) | ([[self.photosArray[indexPath.row][@"event"][@"user"] objectId] isEqualToString:[PFUser currentUser].objectId])) {
            if (indexPath != nil){                
                UIAlertController *deleteSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
                [deleteSheet addAction:[UIAlertAction actionWithTitle:@"Delete Photo" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
                    modalWindow = nil;
                    PFObject *photo = self.photosArray[indexPath.row];
                    [photo deleteInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                        if (succeeded && !error) {
                            [self.photoURLs removeObjectAtIndex:indexPath.row];
                            [self.photosArray removeObjectAtIndex:indexPath.row];
                            [self.collectionView reloadData];
                        }
                    }];
                }]];
                [deleteSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                    modalWindow = nil;
                }]];
                
                
                if (!modalWindow) {
                    modalWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
                    modalWindow.rootViewController = [UIViewController new];
                    modalWindow.windowLevel = UIWindowLevelAlert + 1;
                }
                [modalWindow makeKeyAndVisible];
                [modalWindow.rootViewController presentViewController:deleteSheet animated:YES completion:nil];
            }
        }
    }
}


#pragma mark - <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.photoURLs.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    photoCollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"photoCell" forIndexPath:indexPath];
    
    [cell.imageView sd_setImageWithURL:self.photoURLs[indexPath.row] placeholderImage:[UIImage imageNamed:@"photo-placeholder"]];
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [self.selectionDelegate eventPhotosCell:self didSelectPhotoAtIndex:indexPath.row];
}

#pragma mark <UICollectionViewDelegate>

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    float size = (collectionView.frame.size.height/2)-12;
    return CGSizeMake(size, size);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout
   numberOfColumnsInSection:(NSInteger)section {
    return 2;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout
        insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(5.0, 5.0, 5.0, 5.0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout
interitemSpacingForSectionAtIndex:(NSInteger)section {
    return 1.0;
}


- (BOOL)headerShouldOverlayContentInCollectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout {
    return YES;
}

- (BOOL)footerShouldOverlayContentInCollectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout {
    return YES;
}

+(CGFloat)sectionHeight{
    return 300;
}

-(NSString *)sectionTitle{
    return @"PHOTOS";
}





@end
