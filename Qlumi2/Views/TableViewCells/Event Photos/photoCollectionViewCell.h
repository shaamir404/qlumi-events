//
//  photoCollectionViewCell.h
//  Qlumi
//
//  Created by William Smillie on 10/6/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import <UIKit/UIKit.h>

// 3rd party
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIView+WebCache.h>

@interface photoCollectionViewCell : UICollectionViewCell

@property (nonatomic, retain) UIImageView *imageView;

@end
