//
//  EventAddPhotosTableViewCell.m
//  Qlumi
//
//  Created by Will on 1/19/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import "EventAddPhotosCell.h"
#import "AppDelegate.h"

@implementation EventAddPhotosCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void)setTint:(UIColor *)color{
    [super setTint:color];

    self.addPhotosButton.tintColor = color;
}


- (IBAction)addPhotos:(id)sender {
    [self showCameraPicker];
}


-(void)showCameraPicker{
    
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Upload Photo" message:nil preferredStyle:UIAlertControllerStyleActionSheet];

    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Photo Library"style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [[self topMostController] presentViewController:imagePicker animated:YES completion:nil];
    }]];
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]){
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera"style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [[self topMostController] presentViewController:imagePicker animated:YES completion:nil];
        }]];
    }
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel"style:UIAlertActionStyleCancel handler:nil]];
    
    UIPopoverPresentationController *popPresenter = [actionSheet popoverPresentationController];
    popPresenter.sourceView = self.addPhotosButton;
    popPresenter.sourceRect = self.addPhotosButton.bounds;
    popPresenter.delegate = self;

    [[self topMostController] presentViewController:actionSheet animated:YES completion:nil];
}

- (UIViewController*) topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}


-(BOOL)popoverPresentationControllerShouldDismissPopover:(UIPopoverPresentationController *)popoverPresentationController{
    return YES;
}


#pragma mark - UIImagePickerDelegate


-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    [picker dismissViewControllerAnimated:YES completion:nil];

    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    [self.photoSelectionDelegate AddPhotosCell:self didSelectPhoto:image];
}



+(CGFloat)sectionHeight{
    //if (self.shouldDisplaySectionHeader) {
    //    return 64;
    //}else{
        return 64;
    //}
}


-(NSString *)sectionTitle{
    if (self.shouldDisplaySectionHeader) {
        return @"PHOTOS";
    }else{
        return @"";
    }
}

@end
