//
//  photoCollectionViewCell.m
//  Qlumi
//
//  Created by William Smillie on 10/6/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import "photoCollectionViewCell.h"

@implementation photoCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.imageView = [[UIImageView alloc] initWithFrame:self.bounds];
        self.imageView.contentMode = UIViewContentModeScaleAspectFill;
        self.imageView.backgroundColor = [UIColor greenColor];
        self.imageView.clipsToBounds = YES;
        
        [self.imageView sd_setShowActivityIndicatorView:YES];
        
        [self addSubview:self.imageView];
    }
    return self;
}

@end
