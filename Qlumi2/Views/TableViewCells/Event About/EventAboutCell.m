//
//  EventAboutTableViewCell.m
//  Qlumi
//
//  Created by William Smillie on 9/26/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import "EventAboutCell.h"

@implementation EventAboutCell

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}


-(void)layoutSubviews{
    [super layoutSubviews];
}

+(CGFloat)sectionHeight{
    return 145;
}

-(NSString *)sectionTitle{
    return @"A FEW WORDS";
}

@end
