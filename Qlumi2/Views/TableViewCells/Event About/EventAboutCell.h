//
//  EventAboutTableViewCell.h
//  Qlumi
//
//  Created by William Smillie on 9/26/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import <UIKit/UIKit.h>

// My Class
#import "EventTableViewBaseCell.h"

@interface EventAboutCell : EventTableViewBaseCell

@property (strong, nonatomic) IBOutlet UITextView *textView;

@end
