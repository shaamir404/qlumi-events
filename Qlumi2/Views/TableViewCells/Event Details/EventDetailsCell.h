//
//  EventDetailsTableViewCell.h
//  Qlumi
//
//  Created by William Smillie on 9/26/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Event.h"

// My Class
#import "EventTableViewBaseCell.h"

@class EventDetailsCell;
@protocol EventDetailsRSVPDelegate <NSObject>
-(void)detailCell:(EventDetailsCell *)cell didRSVP:(NSString *)rsvp;
@end

@interface EventDetailsCell : EventTableViewBaseCell
@property (nonatomic, weak) id <EventDetailsRSVPDelegate> RSVPDelegate;

@property (strong, nonatomic) UILabel *dateLabel;
@property (strong, nonatomic) UILabel *timeLabel;
@property (strong, nonatomic) UILabel *addressLabel;
@property (strong, nonatomic) UILabel *hostsLabel;

@property (strong, nonatomic) UIButton *yesButton;
@property (strong, nonatomic) UIButton *noButton;
@property (strong, nonatomic) UIButton *maybeButton;

@property (strong, nonatomic) UIImageView *profilePictureImageView;

-(void)setEditingEnabled:(BOOL)enabled;

@end
