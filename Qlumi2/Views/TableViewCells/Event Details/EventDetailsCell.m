//
//  EventDetailsTableViewCell.m
//  Qlumi
//
//  Created by William Smillie on 9/26/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import "EventDetailsCell.h"

#import "AppDelegate.h"

@implementation EventDetailsCell

@synthesize yesButton, noButton, maybeButton, dateLabel, timeLabel;

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {

    }
    return self;
}


-(void)layoutSubviews{
    [super layoutSubviews];
    
    yesButton.layer.cornerRadius = 5;
    noButton.layer.cornerRadius = 5;
    maybeButton.layer.cornerRadius = 5;
    
    yesButton.layer.borderWidth = 1;
    noButton.layer.borderWidth = 1;
    maybeButton.layer.borderWidth = 1;
    
    self.profilePictureImageView.hidden = YES;


    
}

-(void)setEditingEnabled:(BOOL)enabled{
    //editingEnabled = enabled;
    if (enabled){
        yesButton.hidden = YES;
        noButton.hidden = YES;
        maybeButton.hidden = YES;
    }
    else{
        yesButton.hidden = NO;
        noButton.hidden = NO;
        maybeButton.hidden = NO;
    }
}



-(void)setTint:(UIColor *)color{
    [super setTint:color];
    
    yesButton.layer.borderColor = color.CGColor;
    noButton.layer.borderColor = color.CGColor;
    maybeButton.layer.borderColor = color.CGColor;
    
}


- (IBAction)rsvpNo:(id)sender {
    [self setRSVP:@"no"];
}
- (IBAction)rsvpMaybe:(id)sender {
    [self setRSVP:@"maybe"];
}
- (IBAction)rsvpYes:(id)sender {
    [self setRSVP:@"yes"];
}

-(void)setRSVP:(NSString *)rsvp{
    [self.RSVPDelegate detailCell:self didRSVP:rsvp];
    
    if ([rsvp isEqualToString:@"yes"]) {
        yesButton.backgroundColor = [UIColor colorWithRed:0.33 green:0.71 blue:0.64 alpha:1.0];
        maybeButton.backgroundColor = [UIColor whiteColor];
        noButton.backgroundColor = [UIColor whiteColor];
    }else if ([rsvp isEqualToString:@"no"]) {
        noButton.backgroundColor = [UIColor colorWithRed:0.88 green:0.44 blue:0.34 alpha:1.0];
        maybeButton.backgroundColor = [UIColor whiteColor];
        yesButton.backgroundColor = [UIColor whiteColor];
    }else if ([rsvp isEqualToString:@"maybe"]) {
        maybeButton.backgroundColor = [UIColor colorWithRed:0.97 green:0.66 blue:0.33 alpha:1.0];
        noButton.backgroundColor = [UIColor whiteColor];
        yesButton.backgroundColor = [UIColor whiteColor];
    }else{
        maybeButton.backgroundColor =  [UIColor whiteColor];
        noButton.backgroundColor = [UIColor whiteColor];
        yesButton.backgroundColor = [UIColor whiteColor];
    }
}


+(CGFloat)sectionHeight{
    return 235;
}

-(NSString *)sectionTitle{
    return @"WHEN & WHERE";
}

@end
