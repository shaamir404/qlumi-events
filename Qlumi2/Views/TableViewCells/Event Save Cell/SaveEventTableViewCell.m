//
//  SaveEventTableViewCell.m
//  Qlumi
//
//  Created by Will Smillie on 4/26/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import "SaveEventTableViewCell.h"

@implementation SaveEventTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.line.alpha = 0;
}



- (IBAction)save:(id)sender {
    [self.delegate saveEventTableViewCellTouchedDown:self];
}

+(CGFloat)sectionHeight{
    return 64;
}

-(NSString *)sectionTitle{
    return @"";
}


@end
