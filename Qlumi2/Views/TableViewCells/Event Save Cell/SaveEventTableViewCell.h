//
//  SaveEventTableViewCell.h
//  Qlumi
//
//  Created by Will Smillie on 4/26/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import "EventTableViewBaseCell.h"

@class SaveEventTableViewCell;
@protocol SaveEventDelegate <NSObject>
- (void)saveEventTableViewCellTouchedDown:(SaveEventTableViewCell *)cell;
@end


@interface SaveEventTableViewCell : EventTableViewBaseCell
@property (nonatomic, weak) id <SaveEventDelegate>delegate;
@property (strong, nonatomic) IBOutlet UIButton *saveButton;

@end
