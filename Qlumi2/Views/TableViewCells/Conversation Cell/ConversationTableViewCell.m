//
//  ConversationTableViewCell.m
//  Qlumi
//
//  Created by Will on 2/16/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import "ConversationTableViewCell.h"

@implementation ConversationTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];

    self.circle.layer.cornerRadius = self.circle.frame.size.height/2;
    self.readIndicator.layer.cornerRadius = self.readIndicator.frame.size.height/2;
    
    self.thumbnailImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.thumbnailImageView.clipsToBounds = YES;
    
    self.circle.hidden = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
