//
//  ConversationTableViewCell.h
//  Qlumi
//
//  Created by Will on 2/16/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConversationTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *groupLabel;
@property (strong, nonatomic) IBOutlet UIImageView *thumbnailImageView;
@property (strong, nonatomic) IBOutlet UITextView *messagePreview;

@property (strong, nonatomic) IBOutlet UIView *circle;
@property (strong, nonatomic) IBOutlet UIView *readIndicator;

@end
