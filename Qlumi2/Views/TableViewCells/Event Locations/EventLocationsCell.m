//
//  ExpandingMapTableViewCell.m
//  Qlumi
//
//  Created by William Smillie on 9/26/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import "EventLocationsCell.h"

@implementation EventLocationsCell
@synthesize mapView, isExpanded;


-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.addedLocations = [[NSArray alloc] init];
        
        mapView.layer.cornerRadius = 5;
        mapView.clipsToBounds = YES;
        [mapView setShowsUserLocation:YES];
        mapView.delegate = self;
    }
    return self;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    
    if (isExpanded) {
        mapView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    }else{
        mapView.frame = CGRectMake(8, 29, mapView.frame.size.width, mapView.frame.size.height);
    }
}

-(NSString *)sectionTitle{
    return @"ON THE MAP";
}

+(CGFloat)sectionHeight{
    return 235;
}




@end
