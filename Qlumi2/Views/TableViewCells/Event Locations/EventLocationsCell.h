//
//  ExpandingMapTableViewCell.h
//  Qlumi
//
//  Created by William Smillie on 9/26/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

// Apple
#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

// My Class
#import "EventTableViewBaseCell.h"

#import "QMapView.h"

@interface EventLocationsCell : EventTableViewBaseCell <MKMapViewDelegate>

@property IBOutlet QMapView *mapView;
@property NSArray *addedLocations;
@property BOOL isExpanded;

@end
