//
//  ScheduleTableViewCell.h
//  
//
//  Created by William Smillie on 11/3/17.
//

#import <UIKit/UIKit.h>
#import "ScheduledObject.h"

@class ScheduleTableViewCell;
@protocol ScheduleCellDelegate <NSObject>
-(void)scheduleTableViewCell:(ScheduleTableViewCell *)cell shouldPresentEventForIndexPath:(NSIndexPath *)indexPath;
@end

@interface ScheduleTableViewCell : UITableViewCell<UICollectionViewDelegate, UICollectionViewDataSource>
@property (nonatomic, weak) id <ScheduleCellDelegate> delegate; //define MyClassDelegate as delegate

@property (strong, nonatomic) NSArray *scheduledObjectsArray;
@property (strong, nonatomic) NSURL *eventImageURL;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UICollectionView *scheduleCollectionView;


@end
