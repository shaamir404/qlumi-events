//
//  ScheduleTableViewCell.m
//  
//
//  Created by William Smillie on 11/3/17.
//

#import "ScheduleTableViewCell.h"
#import "ScheduleCollectionViewCell.h"
#import "AppDelegate.h"

#import <SDWebImage/UIImageView+WebCache.h>

@implementation ScheduleTableViewCell
@synthesize scheduleCollectionView, scheduledObjectsArray;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    scheduledObjectsArray = [[NSArray alloc] init];
    
    scheduleCollectionView.delegate = self;
    scheduleCollectionView.dataSource = self;
    
    [scheduleCollectionView registerNib:[UINib nibWithNibName:@"ScheduleCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"scheduledEventCell"];
}

#pragma mark - UICollectionViewDelegate // DataSource
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSString *string = [(ScheduledObject *)[scheduledObjectsArray objectAtIndex:indexPath.row] title];
    CGSize size = [self getLabelSize:string];
    return CGSizeMake((size.width+24 < 100)?100:size.width+24, 80);
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return scheduledObjectsArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    ScheduleCollectionViewCell *cell;
    
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"scheduledEventCell" forIndexPath:indexPath];
    
    cell.contentView.layer.borderWidth = 1;
    cell.contentView.layer.borderColor = [AppDelegate QOrange].CGColor;
    cell.contentView.layer.cornerRadius = 10;

    if (indexPath.row == 0) {
        cell.nameLabel.text = [(ScheduledObject *)[scheduledObjectsArray objectAtIndex:indexPath.row] title];
        cell.startTimeLabel.text = [(ScheduledObject *)[scheduledObjectsArray objectAtIndex:indexPath.row] startTime];
        cell.endTimeLabel.text = @"";
        
//        cell.nameLabel.layer.shadowOpacity = 1;
//        cell.nameLabel.layer.shadowRadius = 3;
//        cell.nameLabel.layer.shadowOffset = CGSizeMake(0, 0);
//        cell.nameLabel.layer.shadowColor = [UIColor whiteColor].CGColor;
//
//        cell.startTimeLabel.layer.shadowOpacity = 1;
//        cell.startTimeLabel.layer.shadowRadius = 3;
//        cell.startTimeLabel.layer.shadowOffset = CGSizeMake(0, 0);
//        cell.startTimeLabel.layer.shadowColor = [UIColor whiteColor].CGColor;

        
        UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:cell.bounds];
        backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
        
        [backgroundImageView sd_setImageWithURL:self.eventImageURL];
        backgroundImageView.layer.cornerRadius = 10;
        backgroundImageView.clipsToBounds = true;
        cell.backgroundView = backgroundImageView;
        
        cell.backgroundView.alpha = 0.5;
    
    }else{
        cell.nameLabel.text = @"";
        cell.startTimeLabel.text = @"";
        cell.endTimeLabel.text = @"";
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.layer.borderWidth = 0;
        cell.contentView.layer.borderColor = [AppDelegate QOrange].CGColor;
        cell.contentView.layer.cornerRadius = 0;
    }

    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [collectionView selectItemAtIndexPath:indexPath animated:YES scrollPosition:UICollectionViewScrollPositionNone];
    
    if (indexPath.row == 0) {
        NSIndexPath *indexPath = [(UITableView *)self.superview indexPathForCell:self];
        [self.delegate scheduleTableViewCell:self shouldPresentEventForIndexPath:indexPath];
    }
}

- (CGSize)getLabelSize:(NSString *)string {
    CGRect rect = [string boundingRectWithSize:(CGSize){500, CGFLOAT_MAX} options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName: [UIFont systemFontOfSize:17]} context:nil];
    return rect.size;
}

@end
