//
//  ScheduleTableViewCell.m
//  Qlumi
//
//  Created by William Smillie on 10/30/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import "EventScheduleCell.h"
#import "ScheduleCollectionViewCell.h"
#import "ScheduledObject.h"
#import "AppDelegate.h"

@interface EventScheduleCell (){
    bool editingEnabled;
    
    UIAlertController *alert;
    NSDateFormatter *timeFormatter;
    UIDatePicker *timePicker;
    
    UITextField *startTextField;
    UITextField *endTextField;
}
@end

@implementation EventScheduleCell
@synthesize collectionView;

- (void)awakeFromNib {
    [super awakeFromNib];
    
    timePicker = [[UIDatePicker alloc] init];
    timePicker.datePickerMode = UIDatePickerModeTime;
    timePicker.minuteInterval = 5;
    [timePicker addTarget:self action:@selector(timePickerDidChange:) forControlEvents:UIControlEventValueChanged];

    timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"h:mm a"];

    self.scheduleArray = [[NSMutableArray alloc] init];
    
    collectionView.delegate = self;
    collectionView.dataSource = self;
    collectionView.alwaysBounceHorizontal = YES;
    
    [collectionView registerNib:[UINib nibWithNibName:@"ScheduleCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"scheduledEventCell"];
}

-(void)setEditingEnabled:(BOOL)enabled{
    editingEnabled = enabled;
    [collectionView reloadData];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

#pragma mark - UICollectionViewDelegate // DataSource
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row != 0) {
        NSString *string = [(ScheduledObject *)[self.scheduleArray objectAtIndex:indexPath.row-1] title];
        CGSize size = [self getLabelSize:string];
        return CGSizeMake((size.width+24 < 100)?100:size.width+24, 80);
    }else{
        return CGSizeMake(190, 80);
    }
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return editingEnabled ? self.scheduleArray.count+1 : self.scheduleArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    ScheduleCollectionViewCell *cell;
    
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"scheduledEventCell" forIndexPath:indexPath];
    
    cell.contentView.layer.borderWidth = 1;
    cell.contentView.layer.borderColor = [AppDelegate QOrange].CGColor;
    cell.contentView.layer.cornerRadius = 10.0;
    
    if (editingEnabled) {
        if (indexPath.row == 0) {
            cell.nameLabel.text = @"Add Activity (optional)";
            cell.startTimeLabel.text = @"";
            cell.endTimeLabel.text = @"";
        }else{
            if (self.scheduleArray.count > 0) {
                ScheduledObject *obj = [self.scheduleArray objectAtIndex:indexPath.row-1];
                cell.nameLabel.text = [obj title];
                cell.startTimeLabel.text = obj.startTime;
                cell.endTimeLabel.text = obj.endTime;
            }
        }
    }else{
        ScheduledObject *obj = [self.scheduleArray objectAtIndex:indexPath.row];
        cell.nameLabel.text = [obj title];
        cell.startTimeLabel.text = obj.startTime;
        cell.endTimeLabel.text = obj.endTime;
    }
    

    cell.backgroundColor = [UIColor whiteColor];

    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (editingEnabled) {
        if (indexPath.row == 0) {
            [self showNewScheduleItemForm];
        }else{
            UIAlertController *editAlert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
            [editAlert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
            [editAlert addAction:[UIAlertAction actionWithTitle:@"Edit" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self editScheduleItemAtIndexPath:indexPath];
            }]];
            [editAlert addAction:[UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
                [self.scheduleArray removeObjectAtIndex:indexPath.row-1];
                [self.collectionView reloadData];
            }]];
            [[self topMostController] presentViewController:editAlert animated:YES completion:nil];
        }
    }else{
        [collectionView selectItemAtIndexPath:indexPath animated:YES scrollPosition:UICollectionViewScrollPositionNone];
    }
}


#pragma mark - ScheduleObject Creation/editing
-(void)showNewScheduleItemForm{
    __weak typeof(self) weakSelf = self;
    
    alert = [UIAlertController alertControllerWithTitle:@"New Activity" message:nil preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"What's Happening?";
        textField.autocapitalizationType = UITextAutocapitalizationTypeWords;
        textField.keyboardType = UIKeyboardTypeDefault;
    }];
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        if (self.scheduleArray.count > 0) {
            ScheduledObject *lastObject = self.scheduleArray.lastObject;
            textField.text = lastObject.endTime;
        }

        textField.placeholder = @"From";
        textField.inputView = timePicker;
        textField.delegate = weakSelf;
    }];
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"To";
        textField.inputView = timePicker;
        textField.delegate = weakSelf;
    }];
    [alert addAction:[UIAlertAction actionWithTitle:@"Add" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if ([[timeFormatter dateFromString:alert.textFields[1].text] compare:[timeFormatter dateFromString:alert.textFields[2].text]] != NSOrderedDescending) {
            [self.scheduleArray insertObject:[[ScheduledObject alloc] initWithTitle:alert.textFields[0].text startTime:alert.textFields[1].text endTime:alert.textFields[2].text] atIndex:self.scheduleArray.count];
            [self.collectionView reloadData];
        }else{
            UIAlertController *error = [UIAlertController alertControllerWithTitle:@"Check Times" message:@"End times mush be after start times..." preferredStyle:UIAlertControllerStyleAlert];
            [error addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
            [[self topMostController] presentViewController:error animated:YES completion:nil];
        }
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [[self topMostController] presentViewController:alert animated:YES completion:nil];
}



-(void)editScheduleItemAtIndexPath:(NSIndexPath *)indexPath{
    
    ScheduledObject *thisObject = [self.scheduleArray objectAtIndex:indexPath.row-1];
    __weak typeof(self) weakSelf = self;
    
    alert = [UIAlertController alertControllerWithTitle:@"New Schedule Item" message:nil preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Title";
        textField.text = thisObject.title;
        textField.keyboardType = UIKeyboardTypeDefault;
    }];
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        startTextField = textField;
        textField.placeholder = @"Start Time";
        textField.text = thisObject.startTime;
        textField.inputView = timePicker;
        textField.delegate = weakSelf;
        textField.tag = 0;
    }];
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        endTextField = textField;
        textField.placeholder = @"End Time";
        textField.text = thisObject.endTime;
        textField.inputView = timePicker;
        textField.delegate = weakSelf;
        textField.tag = 1;
    }];
    [alert addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if ([[timeFormatter dateFromString:alert.textFields[1].text] compare:[timeFormatter dateFromString:alert.textFields[2].text]] != NSOrderedDescending) {
            [self.scheduleArray removeObjectAtIndex:indexPath.row-1];
            [self.scheduleArray insertObject:[[ScheduledObject alloc] initWithTitle:alert.textFields[0].text startTime:alert.textFields[1].text endTime:alert.textFields[2].text] atIndex:indexPath.row-1];
            [self.collectionView reloadData];
        }else{
            UIAlertController *error = [UIAlertController alertControllerWithTitle:@"Check Times" message:@"End times mush be after start times..." preferredStyle:UIAlertControllerStyleAlert];
            [error addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
            [[self topMostController] presentViewController:error animated:YES completion:nil];
        }
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [[self topMostController] presentViewController:alert animated:YES completion:nil];
}


-(IBAction)timePickerDidChange:(id)sender{
    for (UITextField *textField in alert.textFields) {
        if (textField.isFirstResponder) {
            [textField setText:[timeFormatter stringFromDate:[(UIDatePicker *)sender date]]];
        }
    }
}

#pragma mark - UITextFieldDelegate

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if (textField.text.length > 0) {
        timePicker.date = [timeFormatter dateFromString:textField.text];
    }
    
    return YES;
}

#pragma mark - Helper Methods


- (UIViewController*)topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}

- (CGSize)getLabelSize:(NSString *)string {
    CGRect rect = [string boundingRectWithSize:(CGSize){500, CGFLOAT_MAX} options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName: [UIFont systemFontOfSize:17]} context:nil];
    return rect.size;
}

+(CGFloat)sectionHeight{
    return 160;
}

-(NSString *)sectionTitle{
    return @"Activities";
}



@end
