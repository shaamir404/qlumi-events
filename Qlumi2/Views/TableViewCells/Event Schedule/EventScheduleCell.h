//
//  ScheduleTableViewCell.h
//  Qlumi
//
//  Created by William Smillie on 10/30/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import <UIKit/UIKit.h>

// My Class
#import "EventTableViewBaseCell.h"
#import "ScheduledObject.h"

@interface EventScheduleCell : EventTableViewBaseCell <UICollectionViewDelegate, UICollectionViewDataSource, UITextFieldDelegate>

@property (nonatomic, retain) NSMutableArray *scheduleArray;
@property (nonatomic, strong) IBOutlet UICollectionView *collectionView;

-(void)setEditingEnabled:(BOOL)enabled;

@end
