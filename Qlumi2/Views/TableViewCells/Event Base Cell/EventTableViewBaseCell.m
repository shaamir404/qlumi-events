//
//  EventTableViewBaseCell.m
//  Qlumi2
//
//  Created by William Smillie on 12/14/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import "EventTableViewBaseCell.h"

@implementation EventTableViewBaseCell

- (void)awakeFromNib {
    [super awakeFromNib];

    self.line = [[UIView alloc] initWithFrame:CGRectMake(8, 8, self.frame.size.width-16, 1)];
    self.line.backgroundColor = [UIColor colorWithWhite:0.9f alpha:1];
    self.line.tag = 1;
    [self insertSubview:self.line atIndex:0];
    
    self.sectionTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 8, 100, 100)];
    self.sectionTitleLabel.font = [UIFont boldSystemFontOfSize:14];
    self.sectionTitleLabel.text = [self sectionTitle];
    self.sectionTitleLabel.textAlignment = NSTextAlignmentCenter;
    self.sectionTitleLabel.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.sectionTitleLabel];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

-(void)layoutSubviews{
    [self.sectionTitleLabel setAdjustsFontSizeToFitWidth:NO];
    [self.sectionTitleLabel sizeToFit];
    self.sectionTitleLabel.text = [self sectionTitle];
    self.sectionTitleLabel.frame = CGRectMake(self.sectionTitleLabel.frame.origin.x-4, self.sectionTitleLabel.frame.origin.y+5, self.sectionTitleLabel.frame.size.width+8, self.sectionTitleLabel.frame.size.height);
    self.sectionTitleLabel.center = CGPointMake(self.center.x, self.sectionTitleLabel.center.y);
    self.line.frame = CGRectMake(8, 8+(self.sectionTitleLabel.frame.size.height/2), self.frame.size.width-16, 1);
}

-(void)setTint:(UIColor *)color{
    [[self viewWithTag:1] setBackgroundColor:color];
}

@end
