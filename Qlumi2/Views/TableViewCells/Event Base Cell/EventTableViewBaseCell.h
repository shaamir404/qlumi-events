//
//  EventTableViewBaseCell.h
//  Qlumi2
//
//  Created by William Smillie on 12/14/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventTableViewBaseCell : UITableViewCell

@property (strong, nonatomic) UILabel *sectionTitleLabel;
@property (strong, nonatomic) UIView *line;

@property (weak, nonatomic) UIColor *tint;

-(void)setTint:(UIColor *)color;
-(NSString *)sectionTitle;
+(CGFloat)sectionHeight;

@end
