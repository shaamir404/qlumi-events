//
//  FeaturedWebCell.h
//  Qlumi
//
//  Created by Will Smillie on 4/23/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "EventTableViewBaseCell.h"

#import <SafariServices/SFSafariViewController.h>


@interface FeaturedWebCell : EventTableViewBaseCell <UIWebViewDelegate>

@property (nonatomic, strong) IBOutlet UIWebView *webView;
@property (nonatomic, strong) NSString *sectionTitle;

@end
