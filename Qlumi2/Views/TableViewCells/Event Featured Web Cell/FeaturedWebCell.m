//
//  FeaturedWebCell.m
//  Qlumi
//
//  Created by Will Smillie on 4/23/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import "FeaturedWebCell.h"

@interface FeaturedWebCell (){
    BOOL didLoad;
}
@end

@implementation FeaturedWebCell

-(void)awakeFromNib{
    [super awakeFromNib];
    self.webView.delegate = self;
    
    didLoad = NO;
}


-(void)layoutSubviews{
    [super layoutSubviews];
}

+(CGFloat)sectionHeight{
    return 400;
}

-(void)setSectionTitle:(NSString *)sectionTitle{
    _sectionTitle = sectionTitle;
}


#pragma mark - UIWebViewDelegate

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    if (!didLoad){
        didLoad = YES;
        return YES;
    }else{
        if (![request.URL.absoluteString isEqualToString:@"about:blank"]) {
            SFSafariViewController *sf = [[SFSafariViewController alloc] initWithURL:request.URL];
            [[self topMostController] presentViewController:sf animated:YES completion:nil];
        }
        
        return NO;
    }
}

- (UIViewController*) topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}


@end
