//
//  EventAddPhotosTableViewCell.m
//  Qlumi
//
//  Created by Will on 1/19/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import "EventSendSMSButtonCell.h"
#import "AppDelegate.h"
#import "SMSContactSelectorViewController.h"

@implementation EventSendSMSButtonCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.shouldDisplaySectionHeader = NO;
    self.line.alpha = 0;
}

-(void)setTint:(UIColor *)color{
    [super setTint:color];
    self.sendSMSButton.tintColor = color;
}


- (IBAction)sendSMS:(id)sender {
    [self showGuestsPicker];
}


-(void)showGuestsPicker{
    SMSContactSelectorViewController *contactSelector = [[SMSContactSelectorViewController alloc] init];
    [contactSelector setGuestsArray:self.guestsArray];
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:contactSelector];

    [[self topMostController] setDefinesPresentationContext:YES];
    [[self topMostController] setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    [[self topMostController] presentViewController:nav animated:YES completion:nil];
}

- (UIViewController*) topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}


-(BOOL)popoverPresentationControllerShouldDismissPopover:(UIPopoverPresentationController *)popoverPresentationController{
    return YES;
}


#pragma mark - Guest Picker


//[self.numberSelectionDelegate EventSendSMSButtonCell:self didSelectNumbers:nil];

+(CGFloat)sectionHeight{
    return 64;
}

-(NSString *)sectionTitle{
    return @"";
}

@end
