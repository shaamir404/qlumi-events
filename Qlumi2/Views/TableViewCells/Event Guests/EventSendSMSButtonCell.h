//
//  EventAddPhotosTableViewCell.h
//  Qlumi
//
//  Created by Will on 1/19/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventTableViewBaseCell.h"


@class EventSendSMSButtonCell;
@protocol EventSendSMSButtonCellDelegate <NSObject>
- (void)EventSendSMSButtonCell:(EventSendSMSButtonCell *)cell didSelectNumbers:(NSArray *)numbers;
@end



@interface EventSendSMSButtonCell : EventTableViewBaseCell
@property (nonatomic, weak) id <EventSendSMSButtonCellDelegate> numberSelectionDelegate;
@property (strong, nonatomic) IBOutlet UIButton *sendSMSButton;
@property (nonatomic) BOOL shouldDisplaySectionHeader;
@property (nonatomic, strong) NSMutableArray *guestsArray;


-(void)setTint:(UIColor *)color;
@end
