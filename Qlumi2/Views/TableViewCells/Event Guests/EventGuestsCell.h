//
//  EventGuestsCell.h
//  Qlumi
//
//  Created by William Smillie on 1/2/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import "EventTableViewBaseCell.h"
#import "GuestsCollectionViewCell.h"
#import "contactsGroupCollectionView.h"
#import "Qlumi-Swift.h"

@class EventGuestsCell;
@protocol GuestSelectionDelegate
-(void)guestsCell:(EventGuestsCell *)guestsCell didSetGuestsArray:(NSArray *)guestsArray;
-(void)guestsCell:(EventGuestsCell *)guestsCell didAddGuest:(NSDictionary *)guest;
-(void)guestsCell:(EventGuestsCell *)guestsCell didRemoveGuest:(NSDictionary *)guest;
@end

@interface EventGuestsCell : EventTableViewBaseCell <UICollectionViewDelegate, UICollectionViewDataSource, UITextFieldDelegate, contactGroupCVDelegate, EPPickerDelegate>{
    NSMutableArray *_guestsArray;
}
@property (nonatomic, weak) id <GuestSelectionDelegate> delegate;
@property (nonatomic, strong) NSString *customHeader;

@property (nonatomic, strong) NSMutableArray *guestsArray;
@property (nonatomic, strong) IBOutlet UICollectionView *collectionView;

@property (nonatomic) BOOL selectionEnabled;
@property (nonatomic, strong) NSMutableArray *selectedGuests;
@property (weak, nonatomic) IBOutlet UIButton *selectAllButton;


-(void)setEditingEnabled:(BOOL)enabled;

@end
