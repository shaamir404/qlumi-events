//
//  EventGuestsCell.m
//  Qlumi
//
//  Created by William Smillie on 1/2/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import "EventGuestsCell.h"
#import "NewEventTableViewController.h"

@interface EventGuestsCell (){
    bool editingEnabled;
    UIAlertController *alert;
    UINavigationController *nav;
    
    NSMutableDictionary *profilePictureURLs;
    UIAlertController *contactPickerAlert;
    
    UIWindow *modalWindow;
    contactsGroupCollectionView *contactGroupView;
    
    BOOL creatingGroup;
    BOOL selectedAll;
    BOOL addBtnAdded;
}
@end

@implementation EventGuestsCell
//@synthesize guestsArray = _guestsArray;
@synthesize collectionView;

- (void)awakeFromNib {
    [super awakeFromNib];

    self.guestsArray = [[NSMutableArray alloc] init];
    self.selectedGuests = [[NSMutableArray alloc] init];
    creatingGroup = NO;
    addBtnAdded = NO;
    collectionView.delegate = self;
    collectionView.dataSource = self;
    collectionView.alwaysBounceHorizontal = YES;
    
    self.selectAllButton.hidden = YES;
    
    [collectionView registerNib:[UINib nibWithNibName:@"GuestsCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"guestsCell"];
    
    
    //[collectionView transform] = CATransform3DGetAffineTransform((rotationAngle: (-(CGFloat)(Double.pi))));
    
    
    
}

-(void)setGuestsArray:(NSMutableArray *)guests{
    _guestsArray = [guests mutableCopy];

    profilePictureURLs = [[NSMutableDictionary alloc] init];
    NSMutableArray *phoneNumbers = [[NSMutableArray alloc] init];
    for (NSMutableDictionary *guest in self.guestsArray) {
        [phoneNumbers addObject:guest[@"phoneNumber"]];
    }
    
    PFQuery *usersQuery = [PFUser query];
    [usersQuery whereKey:@"phoneNumber" containedIn:phoneNumbers];
    [usersQuery findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        if (!error) {
            for (PFUser *profile in objects) {
                PFFile *picFile = profile[@"profilePicture"];
                if (picFile) {
                    [profilePictureURLs setObject:picFile.url forKey:profile[@"phoneNumber"]];
                }
            }
            [self.collectionView reloadData];
        }
    }];
    [self.collectionView reloadData];
    [self.delegate guestsCell:self didSetGuestsArray:guests];
}

-(void)setEditingEnabled:(BOOL)enabled{
    editingEnabled = enabled;
    [collectionView reloadData];
}

-(void)setSelectionEnabled:(BOOL)selectionEnabled{
    _selectionEnabled = selectionEnabled;
    self.selectAllButton.hidden = NO;
    [collectionView reloadData];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}


#pragma mark - UICollectionViewDelegate // DataSource

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(64, 64);
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return editingEnabled?self.guestsArray.count+1:self.guestsArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    GuestsCollectionViewCell *cell;
    
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"guestsCell" forIndexPath:indexPath];
    
    
    
    if (editingEnabled){
        if (indexPath.row == 0){
        
                cell.nameLabel.text = @"Add Guest";
                cell.profilePicture.image = nil;
                cell.profilePicture.image = [UIImage imageNamed:@"add"];
                cell.profilePicture.backgroundColor = [UIColor whiteColor];
                cell.rsvpImageView.hidden = YES;
                
                cell.initialsLabel.text = @"";
                return cell;
           
        }
        else if (indexPath.row != 0){
             cell = [self userCellAtIndexPath:indexPath];
        }
    }
    else{
      cell = [self userCellAtIndexPath:indexPath];
    }
    
    
    
   
    
    if (self.guestsArray.count > 0 && self.selectedGuests.count > 0){
        if ([self.selectedGuests containsObject:[self.guestsArray objectAtIndex:indexPath.row]]) {
            cell.selectionOverlayImageView.alpha = 1;
        }else{
            cell.selectionOverlayImageView.alpha = 0;
        }
    }
    
    return cell;
}

-(GuestsCollectionViewCell *)userCellAtIndexPath:(NSIndexPath *)indexPath{
    GuestsCollectionViewCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:@"guestsCell" forIndexPath:indexPath];
    
    NSDictionary *guest;
    
    
    if (indexPath.row == 0){
        NSLog(@"******************************************************");
    }
    
    
    if (editingEnabled){
        guest = [self.guestsArray objectAtIndex:indexPath.row-1];
    }
    else{
        guest = [self.guestsArray objectAtIndex:indexPath.row];
    }
    
    
    cell.nameLabel.text = guest[@"name"];
    
    
    if ([profilePictureURLs objectForKey:guest[@"phoneNumber"]]) {
        [cell.profilePicture sd_setImageWithURL:[profilePictureURLs objectForKey:guest[@"phoneNumber"]] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            dispatch_async(dispatch_get_main_queue(), ^{
            [collectionView reloadData];
            });
        }];
    }else{
        cell.profilePicture.image = nil;
    }
    
    
    cell.rsvpImageView.image = [cell imageForRSVP:guest[@"rsvp"]];
    cell.rsvpImageView.backgroundColor = [cell colorForRSVP:guest[@"rsvp"]];
    cell.rsvpImageView.hidden = NO;
    cell.containerView.backgroundColor = cell.colorsArray[(indexPath.row + indexPath.section) % cell.colorsArray.count];
    
    
    if (!cell.profilePicture.image) {
        cell.profilePicture.hidden = YES;
        NSArray *components = [guest[@"name"] componentsSeparatedByString:@" "];
        if (components.count > 1) {
            cell.initialsLabel.text = [NSString stringWithFormat:@"%@%@", ([components[0] length] > 0)?[components[0] substringToIndex:1]:@"", ([components[1] length] > 0)?[components[1] substringToIndex:1]:@""];
        }else{
            cell.initialsLabel.text = [NSString stringWithFormat:@"%@", ([components[0] length] > 0)?[components[0] substringToIndex:1]:@""];
        }
    }else{
        cell.profilePicture.hidden = NO;
        cell.initialsLabel.text = @"";
    }
    
    return cell;
    
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (editingEnabled) {
        if (indexPath.row == 0) {
            [self showNewGuestItemForm];
        }else{
            UIAlertController *editAlert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
            [editAlert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
            [editAlert addAction:[UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
                [self.guestsArray removeObjectAtIndex:indexPath.row-1];
                [self setGuestsArray:self.guestsArray];
                [self.collectionView reloadData];
                
            }]];
            [(NewEventTableViewController *)[self viewController] presentViewController:editAlert animated:YES completion:nil];
        }
    }else if (self.selectionEnabled){
        if ([self.selectedGuests containsObject:[self.guestsArray objectAtIndex:indexPath.row-1]]) {
            [self.selectedGuests removeObject:[self.guestsArray objectAtIndex:indexPath.row-1]];
            [self.delegate guestsCell:self didRemoveGuest:[self.guestsArray objectAtIndex:indexPath.row-1]];
        }else{
            [self.selectedGuests addObject:[self.guestsArray objectAtIndex:indexPath.row-1]];
            [self.delegate guestsCell:self didAddGuest:[self.guestsArray objectAtIndex:indexPath.row-1]];
        }
        [self.collectionView reloadData];
    }else{
        [collectionView selectItemAtIndexPath:indexPath animated:YES scrollPosition:UICollectionViewScrollPositionNone];
    }
}


#pragma mark - ScheduleObject Creation/editing
-(void)showNewGuestItemForm{
    contactPickerAlert = [UIAlertController alertControllerWithTitle:@"Contact Groups\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        contactPickerAlert.modalPresentationStyle = UIModalPresentationPopover;
        contactPickerAlert.popoverPresentationController.sourceView = self.contentView;
        contactPickerAlert.popoverPresentationController.sourceRect = self.contentView.bounds;
    }

    CGFloat margin = 8.0F;
    contactGroupView = [[contactsGroupCollectionView alloc] initWithFrame:CGRectMake(margin, margin*2, contactPickerAlert.view.bounds.size.width - margin * 4.0F, 100.0F)];
    contactGroupView.pickerDelegate = self;
    [contactPickerAlert.view addSubview:contactGroupView];
    
    UIAlertAction *somethingAction = [UIAlertAction actionWithTitle:@"Open My Contacts" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        creatingGroup = NO;
        
        if (!modalWindow) {
            modalWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
            modalWindow.rootViewController = [UIViewController new];
            modalWindow.windowLevel = UIWindowLevelAlert + 1;
        }
        EPContactsPicker *picker = [[EPContactsPicker alloc] initWithDelegate:self multiSelection:YES];
        picker.contactDelegate = self;
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:picker];
        [modalWindow makeKeyAndVisible];
        [modalWindow.rootViewController presentViewController:nav animated:YES completion:nil];
        
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {}];
    [contactPickerAlert addAction:somethingAction];
    [contactPickerAlert addAction:cancelAction];
    [(NewEventTableViewController *)[self viewController] presentViewController:contactPickerAlert animated:YES completion:^{}];
}

-(void)contactGroupPickerCreateNewGroup:(contactsGroupCollectionView *)contactGroupPicker{
    creatingGroup = YES;
    
    if (!modalWindow) {
        modalWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        modalWindow.rootViewController = [UIViewController new];
        modalWindow.windowLevel = UIWindowLevelAlert + 1;
    }
    
    EPContactsPicker *picker = [[EPContactsPicker alloc] initWithDelegate:self multiSelection:YES];
    picker.contactDelegate = self;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:picker];
    
    [modalWindow makeKeyAndVisible];
    [modalWindow.rootViewController presentViewController:nav animated:YES completion:nil];
}

-(void)contactGroupPicker:(contactsGroupCollectionView *)contactGroupPicker didSelectGroups:(NSArray *)groupsArray{
    [contactPickerAlert dismissViewControllerAnimated:YES completion:^{
        modalWindow = nil;
    }];
    
    if (!self.guestsArray) {
        self.guestsArray = [[NSMutableArray alloc] init];
    }
    
    NSMutableArray *guests = [self.guestsArray mutableCopy];
    for (NSMutableDictionary *guest in groupsArray) {
        [guests addObject:guest];
    }
    
    [self setGuestsArray:guests];
    [self.collectionView reloadData];
}


#pragma mark - ContactPickerDelegate

-(void)epContactPicker:(EPContactsPicker *)contactPicker didCancel:(NSError *)error{
    [contactPicker dismissViewControllerAnimated:YES completion:^{}];
    modalWindow = nil;
}


-(void)epContactPicker:(EPContactsPicker *)contactPicker didSelectMultipleContacts:(NSArray *)contacts{
    [contactPicker dismissViewControllerAnimated:YES completion:^{
    }];
    modalWindow = nil;
    if (!self.guestsArray) {
        self.guestsArray = [[NSMutableArray alloc] init];
    }
    
    
    if (creatingGroup) {
        NSMutableArray *guests = [[NSMutableArray alloc] init];
        for (EPContact *contact in contacts) {
            NSLog(@"Guests: %@", contact.phoneNumber);

            NSMutableDictionary *guest = [[NSMutableDictionary alloc] init];
            [guest setObject:[NSString stringWithFormat:@"%@ %@", contact.firstName, contact.lastName] forKey:@"name"];
            [guest setObject:[[contact.phoneNumber componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""] forKey:@"phoneNumber"];
            [guests addObject:guest];
        }
        
        
        UIAlertController *nameGroupAlert = [UIAlertController alertControllerWithTitle:@"Group Name" message:@"\n\n\n" preferredStyle:UIAlertControllerStyleAlert];
        [nameGroupAlert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
            
        }];
        [nameGroupAlert addAction:[UIAlertAction actionWithTitle:@"Create Group" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            modalWindow = nil;
            
            PFObject *contactGroup = [PFObject objectWithClassName:@"ContactGroup"];
            contactGroup[@"name"] = nameGroupAlert.textFields[0].text;
            contactGroup[@"owner"] = [PFUser currentUser];
            contactGroup[@"contacts"] = guests;
            [contactGroup saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                [contactGroupView refresh];
            }];
        }]];
        [nameGroupAlert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            modalWindow = nil;
        }]];
        [(NewEventTableViewController *)[self viewController] presentViewController:nameGroupAlert animated:YES completion:nil];
        
        
        if (!modalWindow) {
            modalWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
            modalWindow.rootViewController = [UIViewController new];
            modalWindow.windowLevel = UIWindowLevelAlert + 1;
        }
        
        [modalWindow makeKeyAndVisible];
        [modalWindow.rootViewController presentViewController:nameGroupAlert animated:YES completion:nil];
    }else{
        NSMutableArray *guests = [self.guestsArray mutableCopy];
        for (EPContact *contact in contacts) {
            NSMutableDictionary *guest = [[NSMutableDictionary alloc] init];
            [guest setObject:[NSString stringWithFormat:@"%@ %@", contact.firstName, contact.lastName] forKey:@"name"];
            if (contact.phoneNumber.length > 0) {
                NSLog(@"Guests: %@", contact.phoneNumber);

                [guest setObject:[[contact.phoneNumber componentsSeparatedByCharactersInSet:
                                   [[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""] forKey:@"phoneNumber"];
                [guest setObject:@"none" forKey:@"rsvp"];
                [guests addObject:guest];
            }
        }
        
        [self setGuestsArray:guests];
        [self.collectionView reloadData];
    }
}

- (IBAction)selectAll:(id)sender {

    if (selectedAll) {
        for (NSDictionary *guest in self.guestsArray) {
            [self.selectedGuests removeObject:guest];
            [self.delegate guestsCell:self didRemoveGuest:guest];
        }
        [self.collectionView reloadData];
        selectedAll = NO;
        [self.selectAllButton setTitle:@"Select All" forState:UIControlStateNormal];
    }else{
        for (NSDictionary *guest in self.guestsArray) {
            [self.selectedGuests addObject:guest];
            [self.delegate guestsCell:self didAddGuest:guest];
        }
        [self.collectionView reloadData];
        selectedAll = YES;
        [self.selectAllButton setTitle:@"Deselect All" forState:UIControlStateNormal];
    }
}


#pragma mark - helper methods

- (UIViewController*)viewController
{
    for (UIView* next = [self superview]; next; next = next.superview)
    {
        UIResponder* nextResponder = [next nextResponder];
        
        if ([nextResponder isKindOfClass:[UIViewController class]])
        {
            return (UIViewController*)nextResponder;
        }
    }
    
    return nil;
}

+(CGFloat)sectionHeight{
    return 133;
}

-(NSString *)sectionTitle{
    if (self.customHeader) {
        return self.customHeader;
    }
    return @"GUESTS";
}

-(IBAction)dismiss:(id)sender{
    [nav dismissViewControllerAnimated:YES completion:nil];
}

@end
