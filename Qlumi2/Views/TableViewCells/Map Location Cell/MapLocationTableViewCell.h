//
//  MapLocationTableViewCell.h
//  Qlumi
//
//  Created by William Smillie on 10/5/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIView+WebCache.h>

@class MapLocationTableViewCell;

@protocol MapLocationTableViewCellDelegate <NSObject>
- (void)MapLocationCell:(MapLocationTableViewCell *)cell cellDidDeleteAtIndexPath:(NSIndexPath *)indexPath;
@end


@interface MapLocationTableViewCell : UITableViewCell <UIScrollViewDelegate>
@property (nonatomic, weak) id <MapLocationTableViewCellDelegate> delegate;

-(void)setSwipeToDeleteEnabled:(BOOL)enabled;
@property (strong, nonatomic) IBOutlet UIScrollView *swipeToDeleteScrollView;
@property (strong, nonatomic) UIButton *deleteButton;

@property (strong, nonatomic) IBOutlet UIView *dot;
@property (strong, nonatomic) IBOutlet UIView *dotDeselectView;

@property (strong, nonatomic) IBOutlet UIView *topLine;
@property (strong, nonatomic) IBOutlet UIView *bottomLine;
@property (strong, nonatomic) IBOutlet UIView *containerView;

@property (strong, nonatomic) IBOutlet UIImageView *thumbnailImageView;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *addressLabel;
@property (strong, nonatomic) IBOutlet UILabel *detailsLabel;
@end
