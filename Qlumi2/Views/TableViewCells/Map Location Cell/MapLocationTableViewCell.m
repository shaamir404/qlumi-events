//
//  MapLocationTableViewCell.m
//  Qlumi
//
//  Created by William Smillie on 10/5/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import "MapLocationTableViewCell.h"
#import "MapTableViewController.h"

@interface MapLocationTableViewCell (){
    BOOL canSwipeToDelete;
    BOOL isAnimated;
}
@end

@implementation MapLocationTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.swipeToDeleteScrollView.contentSize = CGSizeMake(self.swipeToDeleteScrollView.frame.size.width+self.swipeToDeleteScrollView.frame.size.height+8, self.swipeToDeleteScrollView.frame.size.height);
    self.swipeToDeleteScrollView.clipsToBounds = NO;
    self.swipeToDeleteScrollView.userInteractionEnabled = YES;
    self.swipeToDeleteScrollView.alwaysBounceHorizontal = YES;
    self.swipeToDeleteScrollView.delegate = self;
//    self.swipeToDeleteScrollView.delaysContentTouches = YES;
//    self.swipeToDeleteScrollView.canCancelContentTouches = NO;
    
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedCell:)];
    [recognizer setNumberOfTapsRequired:1];
    [self.swipeToDeleteScrollView addGestureRecognizer:recognizer];

    self.deleteButton = [[UIButton alloc] initWithFrame:CGRectMake(self.swipeToDeleteScrollView.frame.origin.x + self.swipeToDeleteScrollView.frame.size.width+8, 8, self.swipeToDeleteScrollView.frame.size.height-16, self.swipeToDeleteScrollView.frame.size.height-16)];
    [self.deleteButton setTitle:@"Delete" forState:UIControlStateNormal];
    [self.deleteButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [self.swipeToDeleteScrollView addSubview:self.deleteButton];

    
    self.dot.layer.cornerRadius = self.dot.frame.size.width/2;
    self.contentView.backgroundColor = [UIColor clearColor];
    
    self.dotDeselectView.layer.cornerRadius = self.dotDeselectView.frame.size.width/2;
    
//    self.containerView.layer.shadowOpacity = 0.5f;
//    self.containerView.layer.shadowRadius = 4.0f;
//    self.containerView.layer.shadowOffset = CGSizeMake(0, 2);
//    self.containerView.layer.shadowColor = [UIColor blackColor].CGColor;
    
    [self.thumbnailImageView sd_setShowActivityIndicatorView:YES];
    [self.thumbnailImageView sd_setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [self.thumbnailImageView setImage:[UIImage imageNamed:@"photo-placeholder"]];
}

-(void)tappedCell:(UITapGestureRecognizer *) sender
{
//    MapTableViewController *mapTVC = (MapTableViewController *)[[[self nextResponder] nextResponder] nextResponder];
//    CGPoint touchLocation = [sender locationOfTouch:0 inView:self.superview];
//    NSIndexPath *indexPath = [(UITableView *)self.superview indexPathForRowAtPoint:touchLocation];
//    [mapTVC tableView:(UITableView *)self.superview didSelectRowAtIndexPath:indexPath];
}


-(void)layoutSubviews{
    self.deleteButton.frame = CGRectMake(self.frame.size.width+8, 8, self.swipeToDeleteScrollView.frame.size.height-16, self.swipeToDeleteScrollView.frame.size.height-16);
}

-(void)setSwipeToDeleteEnabled:(BOOL)enabled{
    canSwipeToDelete = enabled;
    [self.swipeToDeleteScrollView setAlwaysBounceHorizontal:enabled];
}

#pragma mark — UIScrollViewDelegate

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView.contentOffset.x > self.deleteButton.frame.size.width+8) {
        if (!isAnimated) {
            isAnimated = YES;
            [UIView animateWithDuration:0.25 animations:^{
                scrollView.backgroundColor = [UIColor redColor];
                [self.deleteButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            }];
        }
    }else{
        if (isAnimated) {
            isAnimated = NO;
            [UIView animateWithDuration:0.25 animations:^{
                scrollView.backgroundColor = [UIColor clearColor];
                [self.deleteButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
            }];
        }
    }
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if (scrollView.contentOffset.x > self.deleteButton.frame.size.width+8) {
        [self deleteCell:self];
    }
}

-(IBAction)deleteCell:(id)sender{
    NSIndexPath *indexPath = [(UITableView *)self.superview indexPathForCell:self];
    [self.delegate MapLocationCell:self cellDidDeleteAtIndexPath:indexPath];
}



@end
