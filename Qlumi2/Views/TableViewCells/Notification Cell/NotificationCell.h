//
//  NotificationCell.h
//  Qlumi
//
//  Created by Will on 1/31/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIView+WebCache.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface NotificationCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *notificationImageView;
@property (strong, nonatomic) IBOutlet UILabel *notificationLabel;



@property (strong, nonatomic) IBOutlet UIView *topLine;
@property (strong, nonatomic) IBOutlet UIView *bottomLine;
@property (strong, nonatomic) IBOutlet UIView *bubble;
@property (strong, nonatomic) IBOutlet UIView *blankView;
@end
