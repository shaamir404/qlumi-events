//
//  NotificationCell.m
//  Qlumi
//
//  Created by Will on 1/31/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import "NotificationCell.h"

@implementation NotificationCell

- (void)awakeFromNib {
    [super awakeFromNib];

    self.bubble.layer.cornerRadius = self.bubble.frame.size.height/2;
    self.blankView.layer.cornerRadius = self.blankView.frame.size.height/2;
    
    self.notificationImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.notificationImageView.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
