//
//  EventDeleteCell.m
//  Qlumi
//
//  Created by Will on 2/20/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import "EventDeleteCell.h"

@implementation EventDeleteCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.line.alpha = 0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(IBAction)deleteEvent:(id)sender{
    [self.delegate eventDeleteCellShouldDelete:self];
}


+(CGFloat)sectionHeight{
    return 64;
}

-(NSString *)sectionTitle{
    return @"";
}

@end
