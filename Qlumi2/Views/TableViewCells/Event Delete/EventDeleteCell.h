//
//  EventDeleteCell.h
//  Qlumi
//
//  Created by Will on 2/20/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import "EventTableViewBaseCell.h"

@class EventDeleteCell;
@protocol deleteEventCellDelegate
-(void)eventDeleteCellShouldDelete:(EventDeleteCell *)cell;
@end

@interface EventDeleteCell : EventTableViewBaseCell
@property (nonatomic, weak) id <deleteEventCellDelegate> delegate;

@property (nonatomic, strong) IBOutlet UIButton *deleteButton;

@end
