//
//  ProfilePictureFormCell.m
//  Qlumi
//
//  Created by Will on 2/13/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import "ProfilePictureFormCell.h"

@implementation ProfilePictureFormCell

-(void)configure{
    self.profilePictureImageView = [[UIImageView alloc] init];
    self.profilePictureImageView.image = [UIImage imageNamed:@"photo-placeholder"];
    self.profilePictureImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.profilePictureImageView.clipsToBounds = YES;
    self.profilePictureImageView.layer.cornerRadius = 50;
    [self addSubview:self.profilePictureImageView];
}

-(void)layoutSubviews{
    [super layoutSubviews];
    
    self.profilePictureImageView.frame  = CGRectMake(0, 0, 100, 100);
    CGPoint center = CGPointMake(self.center.x, 50+8);
    self.profilePictureImageView.center = center;
}

-(void)update{
    self.profilePictureImageView.image = self.rowDescriptor.value?(UIImage *)self.rowDescriptor.value:[UIImage imageNamed:@"photo-placeholder"];
}

+(CGFloat)formDescriptorCellHeightForRowDescriptor:(XLFormRowDescriptor *)rowDescriptor{
    return 116;
}


-(void)formDescriptorCellDidSelectedWithFormController:(XLFormViewController *)controller{
    [controller deselectFormRow:self.rowDescriptor];
    
    
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.allowsEditing = YES;
    imagePicker.delegate = self;
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Upload Photo" message:nil preferredStyle:UIAlertControllerStyleAlert];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Photo Library"style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [controller presentViewController:imagePicker animated:YES completion:nil];
    }]];
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]){
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera"style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [controller presentViewController:imagePicker animated:YES completion:nil];
        }]];
    }
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel"style:UIAlertActionStyleCancel handler:nil]];
    [controller presentViewController:actionSheet animated:YES completion:nil];
}


#pragma mark - imagePicker

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    
    self.rowDescriptor.value = info[UIImagePickerControllerEditedImage];
    self.profilePictureImageView.image = (UIImage *)self.rowDescriptor.value;
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}


@end
