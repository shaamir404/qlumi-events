//
//  ProfilePictureFormCell.h
//  Qlumi
//
//  Created by Will on 2/13/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import <XLForm/XLForm.h>
#import <UIKit/UIKit.h>

@interface ProfilePictureFormCell : XLFormBaseCell <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property BOOL editable;
@property (nonatomic, strong) UIImageView *profilePictureImageView;;

@end
