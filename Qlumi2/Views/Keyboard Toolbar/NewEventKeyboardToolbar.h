//
//  NewEventKeyboardToolbar.h
//  Qlumi2
//
//  Created by William Smillie on 12/13/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import <UIKit/UIKit.h>

//3rd Party
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIView+WebCache.h>

//My Classes
#import "ImagePickerCollectionViewCell.h"
#import "TypeTagTextField.h"


@class NewEventKeyboardToolbar;
@protocol NewEventKeyboardToolbarDelegate <NSObject>
- (void)keyboardToolbar:(NewEventKeyboardToolbar *)toolbar didRequestNextResponder:(id)sender;
- (void)keyboardToolbar:(NewEventKeyboardToolbar *)toolbar didRequestLastResponder:(id)sender;
- (void)keyboardToolbar:(NewEventKeyboardToolbar *)toolbar didSelectImage:(UIImage *)image;
- (void)keyboardToolbar:(NewEventKeyboardToolbar *)toolbar textDidChange:(NSString *)text;
@end


@interface NewEventKeyboardToolbar : UIView <UICollectionViewDelegate, UICollectionViewDataSource, TypeTagTextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, weak) id <NewEventKeyboardToolbarDelegate> delegate;

@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) IBOutlet UIToolbar *toolbar;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *lastButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *nextButton;
@property (strong, nonatomic) IBOutlet TypeTagTextField *textField;

-(IBAction)nextButtonPressed:(id)sender;
-(IBAction)lastButtonPressed:(id)sender;

-(void)showImageTags:(BOOL)show;

@end
