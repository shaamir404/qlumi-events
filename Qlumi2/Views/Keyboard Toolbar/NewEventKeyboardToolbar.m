//
//  NewEventKeyboardToolbar.m
//  Qlumi2
//
//  Created by William Smillie on 12/13/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import "NewEventKeyboardToolbar.h"
#import "AppDelegate.h"

@interface NewEventKeyboardToolbar (){
    NSMutableArray *imageURLsArray;
    UIWindow *alertWindow;
    UIBarButtonItem *textFieldItem;
    UIImagePickerController *imagePicker;

}
@end

@implementation NewEventKeyboardToolbar

-(void)awakeFromNib{
    [super awakeFromNib];
    
    imagePicker = [[UIImagePickerController alloc] init];

    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.collectionViewLayout = layout;
    [self.collectionView registerNib:[UINib nibWithNibName:@"ImagePickerCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"imageCell"];
    
//    self.textField = [[TypeTagTextField alloc] init];
    self.textField.typeTagDelegate = self;
    
//    textFieldItem = [[UIBarButtonItem alloc] initWithCustomView:];
}


-(void)layoutSubviews{
    [super layoutSubviews];
    
    self.toolbar.layer.masksToBounds = NO;
    self.toolbar.layer.shadowOpacity = 0.5f;
    self.toolbar.layer.shadowRadius = 5;
    self.toolbar.layer.shadowOffset = CGSizeZero;
    self.toolbar.layer.shadowColor = [UIColor blackColor].CGColor;
    self.toolbar.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.bounds].CGPath;

    self.layer.masksToBounds = NO;
    self.layer.shadowOpacity = 0.0f;
    self.layer.shadowRadius = 5;
    self.layer.shadowOffset = CGSizeZero;
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.bounds].CGPath;

    CGRect textFieldFrame = self.textField.frame;
    textFieldFrame.size.width = self.frame.size.width-200;
    self.textField.frame = textFieldFrame;
}


#pragma mark — TypeTagsTextField
-(void)TypeTagTextField:(TypeTagTextField *)textField didSelectTag:(NSString *)tag
{
    [self getImagesForKeyword:tag withCallback:^(NSArray * imagesArray) {
        imageURLsArray = [[NSMutableArray alloc] init];
        for (NSDictionary *imageObj in imagesArray) {
            [imageURLsArray addObject:imageObj[@"urls"][@"regular"]];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.collectionView reloadData];
            [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
        });
    }];
}


-(void)TypeTagTextField:(TypeTagTextField *)textField textDidChange:(NSString *)text{
    [self.delegate keyboardToolbar:self textDidChange:text];
}


#pragma mark - UICollectionViewDelegate // DataSource
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(174, 108);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(4, 4, 4, 4);
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (imageURLsArray.count == 0) {
        [self setImagePickerHidden:YES];
    }else{
        [self setImagePickerHidden:NO];
    }
    return imageURLsArray.count+1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    ImagePickerCollectionViewCell *cell;
    
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"imageCell" forIndexPath:indexPath];
    
//    [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:imageURLsArray[indexPath.row] options:SDWebImageDownloaderLowPriority progress:nil completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, BOOL finished) {
//        [imageURLsArray addObject:image];
//        [self.collectionView reloadData];
//    }];

    
    if (indexPath.row == 0) {
        cell.uploadLabel.hidden = NO;
        cell.imageView.image = nil;
        
        
        cell.contentView.layer.borderWidth = 1;
        cell.contentView.layer.borderColor = [AppDelegate QOrange].CGColor;
        cell.contentView.layer.cornerRadius = 10;
    }else{
        cell.uploadLabel.hidden = YES;
        [cell.imageView sd_setShowActivityIndicatorView:YES];
        [cell.imageView sd_setIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [cell.imageView sd_setImageWithURL:imageURLsArray[indexPath.row-1]];
        
        
        cell.contentView.layer.borderWidth = 0;
        cell.contentView.layer.borderColor = [UIColor clearColor].CGColor;
    }
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    ImagePickerCollectionViewCell *cell = [self collectionView:self.collectionView cellForItemAtIndexPath:indexPath];
    if (indexPath.row != 0) {
        [self.delegate keyboardToolbar:self didSelectImage:cell.imageView.image];
    }else{
        [self showCameraPicker];
    }
}

-(void)showCameraPicker{
    
    imagePicker.delegate = self;
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Upload Your Own Image" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Photo Library"style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [[self topMostController] presentViewController:imagePicker animated:YES completion:nil];
    }]];
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]){
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera"style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [[self topMostController] presentViewController:imagePicker animated:YES completion:nil];
        }]];
    }
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel"style:UIAlertActionStyleCancel handler:nil]];
    [[self topMostController] presentViewController:actionSheet animated:YES completion:nil];
}

- (UIViewController*) topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}


#pragma mark - UIImagePickerDelegate


-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    [picker dismissViewControllerAnimated:YES completion:nil];

    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];

    [self.delegate keyboardToolbar:self didSelectImage:image];
}


#pragma mark - IBActions

-(IBAction)nextButtonPressed:(id)sender{
    [self.delegate keyboardToolbar:self didRequestNextResponder:sender];
}
-(IBAction)lastButtonPressed:(id)sender{
    [self.delegate keyboardToolbar:self didRequestLastResponder:sender];
}

-(void)showImageTags:(BOOL)show{
    [self.textField showTags:show];
    if (!show) {
        [self setImagePickerHidden:YES];
    }
}

-(void)setImagePickerHidden:(BOOL)hidden{
    [UIView animateWithDuration:0.3 animations:^{
        if (hidden) {
            self.collectionView.alpha = 0;
            self.layer.shadowOpacity = 0;
        }else{
            self.collectionView.alpha = 1;
            self.layer.shadowOpacity = 0.5f;
        }
    }];
}


#pragma mark - UpSplash Image API

- (void)getImagesForKeyword:(NSString *)keyword withCallback:(void (^)(NSArray * imagesArray))completionHandler {
    
    keyword = [keyword stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    NSString *urlString = [NSString stringWithFormat:@"https://api.unsplash.com/search/photos?client_id=5d4592e505f01a9c0a941a1eebb68b5d1e838ee5c3fae3e4b98d984e1334be2e&page=1&query=%@", keyword];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (!error) {
            NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            if (completionHandler) {
                completionHandler(dictionary[@"results"]);
            }
        }
    }];
    [dataTask resume];
}

-(id)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    id hitView = [super hitTest:point withEvent:event];
    if (hitView == self) return nil;
    else return hitView;
}


@end
