//
//  contactsGroupCollectionView.m
//  Qlumi
//
//  Created by Will on 2/8/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import "contactsGroupCollectionView.h"
#import "Qlumi-Swift.h"

@interface contactsGroupCollectionView () <EPPickerDelegate>{
    NSMutableDictionary *phoneNumbers;
}
@end


@implementation contactsGroupCollectionView

-(instancetype)initWithFrame:(CGRect)frame{
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    if (self = [super initWithFrame:frame collectionViewLayout:layout]) {
        [self setDataSource:self];
        [self setDelegate:self];
        
        self.backgroundColor = [UIColor clearColor];
        
        [self registerClass:[contactGroupCollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
        [self registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"newCell"];
        
        [self refresh];
        
    }
    return self;
}

-(void)refresh{
    PFQuery *myGroupsQ = [PFQuery queryWithClassName:@"ContactGroup"];
    [myGroupsQ whereKey:@"owner" equalTo:[PFUser currentUser]];
    [myGroupsQ findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        self.contactGroupsArray = [[NSMutableArray alloc] init];
        
        for (PFObject *o in objects){
            NSMutableDictionary *group = [[NSMutableDictionary alloc] init];
            group[@"name"] = o[@"name"];
            group[@"contacts"] = o[@"contacts"];
            
            NSMutableArray *phoneNumbers = [[NSMutableArray alloc] init];
            for (NSDictionary *user in group[@"contacts"]) {
                [phoneNumbers addObject:user[@"phoneNumber"]];
            }
            
            
            PFQuery *usersQuery = [PFUser query];
            [usersQuery whereKey:@"phoneNumber" containedIn:phoneNumbers];
            [usersQuery findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
                if (!error) {
                    for (PFUser *profile in objects) {
                        PFFile *picFile = profile[@"profilePicture"];
                        if (picFile) {
                            [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:picFile.url] options:SDWebImageDownloaderLowPriority progress:nil completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, BOOL finished) {
                                for (NSMutableDictionary *user in group[@"contacts"]) {
                                    if ([user[@"phoneNumber"] isEqualToString:profile[@"phoneNumber"]]) {
                                        user[@"image"] = image;
                                        [self reloadData];
                                    }
                                }
                            }];
                        }
                    }
                }
            }];

            [self.contactGroupsArray addObject:group];
        }
    }];
}

-(void)setContactGroupsArray:(NSMutableArray *)contactGroupsArray{
    _contactGroupsArray = contactGroupsArray;
    [self reloadData];
}

#pragma mark - UICollectionViewDelegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.contactGroupsArray.count+1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == self.contactGroupsArray.count) {
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"newCell" forIndexPath:indexPath];
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 6, 64, 64)];
        imgView.image = [UIImage imageNamed:@"add"];
        imgView.contentMode = UIViewContentModeScaleAspectFit;
        imgView.backgroundColor = [UIColor clearColor];
        imgView.layer.cornerRadius = imgView.frame.size.height/2;
        [cell addSubview:imgView];
        
        return cell;
    }else{
        contactGroupCollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
        
        [cell.groupPicView reset];
        for (NSDictionary *userDict in self.contactGroupsArray[indexPath.row][@"contacts"]) {
            [cell.groupPicView addImage:userDict[@"image"] withInitials:userDict[@"name"]];
        }
        [cell.groupPicView updateLayout];
        
        cell.nameLabel.text = self.contactGroupsArray[indexPath.row][@"name"];
        
        return cell;
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == self.contactGroupsArray.count) {
        [self.pickerDelegate contactGroupPickerCreateNewGroup:self];
    }else{
        [self.pickerDelegate contactGroupPicker:self didSelectGroups:self.contactGroupsArray[indexPath.row][@"contacts"]];
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(64, 76);
}

@end
