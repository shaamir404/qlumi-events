//
//  contactsGroupCollectionView.h
//  Qlumi
//
//  Created by Will on 2/8/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "contactGroupCollectionViewCell.h"
#import <Parse/Parse.h>
#import <SDWebImage/SDWebImageDownloader.h>

@class contactsGroupCollectionView;
@protocol contactGroupCVDelegate <NSObject>
-(void)contactGroupPicker:(contactsGroupCollectionView *)contactGroupPicker didSelectGroups:(NSArray *)groupsArray;
-(void)contactGroupPickerCreateNewGroup:(contactsGroupCollectionView *)contactGroupPicker;
@end


@interface contactsGroupCollectionView : UICollectionView <UICollectionViewDelegate, UICollectionViewDataSource>
-(void)refresh;
@property (nonatomic, weak) id <contactGroupCVDelegate>pickerDelegate;
@property (nonatomic, strong) NSMutableArray *contactGroupsArray;
@end
