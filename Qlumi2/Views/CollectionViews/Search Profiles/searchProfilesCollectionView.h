//
//  searchProfilesCollectionView.h
//  Qlumi
//
//  Created by William Smillie on 11/29/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "searchProfile.h"

@class searchProfilesCollectionView;             //define class, so protocol can see MyClass
@protocol searchProfilesCollectionViewSelectionDelegate <NSObject>   //define delegate protocol
- (void)searchProfilesView:(searchProfilesCollectionView *)profilesCV didSelectProfile:(searchProfile *)searchProfile;
@end


@interface searchProfilesCollectionView : UICollectionView<UICollectionViewDelegate, UICollectionViewDataSource>{
    NSArray *searchProfiles;
}

@property (nonatomic, strong) NSArray *searchProfiles;
@property (nonatomic, weak) id <searchProfilesCollectionViewSelectionDelegate>selectionDelegate; //define MyClassDelegate as delegate


@end
