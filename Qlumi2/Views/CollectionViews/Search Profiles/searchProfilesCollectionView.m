//
//  searchProfilesCollectionView.m
//  Qlumi
//
//  Created by William Smillie on 11/29/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import "searchProfilesCollectionView.h"
#import "FilterProfileCollectionViewCell.h"

@implementation searchProfilesCollectionView
@synthesize selectionDelegate, searchProfiles;

-(void)drawRect:(CGRect)rect{
    [super drawRect:rect];
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [layout setSectionInset:UIEdgeInsetsMake(2, 8, 8, 8)];
    self.collectionViewLayout = layout;
    
    searchProfile *cheapDinner = [[searchProfile alloc] initWithName:@"Cheap Dinners"];
    cheapDinner.filters[@"keyword"] = @"Dinner";
    cheapDinner.filters[@"minprice"] = @"0";
    cheapDinner.filters[@"maxprice"] = @"1";

    searchProfile *walkingDistance = [[searchProfile alloc] initWithName:@"Walking Distance Breakfast"];
    walkingDistance.filters[@"keyword"] = @"Breakfast";
    walkingDistance.filters[@"radius"] = @"1000";

    searchProfile *clubs = [[searchProfile alloc] initWithName:@"Clubs"];
    clubs.filters[@"keyword"] = @"club";

    searchProfile *parks = [[searchProfile alloc] initWithName:@"Parks"];
    parks.filters[@"keyword"] = @"Park";

    
    searchProfile *coffee = [[searchProfile alloc] initWithName:@"Coffee Shops"];
    coffee.filters[@"keyword"] = @"Coffee Shop";

    searchProfile *newProfile = [[searchProfile alloc] initWithName:@"Add New Profile..."];
    
    searchProfiles = [[NSArray alloc] initWithObjects:cheapDinner, walkingDistance, clubs, parks, coffee, newProfile, nil];
    
    self.delegate = self;
    self.dataSource = self;
    
    [self registerNib:[UINib nibWithNibName:@"FilterProfileCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"Cell"];
    
    self.backgroundColor = [UIColor clearColor];
}



#pragma mark - UICollectionViewDelegate // DataSource
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSString *string = [(searchProfile *)[searchProfiles objectAtIndex:indexPath.row] name];
    CGSize size = [self getLabelSize:string];
    return CGSizeMake(size.width+8, 30);
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return searchProfiles.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    FilterProfileCollectionViewCell *cell;
    
    cell = [self dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    
    cell.titleLabel.text = [(searchProfile *)[searchProfiles objectAtIndex:indexPath.row] name];
    cell.titleLabel.frame = CGRectMake(4, 0, cell.frame.size.width-8, cell.frame.size.height);
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [self selectItemAtIndexPath:indexPath animated:YES scrollPosition:UICollectionViewScrollPositionNone];
    [self.selectionDelegate searchProfilesView:self didSelectProfile:[searchProfiles objectAtIndex:indexPath.row]];
};

- (CGSize)getLabelSize:(NSString *)string {
    CGRect rect = [string boundingRectWithSize:(CGSize){250, CGFLOAT_MAX} options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName: [UIFont systemFontOfSize:12]} context:nil];
    return rect.size;
}

@end
