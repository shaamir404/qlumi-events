//
//  ImagePickerCollectionViewCell.h
//  Qlumi2
//
//  Created by William Smillie on 12/13/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImagePickerCollectionViewCell : UICollectionViewCell

@property IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UILabel *uploadLabel;

@end
