//
//  ImagePickerCollectionViewCell.m
//  Qlumi2
//
//  Created by William Smillie on 12/13/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import "ImagePickerCollectionViewCell.h"

@interface ImagePickerCollectionViewCell (){
    UIView *checkView;
}
@end

@implementation ImagePickerCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];

    self.contentView.layer.masksToBounds = YES;
    self.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    self.layer.shadowOffset = CGSizeMake(0, 2);
    self.layer.shadowRadius = 2.0f;
    self.layer.shadowOpacity = 1.0f;
    self.layer.masksToBounds = NO;
    self.layer.cornerRadius = 10;
    
    checkView = [[UIView alloc] initWithFrame:self.imageView.bounds];
    checkView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    checkView.alpha = 0;
    [self.imageView addSubview:checkView];
    
    UIImageView *checkImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, (checkView.frame.size.width/2)/2, (checkView.frame.size.width/2)/2)];
    checkImgView.center = checkView.center;
    checkImgView.image = [UIImage imageNamed:@"check"];
    checkImgView.contentMode = UIViewContentModeScaleAspectFit;
    [checkView addSubview:checkImgView];
}

-(void)setSelected:(BOOL)selected{
    [super setSelected:selected];
    if (self.uploadLabel.hidden) {
        if (selected) {
            [UIView animateWithDuration:0.25f animations:^{
                checkView.alpha = 1.0;
            }];
        }else{
            [UIView animateWithDuration:0.25f animations:^{
                checkView.alpha = 0.0;
            }];
        }
    }
}

@end
