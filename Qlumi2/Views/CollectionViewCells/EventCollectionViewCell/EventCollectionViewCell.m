//
//  EventCollectionViewCell.m
//  Qlumi
//
//  Created by William Smillie on 9/25/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import "EventCollectionViewCell.h"

@implementation EventCollectionViewCell
@synthesize bgImageView, titleLabel;

-(void)awakeFromNib{
    [super awakeFromNib];
    
    [bgImageView sd_setShowActivityIndicatorView:YES];
    [bgImageView sd_setIndicatorStyle:UIActivityIndicatorViewStyleGray];

//    self.backgroundColor = [UIColor lightGrayColor];
    
    bgImageView.frame = self.bounds;
    bgImageView.clipsToBounds = YES;
    
    
    titleLabel.frame = CGRectMake(16, 32, self.frame.size.width-32, self.frame.size.height-64);
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.numberOfLines = 0;
    
    titleLabel.layer.shadowColor = [UIColor blackColor].CGColor;
    titleLabel.layer.shadowOffset = CGSizeMake(0, 0);
    titleLabel.layer.shadowRadius = 10.0f;
    titleLabel.layer.shadowOpacity = 1.0f;
    
//    [self addSubview:titleLabel];
}

-(void)layoutSubviews{
    [super layoutSubviews];
    
    bgImageView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    bgImageView.contentMode = UIViewContentModeScaleAspectFill;

    self.contentView.layer.masksToBounds = YES;
    self.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    self.layer.shadowOffset = CGSizeMake(0, 2);
    self.layer.shadowRadius = 2.0f;
    self.layer.shadowOpacity = 1.0f;
    self.layer.masksToBounds = NO;
}


@end
