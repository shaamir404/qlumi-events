//
//  EventCollectionViewCell.h
//  Qlumi
//
//  Created by William Smillie on 9/25/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

// Apple
#import <UIKit/UIKit.h>

// 3rd Party
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIView+WebCache.h>

// My Files
#import "Event.h"

@interface EventCollectionViewCell : UICollectionViewCell

@property IBOutlet UIImageView *bgImageView;
@property IBOutlet UILabel *titleLabel;

@end
