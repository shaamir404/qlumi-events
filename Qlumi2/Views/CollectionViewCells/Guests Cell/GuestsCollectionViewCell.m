//
//  GuestsCollectionViewCell.m
//  Qlumi
//
//  Created by William Smillie on 1/2/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import "GuestsCollectionViewCell.h"
#import "Qlumi-Swift.h"

@implementation GuestsCollectionViewCell

-(void)awakeFromNib{
    [super awakeFromNib];
    
    self.colorsArray = [EPGlobalConstants colorsArray];

    
    self.profilePicture.contentMode = UIViewContentModeScaleAspectFill;
    self.rsvpImageView.contentMode = UIViewContentModeScaleAspectFill;

    self.containerView.layer.cornerRadius = self.containerView.frame.size.height/2;
    self.rsvpImageView.layer.cornerRadius = self.rsvpImageView.frame.size.height/2;
    
    self.containerView.clipsToBounds = YES;
    self.rsvpImageView.clipsToBounds = YES;
    
    [self.rsvpImageView setTintColor:[UIColor whiteColor]];
    
    self.selectionOverlayImageView.contentMode = UIViewContentModeCenter;
    self.selectionOverlayImageView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    self.selectionOverlayImageView.layer.cornerRadius = self.rsvpImageView.frame.size.height/2;
    self.selectionOverlayImageView.alpha = 0;
    
//    CGFloat borderWidth = 2.0f;
//    self.rsvpImageView.frame = CGRectInset(self.frame, -borderWidth, -borderWidth);
//    self.rsvpImageView.layer.borderColor = [UIColor whiteColor].CGColor;
//    self.rsvpImageView.layer.borderWidth = borderWidth;
//    self.rsvpImageView.backgroundColor = [UIColor whiteColor];
}




-(UIImage *)imageForRSVP:(NSString *)rsvp{
    UIImage *image;
    
    if ([rsvp isEqualToString:@"yes"]) {
        image = [UIImage imageNamed:@"guest-yes"];
    }else if ([rsvp isEqualToString:@"no"]){
        image = [UIImage imageNamed:@"guest-no"];
    }else if ([rsvp isEqualToString:@"maybe"]){
        image = [UIImage imageNamed:@"guest-maybe"];
    }else{
        image = nil;
    }
    
    return [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
}

-(UIColor *)colorForRSVP:(NSString *)rsvp{
    if ([rsvp isEqualToString:@"yes"]) {
        return [UIColor colorWithRed:0.33 green:0.71 blue:0.64 alpha:1.0];
    }else if ([rsvp isEqualToString:@"no"]) {
        return [UIColor colorWithRed:0.88 green:0.44 blue:0.34 alpha:1.0];
    }else if ([rsvp isEqualToString:@"maybe"]) {
        return [UIColor colorWithRed:0.97 green:0.66 blue:0.33 alpha:1.0];
    }else{
        return [UIColor clearColor];
    }
}

@end
