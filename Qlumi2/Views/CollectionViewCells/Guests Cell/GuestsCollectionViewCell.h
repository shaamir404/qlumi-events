//
//  GuestsCollectionViewCell.h
//  Qlumi
//
//  Created by William Smillie on 1/2/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface GuestsCollectionViewCell : UICollectionViewCell



@property (strong, nonatomic) IBOutlet UIView *containerView;
@property (strong, nonatomic) IBOutlet UIImageView *profilePicture;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UIImageView *rsvpImageView;
@property (strong, nonatomic) IBOutlet UILabel *initialsLabel;

@property (strong, nonatomic) IBOutlet UIImageView *selectionOverlayImageView;

@property (strong, nonatomic) NSArray *colorsArray;

-(UIImage *)imageForRSVP:(NSString *)rsvp;
-(UIColor *)colorForRSVP:(NSString *)rsvp;

@end
