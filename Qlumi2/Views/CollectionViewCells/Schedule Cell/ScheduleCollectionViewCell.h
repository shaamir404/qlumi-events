//
//  ScheduleCollectionViewCell.h
//  Qlumi
//
//  Created by William Smillie on 10/30/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScheduleCollectionViewCell : UICollectionViewCell

@property IBOutlet UILabel *nameLabel;
@property IBOutlet UILabel *startTimeLabel;
@property IBOutlet UILabel *endTimeLabel;

@end
