//
//  contactGroupCollectionViewCell.h
//  Qlumi
//
//  Created by Will on 2/8/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GLGroupChatPicView/GLGroupChatPicView.h>

@interface contactGroupCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong)GLGroupChatPicView *groupPicView;
@property (nonatomic, strong)UILabel *nameLabel;

@end
