//
//  contactGroupCollectionViewCell.m
//  Qlumi
//
//  Created by Will on 2/8/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import "contactGroupCollectionViewCell.h"

@implementation contactGroupCollectionViewCell

-(id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.groupPicView = [[GLGroupChatPicView alloc] initWithFrame:self.bounds];
        [self addSubview:self.groupPicView];
        
        self.nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.groupPicView.frame.origin.y+self.groupPicView.frame.size.height, self.frame.size.width, 12)];
        [self.nameLabel setTextAlignment:NSTextAlignmentCenter];
        self.nameLabel.font = [UIFont systemFontOfSize:10];
        [self.nameLabel setAdjustsFontSizeToFitWidth:YES];
        [self addSubview:self.nameLabel];
    }
    return self;
}

@end
