
//
//  FilterTableViewCell.h
//  Qlumi
//
//  Created by William Smillie on 10/12/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterProfileCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) IBOutlet UILabel *titleLabel;

- (void)setSelected:(BOOL)selected;

@end
