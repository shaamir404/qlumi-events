//
//  FilterTableViewCell.m
//  Qlumi
//
//  Created by William Smillie on 10/12/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import "FilterProfileCollectionViewCell.h"

#import "AppDelegate.h"

@implementation FilterProfileCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
//    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(4, 0, self.frame.size.width-8, self.frame.size.height)];
    self.titleLabel.font = [UIFont systemFontOfSize:12];
//    [self addSubview:self.titleLabel];
    [self setSelected:NO];
    
    
    self.layer.masksToBounds = NO;
    self.layer.shadowOpacity = 0.5f;
    self.layer.shadowRadius = 1.0f;
    self.layer.shadowOffset = CGSizeMake(0, 1);
    self.layer.shadowColor = [UIColor blackColor].CGColor;
}

-(void)layoutSubviews{
}

- (void)setSelected:(BOOL)selected {

    if (selected){
        self.backgroundColor = [AppDelegate QOrange];
        self.titleLabel.textColor = [UIColor whiteColor];
    }else{
        self.backgroundColor = [UIColor whiteColor];
        self.titleLabel.textColor = [UIColor blackColor];
    }
    
    [self setNeedsDisplay];
}

@end
