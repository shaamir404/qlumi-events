//
//  QCalloutView.h
//  Qlumi
//
//  Created by Will on 2/6/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AXRatingView/AXRatingView.h>


@interface QCalloutView : UIView

@property (strong, nonatomic) IBOutlet UIView *containerView;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *addressLabel;
@property (strong, nonatomic) IBOutlet UIToolbar *actionsBar;

@property (strong, nonatomic) IBOutlet AXRatingView *ratingView;

@property (strong, nonatomic) IBOutlet UIButton *actionButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *callButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *websiteButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *directionsButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *shareButton;
@property (strong, nonatomic) IBOutlet UIButton *revealOnMapButton;


@end
