//
//  QCalloutView.m
//  Qlumi
//
//  Created by Will on 2/6/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import "QCalloutView.h"

@implementation QCalloutView

-(id)init{
    if ((self = [[[NSBundle mainBundle] loadNibNamed:@"QCalloutView" owner:self options:nil] firstObject])){
        
        self.ratingView.baseColor = [UIColor clearColor];
        self.ratingView.highlightColor = [UIColor colorWithRed:0.96 green:0.74 blue:0.17 alpha:1.0];
        self.ratingView.stepInterval = 0.5;
        
        self.containerView.clipsToBounds = YES;
        self.containerView.layer.cornerRadius = 10;
        
        self.layer.masksToBounds = NO;
        self.layer.shadowOffset = CGSizeMake(0, 0);
        self.layer.shadowRadius = 15;
        self.layer.shadowOpacity = 1.0;//0.15
    }
    return self;
}


-(void)roundTopCornersForView:(UIView *)view{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:view.bounds byRoundingCorners:UIRectCornerTopLeft| UIRectCornerTopRight cornerRadii:CGSizeMake(10.0, 10.0)];
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = view.bounds;
    maskLayer.path = maskPath.CGPath;
    view.layer.mask = maskLayer;
}

@end
