//
//  AppDelegate.h
//  Qlumi2
//
//  Created by William Smillie on 12/11/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

//Apple
#import <UIKit/UIKit.h>

//3rd Party
#import <Parse/Parse.h>
#import <CoreLocation/CoreLocation.h>

//My Classes
#import "Event.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, CLLocationManagerDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) CLLocationManager *locationManager;
@property CLLocationCoordinate2D userCoordinates;

@property (strong, nonatomic) NSArray *NotificationsArray;

+(UIColor *)QOrange;
+(UIColor *)QBlue;
+(UIColor *)QWhite;
+(UIColor *)QLiteGray;

@end
