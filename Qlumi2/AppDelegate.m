//
//  AppDelegate.m
//  Qlumi2
//
//  Created by William Smillie on 12/11/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import "AppDelegate.h"

#import "Qlumi-Swift.h"
#import "IQKeyboardManager.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [Event registerSubclass];
    
    [Parse initializeWithConfiguration:[ParseClientConfiguration configurationWithBlock:^(id<ParseMutableClientConfiguration> configuration) {
        configuration.applicationId = @"Qlumi";
        configuration.server = @"https://qlumi-server.herokuapp.com/parse";
        configuration.clientKey = @"6A0B698B8BDA5BAA9D347F9978FA3BF4";
        configuration.localDatastoreEnabled = NO;
    }]];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"JosefinSans-Regular" size:20]}];
    [[UIBarButtonItem appearance] setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"JosefinSans-Regular" size:18]} forState:UIControlStateNormal];
    [[UISegmentedControl appearance] setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"JosefinSans-Regular" size:16]} forState:UIControlStateNormal];
//    [[UILabel appearanceWhenContainedIn:[UIAlertController class], nil] setFont:[UIFont fontWithName:@"JosefinSans-Regular" size:16]];
//    [[UILabel appearanceWhenContainedInInstancesOfClasses:[UIAlertController self]] setFont:[UIFont fontWithName:@"JosefinSans-Regular" size:16]];
//    [UILabel appearanceWhenContainedInInstancesOfClasses:@[[UIAlertController class]]].font = [UIFont fontWithName:@"JosefinSans-Regular" size:16];
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    [self.locationManager requestWhenInUseAuthorization];
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


#pragma mark — CLLocationManagerDelegate

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
        case kCLAuthorizationStatusRestricted:
        case kCLAuthorizationStatusDenied:
        {
            // do some error handling
        }
            break;
        default:{
            [self.locationManager startUpdatingLocation];
        }
            break;
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    self.userCoordinates = self.locationManager.location.coordinate;
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
}



#pragma mark - helper methods

+(UIColor *)QOrange{
    return [UIColor colorWithRed:0.93 green:0.38 blue:0.30 alpha:1.0];
}

+(UIColor *)QBlue{
    return [UIColor colorWithRed:0.36 green:0.67 blue:0.93 alpha:1.0];
}
+(UIColor *)QWhite{
    return [UIColor colorWithRed:255 green:255 blue:255 alpha:1.0];
}

+(UIColor *)QLiteGray{
    return [UIColor colorWithRed:211 green:211 blue:211 alpha:1.0];
}


@end
