//
//  MapTableViewController.h
//  Qlumi
//
//  Created by William Smillie on 10/5/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

#import <AFNetworking/AFNetworking.h>
#import <INTUAnimationEngine/INTUInterpolationFunctions.h>

#import "AppDelegate.h"
#import "MapLocationTableViewCell.h"
#import "Place.h"
#import "searchProfilesCollectionView.h"
#import "MapTableView.h"
#import "QMapAnnotationView.h"

//#import "PlaceCardViewController.h"

@class MapTableViewController;
@protocol MapTableViewControllerDelegate <NSObject>
- (void)MapTableViewController:(MapTableViewController *)mapTableView willDismissWithAddedPlaces:(NSArray *)annotations;
@end



@interface MapTableViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, MKMapViewDelegate, CLLocationManagerDelegate, searchProfilesCollectionViewSelectionDelegate, UISearchBarDelegate, ReorderTableViewDelegate, MapLocationTableViewCellDelegate, UITextViewDelegate>
@property (nonatomic, weak) id <MapTableViewControllerDelegate> delegate;


@property (strong, nonatomic) NSMutableArray *addedPlacesArray;
@property (strong, nonatomic) UISearchController *searchController;
@property (strong, nonatomic) IBOutlet searchProfilesCollectionView *searchProfilesCV;

//@property (strong, nonatomic) UITextField *searchBar;
//@property MapHeader *headerView;
@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) IBOutlet MapTableView *tableView;

@property (strong, nonatomic) UIView *senderView;
@property (strong, nonatomic) UIViewController *senderVC;


-(void)expandFromMapView:(MKMapView *)fromMap inViewController:(UIViewController *)presentingViewController;
-(void)setCanAddLocations:(BOOL)canAdd animated:(BOOL)animate;

@end
