//
//  MapTableViewController.m
//  Qlumi
//
//  Created by William Smillie on 10/5/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import "MapTableViewController.h"
#import "NewEventTableViewController.h"


@interface MapTableViewController (){
    UIBarButtonItem *closeButton;
    UIView *segmentHeaderView;
    UISegmentedControl *segmentController;

    UITableViewController *searchResultsVC;
    NSMutableArray *searchResults;
    
    NSMutableArray *placesArray;
    AppDelegate *appdel;
    
    UIAlertController *noteAlert;
    
    BOOL firstLoad;
    BOOL canAddLocations;
}

@end

@implementation MapTableViewController
@synthesize delegate;
@synthesize searchProfilesCV, addedPlacesArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    firstLoad = NO;
    
    addedPlacesArray = [[NSMutableArray alloc] init];
    searchResults = [[NSMutableArray alloc] init];

    closeButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismiss:)];
    [self.navigationItem setRightBarButtonItem:closeButton animated:NO];
    [closeButton setBackButtonBackgroundVerticalPositionAdjustment:30 forBarMetrics:UIBarMetricsDefault];
    
    searchResultsVC = [[UITableViewController alloc] init];
    searchResultsVC.tableView.delegate = self;
    searchResultsVC.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:searchResultsVC];
    self.searchController.hidesNavigationBarDuringPresentation = NO;
    self.searchController.searchBar.searchBarStyle = UISearchBarStyleProminent;
    self.searchController.searchBar.delegate = self;
    self.searchController.searchBar.placeholder = @"Enter an address or search...";
    self.searchController.searchBar.showsCancelButton = NO;

    self.navigationItem.titleView = self.searchController.searchBar;
    self.definesPresentationContext = YES;
    
    
    
    appdel = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    searchProfilesCV.selectionDelegate = self;
//    [self.mapView addSubview:searchProfilesCV];
    
    self.mapView.showsUserLocation = YES;
    self.mapView.delegate = self;
    
    
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(addNoteToMap:)];
    longPress.minimumPressDuration = 1.0;
    [self.mapView addGestureRecognizer:longPress];


    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.rowHeight = 81;
    
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.contentInset = UIEdgeInsetsMake(self.tableView.frame.size.height-300, 0, 0, 0);
    [self.tableView setKeyboardDismissMode:UIScrollViewKeyboardDismissModeOnDrag];
    [self.tableView registerNib:[UINib nibWithNibName:@"MapLocationTableViewCell" bundle:nil] forCellReuseIdentifier:@"placeCell"];
    
    segmentHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 36)];
    segmentHeaderView.backgroundColor = [UIColor clearColor];
    
    segmentController = [[UISegmentedControl alloc] initWithItems:@[@"Interests", @"Tagged"]];
    segmentController.tintColor = [AppDelegate QOrange];
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                [UIFont systemFontOfSize:14], NSFontAttributeName,
                                [UIColor whiteColor], NSForegroundColorAttributeName,
                                nil];
    [segmentController setTitleTextAttributes:attributes forState:UIControlStateNormal | UIControlStateSelected];
    segmentController.layer.shadowOpacity = 0.5f;
    segmentController.layer.shadowRadius = 4.0f;
    segmentController.layer.shadowOffset = CGSizeMake(0, 2);
    segmentController.layer.shadowColor = [UIColor blackColor].CGColor;
    [segmentController setSelectedSegmentIndex:0];
    [segmentController addTarget:self action:@selector(segmentDidChange:) forControlEvents:UIControlEventValueChanged];
    
    [segmentHeaderView addSubview:segmentController];
    [self.tableView setTableHeaderView:segmentHeaderView];
    
    if (searchProfilesCV.searchProfiles.count > 0) {
        [searchProfilesCV selectItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UICollectionViewScrollPositionNone];
    }
}

-(void)setCanAddLocations:(BOOL)canAdd animated:(BOOL)animate{
    canAddLocations = canAdd;
    float duration = 0;
    if (animate)
        duration = 0.3;

    if (segmentController.selectedSegmentIndex == 0) {
        [self addAnnotationForPlaces:placesArray andRemoveOldAnnotations:YES];
    }else{
        [self addAnnotationForPlaces:addedPlacesArray andRemoveOldAnnotations:YES];
    }
    [self.tableView reloadData];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.alpha = 0;
    self.tableView.alpha = 0;
    self.searchProfilesCV.alpha = 0;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillAppear:) name:UIKeyboardWillShowNotification object:nil];
}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    self.navigationController.navigationBar.alpha = 0;
    self.tableView.alpha = 0;
    self.searchProfilesCV.alpha = 0;
    
    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.mapView.frame = [UIApplication sharedApplication].keyWindow.frame;
        self.navigationController.navigationBar.alpha = 1;
        self.tableView.alpha = 1;
        self.searchProfilesCV.alpha = 1;
    }completion:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


-(void)viewDidLayoutSubviews{
    self.tableView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    self.mapView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    segmentController.frame = CGRectMake(8, 8, self.tableView.frame.size.width-16, 28);

    CGRect navbarFrame = self.navigationController.navigationBar.frame;
    float topHeight = navbarFrame.size.height + navbarFrame.origin.y;
    searchProfilesCV.frame = CGRectMake(0, topHeight, self.view.frame.size.width, 40);
    
    [self.searchController.searchBar setNeedsLayout];
}

#pragma mark - SearchControllerDelegate

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    [self.navigationItem setLeftBarButtonItems:@[] animated:YES];
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    [self.navigationItem setLeftBarButtonItems:@[closeButton] animated:YES];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    [searchResultsVC.tableView reloadData];

    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(getAutoCompleteSuggestions) object:nil];
    [self performSelector:@selector(getAutoCompleteSuggestions) withObject:nil afterDelay:0.5];
}

- (void)getAutoCompleteSuggestions{
    [self getAutoCompleteSuggestionsForString:self.searchController.searchBar.text callback:^(NSArray *places) {
        searchResults = [[NSMutableArray alloc] initWithArray:places];
        [searchResultsVC.tableView reloadData];
    }];
}

#pragma mark - Search Profiles Delegate

-(void)searchProfilesView:(searchProfilesCollectionView *)profilesCV didSelectProfile:(searchProfile *)searchProfile{
    
    [self getNearByPlacesForProfile:searchProfile successCallback:^(id successValue) {
        [segmentController setSelectedSegmentIndex:0];
        placesArray = [[NSMutableArray alloc] initWithArray:successValue];
        [self addAnnotationForPlaces:placesArray andRemoveOldAnnotations:YES];
    } errorCallback:^(NSString *errorMsg) {

    }];
}


#pragma mark - Table view data source

-(void)addAnnotationForPlaces:(NSArray<Place *> *)places andRemoveOldAnnotations:(BOOL)remove{
    self.mapView.showsUserLocation = NO;
    if (remove) {
        [self.mapView removeAnnotations:self.mapView.annotations];
    }
    
    if (places.count) {
        for (Place *p in places) {
            QMapAnnotation *annotation = [[QMapAnnotation alloc] init];
            p.annotation = annotation;
            [annotation setCoordinate:p.location];
            [annotation setTitle:p.title]; //You can set the subtitle too
            [annotation setSubtitle:p.address];
            annotation.place = p;
            [self.mapView addAnnotation:annotation];
        }
        
        [self.tableView reloadData];
        [self.mapView showAnnotations:self.mapView.annotations animated:YES];
        self.mapView.showsUserLocation = YES;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (addedPlacesArray.count > 0) {
        segmentController.alpha = 1;
    }else{
        segmentController.alpha = 0;
    }

    if (tableView == self.tableView) {
        if (segmentController.selectedSegmentIndex == 0) {
            return placesArray.count;
        }else{
            [segmentController setTitle:[NSString stringWithFormat:@"Added Locations (%li)", addedPlacesArray.count] forSegmentAtIndex:1];
            [self directionsBetweenPlaces:addedPlacesArray];

            return addedPlacesArray.count;
        }
    }else{
        return searchResults.count+1;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.tableView) {
        return 81;
    }else{
        return 44;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.tableView) {
        static NSString *cellIdentifier = @"placeCell";
        
        MapLocationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        cell.delegate = self;
        if(cell == nil) {
            cell = [[MapLocationTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        
        NSMutableArray *thisArray;
        if (segmentController.selectedSegmentIndex == 0) {
            thisArray = placesArray;
            [cell setSwipeToDeleteEnabled:NO];
        }else{
            thisArray = addedPlacesArray;
            [cell setSwipeToDeleteEnabled:YES];
        }
        
        BOOL movingCell = NO;
        if (thisArray == addedPlacesArray) {
            if ([[addedPlacesArray objectAtIndex:indexPath.row] isKindOfClass:[NSString class]] &&
                [[addedPlacesArray objectAtIndex:indexPath.row] isEqualToString:@"DUMMY"]) {
                movingCell = YES;
            }else{
                movingCell = NO;
            }
        }
        
        if (!movingCell) {
            cell.contentView.alpha = 1;

            Place *thisPlace = thisArray[indexPath.row];
            
            CLLocation *locA = [[CLLocation alloc] initWithLatitude:appdel.userCoordinates.latitude longitude:appdel.userCoordinates.longitude];
            CLLocation *locB = [[CLLocation alloc] initWithLatitude:thisPlace.location.latitude longitude:thisPlace.location.longitude];
            CLLocationDistance distance = [locA distanceFromLocation:locB];
            
            float miles = (distance/1609.344);
            
            [cell.thumbnailImageView sd_setImageWithURL:thisPlace.photoURL placeholderImage:[UIImage imageNamed:@"photo-placeholder"]];
            
            cell.nameLabel.text = thisPlace.title;
            cell.addressLabel.text = thisPlace.address;
            cell.detailsLabel.text = [NSString stringWithFormat:@"%.02f Rating • %.1f %@", thisPlace.rating, miles, (miles==1)?@"Mile":@"Miles"];
            cell.dot.backgroundColor = [Place colorForType:thisPlace.type];
            if (thisPlace.enabled) {
                [UIView animateWithDuration:0.2 animations:^{
                    cell.dotDeselectView.alpha = 0;
                }];
            }else{
                [UIView animateWithDuration:0.2 animations:^{
                    cell.dotDeselectView.alpha = 1;
                }];
            }
            
            
            if (indexPath.row == 0) {
                cell.topLine.hidden = YES;
            }else{
                cell.topLine.hidden = NO;
            }
            if (indexPath.row == thisArray.count-1){
                cell.bottomLine.hidden = YES;
            }else{
                cell.bottomLine.hidden = NO;
            }
        }else{
            cell.contentView.alpha = 0;
        }

        
        return cell;
    }else{
        UITableViewCell *cell = [searchResultsVC.tableView dequeueReusableCellWithIdentifier:@"searchCell"];
        if(cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"searchCell"];
        }
        
        if (indexPath.row == 0) {
            cell.textLabel.text = [NSString stringWithFormat:@"Search \"%@\" Nearby", self.searchController.searchBar.text];
            cell.imageView.image = [UIImage imageNamed:@"magnifying-glass"];
        }else{
            Place *place = searchResults[indexPath.row-1];
            cell.textLabel.text = place.title;
            cell.imageView.image = [UIImage imageNamed:@"local-pin"];
        }
        
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.tableView) {
        NSMutableArray *thisArray;
        if (segmentController.selectedSegmentIndex == 0) {
            thisArray = placesArray;
        }else{
            thisArray = addedPlacesArray;
        }
        Place *place = thisArray[indexPath.row];
        
        [place setEnabled:!place.enabled];
        [tableView reloadData];
//        [self.mapView selectAnnotation:place.annotation animated:YES];
    }else{
        [searchResultsVC dismissViewControllerAnimated:YES completion:nil];
        if (indexPath.row == 0) {
            [self reloadSearch];
        }else{
            [self.mapView removeAnnotations:self.mapView.annotations];
            [placesArray removeAllObjects];
            [self.tableView reloadData];
            
            Place *p = searchResults[indexPath.row-1];
            CLGeocoder* geocoder = [[CLGeocoder alloc] init];
            [geocoder geocodeAddressString:p.address completionHandler:^(NSArray* placemarks, NSError* error){
                 for (CLPlacemark* aPlacemark in placemarks){
                     p.location = aPlacemark.location.coordinate;
                     QMapAnnotation *annotation = [[QMapAnnotation alloc] init];
                     p.annotation = annotation;
                     [annotation setCoordinate:p.location];
                     [annotation setTitle:p.title]; //You can set the subtitle too
                     [annotation setSubtitle:p.address];
                     annotation.place = p;
                     [self.mapView addAnnotation:annotation];
                     [self.mapView selectAnnotation:annotation animated:YES];

                 }
             }];
        }
    }
}

- (id)saveObjectAndInsertBlankRowAtIndexPath:(NSIndexPath *)indexPath {
    id object = [addedPlacesArray objectAtIndex:indexPath.row];
    [addedPlacesArray replaceObjectAtIndex:indexPath.row withObject:@"DUMMY"];
    return object;
}

- (void)moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
    id object = [addedPlacesArray objectAtIndex:fromIndexPath.row];
    [addedPlacesArray removeObjectAtIndex:fromIndexPath.row];
    [addedPlacesArray insertObject:object atIndex:toIndexPath.row];
}


- (void)finishReorderingWithObject:(id)object atIndexPath:(NSIndexPath *)indexPath; {
    [addedPlacesArray replaceObjectAtIndex:indexPath.row withObject:object];
    [self addAnnotationForPlaces:addedPlacesArray andRemoveOldAnnotations:YES];
}

-(void)MapLocationCell:(id)cell cellDidDeleteAtIndexPath:(NSIndexPath *)indexPath{
    [addedPlacesArray removeObjectAtIndex:indexPath.row];
    
    [self.tableView beginUpdates];
    [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    [self.tableView endUpdates];
    
    [self addAnnotationForPlaces:addedPlacesArray andRemoveOldAnnotations:YES];
}

#pragma mark - UIScrollViewDelegate


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.tableView) {
        float perc = (self.tableView.contentOffset.y/(self.tableView.frame.size.height-self.tableView.contentInset.top));
        
        CGFloat const inMin = -1;
        CGFloat const inMax = 0;
        CGFloat const outMin = 0.0;
        CGFloat const outMax = .5;
        CGFloat in = perc;
        
        CGFloat percentage = outMin + (outMax - outMin) * (in - inMin) / (inMax - inMin);
        if (percentage > 0.5) {
            percentage = 0.5;
        }
        
        CGRect navbarFrame = self.navigationController.navigationBar.frame;
        float topHeight = navbarFrame.size.height + navbarFrame.origin.y;

        
        if (scrollView.contentOffset.y+topHeight < ([scrollView.superview convertRect:searchProfilesCV.frame fromView:searchProfilesCV.superview].origin.y-topHeight)) {
            if (searchProfilesCV.alpha == 0) {
                [UIView animateWithDuration:0.3 animations:^{
                    CGRect frame = searchProfilesCV.frame;
                    frame.origin.y += searchProfilesCV.frame.size.height;
                    searchProfilesCV.frame = frame;
                    searchProfilesCV.alpha = 1;
                }];
            }
        }else{
            if (searchProfilesCV.alpha == 1) {
                [UIView animateWithDuration:0.3 animations:^{
                    CGRect frame = searchProfilesCV.frame;
                    frame.origin.y -= searchProfilesCV.frame.size.height;
                    searchProfilesCV.frame = frame;
                    searchProfilesCV.alpha = 0;
                }];
            }
        }
        
        self.tableView.backgroundColor = [UIColor colorWithWhite:1.0 alpha:(float)INTUInterpolateCGFloat(0, 1, percentage)];
    }
}

-(void)roundTopCornersForView:(UIView *)view{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:view.bounds byRoundingCorners:UIRectCornerTopLeft| UIRectCornerTopRight cornerRadii:CGSizeMake(32, 32)];
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = view.bounds;
    maskLayer.path = maskPath.CGPath;
    view.layer.mask = maskLayer;
}



#pragma mark - MKMapViewDelegate

-(void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated{
//    [searchBar resignFirstResponder];
}

-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation{
    if (firstLoad == YES){
        firstLoad = NO;
                
        MKCoordinateRegion mapRegion;
        mapRegion.center = mapView.userLocation.coordinate;
        mapRegion.span.latitudeDelta = 0.2;
        mapRegion.span.longitudeDelta = 0.2;
        [mapView setRegion:mapRegion animated: YES];
    }
}


- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[QMapAnnotation class]])
    {
        QMapAnnotation *qannotation = (QMapAnnotation *)annotation;
        
        QMapAnnotationView *pinView = (QMapAnnotationView*)[self.mapView dequeueReusableAnnotationViewWithIdentifier:@"MyAnnotation"];
        if (!pinView){
            pinView = [[QMapAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"MyAnnotation"];
            pinView.canShowCallout = YES;
            pinView.calloutOffset = CGPointMake(0, 0);
        }
        
        if (canAddLocations) {
            UIButton *accessoryButton = [UIButton buttonWithType:UIButtonTypeContactAdd];
            pinView.rightCalloutAccessoryView = accessoryButton;
        }else{
            pinView.rightCalloutAccessoryView = nil;
        }
        pinView.annotation = annotation;
        
        UIImage *image = [Place iconForType:qannotation.place.type];
        pinView.typeImageView.image = image;
        pinView.pinView.backgroundColor = [Place colorForType:qannotation.place.type];

        return pinView;
    }
    return nil;
}


-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)annotationView{
    MKCoordinateRegion mapRegion;
    mapRegion.center = annotationView.annotation.coordinate;
    mapRegion.span.latitudeDelta = 0.005;
    mapRegion.span.longitudeDelta = 0.005;
    [self.mapView setRegion:mapRegion animated:YES];
    [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
}

- (void)mapView:(MKMapView *)mapView annotationView:(QMapAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control{
    QMapAnnotation *annotation = view.annotation;
    
    [annotation.place setEnabled:YES];
    
    [addedPlacesArray addObject:annotation.place];
    [segmentController setTitle:[NSString stringWithFormat:@"Added Locations (%li)", addedPlacesArray.count] forSegmentAtIndex:1];
    [self.tableView reloadData];
}


-(void)directionsBetweenPlaces:(NSArray *)places{
    if (![places containsObject:@"DUMMY"]) {
        for (id overlay in self.mapView.overlays) {
            if ([overlay isKindOfClass:[MKPolyline class]]){
                [self.mapView removeOverlay:overlay];
            }
        }
        
        for (int i=0; i<places.count; i++) {
            if (i+1 < places.count) {
                Place *p = places[i];
                Place *p2 = places[i+1];
                
                [self getPathDirections:p withDestination:p2];
            }
        }
    }
}

-(void)getPathDirections:(Place *)source withDestination:(Place *)destination{
    MKPlacemark *mks = [[MKPlacemark alloc] initWithCoordinate:source.location addressDictionary:nil];
    MKPlacemark *mkd = [[MKPlacemark alloc] initWithCoordinate:destination.location addressDictionary:nil];

    MKDirectionsRequest *directionsRequest = [[MKDirectionsRequest alloc] init];
    [directionsRequest setSource:[[MKMapItem alloc] initWithPlacemark:mks]];
    [directionsRequest setDestination:[[MKMapItem alloc] initWithPlacemark:mkd]];
    directionsRequest.transportType = MKDirectionsTransportTypeAutomobile;
    MKDirections *directions = [[MKDirections alloc] initWithRequest:directionsRequest];
    [directions calculateDirectionsWithCompletionHandler:^(MKDirectionsResponse *response, NSError *error) {
        if (!error) {
            for (MKRoute *route in [response routes]) {
                [self.mapView addOverlay:[route polyline] level:MKOverlayLevelAboveRoads];
            }
        }
    }];
}


- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id)overlay
{
    if ([overlay isKindOfClass:[MKPolyline class]]) {
        MKPolylineRenderer *renderer = [[MKPolylineRenderer alloc] initWithOverlay:overlay];
        
        CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
        CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
        CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
        UIColor *color = [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];

        
        [renderer setStrokeColor:color];
        [renderer setLineWidth:3.0];
        return renderer;
    }
    return nil;
}



#pragma mark - IBActions


- (void)addNoteToMap:(UIGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state != UIGestureRecognizerStateBegan)
        return;
    
    CGPoint touchPoint = [gestureRecognizer locationInView:self.mapView];
    CLLocationCoordinate2D touchMapCoordinate =
    [self.mapView convertPoint:touchPoint toCoordinateFromView:self.mapView];
    
    noteAlert = [UIAlertController alertControllerWithTitle:@"New Note" message:@"\n\n\n\n\n\n\n\n" preferredStyle:UIAlertControllerStyleAlert];
    
    noteAlert.view.autoresizesSubviews = YES;
    UITextView *textView = [[UITextView alloc] initWithFrame:CGRectZero];
    textView.translatesAutoresizingMaskIntoConstraints = NO;
    textView.editable = YES;
    textView.dataDetectorTypes = UIDataDetectorTypeAll;
    textView.text = @"";
    textView.userInteractionEnabled = YES;
    textView.backgroundColor = [UIColor clearColor];
    textView.scrollEnabled = YES;
    textView.delegate = self;
    NSLayoutConstraint *leadConstraint = [NSLayoutConstraint constraintWithItem:noteAlert.view attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:textView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:-8.0];
    NSLayoutConstraint *trailConstraint = [NSLayoutConstraint constraintWithItem:noteAlert.view attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:textView attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:8.0];
    
    NSLayoutConstraint *topConstraint = [NSLayoutConstraint constraintWithItem:noteAlert.view attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:textView attribute:NSLayoutAttributeTop multiplier:1.0 constant:-64.0];
    NSLayoutConstraint *bottomConstraint = [NSLayoutConstraint constraintWithItem:noteAlert.view attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:textView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:64.0];
    [noteAlert.view addSubview:textView];
    [NSLayoutConstraint activateConstraints:@[leadConstraint, trailConstraint, topConstraint, bottomConstraint]];
    
    
    [noteAlert addAction:[UIAlertAction actionWithTitle:@"Create" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        Place *notePlace = [[Place alloc] init];
        notePlace.title = textView.text;
        notePlace.type  = GPlaceTypeNote;
        notePlace.location = touchMapCoordinate;
        notePlace.enabled = YES;
        
        
        QMapAnnotation *annot = [[QMapAnnotation alloc] init];
        annot.coordinate = touchMapCoordinate;
        annot.title = notePlace.title;
        annot.place = notePlace;
        annot.isNote = YES;
        [self.mapView addAnnotation:annot];
        
        [addedPlacesArray addObject:notePlace];
        [self.tableView reloadData];
    }]];
    [noteAlert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:noteAlert animated:YES completion:^{}];
}

-(void)textViewDidBeginEditing:(UITextView *)textView{
    CGRect frame = noteAlert.view.frame;
    frame.origin.y = 120;
    noteAlert.view.frame = frame;
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    [noteAlert.view endEditing:YES];
}


-(IBAction)segmentDidChange:(UISegmentedControl *)sender{
    if (sender.selectedSegmentIndex == 0) {
        for (id overlay in self.mapView.overlays) {
            if ([overlay isKindOfClass:[MKPolyline class]]){
                [self.mapView removeOverlay:overlay];
            }
        }
        [self addAnnotationForPlaces:placesArray andRemoveOldAnnotations:YES];
    }else{
        [self addAnnotationForPlaces:addedPlacesArray andRemoveOldAnnotations:YES];        
    }
    [self.tableView reloadData];
}

-(IBAction)dismiss:(id)sender{
    if (canAddLocations) {
        [delegate MapTableViewController:self willDismissWithAddedPlaces:addedPlacesArray];
    }
    
    if ([self isModal]) {
        [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.navigationController.navigationBar.alpha = 0;
            self.tableView.alpha = 0;
            self.searchProfilesCV.alpha = 0;
            self.mapView.frame = [self.senderView.superview convertRect:self.senderView.frame toView:self.navigationController.view];
        }completion:^(BOOL finished){
            [self dismissViewControllerAnimated:NO completion:^{
                self.navigationController.navigationBar.alpha = 1;
                self.tableView.alpha = 1;
            }];
        }];
        /*if ([[(UINavigationController *)self.presentingViewController viewControllers][0] isKindOfClass:[NewEventTableViewController class]]) {
            [self addAnnotationForPlaces:addedPlacesArray andRemoveOldAnnotations:YES];
            [(QMapView *)self.senderView addAnnotations:self.mapView.annotations];
            [(QMapView *)self.senderView setRegion:self.mapView.region];
        }*/
        [(QMapView *)self.senderView addAnnotations:self.mapView.annotations];
        [(QMapView *)self.senderView setRegion:self.mapView.region];
        [self dismissViewControllerAnimated:YES completion:nil];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}



#pragma mark - Data

-(void)reloadSearch{
    [self.mapView removeAnnotations:self.mapView.annotations];
    placesArray = [[NSMutableArray alloc] init];
    
    [self getNearByPlacesForString:self.searchController.searchBar.text successCallback:^(NSArray *places) {
        placesArray = [places mutableCopy];
        [self addAnnotationForPlaces:placesArray andRemoveOldAnnotations:YES];
    } errorCallback:^(NSString *errorMsg) {
        
    }];
}


#pragma mark - Google Places API

-(void)getAutoCompleteSuggestionsForString:(NSString *)searchString callback:(void (^)(id))callback{
    
    searchString = [[searchString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]] mutableCopy];
    
    NSMutableURLRequest* request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET" URLString:[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&key=AIzaSyC67qOLCwRi1_6Z8g1zKA6OQkJekYIBDz8", searchString] parameters:@{} error:NULL];
    [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFHTTPRequestOperation *operation = [manager HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject){
        if ([responseObject[@"status"] isEqualToString:@"OK"]) {
            NSArray *results = responseObject[@"predictions"];

            NSMutableArray *returnArray = [[NSMutableArray alloc] init];
            for (NSDictionary *result in results){
                Place *p = [[Place alloc] initWithPredicationDict:result];
                [returnArray addObject:p];
            }
            callback(returnArray);
        }
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {

    }];
    
    [manager.operationQueue addOperation:operation];
}

-(void)getNearByPlacesForString:(NSString *)searchString successCallback:(void (^)(id))successCallback errorCallback:(void (^)(NSString *))errorCallback{
    
    searchString = [[searchString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]] mutableCopy];
    
    NSMutableURLRequest* request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET" URLString:[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=%f,%f&radius=%i&keyword=%@&key=AIzaSyC67qOLCwRi1_6Z8g1zKA6OQkJekYIBDz8", self.mapView.centerCoordinate.latitude, self.mapView.centerCoordinate.longitude, [self visibileRadiusFromMap:self.mapView], searchString] parameters:@{} error:NULL];
    [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFHTTPRequestOperation *operation = [manager HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject){
        if (![responseObject[@"status"] isEqualToString:@"Failed"]) {
            NSMutableArray *placesArray = [[NSMutableArray alloc] init];
            NSArray *results = responseObject[@"results"];
            for (NSDictionary *result in results){
                Place *p = [[Place alloc] initWithDictionary:result];
                [placesArray addObject:p];
            }
            successCallback(placesArray);
        }else{
            successCallback(responseObject[@"error_message"]);
        }
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        errorCallback(error.localizedDescription);
    }];
    
    [manager.operationQueue addOperation:operation];
}

- (void)getNearByPlacesForProfile:(searchProfile *)searchProfile successCallback:(void (^)(id successValue)) successCallback errorCallback:(void (^)( NSString *errorMsg)) errorCallback{
    
    NSMutableString *url = [NSMutableString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=AIzaSyC67qOLCwRi1_6Z8g1zKA6OQkJekYIBDz8&location=%f,%f&radius=%i", self.mapView.centerCoordinate.latitude, self.mapView.centerCoordinate.longitude, [self visibileRadiusFromMap:self.mapView]];
    [searchProfile.filters enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL * _Nonnull stop) {
        if (obj) {
            [url appendString:[NSString stringWithFormat:@"&%@=%@", key, obj]];
        }
    }];
    
    url = [[url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]] mutableCopy];
    
    NSMutableURLRequest* request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET" URLString:url parameters:@{} error:NULL];
    
    [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFHTTPRequestOperation *operation = [manager HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject){
        if (![responseObject[@"status"] isEqualToString:@"Failed"]) {
            NSMutableArray *placesArray = [[NSMutableArray alloc] init];
            NSArray *results = responseObject[@"results"];
            for (NSDictionary *result in results){
                Place *p = [[Place alloc] initWithDictionary:result];
                [placesArray addObject:p];
            }
            successCallback(placesArray);
        }else{
            successCallback(responseObject[@"error_message"]);
        }
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        errorCallback(error.localizedDescription);
    }];
    
    [manager.operationQueue addOperation:operation];
}


#pragma mark - Helper Methods

- (int)visibileRadiusFromMap:(MKMapView *)mapView;
{
    CLLocationCoordinate2D centerCoor = mapView.centerCoordinate;
    CLLocation *centerLocation = [[CLLocation alloc] initWithLatitude:centerCoor.latitude longitude:centerCoor.longitude];
    CLLocationCoordinate2D topCenterCoor = [mapView convertPoint:CGPointMake(mapView.frame.size.width / 2.0f, 0) toCoordinateFromView:mapView];
    CLLocation *topCenterLocation = [[CLLocation alloc] initWithLatitude:topCenterCoor.latitude longitude:topCenterCoor.longitude];
    CLLocationDistance radius = [centerLocation distanceFromLocation:topCenterLocation];
    
    return (int)(radius + 0.5);
}


-(void)expandFromMapView:(MKMapView *)fromMap inViewController:(UIViewController *)presentingViewController{
    self.senderView = fromMap;
    self.view.clipsToBounds = YES;
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:self];
    [nav setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    
    for (MKPointAnnotation *annotation in fromMap.annotations) {
        [self.mapView addAnnotation:annotation];
    }
    
    nav.view.alpha = 0;
    self.searchProfilesCV.alpha = 0;

    [presentingViewController presentViewController:nav animated:NO completion:^{
        nav.view.alpha = 1;
        self.mapView.frame = [presentingViewController.navigationController.view convertRect:fromMap.frame fromView:fromMap.superview];
        
        [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.mapView.frame = [UIApplication sharedApplication].keyWindow.frame;
            nav.navigationBar.alpha = 1;
            self.tableView.alpha = 1;
            self.searchProfilesCV.alpha = 1;
            [self.mapView setRegion:fromMap.region animated:NO];
        }completion:nil];
    }];
}


-(CLLocationCoordinate2D)getLocationFromAddressString:(NSString*)addressStr {
    double latitude = 0, longitude = 0;
    addressStr = [[addressStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]] mutableCopy];
    NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", addressStr];
    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
    if (result) {
        NSScanner *scanner = [NSScanner scannerWithString:result];
        if ([scanner scanUpToString:@"\"lat\" :" intoString:nil] && [scanner scanString:@"\"lat\" :" intoString:nil]) {
            [scanner scanDouble:&latitude];
            if ([scanner scanUpToString:@"\"lng\" :" intoString:nil] && [scanner scanString:@"\"lng\" :" intoString:nil]) {
                [scanner scanDouble:&longitude];
            }
        }
    }
    CLLocationCoordinate2D center;
    center.latitude=latitude;
    center.longitude = longitude;
    return center;
    
}

- (BOOL)isModal {
    return self.presentingViewController.presentedViewController == self
    || (self.navigationController != nil && self.navigationController.presentingViewController.presentedViewController == self.navigationController)
    || [self.tabBarController.presentingViewController isKindOfClass:[UITabBarController class]];
}


- (void)keyboardWillAppear:(NSNotification *)notification
{
    [self.searchController.searchBar setShowsCancelButton:NO animated:NO];
}



@end
