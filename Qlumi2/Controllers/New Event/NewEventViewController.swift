//
//  NewEventViewController.swift
//  Qlumi
//
//  Created by iMEDHealth_Dev on 11/11/2019.
//  Copyright © 2019 Red Door Endeavors. All rights reserved.
//

import UIKit
import AFNetworking
import SDWebImage
import MobileCoreServices
import MapKit
import Parse
import SVProgressHUD
import LPPhotoViewer

let IS_IPHONEX = UIScreen.main.bounds.height / UIScreen.main.bounds.width > 1.8 ? true:false
let appFontRegular = "JosefinSans-Regular"
let appFontBold = "JosefinSans-Bold"

@objc
protocol NewEventViewControllerDelegate {
    func didAddEvent(event: Event)
}

protocol UpdateEventDelegate {
    func didUpdateEvent(event: Event)
}

@objc
enum NewEventHierarchy: Int {
    case NEW
    case EDIT
    case DETAILS
}

class NewEventViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerView_height: NSLayoutConstraint!
    @IBOutlet weak var eventImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var showBackgroundsButton: UIButton!
    @IBOutlet weak var backgroundsCollectionView: UICollectionView!
    @IBOutlet weak var nameTextField: TypeTagTextField!
    @IBOutlet weak var timeTextField: UITextField!
    @IBOutlet @objc weak var dateTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var addressesTableView: UITableView!
    @IBOutlet weak var addressesTableView_height: NSLayoutConstraint!
    @IBOutlet weak var rsvpView: UIView!
    @IBOutlet weak var rsvpMaybeButton: UIButton!
    @IBOutlet weak var rsvpYesButton: UIButton!
    @IBOutlet weak var rsvpNoButton: UIButton!
    
    @IBOutlet weak var mapContainerView: UIView!
    @IBOutlet weak var mapView: QMapView!
    @IBOutlet weak var detailsTextView: UITextView!
    @IBOutlet weak var activitiesErrorLabel: UILabel!
    @IBOutlet weak var activitiesCollectionView: UICollectionView!
    @IBOutlet weak var guestsErrorLabel: UILabel!
    @IBOutlet weak var guestsCollectionView: UICollectionView!
    @IBOutlet weak var sendSMSView: UIView!
    @IBOutlet weak var sendSMSView_height: NSLayoutConstraint!
    
    @IBOutlet weak var photosView_height: NSLayoutConstraint!
    @IBOutlet weak var photosCollectionView: UICollectionView!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var deleteButton_height: NSLayoutConstraint!
    
    @objc public var passedEvent: Event?
    @objc public var delegate: NewEventViewControllerDelegate?
    var updateDelegate: UpdateEventDelegate?
    @objc public var hierarchy: NewEventHierarchy = NewEventHierarchy.NEW
    
    var rsvpObject: PFObject?
    
    var timePicker: UIDatePicker!
    var datePicker: UIDatePicker!
    var selectedDate: Date?
    var selectedTime: Date?
    
    var colorsArray: [UIColor] = []
    
    var pickerType = ""
    
    //MARK: - Event Images
    var backgroundImages: [String] = []
    
    //MARK: - Address autocompletion
    var searchString = ""
    var places: [Place] = []
    var currentPlace: Place?
    
    //MARK: - Map
    var addedPlaces: [Place] = []
    
    //MARK: - Schedule/Activity
    var schedulesArray: [ScheduledObject] = []
    var addScheduleAlert: UIAlertController!
    
    //MARK: - Guests
    var guestsArray: [[String: Any]] = []
    var profilePics: [String: Any] = [:]
    var modalWindow: UIWindow?
    var contactsPickerAlert: UIAlertController!
    var contactGroupView: contactsGroupCollectionView!
    var creatingGroup = false
    
    //MARK: - Event Photos
    var eventPhotos = [PFObject]()
    var photoUrls: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupHeaderView()
        switch hierarchy {
        case .NEW:
            self.title = "New Event"
            let createButton = UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector(createEvent))
            self.navigationItem.rightBarButtonItem = createButton
            break
        case .EDIT:
            self.title = "Update Event"
            let updateButton = UIBarButtonItem(title: "Update", style: .done, target: self, action: #selector(createEvent))
            self.navigationItem.rightBarButtonItem = updateButton
            self.deleteButton_height.constant = 33
            self.deleteButton.updateConstraints()
            break
        case .DETAILS:
            self.title = passedEvent?.title ?? ""
            let editButton = UIBarButtonItem(title: "Edit", style: .done, target: self, action: #selector(showEditEvent))
            self.navigationItem.rightBarButtonItem = editButton
            self.deleteButton_height.constant = 33
            self.deleteButton.updateConstraints()
            let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(onLongPressGesture(_:)))
            longPressGesture.delaysTouchesBegan = true
            longPressGesture.cancelsTouchesInView = true
            longPressGesture.minimumPressDuration = 1
            photosCollectionView.addGestureRecognizer(longPressGesture)
            break
        }
        let closeButton = UIBarButtonItem(title: "❮ Back", style: .done, target: self, action: #selector(dismissVC))
        self.navigationItem.leftBarButtonItem = closeButton
        addressesTableView.rowHeight = 40
        nameTextField.showTags(true)
        nameTextField.typeTagDelegate = self
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(onMapTapGesture(_:)))
        tapGestureRecognizer.numberOfTapsRequired = 1
        self.mapView.addGestureRecognizer(tapGestureRecognizer)
        self.mapView.shouldShowResearch = false
        colorsArray = EPGlobalConstants.colorsArray() as! [UIColor]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        mapView.showsUserLocation = true
        self.getGuestsProfilePics()
        if passedEvent != nil {
            self.eventImageView.sd_setImage(with: URL(string: passedEvent?.image.url ?? "")!, completed: nil)
            self.nameLabel.text = passedEvent?.title ?? ""
            self.nameTextField.text = passedEvent?.title ?? ""
            self.addressTextField.text = passedEvent?.address ?? ""
            if let date = passedEvent?.date {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "h:mm a"
                selectedTime = date
                timeTextField.text = dateFormatter.string(from: date)
                dateFormatter.dateFormat = "EEEE, MMM dd, yyyy"
                selectedDate = date
                dateTextField.text = dateFormatter.string(from: date)
            }
//            var placeIds: [String] = []
            let places: [[String: Any]] = passedEvent?.locations as? [[String:Any]] ?? []
//            for location in places {
//                if let placeId = location["placeId"] as? String {
//                    placeIds.append(placeId)
//                }
//            }
            self.addedPlaces = []
            self.getPlaces(from: places)
            
            detailsTextView.text = passedEvent?.about ?? ""
            self.schedulesArray = []
            if let schedules = passedEvent?.schedule as? [[String: Any]]{
                for scheduleObj in schedules {
                    let eventSchedule = ScheduledObject()
                    eventSchedule.title = scheduleObj["title"] as? String ?? ""
                    eventSchedule.startTime = scheduleObj["startTime"] as? String ?? ""
                    eventSchedule.endTime = scheduleObj["endTime"] as? String ?? ""
                    self.schedulesArray.append(eventSchedule)
                }
            }
            self.activitiesCollectionView.reloadData()
            self.guestsArray = passedEvent?.guests as? [[String: Any]] ?? []
            self.guestsCollectionView.reloadData()
            
        }
        if hierarchy == .DETAILS {
            setPhotosArray(passedEvent?.photos)
            self.disableEventEditing()
            activitiesCollectionView.isHidden = schedulesArray.count == 0 ? true:false
            activitiesErrorLabel.isHidden = schedulesArray.count == 0 ? false:true
            guestsCollectionView.isHidden = guestsArray.count == 0 ? true:false
            guestsErrorLabel.isHidden = guestsArray.count == 0 ? false:true
            getRSVPObject()
            sendSMSView_height.constant = guestsArray.count == 0 ? 0:50
            UIView.animate(withDuration: 0.2) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.mapView.shouldShowResearch = false
    }
    
    @IBAction func showBackgroundsCollectionView(_ sender: UIButton) {
        self.backgroundsCollectionView.isHidden = false
        self.showBackgroundsButton.isHidden = true
    }
    
    @IBAction func setRSVPStatus(_ sender: UIButton) {
        let rsvp = sender.titleLabel?.text?.lowercased() ?? ""
        rsvpObject?.setObject(rsvp, forKey: "rsvp")
        
        rsvpObject?.saveInBackground(block: { succeeded, error in
            if succeeded {
                self.setRSVPValues()
            }
        })
    }
    
    @IBAction func sendSMS(_ sender: UIButton) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "ContactsSelectionViewController") as! ContactsSelectionViewController
        vc.guestsArray = self.guestsArray
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func addPhotos(_ sender: UIButton) {
        pickerType = "photo"
        self.showImagePickerController()
    }
    
    @objc func onMapTapGesture(_ gestureRecognizer: UITapGestureRecognizer) {
        detailsTextView.resignFirstResponder()
        let mapVC: MapViewController = MapViewController(editingEnabled: true, andSearchingEnabled: true)
        let nvc = UINavigationController(rootViewController: mapVC)
        self.present(nvc, animated: true) {
            if self.hierarchy != .DETAILS {
                mapVC.searchController.searchBar.placeholder = "Select Event Locations"
                mapVC.navigationItem.leftBarButtonItem?.title = "Next"
                
            }
            
            mapVC.addedPlacesArray = self.addedPlaces as? NSMutableArray ?? NSMutableArray()
            mapVC.mapView.addAnnotations(self.mapView.annotations)
            mapVC.mapView.setRegion(self.mapView.region, animated: true)
            mapVC.mapListController.tableView.reloadData()
            
            if mapVC.addedPlacesArray.count > 0 {
                mapVC.segmentedControl.selectedSegmentIndex = 1
                mapVC.segmentedControl.setTitle("Tagged (\(mapVC.addedPlacesArray.count))", forSegmentAt: 1)
            }else {
                mapVC.segmentedControl.selectedSegmentIndex = 0
            }
            if self.hierarchy != .DETAILS {
                mapVC.delegate = self
                if self.currentPlace != nil {
                    mapVC.searchController.searchBar.text = self.currentPlace?.title
                    Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false, block: { (timer) in
                        let isResponder = mapVC.searchController.searchBar.becomeFirstResponder()
                        print(isResponder)
                    })
                }else {
                    let titleString = NSAttributedString(string: "Enter the event location or address above", attributes: [.font: UIFont(name: "JosefinSans-Bold", size: 18.0)!])
                    let alert = UIAlertController(title: "", message: nil, preferredStyle: .alert)
                    alert.setValue(titleString, forKey: "attributedTitle")
                    alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                    nvc.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    @objc func onLongPressGesture(_ gestureRecognizer: UILongPressGestureRecognizer) {
        if gestureRecognizer.state == .began {
            let point = gestureRecognizer.location(in: self.photosCollectionView)
            if let indexPath = photosCollectionView.indexPathForItem(at: point) {
                let user: PFUser = eventPhotos[indexPath.row]["user"] as! PFUser
                let user1: PFUser = (eventPhotos[indexPath.row]["event"] as! Event)["user"] as! PFUser
                if (user.objectId == PFUser.current()?.objectId) || (user1.objectId == PFUser.current()?.objectId){
                    let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                    actionSheet.addAction(UIAlertAction(title: "Delete Photo", style: .default, handler: { (action) in
                        let photo = self.eventPhotos[indexPath.row]
                        SVProgressHUD.show()
                        photo.deleteInBackground { (success, eerror) in
                            SVProgressHUD.dismiss()
                            self.eventPhotos.remove(at: indexPath.row)
                            self.photoUrls.remove(at: indexPath.row)
                            self.photosCollectionView.reloadData()
                        }
                    }))
                    actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                    self.present(actionSheet, animated: true, completion: nil)
                }
            }
        }
    }
    
    @IBAction func saveEvent(_ sender: UIButton) {
        detailsTextView.resignFirstResponder()
        createEvent()
    }
    
    @IBAction func deleteEvent(_ sender: UIButton) {
        detailsTextView.resignFirstResponder()
        let titleString = NSAttributedString(string: "Delete Event", attributes: [.font: UIFont(name: "JosefinSans-Bold", size: 18.0)!])
        let messageString = NSAttributedString(string: "Are you sure you would like to delete this event?", attributes: [.font: UIFont(name: "JosefinSans-Regular", size: 15.0)!])
        let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
        alert.setValue(titleString, forKey: "attributedTitle")
        alert.setValue(messageString, forKey: "attributedMessage")
        alert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (action) in
            self.passedEvent?.setObject(true, forKey: "archived")
            self.passedEvent?.saveInBackground(block: { succeeded, error in
                if error == nil && succeeded {
                    self.dismissVC()
                }
            })
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func createEvent() {
        if isValidInput() {
            let calendar = Calendar.current
            var dateComponent: DateComponents!
            if selectedDate != nil {
                dateComponent = calendar.dateComponents([.year, .month, .day], from: selectedDate!)
            }
            var timeComponents: DateComponents!
            if selectedTime != nil {
                timeComponents = calendar.dateComponents([.hour, .minute, .second], from: selectedTime!)
            }
            dateComponent.setValue(timeComponents.hour, for: .hour)
            dateComponent.setValue(timeComponents.minute, for: .minute)
            dateComponent.setValue(timeComponents.second, for: .second)
            
            let date = calendar.date(from: dateComponent)
            
            var scheduleArray: [[String: Any]] = []
            for scheduleObject in self.schedulesArray {
                scheduleArray.append(["title": scheduleObject.title, "startTime": scheduleObject.startTime, "endTime": scheduleObject.endTime])
            }
            
            var guestArray: [[String: Any]] = []
            for guest in guestsArray {
                var newGuest = guest
                if let _ = newGuest["rsvp"] {
                    
                }else {
                    newGuest["rsvp"] = "none"
                }
                guestArray.append(newGuest)
            }
            var locations: [Place] = []
            if currentPlace != nil {
                locations.append(currentPlace!)
            }
            locations.append(contentsOf: addedPlaces)
            
            var event: Event!
            if passedEvent != nil {
                event = passedEvent
            }else {
                event = Event()
            }
            event.locations = self.getLocationArray(from: locations)
            event.title = nameTextField.text ?? ""
            event.about = detailsTextView.text ?? ""
            event.date = date
            event.address = addressTextField.text ?? ""
            event.user = PFUser.current()
            do {
                event.image = try PFFile(name: "image.jpg", data: (self.eventImageView.image ?? UIImage()).jpegData(compressionQuality: 0.4) ?? Data(), contentType: nil)
            }catch {
                
            }
            event.schedule = scheduleArray
            event.guests = guestArray
            SVProgressHUD.show()
            event.saveInBackground(block: { succeeded, error in
                SVProgressHUD.dismiss()
                if succeeded {
                    
                    let titleString = NSAttributedString(string: "Done!", attributes: [.font: UIFont(name: "JosefinSans-Bold", size: 18.0)!])
                    let messageString = NSAttributedString(string: "Your event has been created", attributes: [.font: UIFont(name: "JosefinSans-Regular", size: 15.0)!])
                    let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
                    alert.setValue(titleString, forKey: "attributedTitle")
                    alert.setValue(messageString, forKey: "attributedMessage")
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadEvents"), object: nil)
                        if self.hierarchy == .EDIT {
                            self.updateDelegate?.didUpdateEvent(event: event)
                            self.navigationController?.popViewController(animated: true)
                        }else {
                            if let value = UserDefaults.standard.value(forKey: "NewEventsCounts") as? Int {
                                if value % 3 == 0 {
                                    if #available(iOS 10.3, *) {
                                        SKStoreReviewController.requestReview()
                                    } else {
                                        // Fallback on earlier versions
                                    }
                                }
                                UserDefaults.standard.set(value + 1, forKey: "NewEventsCounts")
                                UserDefaults.standard.synchronize()
                            }else {
                                UserDefaults.standard.set(1, forKey: "NewEventsCounts")
                                UserDefaults.standard.synchronize()
                            }
                            
                            self.delegate?.didAddEvent(event: event)
                            self.dismiss(animated: true, completion: nil)
                        }
                    }))
                    self.present(alert, animated: true)
                }
            })
        }
    }
    
    @objc func showEditEvent() {
        let eventVC = self.storyboard?.instantiateViewController(withIdentifier: "NewEventViewController") as! NewEventViewController
        eventVC.passedEvent = passedEvent
        eventVC.hierarchy = .EDIT
        eventVC.updateDelegate = self
        self.navigationController?.pushViewController(eventVC, animated: true)
    }
    
    @objc func dismissVC() {
        if let _ = self.navigationController?.popViewController(animated: true) {
            
        }else {
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension NewEventViewController {
    func setupHeaderView() {
        eventImageView.image = UIImage(named: "camera-placeholder")
    }
    
    func addPinsToMap() {
        mapView.removeAnnotations(mapView?.annotations ?? [])
        var locations = addedPlaces
        if let currentPlace1 = currentPlace {
            if !locations.contains(currentPlace1) {
                locations.insert(currentPlace1, at: 0)
            }
        }
        for place in locations {
            let mapAnnotation = QMapAnnotation()
            place.annotation = mapAnnotation
            mapAnnotation.coordinate = place.location
            mapAnnotation.title = place.title
            mapAnnotation.subtitle = place.address
            mapAnnotation.place = place
            mapView.addAnnotation(mapAnnotation)
        }
        mapView.zoomToPins()
    }
    
    func disableEventEditing() {
        saveButton.isHidden = true
        showBackgroundsButton.isHidden = true
        backgroundsCollectionView.isHidden = true
        nameTextField.isUserInteractionEnabled = false
        timeTextField.isUserInteractionEnabled = false
        dateTextField.isUserInteractionEnabled = false
        addressTextField.isUserInteractionEnabled = false
        detailsTextView.isEditable = false
        self.addressesTableView_height.constant = 45
        self.photosView_height.constant = 300
        self.addressesTableView.isHidden = true
        self.rsvpView.isHidden = false
    }
    
    func enableEventEditing() {
        saveButton.isHidden = true
        showBackgroundsButton.isHidden = false
        backgroundsCollectionView.isHidden = false
        nameTextField.isUserInteractionEnabled = true
        timeTextField.isUserInteractionEnabled = true
        dateTextField.isUserInteractionEnabled = true
        addressTextField.isUserInteractionEnabled = true
        mapContainerView.isUserInteractionEnabled = true
        detailsTextView.isEditable = true
    }
    
    func setRSVPValues() {
        let rsvp = rsvpObject?.value(forKey: "rsvp") as? String ?? ""
        rsvpMaybeButton.backgroundColor = .white
        rsvpNoButton.backgroundColor = .white
        rsvpMaybeButton.backgroundColor = .white
        if rsvp == "yes" {
            rsvpYesButton.backgroundColor = UIColor(red: 0.33, green: 0.71, blue: 0.64, alpha: 1.0)
        }else if rsvp == "no" {
            rsvpNoButton.backgroundColor = UIColor(red: 0.88, green: 0.44, blue: 0.34, alpha: 1.0)
        }else if rsvp == "maybe" {
            rsvpMaybeButton.backgroundColor = UIColor(red: 0.97, green: 0.66, blue: 0.33, alpha: 1.0)
        }
        if PFUser.current()?.objectId == self.passedEvent?.user.objectId {
            rsvpYesButton.isEnabled = false
            rsvpNoButton.isEnabled = false
            rsvpMaybeButton.isEnabled = false
            rsvpYesButton.backgroundColor = UIColor(red: 0.33, green: 0.71, blue: 0.64, alpha: 1.0)
        }
    }
}

extension NewEventViewController {
    func isValidInput() -> Bool {
        var error = ""
        if (nameTextField.text ?? "") == "" {
            error = "Check Event Name"
        }else if (timeTextField.text ?? "") == "" {
            error = "Check Time"
        }else if (dateTextField.text ?? "") == "" {
            error = "Check Date"
        }else if currentPlace == nil {
            error = "Please add address"
        }
        if error != "" {
            let titleString = NSAttributedString(string: "Almost Finished!", attributes: [.font: UIFont(name: "JosefinSans-Bold", size: 18.0)!])
            let messageString = NSAttributedString(string: error, attributes: [.font: UIFont(name: "JosefinSans-Regular", size: 15.0)!])
            let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
            alert.setValue(titleString, forKey: "attributedTitle")
            alert.setValue(messageString, forKey: "attributedMessage")
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return false
        }
        return true
    }
}

extension NewEventViewController {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
    }
}

extension NewEventViewController: UpdateEventDelegate {
    func didUpdateEvent(event: Event) {
        self.passedEvent = event
    }
}
//MARK: - Text Field Delegate
extension NewEventViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        detailsTextView.resignFirstResponder()
        if textField == timeTextField {
            timePicker = UIDatePicker()
            timePicker.datePickerMode = .time
            timePicker.minuteInterval = 15
            timePicker.addTarget(self, action: #selector(didSelectTime(_:)), for: .valueChanged)
            if let date = Calendar.current.date(bySettingHour: 1, minute: 0, second: 0, of: Date()) {
                timePicker.date = date
            }
            textField.inputView = timePicker
        }else if textField == dateTextField {
            datePicker = UIDatePicker()
            datePicker.datePickerMode = .date
            datePicker.addTarget(self, action: #selector(didSelectDate(_:)), for: .valueChanged)
            textField.inputView = datePicker
        }else if textField == addressTextField {
//            self.scrollView.setContentOffset(CGPoint(x: 0, y: 300), animated: true)
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == addressTextField {
            var string1 = textField.text ?? ""
            string1.append(string)
            searchString = string1
            print(searchString)
            if searchString == "" {
//                addressesTableView_height.constant = 0
            }else {
//                addressesTableView_height.constant = 200
                NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(search), object: nil)
                self.perform(#selector(search), with: nil, afterDelay: 0.5)
            }
            UIView.animate(withDuration: 0.2) {
                self.view.layoutIfNeeded()
            }
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == addressTextField {
            addressesTableView_height.constant = 0
            UIView.animate(withDuration: 0.2) {
                self.view.layoutIfNeeded()
            }
        }
    }
}

extension NewEventViewController: TypeTagTextFieldDelegate {
    func typeTagTextField(_ textField: TypeTagTextField!, didSelectTag tag: String!) {
        if hierarchy == .DETAILS {
            return
        }
        ApiManager.shared.getImages(with: tag) { (images) in
            self.backgroundImages = images
            if self.backgroundImages.count > 0 {
                self.backgroundsCollectionView.isHidden = false
                self.showBackgroundsButton.isHidden = false
                self.backgroundsCollectionView.reloadData()
            }else {
                self.backgroundsCollectionView.isHidden = true
                self.showBackgroundsButton.isHidden = true
            }
        }
    }
    
    func typeTagTextField(_ textField: TypeTagTextField!, textDidChange text: String!) {
        nameLabel.text = text.capitalized
    }
}

extension NewEventViewController {
    @objc func didSelectScheduleTime(_ sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        let timeString = dateFormatter.string(from: sender.date)
        for textField in addScheduleAlert?.textFields ?? [] {
            if textField.isFirstResponder {
                textField.text = timeString
            }
        }
    }
    
    @objc func didSelectTime(_ sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        selectedTime = sender.date
        timeTextField.text = dateFormatter.string(from: sender.date)
    }
    
    @objc func didSelectDate(_ sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE, MMM dd, yyyy"
        selectedDate = sender.date
        dateTextField.text = dateFormatter.string(from: sender.date)
    }
}

extension NewEventViewController {
    @objc func search() {
        ApiManager.shared.getPlaces(searchString: searchString) { (places) in
            self.places = places
            if places.count == 0 {
                self.addressesTableView_height.constant = 0
            }else {
                self.scrollView.setContentOffset(CGPoint(x: 0, y: 250), animated: true)
                self.addressesTableView_height.constant = 200
            }
            UIView.animate(withDuration: 0.2, animations: {
                self.view.layoutIfNeeded()
            })
            self.addressesTableView.reloadData()
        }
    }
    
    func getLocationArray(from places: [Place]) -> [[String: Any]]{
        var locationsArray: [[String: Any]] = []
        for place in places {
            var dict: [String: Any] = [:]
            dict["placeId"] = place.placeId
            dict["title"] = place.title
            dict["address"] = place.address
            dict["latitude"] = "\(place.location.latitude)"
            dict["longitude"] = "\(place.location.longitude)"
            if let _ = place.photoURL {
                dict["photoUrl"] = place.photoURL.absoluteString
            }
            
            locationsArray.append(dict)
        }
        return locationsArray
    }
    
    func getRSVPObject() {
        let rsvpQ = PFQuery(className: "Invite")
        rsvpQ.whereKey("phoneNumber", equalTo: PFUser.current()?["phoneNumber"])
        rsvpQ.whereKey("event", equalTo: passedEvent)
        rsvpQ.getFirstObjectInBackground(block: { object, error in
            if object != nil {
                self.rsvpObject = object
                self.setRSVPValues()
            } else {
                var object = PFObject(className: "Invite")
                object.setValue(PFUser.current()?["phoneNumber"], forKey: "phoneNumber")
//                object.set(PFUser.current()["phoneNumber"], forKey: "phoneNumber")
                object.setObject(self.passedEvent, forKey: "event")
                object.setObject(NSNumber(value: false), forKey: "shouldSendSMS")
                object.saveInBackground(block: { (succeeded, error) in
                    if succeeded {
                        self.rsvpObject = object
                        self.setRSVPValues()
                    }
                })
            }
        })
    }
    
    func setPhotosArray(_ photosArray: PFRelation<PFObject>?) {
        let pQ = photosArray?.query()
        pQ?.includeKey("event")
        pQ?.findObjectsInBackground(block: { photos, error in
            self.eventPhotos = photos ?? []
            var urls: [String] = []
            for photo in photos ?? [] {
                if let url = (photo["image"] as? PFFile)?.url {
                    urls.append(url)
                }
            }
            self.photoUrls = urls
            self.photosCollectionView.reloadData()
        })
    }
    
    func savePhoto(with image: UIImage) {
        let photo = PFObject(className: "Photo")
        photo["image"] = PFFile(name: "photo.png", data: image.jpegData(compressionQuality: 0.5) ?? Data())
        photo["event"] = passedEvent
        photo["user"] = PFUser.current()
        SVProgressHUD.show()
        photo.saveInBackground(block: { succeeded, error in
            SVProgressHUD.dismiss()
            if error == nil && succeeded {
                let relation = self.passedEvent?.relation(forKey: "photos")
                relation?.add(photo)
                self.passedEvent?.saveInBackground(block: { succeeded, error in
                    SVProgressHUD.dismiss()
                    if succeeded {
                        if let photoCount = self.passedEvent?.photoCount {
                            let count = photoCount.intValue + 1
                            self.passedEvent?.photoCount = NSNumber(integerLiteral: count)
                        }else {
                            self.passedEvent?.photoCount = NSNumber(integerLiteral: 1)
                        }
                        
                        self.setPhotosArray(self.passedEvent?.photos)
                    } else {
                        self.passedEvent?.saveEventually()
                    }
                })
            }
        })
    }

}

extension NewEventViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.originalImage] as? UIImage {
            if self.pickerType == "background" {
                eventImageView.image = image
            }else {
                savePhoto(with: image)
            }
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func showImagePickerController() {
        let imagePicker = UIImagePickerController()
        imagePicker.mediaTypes = [kUTTypeImage as String]
        imagePicker.delegate = self
        let titleString = NSAttributedString(string: "Upload Your Own Image", attributes: [.font: UIFont(name: "JosefinSans-Bold", size: 18.0)!])
        
        let actionSheet = UIAlertController(title: "", message: nil, preferredStyle: .actionSheet)
        actionSheet.setValue(titleString, forKey: "attributedTitle")
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action) in
            imagePicker.sourceType = .photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }))
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            imagePicker.sourceType = .camera
            self.present(imagePicker, animated: true, completion: nil)
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
}

extension NewEventViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if hierarchy == .DETAILS {
            return CGSize.zero
        }
        if collectionView == backgroundsCollectionView {
            return CGSize(width: 160, height: 100)
        }else if collectionView == activitiesCollectionView {
            return CGSize(width: 100, height: 80)
        }else if collectionView == guestsCollectionView {
            return CGSize(width: 60, height: 75)
        }
        return CGSize.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionHeader {
            if collectionView == backgroundsCollectionView {
                let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "backgroundSectionHeader", for: indexPath) as! BackgroundSectionHeader
                sectionHeader.onPickImageAction = {
                    self.detailsTextView.resignFirstResponder()
                    self.pickerType = "background"
                    self.showImagePickerController()
                }
                return sectionHeader
            }else if collectionView == activitiesCollectionView {
                let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "activitySectionHeader", for: indexPath) as! ActivitySectionHeader
                sectionHeader.onAddActivity = {
                    self.detailsTextView.resignFirstResponder()
                    self.showAddSchedulePopup()
                }
                return sectionHeader
            }else if collectionView == guestsCollectionView {
                let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "guestSectionHeader", for: indexPath) as! GuestSectionHeader
                sectionHeader.onAddGuest = {
                    self.detailsTextView.resignFirstResponder()
                    self.showAddGuestsPicker()
                }
                return sectionHeader
            }
        }
        return UICollectionReusableView()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == backgroundsCollectionView {
            return CGSize(width: 160, height: 100)
        }else if collectionView == activitiesCollectionView {
            return CGSize(width: 150, height: 80)
        }else if collectionView == guestsCollectionView {
            return CGSize(width: 60, height: 75)
        }else if collectionView == photosCollectionView {
            return CGSize(width: 99.0, height: 99.0)
        }
        return CGSize.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == backgroundsCollectionView {
            return backgroundImages.count
        }else if collectionView == activitiesCollectionView {
            return schedulesArray.count
        }else if collectionView == guestsCollectionView {
            return guestsArray.count
        }else if collectionView == photosCollectionView {
            return photoUrls.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == backgroundsCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "backgroundCollectionCell", for: indexPath) as! BackgroundCollectionCell
            let imageUrlString = backgroundImages[indexPath.row]
            cell.imageView.sd_setShowActivityIndicatorView(true)
            cell.imageView.sd_setIndicatorStyle(UIActivityIndicatorView.Style.gray)
            cell.imageView.sd_setImage(with: URL(string: imageUrlString), completed: nil)
            return cell
        }else if collectionView == activitiesCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "activityCollectionCell", for: indexPath) as! ActivityCollectionCell
            let scheduleObject: ScheduledObject = self.schedulesArray[indexPath.row]
            cell.titleLabel.text = scheduleObject.title
            cell.timeLabel.text = "\(scheduleObject.startTime ?? "")\n\(scheduleObject.endTime ?? "")"
            return cell
        }else if collectionView == guestsCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "guestCollectionCell", for: indexPath) as! GuestCollectionCell
            let guest = guestsArray[indexPath.row]
            cell.nameLabel.text = guest["name"] as? String ?? ""
            let phoneNumber = guest["phoneNumber"] as? String ?? ""
            var isProfilePic = false
            if let profilePic = profilePics[phoneNumber] as? String {
                isProfilePic = true
                cell.profileImageView.sd_setImage(with: URL(string: profilePic), completed: nil)
            }else {
                cell.profileImageView.image = nil
            }
            let rsvp = guest["rsvp"] as? String ?? ""
            cell.rsvpImageView.image = UIImage(named: "rsvp-]\(rsvp)")
            cell.rsvpImageView.backgroundColor = self.color(forRSVP: rsvp)
            cell.containerView.backgroundColor = colorsArray[indexPath.row % colorsArray.count]
            
            if !isProfilePic {
                cell.profileImageView.isHidden = true
                let guestName = guest["name"] as? String ?? ""
                let components = guestName.components(separatedBy: " ")
                if components.count > 1 {
                    let firstInitial: String = components[0].count > 0 ? String(components[0].prefix(1)): ""
                    let secondInitial: String = components[1].count > 0 ? String(components[1].prefix(1)): ""
                    cell.userInitialsLAbel.text = "\(firstInitial)\(secondInitial)"
                }else {
                    cell.userInitialsLAbel.text = components[0].count > 0 ? String(components[0].prefix(1)): ""
                }
            }else {
                cell.profileImageView.isHidden = false
                cell.userInitialsLAbel.text = ""
            }
            return cell
            
        }else if collectionView == photosCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "photoCell", for: indexPath)
            let imageView = cell.viewWithTag(3001) as! UIImageView
            let urlString = self.photoUrls[indexPath.row]
            imageView.sd_setImage(with: URL(string: urlString), completed: nil)
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.detailsTextView.resignFirstResponder()
        if collectionView == backgroundsCollectionView {
            if hierarchy == .DETAILS {
                return
            }
            if let cell = collectionView.cellForItem(at: indexPath) as? BackgroundCollectionCell {
                let image = cell.imageView.image
                backgroundsCollectionView.isHidden = true
                showBackgroundsButton.isHidden = false
                UIView.transition(with: eventImageView, duration: 0.3, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
                    self.eventImageView.image = image
                }, completion: nil)
            }
        }else if collectionView == activitiesCollectionView {
            if hierarchy == .DETAILS {
                return
            }
            let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            actionSheet.addAction(UIAlertAction(title: "Edit", style: .default, handler: { (action) in
                self.showEditSchedulePopup(for: indexPath.row)
            }))
            actionSheet.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (action) in
                self.schedulesArray.remove(at: indexPath.row)
                self.activitiesCollectionView.reloadData()
            }))
            actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            present(actionSheet, animated: true, completion: nil)
        }else if collectionView == guestsCollectionView {
            if hierarchy == .DETAILS {
                return
            }
            let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            actionSheet.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (action) in
                self.guestsArray.remove(at: indexPath.row)
                self.guestsCollectionView.reloadData()
            }))
            actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
                
            }))
            self.present(actionSheet, animated: true, completion: nil)
        }else if collectionView == photosCollectionView {
            let photoViewer = LPPhotoViewer()
            photoViewer.imgArr = self.photoUrls
            photoViewer.currentIndex = indexPath.row
            self.present(photoViewer, animated: true, completion: nil)
        }
    }
}

extension NewEventViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "placeCell", for: indexPath)
        cell.imageView?.image = UIImage(named: "local-pin")
        cell.textLabel?.text = places[indexPath.row].title
        cell.imageView?.contentMode = .scaleAspectFit
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        currentPlace = places[indexPath.row]
        addressTextField.text = currentPlace?.title ?? ""
        addressTextField.resignFirstResponder()
        addPinsToMap()
    }
}

extension NewEventViewController: MapViewControllerDelegate {
    func mapViewController(_ mapVC: MapViewController!, didDismissWithPlaces places: [Any]!) {
        self.addedPlaces = places as? [Place] ?? []
        addPinsToMap()
    }
}

extension NewEventViewController {
    func getPlaces(from locations: [[String: Any]]) {
        if locations.count == 0 {
            if self.addedPlaces.count > 0 {
                currentPlace = addedPlaces[0]
                addedPlaces.removeFirst()
                addressTextField.text = currentPlace?.title ?? ""
            }
            addPinsToMap()
            return
        }
        var locations1 = locations
        var location = locations1.removeLast()
        if let placeId = location["placeId"] as? String {
            ApiManager.shared.getPlace(with: placeId) { (place) in
                if let place = place {
                    self.addedPlaces.append(place)
                }
                self.getPlaces(from: locations1)
            }
        }else {
            let place = Place()
            place.title = location["title"] as? String ?? ""
            let latitude = location["latitude"] as? String ?? "0.0"
            let longitude = location["longitude"] as? String ?? "0.0"
            place.location = CLLocationCoordinate2D(latitude: CLLocationDegrees(Double(latitude)!), longitude: CLLocationDegrees(Double(longitude)!))
            place.type = GPlaceTypeNote
            self.addedPlaces.append(place)
            self.getPlaces(from: locations1)
        }
    }
    
//    func getPlacesByIDs(_ ids: [String]) {
//
//        var placesArray: [Place] = []
//
//        var completedRequests = 0
//
//
//        for id in ids {
//            ApiManager.shared.getPlace(with: id) { (place) in
//                completedRequests += 1
//                guard let place = place else { return }
//                placesArray.append(place)
//                if completedRequests == ids.count {
//                    self.fetchDidComplete(with: placesArray)
//                }
//            }
//        }
//    }
//
//    func fetchDidComplete(with placesArray: [Place]) {
//        var placeIds: [String] = []
//        let places: [[String: Any]] = passedEvent?.locations as? [[String:Any]] ?? []
//        for location in places {
//            if let placeId = location["placeId"] as? String {
//                placeIds.append(placeId)
//            }
//        }
//        var newPlacesArray: [Place] = []
//        for placeId in placeIds {
//            for place in placesArray {
//                if place.placeId == placeId {
//                    newPlacesArray.append(place)
//                    break
//                }
//            }
//        }
//        for i in 0..<places.count {
//            if let _ = places[i]["placeId"] as? String {
//
//            }else {
//                let place = Place()
//                place.title = places[i]["title"] as? String ?? ""
//                let latitude = places[i]["latitude"] as? String ?? "0.0"
//                let longitude = places[i]["longitude"] as? String ?? "0.0"
//                place.location = CLLocationCoordinate2D(latitude: CLLocationDegrees(Double(latitude)!), longitude: CLLocationDegrees(Double(longitude)!))
//                place.type = GPlaceTypeNote
//                if newPlacesArray.count < i {
//                    newPlacesArray.append(place)
//                }else {
//                    newPlacesArray.insert(place, at: i)
//                }
//
//            }
//        }
//        self.addedPlaces = newPlacesArray
//        if addedPlaces.count > 0 {
//            self.currentPlace = addedPlaces[0]
//            addedPlaces.remove(at: 0)
//            addressTextField.text = currentPlace?.title ?? ""
//        }
//        addPinsToMap()
//    }
}

extension NewEventViewController {
    func showAddSchedulePopup() {
        let titleString = NSAttributedString(string: "New Activity", attributes: [.font: UIFont(name: "JosefinSans-Bold", size: 18.0)!])
        addScheduleAlert = UIAlertController(title: "", message: nil, preferredStyle: .alert)
        addScheduleAlert.setValue(titleString, forKey: "attributedTitle")
        addScheduleAlert.addTextField { (textField) in
            textField.font = UIFont(name: "JosefinSans-Regular", size: 15.0)
            textField.placeholder = "What's Happening?"
            textField.autocapitalizationType = UITextAutocapitalizationType.words
        }
        addScheduleAlert.addTextField { (textField) in
            if self.schedulesArray.count > 0 {
                let lastObject = self.schedulesArray.last
                textField.text = lastObject?.endTime
            }
            textField.font = UIFont(name: "JosefinSans-Regular", size: 15.0)
            textField.placeholder = "From"
            self.timePicker = UIDatePicker()
            self.timePicker.datePickerMode = .time
            self.timePicker.minuteInterval = 5
            self.timePicker.addTarget(self, action: #selector(self.didSelectScheduleTime(_:)), for: .valueChanged)
            
            textField.inputView = self.timePicker
        }
        addScheduleAlert.addTextField { (textField) in
            textField.font = UIFont(name: "JosefinSans-Regular", size: 15.0)
            textField.placeholder = "To"
            self.timePicker = UIDatePicker()
            self.timePicker.datePickerMode = .time
            self.timePicker.minuteInterval = 5
            self.timePicker.addTarget(self, action: #selector(self.didSelectScheduleTime(_:)), for: .valueChanged)
            
            textField.inputView = self.timePicker
        }
        addScheduleAlert.addAction(UIAlertAction(title: "Add", style: .default, handler: { (action) in
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "h:mm a"
            let textFields: [UITextField] = self.addScheduleAlert.textFields ?? []
            let text0 = textFields[0].text ?? ""
            let text1 = textFields[1].text ?? ""
            let text2 = textFields[2].text ?? ""
            if text1 == "" || text2 == "" {
                let newScheduleObject: ScheduledObject = ScheduledObject(title: text0, startTime: text1, endTime: text2)
                self.schedulesArray.append(newScheduleObject)
                self.activitiesCollectionView.reloadData()
            }else {
                let time1 = dateFormatter.date(from: text1)
                let time2 = dateFormatter.date(from: text2)
                if time2! < time1! {
                    let titleString = NSAttributedString(string: "Error", attributes: [.font: UIFont(name: "JosefinSans-Bold", size: 18.0)!])
                    let messageString = NSAttributedString(string: "End times must be after start times...", attributes: [.font: UIFont(name: "JosefinSans-Regular", size: 15.0)!])
                    let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
                    alert.setValue(titleString, forKey: "attributedTitle")
                    alert.setValue(messageString, forKey: "attributedMessage")
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }else {
                    let newScheduleObject: ScheduledObject = ScheduledObject(title: text0, startTime: text1, endTime: text2)
                    self.schedulesArray.append(newScheduleObject)
                    self.activitiesCollectionView.reloadData()
                }
            }
        }))
        addScheduleAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(addScheduleAlert, animated: true, completion: nil)
    }
    
    func showEditSchedulePopup(for index: Int) {
        let titleString = NSAttributedString(string: "New Schedule Item", attributes: [.font: UIFont(name: "JosefinSans-Bold", size: 18.0)!])
        addScheduleAlert = UIAlertController(title: "", message: nil, preferredStyle: .alert)
        addScheduleAlert.setValue(titleString, forKey: "attributedTitle")
        addScheduleAlert.addTextField { (textField) in
            textField.placeholder = "Title"
            textField.font = UIFont(name: "JosefinSans-Regular", size: 15.0)
            textField.autocapitalizationType = UITextAutocapitalizationType.words
            textField.text = self.schedulesArray[index].title
        }
        addScheduleAlert.addTextField { (textField) in
            
            textField.placeholder = "Start Time"
            textField.font = UIFont(name: "JosefinSans-Regular", size: 15.0)
            self.timePicker = UIDatePicker()
            self.timePicker.datePickerMode = .time
            self.timePicker.minuteInterval = 5
            self.timePicker.addTarget(self, action: #selector(self.didSelectScheduleTime(_:)), for: .valueChanged)
            textField.inputView = self.timePicker
            textField.text = self.schedulesArray[index].startTime
        }
        addScheduleAlert.addTextField { (textField) in
            textField.placeholder = "End Time"
            textField.font = UIFont(name: "JosefinSans-Regular", size: 15.0)
            self.timePicker = UIDatePicker()
            self.timePicker.datePickerMode = .time
            self.timePicker.minuteInterval = 5
            self.timePicker.addTarget(self, action: #selector(self.didSelectScheduleTime(_:)), for: .valueChanged)
            textField.inputView = self.timePicker
            textField.text = self.schedulesArray[index].endTime
        }
        addScheduleAlert.addAction(UIAlertAction(title: "Save", style: .default, handler: { (action) in
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "h:mm a"
            let textFields: [UITextField] = self.addScheduleAlert.textFields ?? []
            let text0 = textFields[0].text ?? ""
            let text1 = textFields[1].text ?? ""
            let text2 = textFields[2].text ?? ""
            if text1 == "" || text2 == "" {
                self.schedulesArray[index].title = text0
                self.schedulesArray[index].startTime = text1
                self.schedulesArray[index].endTime = text2
                self.activitiesCollectionView.reloadData()
            }else {
                let time1 = dateFormatter.date(from: text1)
                let time2 = dateFormatter.date(from: text2)
                if time2! < time1! {
                    let titleString = NSAttributedString(string: "Error", attributes: [.font: UIFont(name: "JosefinSans-Bold", size: 18.0)!])
                    let messageString = NSAttributedString(string: "End times must be after start times...", attributes: [.font: UIFont(name: "JosefinSans-Regular", size: 15.0)!])
                    let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
                    alert.setValue(titleString, forKey: "attributedTitle")
                    alert.setValue(messageString, forKey: "attributedMessage")
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }else {
                    self.schedulesArray[index].title = text0
                    self.schedulesArray[index].startTime = text1
                    self.schedulesArray[index].endTime = text2
                    self.activitiesCollectionView.reloadData()
                }
            }
        }))
        addScheduleAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(addScheduleAlert, animated: true, completion: nil)
    }
}

extension NewEventViewController {
    
    func showAddGuestsPicker() {
        let titleString = NSAttributedString(string: "Contact Groups\n\n\n\n\n\n", attributes: [.font: UIFont(name: "JosefinSans-Bold", size: 18.0)!])
        contactsPickerAlert = UIAlertController(title: "", message: nil, preferredStyle: .actionSheet)
        contactsPickerAlert.setValue(titleString, forKey: "attributedTitle")
        contactGroupView = contactsGroupCollectionView(frame: CGRect(x: 8, y: 16, width: contactsPickerAlert.view.bounds.size.width - 24, height: 100))
        contactGroupView.pickerDelegate = self
        contactsPickerAlert.view.addSubview(contactGroupView)
        
        contactsPickerAlert.addAction(UIAlertAction(title: "Open My Contacts", style: .default, handler: { (action) in
            self.creatingGroup = false
            if self.modalWindow == nil {
                self.modalWindow = UIWindow(frame: UIScreen.main.bounds)
                self.modalWindow?.rootViewController = UIViewController()
                self.modalWindow?.windowLevel = UIWindow.Level.alert + 1
            }
            let vc = EPContactsPicker(delegate: self, multiSelection: true)
            let nvc = UINavigationController(rootViewController: vc)
            self.modalWindow?.makeKeyAndVisible()
            self.modalWindow?.rootViewController!.present(nvc, animated: true, completion: nil)
        }))
        contactsPickerAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(contactsPickerAlert, animated: true, completion: nil)
    }
    
    func getGuestsProfilePics() {
        let usersQuery = PFUser.query()
        var phoneNumbers: [String] = []
        for guest in self.guestsArray {
            let phoneNumber = guest["phoneNumber"] as? String ?? ""
            phoneNumbers.append(phoneNumber)
        }
        usersQuery?.whereKey("phoneNumber", containedIn: phoneNumbers)
        usersQuery?.findObjectsInBackground(block: { objects, error in
            if error == nil {
                for profile in objects ?? [] {
                    guard let profile = profile as? PFUser else {
                        continue
                    }
                    let picFile = profile["profilePicture"] as? PFFile
                    if picFile != nil {
                        let phone = profile["PhoneNumber"] as? String ?? ""
                        self.profilePics[phone] = picFile?.url
                    }
                }
                self.guestsCollectionView.reloadData()
            }
        })
    }
    
    func color(forRSVP rsvp: String?) -> UIColor? {
        if (rsvp == "yes") {
            return UIColor(red: 0.33, green: 0.71, blue: 0.64, alpha: 1.0)
        } else if (rsvp == "no") {
            return UIColor(red: 0.88, green: 0.44, blue: 0.34, alpha: 1.0)
        } else if (rsvp == "maybe") {
            return UIColor(red: 0.97, green: 0.66, blue: 0.33, alpha: 1.0)
        } else {
            return UIColor.clear
        }
    }
}

extension NewEventViewController: contactGroupCVDelegate {
    func contactGroupPicker(_ contactGroupPicker: contactsGroupCollectionView!, didSelectGroups groupsArray: [Any]!) {
        contactsPickerAlert.dismiss(animated: true, completion: {
            self.modalWindow = nil
        })
        let groupsArray1 = groupsArray as? [[String: Any]] ?? []
        self.guestsArray.append(contentsOf: groupsArray1)
        self.guestsCollectionView.reloadData()
        
    }
    
    func contactGroupPickerCreateNewGroup(_ contactGroupPicker: contactsGroupCollectionView!) {
        creatingGroup = true
        if self.modalWindow == nil {
            self.modalWindow = UIWindow(frame: UIScreen.main.bounds)
            self.modalWindow?.rootViewController = UIViewController()
            self.modalWindow?.windowLevel = UIWindow.Level.alert + 1
        }
        let vc = EPContactsPicker(delegate: self, multiSelection: true)
        let nvc = UINavigationController(rootViewController: vc)
        self.modalWindow?.makeKeyAndVisible()
        self.modalWindow?.rootViewController!.present(nvc, animated: true, completion: nil)
    }
}

extension NewEventViewController: EPPickerDelegate {
    func epContactPicker(contactPicker: EPContactsPicker, didSelectMultipleContacts contacts: [Any]) {
        contactPicker.dismiss(animated: true, completion: nil)
        self.modalWindow = nil
        if creatingGroup {
            var newGuests: [[String: Any]] = []
            for contact in contacts {
                let newContact = contact as! EPContact
                let name = newContact.firstName + " " + newContact.lastName
                let phoneNumber = newContact.phoneNumber?.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "") ?? ""
                newGuests.append(["name": name, "phoneNumber": phoneNumber])
            }
            let titleString = NSAttributedString(string: "New Group", attributes: [.font: UIFont(name: "JosefinSans-Bold", size: 18.0)!])
            let alert = UIAlertController(title: "", message: nil, preferredStyle: .alert)
            alert.setValue(titleString, forKey: "attributedTitle")
            alert.addTextField { (textField) in
                textField.font = UIFont(name: "JosefinSans-Regular", size: 15.0)
            }
            alert.addAction(UIAlertAction(title: "Create Group", style: .default, handler: { (action) in
                self.modalWindow = nil
                let contactGroup = PFObject(className: "ContactGroup")
                contactGroup["name"] = alert.textFields?[0].text ?? ""
                contactGroup["owner"] = PFUser.current()
                contactGroup["contacts"] = newGuests
                contactGroup.saveInBackground(block: { succeeded, error in
                    self.contactGroupView.refresh()
                })
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
                self.modalWindow = nil
            }))
//            self.present(alert, animated: true, completion: nil)
            if self.modalWindow == nil {
                self.modalWindow = UIWindow(frame: UIScreen.main.bounds)
                self.modalWindow?.rootViewController = UIViewController()
                self.modalWindow?.windowLevel = UIWindow.Level.alert + 1
            }
            self.modalWindow?.makeKeyAndVisible()
            self.modalWindow?.rootViewController!.present(alert, animated: true, completion: nil)
        }else {
            var newGuests: [[String: Any]] = []
            for contact in contacts {
                let newContact = contact as! EPContact
                let name = newContact.firstName + " " + newContact.lastName
                let phoneNumber = newContact.phoneNumber?.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "") ?? ""
                newGuests.append(["name": name, "phoneNumber": phoneNumber, "rsvp": "none"])
            }
            self.guestsArray.append(contentsOf: newGuests)
            self.guestsCollectionView.reloadData()
        }
    }
    
    func epContactPicker(contactPicker: EPContactsPicker, didSelectContact contact: Any) {
        contactsPickerAlert.dismiss(animated: true) {
            self.modalWindow = nil
        }
    }
    
    func epContactPicker(contactPicker: EPContactsPicker, didCancel error: NSError) {
        contactPicker.dismiss(animated: true, completion: nil)
        self.modalWindow = nil
    }
    
    func epContactPicker(contactPicker: EPContactsPicker, didContactFetchFailed error: NSError) {
        contactsPickerAlert.dismiss(animated: true) {
            self.modalWindow = nil
        }
    }
}

extension UIViewController {
    func showAlertView(with title: String, and message: String) {
        let attributedTitle = NSAttributedString(string: title, attributes: [.font: UIFont(name: appFontBold, size: 18.0)!])
        let attributedMessage = NSAttributedString(string: message, attributes: [.font: UIFont(name: appFontRegular, size: 15.0)!])
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        alert.setValue(attributedTitle, forKey: "attributedTitle")
        alert.setValue(attributedMessage, forKey: "attributedMessage")
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

