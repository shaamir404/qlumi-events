//
//  NewEventTableViewController.m
//  Qlumi2
//
//  Created by William Smillie on 12/13/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import "NewEventTableViewController.h"
#import "HomeViewController.h"

#import <Parse/Parse.h>

#import "Qlumi-Swift.h"

@interface NewEventTableViewController ()<EPPickerDelegate>{
    BOOL isFirstLoad;
    
    MapViewController *mapViewController;
    NewEventKeyboardToolbar *keyboardToolbar;
    
    NSMutableArray *rows;
    NSMutableArray *responders;
    NSMutableArray *addedPlaces;
    NSMutableArray *addedGuests;

    int currentResponderIndex;
    QResponder *currentResponder;
    
    NSDateFormatter *dateFormatter;
    NSDateFormatter *timeFormatter;

    LEColorScheme *colorScheme;
    
    NSDate *selectedDate;
    NSDate *selectedTime;

    /*EventDetailsCell *detailsCell;
    EventLocationsCell *locationsCell;
    EventAboutCell *aboutCell;
    EventScheduleCell *scheduleCell;
    EventGuestsCell *guestsCell;*/
}

@end

@implementation NewEventTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
        
    isFirstLoad = YES;
        
    [self updateColorsForImage:[UIImage imageNamed:@"photo-placeholder"]];
    
    dateFormatter = [[NSDateFormatter alloc] init];
    timeFormatter = [[NSDateFormatter alloc] init];
    
    [timeFormatter setDateFormat:@"h:mm a"];
    [dateFormatter setDateFormat:@"EEEE, MMM d, yyyy"];


    responders = [[NSMutableArray alloc] init];
    rows = [[NSMutableArray alloc] initWithObjects:@"EventDetailsCell", @"EventLocationsCell", @"EventAboutCell", @"EventScheduleCell", @"EventGuestsCell", nil];
    
    [rows addObject:@"SaveEventTableViewCell"];
    if (self.passedEvent) {
        [rows addObject:@"EventDeleteCell"];
    }
    
    [self registerCells];
    [self setupUI];

    if (self.passedEvent) {
        self.headerView.eventLabel.text = self.passedEvent.title;
        
        [self.headerView.imageView sd_setImageWithURL:[NSURL URLWithString:[self.passedEvent image].url] placeholderImage:[UIImage imageNamed:@"photo-placeholder"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            [self updateColorsForImage:image];
            [self.headerView setNeedsLayout];
            [self.headerView setNeedsDisplay];
        }];
        
        NSMutableArray *placeIds = [[NSMutableArray alloc] init];
        for (NSDictionary *location in self.passedEvent.locations) {
            if (location[@"placeId"]) {
                [placeIds addObject:location[@"placeId"]];
            }
        }
        [self getPlacesByIDs:placeIds];
        
        
        [self.tableView reloadData];
    }

    [self goToIndex:0];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if (isFirstLoad) {
        isFirstLoad = NO;
        [keyboardToolbar.textField becomeFirstResponder];
        [self goToIndex:0];
    }
}

- (UIView *)inputAccessoryView{
    return keyboardToolbar;
}
-(BOOL)canBecomeFirstResponder{
    return true;
}


#pragma mark - ToolBar
- (void)keyboardToolbar:(NewEventKeyboardToolbar *)toolbar didRequestNextResponder:(id)sender{
    if (currentResponderIndex < responders.count){
        currentResponderIndex++;
    }
    
    if (currentResponderIndex == 6) {
        [toolbar.textField resignFirstResponder];
        toolbar.hidden = YES;
        [[(QResponder *)responders[currentResponderIndex-1] view] setBackgroundColor:[AppDelegate QBlue]];
    }else{
        toolbar.hidden = NO;
        [self goToIndex:currentResponderIndex];
    }
}

- (void)keyboardToolbar:(NewEventKeyboardToolbar *)toolbar didRequestLastResponder:(id)sender{
    if (currentResponderIndex > 0){
        currentResponderIndex--;
    }
    
    [self goToIndex:currentResponderIndex];
}

- (void)keyboardToolbar:(NewEventKeyboardToolbar *)toolbar didSelectImage:(UIImage *)image{
    [UIView transitionWithView:self.headerView.imageView duration:0.3f options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        self.headerView.imageView.image = image;
        self.headerView.eventLabel.backgroundColor = [UIColor clearColor];
        [self updateColorsForImage:image];
    }completion:nil];
}

- (void)keyboardToolbar:(NewEventKeyboardToolbar *)toolbar textDidChange:(NSString *)text{
    if ([currentResponder.view isKindOfClass:[UITextView class]]) {
        [(UITextView *)currentResponder.view setText:text];
    }else if ([currentResponder.view isKindOfClass:[UILabel class]]) {
        if (currentResponderIndex == 5) {
            [(UILabel *)currentResponder.view setText:[NSString stringWithFormat:@"Hosted By %@", text]];
        }else{
            [(UILabel *)currentResponder.view setText:text];
        }
    }
}


#pragma mark - UITableView Datasource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return rows.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [NSClassFromString(rows[indexPath.row]) sectionHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = [NSString stringWithFormat:@"%@", rows[indexPath.row]];
    
    EventTableViewBaseCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    [cell setTint:colorScheme.backgroundColor];
    
    //cell.separatorInset = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, CGFLOAT_MAX);
   // cell.separatorInset =  UIEdgeInsetsMake(0.0, 1000.0, 0.0, 0.0);
    
    if ([cellIdentifier isEqualToString:@"EventLocationsCell"]) {
        EventLocationsCell *locationsCell = (EventLocationsCell *)cell;
        
        for (Place *p in addedPlaces) {
            QMapAnnotation *annotation = [[QMapAnnotation alloc] init];
            p.annotation = annotation;
            [annotation setCoordinate:p.location];
            [annotation setTitle:p.title]; //You can set the subtitle too
            [annotation setSubtitle:p.address];
            annotation.place = p;
            [locationsCell.mapView addAnnotation:annotation];
        }
        [locationsCell.mapView zoomToPins];

    }else if ([cellIdentifier isEqualToString:@"EventDetailsCell"]){
        EventDetailsCell *detailsCell = (EventDetailsCell *)cell;
        [detailsCell setEditingEnabled:YES];
        if ([[PFUser currentUser].objectId isEqualToString:self.passedEvent.user.objectId]) {
            detailsCell.maybeButton.enabled = NO;
            detailsCell.noButton.enabled = NO;
            detailsCell.yesButton.enabled = NO;
            
            detailsCell.maybeButton.alpha = 0.5;
            detailsCell.noButton.alpha = 0.5;
            detailsCell.yesButton.alpha = 0.5;
        }
        
       
        
        
        UIDatePicker *datePicker = [[UIDatePicker alloc] init];
        datePicker.datePickerMode = UIDatePickerModeDate;
        datePicker.minimumDate = [NSDate new];
        UIDatePicker *timePicker = [[UIDatePicker alloc] init];
        timePicker.datePickerMode = UIDatePickerModeTime;
        timePicker.minuteInterval = 15;

        [datePicker addTarget:self action:@selector(pickerChanged:) forControlEvents:UIControlEventValueChanged];
        [timePicker addTarget:self action:@selector(pickerChanged:) forControlEvents:UIControlEventValueChanged];

        
        [self addResponderIfNeeded:[[QResponder alloc] initWithView:detailsCell.dateLabel placeholder:@"Enter Date" andInputSource:datePicker]];
        [self addResponderIfNeeded:[[QResponder alloc] initWithView:detailsCell.timeLabel placeholder:@"Enter Time" andInputSource:timePicker]];
//        [self addResponderIfNeeded:[[QResponder alloc] initWithView:detailsCell.venueLabel keyboardType:UIKeyboardTypeDefault andPlaceholder:@"Venue Name"]];
        [self addResponderIfNeeded:[[QResponder alloc] initWithView:detailsCell.addressLabel keyboardType:UIKeyboardTypeDefault andPlaceholder:@"Venue Address"]];
        
        if (selectedDate) {
            detailsCell.dateLabel.text = [dateFormatter stringFromDate:selectedDate];
        }else{
            if (self.passedEvent.date) {
                detailsCell.dateLabel.text = [dateFormatter stringFromDate:self.passedEvent.date];
            }
        }
        
        if (selectedTime) {
            detailsCell.timeLabel.text = [timeFormatter stringFromDate:selectedTime];
        }else{
            if (self.passedEvent.date) {
                detailsCell.timeLabel.text = [timeFormatter stringFromDate:self.passedEvent.date];
            }
        }


        if (self.passedEvent) {
            if (self.passedEvent.locations.count > 0) {
                if ([self.passedEvent.locations[0] objectForKey:@"title"] && [self.passedEvent.locations[0] objectForKey:@"address"]) {
//                    detailsCell.venueLabel.text = [self.passedEvent.locations[0] objectForKey:@"title"];
                    detailsCell.addressLabel.text = [self.passedEvent.locations[0] objectForKey:@"address"];
                }
            }
        }

    }else if ([cellIdentifier isEqualToString:@"EventAboutCell"]){
        EventAboutCell *aboutCell = (EventAboutCell *)cell;
        [self addResponderIfNeeded:[[QResponder alloc] initWithView:aboutCell.textView keyboardType:UIKeyboardTypeDefault andPlaceholder:@"Event Description"]];
        
        if (self.passedEvent) {
            aboutCell.textView.text = self.passedEvent.about;
        }
        
    }else if ([cellIdentifier isEqualToString:@"EventScheduleCell"]){
        EventScheduleCell *scheduleCell = (EventScheduleCell *)cell;
        [scheduleCell setEditingEnabled:YES];
        if (self.passedEvent) {
            NSMutableArray *objects = [[NSMutableArray alloc] init];
            for (NSDictionary *i in self.passedEvent.schedule) {
                ScheduledObject *o = [[ScheduledObject alloc] init];
                o.title = i[@"title"];
                o.startTime = i[@"startTime"];
                o.endTime = i[@"endTime"];
                [objects addObject:o];
            }
            [scheduleCell setEditingEnabled:YES];
            [scheduleCell setScheduleArray:objects];
        }
        [[scheduleCell collectionView] reloadData];
        
    }else if ([cellIdentifier isEqualToString:@"EventGuestsCell"]){
        EventGuestsCell *guestsCell = (EventGuestsCell *)cell;
        [guestsCell setEditingEnabled:YES];
        [guestsCell setDelegate:self];
        if (self.passedEvent) {
            [guestsCell setGuestsArray:[self.passedEvent.guests copy]];
        }
        [[guestsCell collectionView] reloadData];
    }else if ([cellIdentifier isEqualToString:@"EventPhotosCell"]){
        [(EventDeleteCell *)cell setDelegate:self];
    }else if ([cellIdentifier isEqualToString:@"SaveEventTableViewCell"]){
        [(SaveEventTableViewCell *)cell setDelegate:self];
    }else if ([cellIdentifier isEqualToString:@"EventDeleteCell"]){
        [(EventDeleteCell *)cell setDelegate:self];
    }

    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 1) {
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:mapViewController];
        [self presentViewController:nav animated:YES completion:^{
            EventLocationsCell *locations = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];

            
            mapViewController.searchController.searchBar.placeholder = @"Select Event Locations";
            mapViewController.addedPlacesArray = [addedPlaces mutableCopy];
            [mapViewController.mapView addAnnotations:locations.mapView.annotations];
            [mapViewController.mapView setRegion:locations.mapView.region];
            [mapViewController.mapListController.tableView reloadData];
            mapViewController.navigationItem.leftBarButtonItem.title = @"Next";
            if (mapViewController.addedPlacesArray.count > 0) {
                [mapViewController.segmentedControl setSelectedSegmentIndex:1];
                [mapViewController.segmentedControl setTitle:[NSString stringWithFormat:@"Tagged (%lu)", (unsigned long)mapViewController.addedPlacesArray.count] forSegmentAtIndex:1];
            }else{
                [mapViewController.segmentedControl setSelectedSegmentIndex:0];
            }
            mapViewController.delegate = self;
            
            UIAlertController *annoyingAlertRequiredByClients = [UIAlertController alertControllerWithTitle:@"Enter the event location or address above" message:nil preferredStyle:UIAlertControllerStyleAlert];
            [annoyingAlertRequiredByClients addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
            [nav presentViewController:annoyingAlertRequiredByClients animated:YES completion:nil];
        }];
    }

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}



#pragma mark - MapViewControllerDelegate

-(void)mapViewController:(MapViewController *)mapVC didDismissWithPlaces:(NSArray *)places{
    
    /*//Add Notes
    for (int i=0; i<self.passedEvent.locations.count; i++) {
        NSDictionary *location = self.passedEvent.locations[i];
        if (!location[@"placeId"]) {
            Place *p = [[Place alloc] init];
            p.location = CLLocationCoordinate2DMake([location[@"latitude"] floatValue], [location[@"longitude"] floatValue]);
            p.title = location[@"title"];
            p.type = GPlaceTypeNote;
            
            [places insertObject:p atIndex:i];
        }
    }*/
    
    addedPlaces = [places copy];
    
    EventDetailsCell *detailsCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    if (addedPlaces.count > 0) {
        detailsCell.addressLabel.text = [(Place *)addedPlaces[0] address];
//        detailsCell.venueLabel.text = [(Place *)addedPlaces[0] title];
    }

//    EventLocationsCell *cell = [self.tableView cellForRowAtIndexPath:];
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:1 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
//    [cell.mapView removeAnnotations:cell.mapView.annotations];
    
    
    [self goToIndex:5];
}

#pragma mark - UI Setup

-(void)setupUI{
    // Map View Controller
    mapViewController = [[MapViewController alloc] initWithEditingEnabled:YES andSearchingEnabled:YES];
    mapViewController.delegate = self;

    
    //Keyboard ToolBar and ImagePicker
    keyboardToolbar = [[[NSBundle mainBundle] loadNibNamed:@"NewEventKeyboardToolbar" owner:self options:nil] objectAtIndex:0];
    keyboardToolbar.delegate = self;

    //Header view
    self.headerView = [[EventHeaderView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 250)];
    self.headerView.expansionMode = GSKStretchyHeaderViewExpansionModeTopOnly;
    self.headerView.contentAnchor = GSKStretchyHeaderViewContentAnchorBottom;
    self.headerView.maximumContentHeight = 250;
    self.headerView.minimumContentHeight = 64;
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone) {
        if ((int)[[UIScreen mainScreen] nativeBounds].size.height == 2436){
            self.headerView.minimumContentHeight = 88;
        }
    }
    
    self.headerView.contentExpands = NO;
    self.headerView.stretchDelegate = self;
    [self.tableView addSubview:self.headerView];

    
    //TableView setup
    //self.tableView.separatorColor = [UIColor lightGrayColor];
    //self.tableView.backgroundColor = [UIColor darkGrayColor];
    self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;

    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    
    
    //BarButtons
    UIBarButtonItem *closeButton = [[UIBarButtonItem alloc] initWithTitle:@"❮ Back" style:UIBarButtonItemStyleDone target:self action:@selector(dismiss:)];
    [closeButton setTintColor:[UIColor whiteColor]];
    self.navigationItem.leftBarButtonItem = closeButton;
    

    UIBarButtonItem *createButton = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleDone target:self action:@selector(createEvent:)];
    [createButton setTintColor:[UIColor whiteColor]];
    self.navigationItem.rightBarButtonItem = createButton;

    
    //Navigation bar
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    
    
    self.headerView.imageView.image = [UIImage imageNamed:@"camera-placeholder"];
    self.headerView.eventLabel.text = @"Enter Event Name";
    [self addResponderIfNeeded:[[QResponder alloc] initWithView:self.headerView.eventLabel keyboardType:UIKeyboardTypeDefault andPlaceholder:@"Add Event"]];
//    self.headerView.eventLabel.backgroundColor = [UIColor orangeColor];
    
}

-(void)registerCells{
    UINib *cellNib = [UINib nibWithNibName:@"cell" bundle:nil];
    [self.tableView registerNib:cellNib forCellReuseIdentifier:@"cell"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"EventLocationsCell" bundle:nil] forCellReuseIdentifier:@"EventLocationsCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"EventDetailsCell" bundle:nil] forCellReuseIdentifier:@"EventDetailsCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"EventAboutCell" bundle:nil] forCellReuseIdentifier:@"EventAboutCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"EventScheduleCell" bundle:nil] forCellReuseIdentifier:@"EventScheduleCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"EventGuestsCell" bundle:nil] forCellReuseIdentifier:@"EventGuestsCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"EventPhotosCell" bundle:nil] forCellReuseIdentifier:@"EventPhotosCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"EventDeleteCell" bundle:nil] forCellReuseIdentifier:@"EventDeleteCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"SaveEventTableViewCell" bundle:nil] forCellReuseIdentifier:@"SaveEventTableViewCell"];

}

-(void)updateColorsForImage:(UIImage *)image{
    LEColorPicker *colorPicker = [[LEColorPicker alloc] init];
    colorScheme = [colorPicker colorSchemeFromImage:image];
    
    [self.tableView reloadData];
}

#pragma mark - GSKStretchyHeaderView

-(void)stretchyHeaderView:(GSKStretchyHeaderView *)headerView didChangeStretchFactor:(CGFloat)stretchFactor{
    float shadowOpacity = 0;
    if (stretchFactor == 0) {
        shadowOpacity = 0.5;
    }else{
        shadowOpacity = 0;
    }

    CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:@"shadowOpacity"];
    anim.fromValue = [NSNumber numberWithFloat:self.navigationController.navigationBar.layer.shadowOpacity];
    anim.toValue = [NSNumber numberWithFloat:shadowOpacity];
    anim.duration = 0.5;
    [self.navigationController.navigationBar.layer addAnimation:anim forKey:@"shadowOpacity"];
    self.navigationController.navigationBar.layer.shadowOpacity = shadowOpacity;
}


#pragma mark - Form Navigation

-(void)addResponderIfNeeded:(QResponder *)sentResponder{
    BOOL added = NO;
    for (int i=0; i<responders.count; i++) {
        QResponder *thisResponder = responders[i];
        if ([sentResponder.placeholder isEqualToString:thisResponder.placeholder]) {
            added = YES;
            [responders replaceObjectAtIndex:i withObject:sentResponder];
        }
    }
    if (!added) {
        [responders addObject:sentResponder];
    }

    [(UITextField *)sentResponder.view setTextColor:[UIColor blackColor]];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedResponder:)];
    [sentResponder.view addGestureRecognizer:tap];
}

-(IBAction)tappedResponder:(UITapGestureRecognizer *)sender{
    for (int i=0; i<responders.count; i++) {
        QResponder *responder = responders[i];
        
        if (responder.view == sender.view) {
            [self goToIndex:(int)[responders indexOfObject:responder]];
        }
    }
    keyboardToolbar.hidden = NO;
    [keyboardToolbar.textField becomeFirstResponder];
}

-(void)goToIndex:(int)index{
    [keyboardToolbar layoutSubviews];
    
    currentResponderIndex = index;
    
    if (index == 0) {
        [keyboardToolbar showImageTags:YES];
    }else{
        [keyboardToolbar showImageTags:NO];
    }
    
    if (index > 0) {
        keyboardToolbar.lastButton.enabled = YES;
    }else{
        keyboardToolbar.lastButton.enabled = NO;
    }
    
    if (index <= responders.count-1) {
        keyboardToolbar.nextButton.enabled = YES;
    }else{
        keyboardToolbar.nextButton.enabled = NO;
    }

    
    for (int i=0; i<responders.count; i++) {
        QResponder *responder = responders[i];
        
        //selected responder
        if (i == index) {
            currentResponder = responder;
            
            if (index > 0) {
                CGRect frame = [responder.view convertRect:responder.view.frame toView:self.tableView];
                [self.tableView scrollRectToVisible:frame animated:YES];
            }

            if (responder.view != self.headerView.eventLabel) {
                [responder.view setBackgroundColor:[AppDelegate QWhite]];
            }
            
            keyboardToolbar.textField.placeholder = responder.placeholder;
            if ([responder.view isKindOfClass:[UILabel class]]) {
                if (![keyboardToolbar.textField.placeholder isEqualToString:[(UITextView *)responder.view text]]) {
                    keyboardToolbar.textField.text = [(UILabel *)responder.view text];
                }
            }
            if ([responder.view isKindOfClass:[UITextView class]]) {
                if (![keyboardToolbar.textField.placeholder isEqualToString:[(UITextView *)responder.view text]]) {
                    keyboardToolbar.textField.text = [(UITextView *)responder.view text];
                }
            }

            
            if (responder.inputSource) {
                keyboardToolbar.textField.inputView = responder.inputSource;
            }else if (responder.keyboardType){
                keyboardToolbar.textField.inputView = nil;
                keyboardToolbar.textField.keyboardType = responder.keyboardType;
            }else{
                keyboardToolbar.textField.inputView = nil;
                keyboardToolbar.textField.keyboardType = UIKeyboardTypeDefault;
            }
            
            [keyboardToolbar.textField reloadInputViews];
            [keyboardToolbar.textField becomeFirstResponder];
        }else{
            if (responder.view == self.headerView.eventLabel) {
                self.headerView.eventLabel.backgroundColor = [UIColor clearColor];
            }else{
                if (responder.view != self.headerView.eventLabel) {
                    [responder.view setBackgroundColor:[AppDelegate QWhite]];
                }
            }
        }
    }
    
    
    if (index == 3) {
        [self tableView:self.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
        [self resignFirstResponder];
    }
}

#pragma mark - saveEventCell

-(void)saveEventTableViewCellTouchedDown:(SaveEventTableViewCell *)cell{
    [self createEvent:nil];
}

#pragma mark - eventDeleteCell

-(void)eventDeleteCellShouldDelete:(EventDeleteCell *)cell{
    UIAlertController *deleteAlert = [UIAlertController alertControllerWithTitle:@"Delete Event" message:@"Are you sure you would like to delete this event?" preferredStyle:UIAlertControllerStyleAlert];
    [deleteAlert addAction:[UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [self.passedEvent setObject:@(YES) forKey:@"archived"];
        [self.passedEvent saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
            if (!error && succeeded) {
                [self dismiss];
            }
        }];
    }]];
    [deleteAlert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:deleteAlert animated:YES completion:nil];
}

#pragma mark - eventGuestsCell

-(void)guestsCell:(EventGuestsCell *)guestsCell didSetGuestsArray:(NSArray *)guestsArray{

    addedGuests = [guestsArray mutableCopy];
}


#pragma mark - IBActions

-(void)setTextForCurrentResponder:(UITextField *)sender{
    UILabel *label = responders[currentResponderIndex];
    label.text = sender.text;
}

-(IBAction)pickerChanged:(UIDatePicker *)sender{
    NSString *returnString;
    
    if (sender.datePickerMode == UIDatePickerModeDate) {
        selectedDate = sender.date;
        returnString = [dateFormatter stringFromDate:sender.date];
    }else if(sender.datePickerMode == UIDatePickerModeTime){
        selectedTime = sender.date;
        returnString = [timeFormatter stringFromDate:sender.date];
    }
    
    [(UILabel*)currentResponder.view setText:returnString];
    [keyboardToolbar.textField setText:returnString];
}


-(IBAction)createEvent:(id)sender{
    NSString *error = nil;
    
    EventDetailsCell *details = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
//    EventLocationsCell *locations = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    EventAboutCell *about = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    EventScheduleCell *schedule = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
//    EventGuestsCell *guests = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:4 inSection:0]];

    
    if (addedPlaces.count == 0)
        error = @"Please add one or more places";
    if ([details.timeLabel.text isEqualToString:@"Time"])
        error = @"Check Time";
    if ([details.dateLabel.text isEqualToString:@"Date"])
        error = @"Check Date";
    if ((self.headerView.eventLabel.text.length == 0) | [self.headerView.eventLabel.text isEqualToString:@"Enter Event Name"])
        error = @"Check Event Name";

    if (error.length > 0) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Almost Finished!" message:error preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    

    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents;
    
    if (selectedDate) {
        dateComponents = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:selectedDate];
    }else{
        dateComponents = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:self.passedEvent.date];
    }
    
    NSDateComponents *timeComponents;
    if (selectedTime) {
        timeComponents = [calendar components:NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond fromDate:selectedTime];
    }else{
        timeComponents = [calendar components:NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond fromDate:self.passedEvent.date];
    }
    [dateComponents setHour:timeComponents.hour];
    [dateComponents setMinute:timeComponents.minute];
    [dateComponents setSecond:timeComponents.second];
    NSDate *newDate = [calendar dateFromComponents:dateComponents];
    
    
    NSMutableArray *scheduleArray = [[NSMutableArray alloc] init];
    for (ScheduledObject *obj in schedule.scheduleArray) {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        dict[@"title"] = obj.title;
        dict[@"startTime"] = obj.startTime;
        dict[@"endTime"] = obj.endTime;
        [scheduleArray addObject:dict];
    }

    
    NSMutableArray *newGuests = [[NSMutableArray alloc] init];
    for (NSMutableDictionary *guest in addedGuests) {
        NSMutableDictionary *newGuest = [[NSMutableDictionary alloc] init];
        if (!guest[@"rsvp"]) {
            [guest setObject:@"none" forKey:@"rsvp"];
        }
        newGuest[@"name"] = guest[@"name"];
        newGuest[@"phoneNumber"] = guest[@"phoneNumber"];
        newGuest[@"rsvp"] = guest[@"rsvp"];
        [newGuests addObject:newGuest];
    }
    
    Event *event;
    
    if (self.passedEvent) {
        event = self.passedEvent;
    }else{
        event = [[Event alloc] init];
    }
    
    event.locations = [self locationsArrayFromPlaces:addedPlaces];
    
    
    event.title = self.headerView.eventLabel.text;
    event.about = about.textView.text;
    event.date = newDate;
    event.user = [PFUser currentUser];
    event.image = [PFFile fileWithName:@"image.png" data:UIImageJPEGRepresentation(self.headerView.imageView.image, 0.4)];
    event.schedule = scheduleArray;
    event.guests = newGuests;
    [SVProgressHUD show];
    [event saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
        [SVProgressHUD dismiss];
        if (succeeded) {
            [self.delegate newEventTableViewController:self didSaveWithEvent:event];
            HomeViewController *homeVC = [(UINavigationController *)[UIApplication sharedApplication].keyWindow.rootViewController viewControllers][0];
            [homeVC refresh];
            
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Done!" message:@"Your event has been created" preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self dismissViewControllerAnimated:YES completion:^{}];
            }]];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }];
}

-(NSArray *)placesFromLocationsArray:(NSArray *)locations{
    NSMutableArray *placesArray = [[NSMutableArray alloc] init];
    for (NSDictionary *d in locations) {
        Place *p = [[Place alloc] initWithDictionary:d];
        [placesArray addObject:p];
    }
    return placesArray;
}

-(NSArray *)locationsArrayFromPlaces:(NSArray *)places{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    for (Place *p in places){
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        dict[@"placeId"] = p.placeId;
        dict[@"title"] = p.title;
        dict[@"address"] = p.address;
        dict[@"latitude"] = [NSString stringWithFormat:@"%f", p.location.latitude];
        dict[@"longitude"] = [NSString stringWithFormat:@"%f", p.location.longitude];
        dict[@"photoUrl"] = p.photoURL.absoluteString;
        [array addObject:dict];
    }
    
    return array;
}

-(void)dismiss{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)dismiss:(id)sender{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Not Saving Your Event?" message:@"Are you sure? After all that work?" preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"Keep Working" style:UIAlertActionStyleDefault handler:nil]];
    [alert addAction:[UIAlertAction actionWithTitle:@"Don't Save" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }]];
    [self presentViewController:alert animated:YES completion:nil];
}



#pragma mark - edit existing event helpers

-(void)getPlacesByIDs:(NSArray *)ids{
        
    NSMutableArray *placesArray = [[NSMutableArray alloc] init];
    
    __block int completedRequests = 0;
    
    
    for (NSString *id in ids) {
        NSMutableURLRequest* request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET" URLString:[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/details/json?key=AIzaSyC67qOLCwRi1_6Z8g1zKA6OQkJekYIBDz8&placeid=%@", id] parameters:@{} error:NULL];
        [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        AFHTTPRequestOperation *operation = [manager HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject){
            completedRequests++;
            NSDictionary *result = responseObject[@"result"];
            Place *p = [[Place alloc] initWithDictionary:result];
            [placesArray addObject:p];
            
            if (completedRequests == ids.count) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self fetchDidComplete:placesArray];
                });
            }
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            if (completedRequests == ids.count) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self fetchDidComplete:placesArray];
                });
            }
            completedRequests++;
        }];
        
        [manager.operationQueue addOperation:operation];
    }
}

-(void)fetchDidComplete:(NSMutableArray *)places{
    NSMutableArray *origOrderArray = [[NSMutableArray alloc] init];
    for (NSDictionary *location in self.passedEvent.locations) {
        if (location[@"placeId"]) {
            [origOrderArray addObject:location[@"placeId"]];
        }
    }
    
    NSMutableArray *newOrderArray = [[NSMutableArray alloc] init];
    for (NSString *id in origOrderArray) {
        for (Place *p in places) {
            if ([p.placeId isEqualToString:id]) {
                [newOrderArray addObject:p];
                break;
            }
        }
    }
    
    places = newOrderArray;
    
    
    //Add Notes
    for (int i=0; i<self.passedEvent.locations.count; i++) {
        NSDictionary *location = self.passedEvent.locations[i];
        if (!location[@"placeId"]) {
            Place *p = [[Place alloc] init];
            p.location = CLLocationCoordinate2DMake([location[@"latitude"] floatValue], [location[@"longitude"] floatValue]);
            p.title = location[@"title"];
            p.type = GPlaceTypeNote;
            
            [places insertObject:p atIndex:i];
        }
    }
    
    addedPlaces = places;
    
    EventDetailsCell *detailsCell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    if (addedPlaces.count > 0) {
        detailsCell.addressLabel.text = [(Place *)addedPlaces[0] address];
//        detailsCell.venueLabel.text = [(Place *)addedPlaces[0] title];
    }
     
    EventLocationsCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    [cell.mapView removeAnnotations:cell.mapView.annotations];
    
    [self.tableView reloadData];
}

@end
