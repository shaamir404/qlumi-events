//
//  NewEventTableViewController.h
//  Qlumi2
//
//  Created by William Smillie on 12/13/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import <UIKit/UIKit.h>

// My Classes
#import "Event.h"
#import "EventHeaderView.h"
#import "NewEventKeyboardToolbar.h"
#import "MapViewController.h"
#import "QResponder.h"
#import "AppDelegate.h"

#import "EventLocationsCell.h"
#import "EventDetailsCell.h"
#import "EventAboutCell.h"
#import "EventScheduleCell.h"
#import "EventGuestsCell.h"
#import "EventPhotosCell.h"
#import "EventDeleteCell.h"
#import "SaveEventTableViewCell.h"

// 3rd Party
#import <LEColorPicker/LEColorPicker.h>
@import SVProgressHUD;

@class NewEventTableViewController;
@protocol NewEventDelegate
-(void)newEventTableViewController:(NewEventTableViewController *)vc didSaveWithEvent:(Event *)event;
@end

@interface NewEventTableViewController : UITableViewController <NewEventKeyboardToolbarDelegate, UICollectionViewDataSource, GSKStretchyHeaderViewStretchDelegate, MapViewControllerDelegate, deleteEventCellDelegate, GuestSelectionDelegate, SaveEventDelegate>
@property (nonatomic, weak) id <NewEventDelegate> delegate;

@property EventHeaderView *headerView;
@property Event *passedEvent;

-(void)showContactPicker;

@end
