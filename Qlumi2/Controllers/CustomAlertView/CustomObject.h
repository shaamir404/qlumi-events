//
//  CustomObject.h
//  Qlumi
//
//  Created by Umer Khan MBP  on 6/12/19.
//  Copyright © 2019 Red Door Endeavors. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
#import <SVProgressHUD/SVProgressHUD.h>


@interface CustomObject : NSObject

- (void) forgetPassword :(NSString *)MobNumber;

@end


