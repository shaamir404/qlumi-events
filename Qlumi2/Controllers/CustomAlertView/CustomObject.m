//
//  CustomObject.m
//  Qlumi
//
//  Created by Umer Khan MBP  on 6/12/19.
//  Copyright © 2019 Red Door Endeavors. All rights reserved.
//

#import "CustomObject.h"

@implementation CustomObject


- (void) forgetPassword:(NSString *)MobNumber {
    [SVProgressHUD show];
    [PFCloud callFunctionInBackground:@"resetPasswordWithPhoneNumber" withParameters:@{@"phoneNumber": MobNumber} block:^(id  _Nullable object, NSError * _Nullable error) {
        [SVProgressHUD dismiss];
        if (!error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self enterResetCodeForPhoneNumber:MobNumber];
            });
        }
    }];
}


-(void)enterResetCodeForPhoneNumber:(NSString *)number{
    UIAlertController *code = [UIAlertController alertControllerWithTitle:@"Code" message:@"Please enter your reset code" preferredStyle:UIAlertControllerStyleAlert];
    [code addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Reset Code";
        textField.keyboardType = UIKeyboardTypePhonePad;
    }];
    [code addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [code addAction:[UIAlertAction actionWithTitle:@"Submit" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self confirmCode:code.textFields[0].text forPhoneNumber:number];
    }]];
    
    
    UIWindow* topWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    topWindow.rootViewController = [UIViewController new];
    topWindow.windowLevel = UIWindowLevelAlert + 1;
    
    
    [topWindow makeKeyAndVisible];
    [topWindow.rootViewController presentViewController:code animated:YES completion:nil];
    
    
   // [self presentViewController:code animated:YES completion:nil];
   // [code dismissViewControllerAnimated:YES completion:nil];
}

-(void)confirmCode:(NSString *)code forPhoneNumber:(NSString *)number{
    PFQuery *query = [PFUser query];
    [query whereKey:@"phoneNumber" equalTo:number];
    [query getFirstObjectInBackgroundWithBlock:^(PFObject * _Nullable object, NSError * _Nullable error) {
        if ([object[@"resetCode"] isEqualToString:code]) {
            [self resetPasswordForUser:(PFUser *)object];
        }else{
            NSLog(@"Failed try again!");
            [self enterResetCodeForPhoneNumber:number];
        }
    }];
}

-(void)resetPasswordForUser:(PFUser *)user{
    UIAlertController *code = [UIAlertController alertControllerWithTitle:@"New Password" message:@"Please enter your new password" preferredStyle:UIAlertControllerStyleAlert];
    [code addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Password";
        [textField setSecureTextEntry:YES];
        textField.keyboardType = UIKeyboardTypeDefault;
    }];
    
    [code addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [code addAction:[UIAlertAction actionWithTitle:@"Submit" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [PFCloud callFunctionInBackground:@"changeUserPassword" withParameters:@{@"userId": user.objectId, @"password": code.textFields[0].text} block:^(id  _Nullable object, NSError * _Nullable error) {
            if (!error) {
                UIAlertController *complete = [UIAlertController alertControllerWithTitle:@"Reset Complete" message:@"Your password has been changed!" preferredStyle:UIAlertControllerStyleAlert];
                [complete addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
                [code dismissViewControllerAnimated:YES completion:nil];

            }else{
                NSLog(@"Error: %@", error);
            }
        }];
    }]];
    UIWindow* topWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    topWindow.rootViewController = [UIViewController new];
    topWindow.windowLevel = UIWindowLevelAlert + 1;
    
    
    [topWindow makeKeyAndVisible];
    [topWindow.rootViewController presentViewController:code animated:YES completion:nil];
    
}





@end
