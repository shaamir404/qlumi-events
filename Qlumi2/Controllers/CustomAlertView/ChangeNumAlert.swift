//
//  ChangeNumAlert.swift
//  Qlumi
//
//  Created by Umer Khan MBP  on 6/24/19.
//  Copyright © 2019 Red Door Endeavors. All rights reserved.
//

import UIKit
import libPhoneNumber_iOS

@objc public protocol ChangeNumAlertDelegate: NSObjectProtocol {
    @objc func didDoneBtnPressing(contact: EPContact)
    @objc func didCancelBtnPressing()
}


class ChangeNumAlert: UIViewController {
    
    
    
    @IBOutlet weak var tfCountryCode: UITextField!
    @IBOutlet weak var tfMobileNumber: UITextField!
    @IBOutlet weak var titleText: UILabel!
    
    var titleLabelString:String = ""
    
    
    
    var delegate : ChangeNumAlertDelegate?
    
    var selectedContactsAlert: EPContact?
    let phoneUtil = NBPhoneNumberUtil()
    var cells: UITableViewCell?
    
    var pickerView: UIPickerView!
    fileprivate var arrPrefixes = [ "(+93) - Afghanistan", "(+355) - Albania", "(+213) - Algeria", "(+1684) - American Samoa", "(+376) - Andorra", "(+244) - Angola", "(+1264) - Anguilla", "(+672) - Antarctica", "(+1268) - Antigua And Barbuda", "(+54) - Argentina", "(+374) - Armenia", "(+297) - Aruba", "(+61) - Australia", "(+43) - Austria", "(+994) - Azerbaijan", "(+1242) - Bahamas The", "(+973) - Bahrain", "(+880) - Bangladesh", "(+1246) - Barbados", "(+375) - Belarus", "(+32) - Belgium", "(+501) - Belize", "(+229) - Benin", "(+1441) - Bermuda", "(+975) - Bhutan", "(+591) - Bolivia", "(+387) - Bosnia and Herzegovina", "(+267) - Botswana", "(+55) - Bouvet Island", "(+55) - Brazil", "(+246) - British Indian Ocean Territory", "(+673) - Brunei", "(+359) - Bulgaria", "(+226) - Burkina Faso", "(+257) - Burundi", "(+855) - Cambodia", "(+237) - Cameroon", "(+1) - Canada", "(+238) - Cape Verde", "(+1345) - Cayman Islands", "(+236) - Central African Republic", "(+235) - Chad", "(+56) - Chile", "(+86) - China", "(+61) - Christmas Island", "(+672) - Cocos (Keeling) Islands", "(+57) - Colombia", "(+269) - Comoros", "(+242) - Republic Of The Congo", "(+242) - Democratic Republic Of The Congo", "(+682) - Cook Islands", "(+506) - Costa Rica", "(+225) - Cote D'Ivoire (Ivory Coast)", "(+385) - Croatia (Hrvatska)", "(+53) - Cuba", "(+357) - Cyprus", "(+420) - Czech Republic", "(+45) - Denmark", "(+253) - Djibouti", "(+1767) - Dominica", "(+1809) - Dominican Republic", "(+670) - East Timor", "(+593) - Ecuador", "(+20) - Egypt", "(+503) - El Salvador", "(+240) - Equatorial Guinea", "(+291) - Eritrea", "(+372) - Estonia", "(+251) - Ethiopia", "(+61) - External Territories of Australia", "(+500) - Falkland Islands", "(+298) - Faroe Islands", "(+679) - Fiji Islands", "(+358) - Finland", "(+33) - France", "(+594) - French Guiana", "(+689) - French Polynesia", "(+262) - French Southern Territories", "(+241) - Gabon", "(+220) - Gambia The", "(+995) - Georgia", "(+49) - Germany", "(+233) - Ghana", "(+350) - Gibraltar", "(+30) - Greece", "(+299) - Greenland", "(+1473) - Grenada", "(+590) - Guadeloupe", "(+1671) - Guam", "(+502) - Guatemala", "(+44) - Guernsey and Alderney", "(+224) - Guinea", "(+245) - Guinea-Bissau", "(+592) - Guyana", "(+509) - Haiti", "(+0) - Heard and McDonald Islands", "(+504) - Honduras", "(+852) - Hong Kong S.A.R.", "(+36) - Hungary", "(+354) - Iceland", "(+91) - India", "(+62) - Indonesia", "(+98) - Iran", "(+964) - Iraq", "(+353) - Ireland", "(+972) - Israel", "(+39) - Italy", "(+1876) - Jamaica", "(+81) - Japan", "(+44) - Jersey", "(+962) - Jordan", "(+7) - Kazakhstan", "(+254) - Kenya", "(+686) - Kiribati", "(+850) - Korea North", "(+82) - Korea South", "(+965) - Kuwait", "(+996) - Kyrgyzstan", "(+856) - Laos", "(+371) - Latvia", "(+961) - Lebanon", "(+266) - Lesotho", "(+231) - Liberia", "(+218) - Libya", "(+423) - Liechtenstein", "(+370) - Lithuania", "(+352) - Luxembourg", "(+853) - Macau S.A.R.", "(+389) - Macedonia", "(+261) - Madagascar", "(+265) - Malawi", "(+60) - Malaysia", "(+960) - Maldives", "(+223) - Mali", "(+356) - Malta", "(+44) - Man (Isle of)", "(+692) - Marshall Islands", "(+596) - Martinique", "(+222) - Mauritania", "(+230) - Mauritius", "(+269) - Mayotte", "(+52) - Mexico", "(+691) - Micronesia", "(+373) - Moldova", "(+377) - Monaco", "(+976) - Mongolia", "(+1664) - Montserrat", "(+212) - Morocco", "(+258) - Mozambique", "(+95) - Myanmar", "(+264) - Namibia", "(+674) - Nauru", "(+977) - Nepal", "(+599) - Netherlands Antilles", "(+31) - Netherlands The", "(+687) - New Caledonia", "(+64) - New Zealand", "(+505) - Nicaragua", "(+227) - Niger", "(+234) - Nigeria", "(+683) - Niue", "(+672) - Norfolk Island", "(+1670) - Northern Mariana Islands", "(+47) - Norway", "(+968) - Oman", "(+92) - Pakistan", "(+680) - Palau", "(+970) - Palestinian Territory Occupied", "(+507) - Panama", "(+675) - Papua new Guinea", "(+595) - Paraguay", "(+51) - Peru", "(+63) - Philippines", "(+64) - Pitcairn Island", "(+48) - Poland", "(+351) - Portugal", "(+1787) - Puerto Rico", "(+974) - Qatar", "(+262) - Reunion", "(+40) - Romania", "(+70) - Russia", "(+250) - Rwanda", "(+290) - Saint Helena", "(+1869) - Saint Kitts And Nevis", "(+1758) - Saint Lucia", "(+508) - Saint Pierre and Miquelon", "(+1784) - Saint Vincent And The Grenadines", "(+684) - Samoa", "(+378) - San Marino", "(+239) - Sao Tome and Principe", "(+966) - Saudi Arabia", "(+221) - Senegal", "(+381) - Serbia", "(+248) - Seychelles", "(+232) - Sierra Leone", "(+65) - Singapore", "(+421) - Slovakia", "(+386) - Slovenia", "(+44) - Smaller Territories of the UK", "(+677) - Solomon Islands", "(+252) - Somalia", "(+27) - South Africa", "(+500) - South Georgia", "(+211) - South Sudan", "(+34) - Spain", "(+94) - Sri Lanka", "(+249) - Sudan", "(+597) - Suriname", "(+47) - Svalbard And Jan Mayen Islands", "(+268) - Swaziland", "(+46) - Sweden", "(+41) - Switzerland", "(+963) - Syria", "(+886) - Taiwan", "(+992) - Tajikistan", "(+255) - Tanzania", "(+66) - Thailand", "(+228) - Togo", "(+690) - Tokelau", "(+676) - Tonga", "(+1868) - Trinidad And Tobago", "(+216) - Tunisia", "(+90) - Turkey", "(+7370) - Turkmenistan", "(+1649) - Turks And Caicos Islands", "(+688) - Tuvalu", "(+256) - Uganda", "(+380) - Ukraine", "(+971) - United Arab Emirates", "(+44) - United Kingdom", "(+1) - United States", "(+1) - United States Minor Outlying Islands", "(+598) - Uruguay", "(+998) - Uzbekistan", "(+678) - Vanuatu", "(+39) - Vatican City State (Holy See)", "(+58) - Venezuela", "(+84) - Vietnam", "(+1284) - Virgin Islands (British)", "(+1340) - Virgin Islands (US)", "(+681) - Wallis And Futuna Islands", "(+212) - Western Sahara", "(+967) - Yemen", "(+38) - Yugoslavia", "(+260) - Zambia", "(+263) - Zimbabwe"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tfCountryCode.delegate = self
        titleText.text = titleLabelString
    }
    
    
    @IBAction func DoneBtnAction(_ sender: Any) {
        UserDefaults.standard.set("PhoneNumber", forKey: "")
        if tfCountryCode.text == ""{}
        else if tfMobileNumber.text == ""{}
        else{
            
            let phNumber = "\(tfCountryCode.text!)\(tfMobileNumber.text!)"
            
            var checkNum:Bool = false
            
            let countryCode = NSLocale.current.regionCode
            do {
                let phoneNumber: NBPhoneNumber = try phoneUtil.parse(phNumber, defaultRegion: countryCode)
                checkNum = phoneUtil.isValidNumber(phoneNumber)
                
            }
            catch let error as NSError {
                print(error.localizedDescription)
            }
            
            
            
            if checkNum {
                UserDefaults.standard.set("PhoneNumber", forKey: phNumber)
                selectedContactsAlert?.phoneNumber = phNumber
                self.delegate?.didDoneBtnPressing(contact: selectedContactsAlert!)
                self.dismiss(animated: true, completion: nil)
                
            }
            else{
                let alert = UIAlertController(title: "Error", message: "Phone number is not valid", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    
    @IBAction func CancelBtnAction(_ sender: Any) {
        UserDefaults.standard.set("PhoneNumber", forKey: "")
        self.delegate?.didCancelBtnPressing()
        self.dismiss(animated: true, completion: nil)
    }
    
}
extension ChangeNumAlert: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == tfCountryCode{
            pickerView = UIPickerView()
            pickerView.delegate = self
            pickerView.dataSource = self
            pickerView.tag = 2801
            textField.inputView = pickerView
        }
        return true
    }
}

extension ChangeNumAlert: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return arrPrefixes.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return arrPrefixes[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        
        // let str = arrPrefixes[row]
        
        
        tfCountryCode.text = arrPrefixes[row].slice(from: "(", to: ")")
        
    }
}
extension String {
    var isPhoneNumber: Bool {
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
            let matches = detector.matches(in: self, options: [], range: NSMakeRange(0, self.count))
            if let res = matches.first {
                return res.resultType == .phoneNumber && res.range.location == 0 && res.range.length == self.count
            } else {
                return false
            }
        } catch {
            return false
        }
    }
}

extension String {
    
    public func isPhone()->Bool {
        if self.isAllDigits() == true {
            let phoneRegex = "[235689][0-9]{6}([0-9]{3})?"
            let predicate = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
            return  predicate.evaluate(with: self)
        }else {
            return false
        }
    }
    
    private func isAllDigits()->Bool {
        let charcterSet  = NSCharacterSet(charactersIn: "+0123456789").inverted
        let inputString = self.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return  self == filtered
    }
}
