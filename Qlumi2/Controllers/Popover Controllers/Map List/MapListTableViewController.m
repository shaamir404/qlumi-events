//
//  MapListTableViewController.m
//  Qlumi
//
//  Created by Will on 2/7/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import "MapListTableViewController.h"
#import "AppDelegate.h"
#import "CalloutController.h"

@interface MapListTableViewController (){
    AppDelegate *appdel;
}

@end

@implementation MapListTableViewController

-(id)initWithEditingEnabled:(BOOL)editable{
    if (self = [super init]) {
        appdel = (AppDelegate *)[UIApplication sharedApplication].delegate;
        self.editingEnabled = editable;
        
        if (editable) {
            self.tableView = [[BVReorderTableView alloc] initWithFrame:self.view.bounds];
        }

        
        [self.tableView registerNib:[UINib nibWithNibName:@"MapLocationTableViewCell" bundle:nil] forCellReuseIdentifier:@"placeCell"];
        self.tableView.allowsSelection = YES;
    }
    return  self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    self.tableView.tableFooterView = [UIView new];
}

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"No Places";
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:18.0f],
                                 NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}



#pragma mark reorder - tableview

- (id)saveObjectAndInsertBlankRowAtIndexPath:(NSIndexPath *)indexPath {
    id object = [self.passedArray objectAtIndex:indexPath.row];
    [self.passedArray replaceObjectAtIndex:indexPath.row withObject:@"DUMMY"];
    return object;
}

- (void)moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
    id object = [self.passedArray objectAtIndex:fromIndexPath.row];
    [self.passedArray removeObjectAtIndex:fromIndexPath.row];
    [self.passedArray insertObject:object atIndex:toIndexPath.row];
}

- (void)finishReorderingWithObject:(id)object atIndexPath:(NSIndexPath *)indexPath; {
    [self.passedArray replaceObjectAtIndex:indexPath.row withObject:object];
    [self.delegate mapList:self didDidReorderAddedPlaces:self.passedArray];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.passedArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 81;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"placeCell";
    MapLocationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell == nil) {
        cell = [[MapLocationTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.thumbnailImageView.image = [UIImage imageNamed:@"photo-placeholder"];
        cell.nameLabel.text = @"Name";
        cell.addressLabel.text = @"Address";
        cell.detailsLabel.text = @"5 Rating • 0.0 Miles";
    }
    cell.delegate = self;

    if ([[self.passedArray objectAtIndex:indexPath.row] isKindOfClass:[NSString class]] &&
        [[self.passedArray objectAtIndex:indexPath.row] isEqualToString:@"DUMMY"]) {
        cell.textLabel.text = @"";
        cell.contentView.backgroundColor = [UIColor clearColor];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }else{
        UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureAction:)];
        [recognizer setNumberOfTapsRequired:1];
        [cell.swipeToDeleteScrollView addGestureRecognizer:recognizer];
        
        
        Place *p = self.passedArray[indexPath.row];
        
        CLLocation *locA = [[CLLocation alloc] initWithLatitude:appdel.userCoordinates.latitude longitude:appdel.userCoordinates.longitude];
        CLLocation *locB = [[CLLocation alloc] initWithLatitude:p.location.latitude longitude:p.location.longitude];
        CLLocationDistance distance = [locA distanceFromLocation:locB];
        float miles = (distance/1609.344);
        
        
        if (self.editingEnabled) {
            cell.swipeToDeleteScrollView.scrollEnabled = YES;
        }else{
            cell.swipeToDeleteScrollView.scrollEnabled = NO;
        }
        
        [cell.thumbnailImageView sd_setImageWithURL:p.photoURL placeholderImage:[UIImage imageNamed:@"photo-placeholder"]];
        cell.nameLabel.text = p.title;
        cell.addressLabel.text = p.address;
        cell.detailsLabel.text = [NSString stringWithFormat:@"%.02f Rating • %.1f %@", p.rating, miles, (miles==1)?@"Mile":@"Miles"];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        
        
        cell.dotDeselectView.hidden = YES;
        cell.dot.backgroundColor = [Place colorForType:p.type];
        cell.topLine.backgroundColor = [Place colorForType:p.type];
        cell.bottomLine.backgroundColor = [Place colorForType:p.type];
        
        
        if (indexPath.row == 0) {
            cell.topLine.hidden = YES;
        }else{
            cell.topLine.hidden = NO;
        }
        if (indexPath.row == self.passedArray.count-1){
            cell.bottomLine.hidden = YES;
        }else{
            cell.bottomLine.hidden = NO;
        }
    }
    return cell;
}

-(void)gestureAction:(UITapGestureRecognizer *) sender
{
    CGPoint touchLocation = [sender locationOfTouch:0 inView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:touchLocation];
    
    [self tableView:self.tableView didSelectRowAtIndexPath:indexPath];
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.delegate mapList:self didSelectPlace:self.passedArray[indexPath.row] atIndex:indexPath.row];
}


-(void)MapLocationCell:(MapLocationTableViewCell *)cell cellDidDeleteAtIndexPath:(NSIndexPath *)indexPath{
    if (self.editingEnabled) {
        [self.delegate mapList:self didDeletePlace:self.passedArray[indexPath.row] atIndex:indexPath.row];
    }
}

@end
