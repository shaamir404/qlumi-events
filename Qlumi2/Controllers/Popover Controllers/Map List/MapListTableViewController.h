//
//  MapListTableViewController.h
//  Qlumi
//
//  Created by Will on 2/7/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapLocationTableViewCell.h"
#import "Place.h"
#import "BVReorderTableView.h"
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>

@class MapListTableViewController;
@protocol MapListDelegate
-(void)mapList:(MapListTableViewController *)mapListController didSelectPlace:(Place *)place atIndex:(NSUInteger)index;
-(void)mapList:(MapListTableViewController *)mapListController didDeletePlace:(Place *)place atIndex:(NSUInteger)index;
-(void)mapList:(MapListTableViewController *)mapListController didDidReorderAddedPlaces:(NSArray *)places;
@end

@interface MapListTableViewController : UITableViewController <DZNEmptyDataSetSource, DZNEmptyDataSetDelegate, MapLocationTableViewCellDelegate>

@property (nonatomic, weak)id <MapListDelegate>delegate;
-(id)initWithEditingEnabled:(BOOL)editable;
@property BOOL editingEnabled;

@property NSMutableArray *passedArray;

@end
