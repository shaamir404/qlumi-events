//
//  NotificationsViewController.h
//  Qlumi
//
//  Created by Will on 1/31/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <SVProgressHUD/SVProgressHUD.h>

#import "NotificationCell.h"
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>
#import "UIBarButtonItem+Badge.h"


@interface NotificationsViewController : UITableViewController <UIPopoverPresentationControllerDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>



-(id)initWithRootViewController:(UIViewController *)vc;

@end
