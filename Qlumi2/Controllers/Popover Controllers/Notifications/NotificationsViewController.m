//
//  NotificationsViewController.m
//  Qlumi
//
//  Created by Will on 1/31/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import "NotificationsViewController.h"
#import "HomeViewController.h"

#import "EventDetailViewController.h"
#import "Event.h"
#import "AppDelegate.h"

@interface NotificationsViewController (){
    AppDelegate *appdel;
    
    UIViewController *vc;
    UIRefreshControl *refreshControl;
    
    int unreadCount;
}

@end

@implementation NotificationsViewController

-(id)initWithRootViewController:(UIViewController *)viewController{
    if (self = [super init]) {
        vc = viewController;
        appdel = (AppDelegate *)[UIApplication sharedApplication].delegate;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor blackColor];
    [refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    [self.tableView setRefreshControl:refreshControl];
    
    
    [[self tableView] registerNib: [UINib nibWithNibName:@"NotificationCell" bundle:nil] forCellReuseIdentifier:@"cell"];

    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    self.tableView.allowsMultipleSelectionDuringEditing = NO;
    self.tableView.separatorColor = [UIColor clearColor];
    
    // A little trick for removing the cell separators
    self.tableView.tableFooterView = [UIView new];
}

-(void)refresh{
    
    PFQuery *notifications = [PFQuery queryWithClassName:@"Notification"];
    [notifications whereKey:@"user" equalTo:[PFUser currentUser]];
    [notifications whereKey:@"read" notEqualTo:@(YES)];
    [notifications orderByDescending:@"createdAt"];
    [notifications includeKey:@"eventObj.user"];
    [notifications findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        [refreshControl endRefreshing];
        
        if (objects) {
            appdel.NotificationsArray = [objects copy];
            
            unreadCount = 0;
            for (int i=0; i<appdel.NotificationsArray.count; i++) {
                BOOL read = [appdel.NotificationsArray[i][@"read"] boolValue];
                if (!read) {
                    unreadCount++;
                }
            }
            if (unreadCount == 0) {
                vc.navigationItem.rightBarButtonItem = nil;
            }else{
                vc.navigationItem.rightBarButtonItem = [(HomeViewController*)vc notificationsButton];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                vc.navigationItem.rightBarButtonItem.badgeValue = [NSString stringWithFormat:@"%i", unreadCount];
            });
            
            [self.tableView reloadData];
        }
    }];
}

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"No Messages... Yet!";
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:18.0f],
                                 NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView {
    return YES;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return appdel.NotificationsArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 72;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NotificationCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];

    PFObject *notification = [appdel.NotificationsArray objectAtIndex:indexPath.row];
    PFFile *imageFile = notification[@"eventObj"][@"image"];
        
    [cell.notificationImageView sd_setImageWithURL:[NSURL URLWithString:imageFile.url] placeholderImage:[UIImage imageNamed:@"photo-placeholder"]];
    cell.notificationLabel.text = notification[@"message"];
    
    
    if ([notification[@"read"] boolValue] == YES) {
        cell.blankView.hidden = NO;
    }else{
        cell.blankView.hidden = YES;
    }
    
    if (indexPath.row == 0) {
        cell.topLine.hidden = YES;
    }else{
        cell.topLine.hidden = NO;
    }
    
    if (indexPath.row == appdel.NotificationsArray.count-1) {
        cell.bottomLine.hidden = YES;
    }else{
        cell.bottomLine.hidden = NO;
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    PFObject *notification = appdel.NotificationsArray[indexPath.row];
    [notification setObject:@(YES) forKey:@"read"];
    [notification saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
    }];
    [self refresh];
    if (unreadCount > 0) {
        unreadCount--;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        vc.navigationItem.rightBarButtonItem.badgeValue = [NSString stringWithFormat:@"%i", unreadCount];
    });
    
    NewEventViewController *eventVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"NewEventViewController"];
    [eventVC setPassedEvent:(Event *)appdel.NotificationsArray[indexPath.row][@"eventObj"]];
    [eventVC setHierarchy:NewEventHierarchyDETAILS];
    UINavigationController * navVC = [[UINavigationController alloc] initWithRootViewController:eventVC];
    navVC.modalPresentationStyle = UIModalPresentationFullScreen;
//    [self presentViewController:navVC animated:true completion:nil];
//    EventDetailViewController *detailVC = [[EventDetailViewController alloc] init];
//    [detailVC setPassedEvent:(Event *)appdel.NotificationsArray[indexPath.row][@"eventObj"]];
    [self dismissViewControllerAnimated:YES completion:^{
        [vc presentViewController:navVC animated:YES completion:nil];
    }];
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSMutableArray *array = [appdel.NotificationsArray mutableCopy];
        
        PFObject *obj = [array objectAtIndex:indexPath.row];
        [obj deleteInBackground];
        [array removeObjectAtIndex:indexPath.row];
        appdel.NotificationsArray = array;
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

-(NSString *)pronounForGender:(NSString *)gender{
    if ([gender isEqualToString:@"male"]) {
        return @"him";
    }else if ([gender isEqualToString:@"female"]) {
        return @"her";
    }else{
        return @"them";
    }
}


@end
