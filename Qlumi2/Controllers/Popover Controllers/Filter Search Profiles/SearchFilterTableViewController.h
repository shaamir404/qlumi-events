//
//  FiltersProfilesTableViewController.h
//  Qlumi
//
//  Created by Will on 2/2/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <DZNEmptyDataSet/DZNEmptyDataSet-umbrella.h>
#import "NewSearchProfileViewController.h"

@class SearchFilterTableViewController;
@protocol SearchFilterSelectionDelegate <NSObject>
- (void)searchFilterTableViewController:(SearchFilterTableViewController *)viewController didSelectFilters:(NSArray *)filtersArray;
- (void)searchFilterTableViewController:(SearchFilterTableViewController *)viewController shouldApplySelectedFilters:(NSArray *)filtersArray;
@end


@interface SearchFilterTableViewController : UITableViewController <UIPopoverPresentationControllerDelegate, NewSearchProfileDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>

-(id)initWithRootViewController:(UIViewController *)vc;

@property (nonatomic, strong) NSMutableArray *selectedFiltersArray;
@property (nonatomic, weak) id <SearchFilterSelectionDelegate> delegate; //define MyClassDelegate as delegate

@end
