//
//  FiltersProfilesTableViewController.m
//  Qlumi
//
//  Created by Will on 2/2/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import "SearchFilterTableViewController.h"

@interface SearchFilterTableViewController (){
    UIViewController *vc;
    
    NSMutableArray *filtersArray;
    UIRefreshControl *refreshControl;
}

@end

@implementation SearchFilterTableViewController
@synthesize selectedFiltersArray;

-(id)initWithRootViewController:(UIViewController *)viewController{
    if (self = [super init]) {
        vc = viewController;
        filtersArray = [[NSMutableArray alloc] init];
        selectedFiltersArray = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Interests";
    
    UIBarButtonItem *newSearchProfileButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(newSearchProfile:)];
    self.navigationItem.rightBarButtonItem = newSearchProfileButton;
    
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor blackColor];
    [refreshControl addTarget:self action:@selector(refreshFilters) forControlEvents:UIControlEventValueChanged];
    [self.tableView setRefreshControl:refreshControl];
    [self.tableView setAllowsMultipleSelection:YES];
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    
    [self refreshFilters];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self.tableView reloadData];
}

-(void)refreshFilters{
    [SVProgressHUD show];
    PFQuery *filters = [PFQuery queryWithClassName:@"SearchProfile"];
    [filters orderByAscending:@"keyword"];
    [filters whereKey:@"user" equalTo:[PFUser currentUser]];
    [filters findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        [refreshControl endRefreshing];
        [SVProgressHUD dismiss];
        filtersArray = [[NSMutableArray alloc] initWithArray:objects];
        [self.tableView reloadData];
    }];
}


-(void)viewDidDisappear:(BOOL)animated{
    [self.delegate searchFilterTableViewController:self shouldApplySelectedFilters:selectedFiltersArray];
}

#pragma mark - empty data source

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"No Filters";
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:18.0f],
                                 NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView {
    
    return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return filtersArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
    }

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.textLabel.text = filtersArray[indexPath.row][@"keyword"];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"Within %.1f miles", [filtersArray[indexPath.row][@"radius"] floatValue]];


//    if ([selectedFiltersArray containsObject:filtersArray[indexPath.row]]) {
    if ([self array:selectedFiltersArray containsPFObject:filtersArray[indexPath.row]]) {
//        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        cell.accessoryView = [self heart];
        
        
        [self.tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
        [self tableView:self.tableView didSelectRowAtIndexPath:indexPath];
    }else{
//        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.accessoryView = nil;
    }
    
    return cell;
}

-(UILabel *)heart{
    UILabel *heart = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 16, 16)];
    heart.text = @"♥";
    return heart;
}

-(BOOL)array:(NSArray *)array containsPFObject:(PFObject *)object{
    for (PFObject *o in array) {
        if ([object.objectId isEqualToString:o.objectId]) {
            return YES;
        }
    }
    return NO;
}

-(NSInteger)array:(NSArray *)array containsPFObjectAtIndex:(PFObject *)object{
    for (int i=0; i<array.count; i++) {
        PFObject *o = array[i];
        if ([object.objectId isEqualToString:o.objectId]) {
            return i;
        }
    }
    return NO;
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (![self array:selectedFiltersArray containsPFObject:filtersArray[indexPath.row]]){
        [selectedFiltersArray addObject:filtersArray[indexPath.row]];
        [tableView cellForRowAtIndexPath:indexPath].accessoryView = [self heart];
    }
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([self array:selectedFiltersArray containsPFObject:filtersArray[indexPath.row]]) {
        NSInteger index = [self array:selectedFiltersArray containsPFObjectAtIndex:filtersArray[indexPath.row]];
        [selectedFiltersArray removeObjectAtIndex:index];
        [tableView cellForRowAtIndexPath:indexPath].accessoryView = nil;
    }
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {

        PFObject *objectToDelete = filtersArray[indexPath.row];
        [objectToDelete deleteInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
            if (succeeded) {
                [filtersArray removeObjectAtIndex:indexPath.row];
                [tableView reloadData];
            }
        }];
    }
}

#pragma mark - IBActions

-(IBAction)newSearchProfile:(id)sender{
    [self dismissViewControllerAnimated:YES completion:^{
        NewSearchProfileViewController *newFilterVC = [[NewSearchProfileViewController alloc] init];
        newFilterVC.delegate = self;
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:newFilterVC];
        [vc presentViewController:nav animated:YES completion:nil];
    }];
}

#pragma mark - newSearchProfileDelegate

-(void)newSearchProfileViewController:(NewSearchProfileViewController *)viewController didCreateSearchProfile:(PFObject *)newProfile{
    [filtersArray addObject:newProfile];
    [selectedFiltersArray addObject:filtersArray.lastObject];
    [self.tableView reloadData];
    [self.delegate searchFilterTableViewController:self shouldApplySelectedFilters:selectedFiltersArray];
}


#pragma mark - PopOverDelegate

- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller
{
    return UIModalPresentationNone;
}

@end
