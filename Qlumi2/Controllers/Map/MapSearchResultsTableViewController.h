//
//  MapSearchResultsTableViewController.h
//  Qlumi
//
//  Created by Will on 2/5/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/AFNetworking.h>
#import "Place.h"

@class MapSearchResultsTableViewController;
@protocol MapSearchResultsSelectionDelegate <NSObject>
-(void)mapSearchResultsTableViewController:(MapSearchResultsTableViewController *)tableViewController didSelectPlace:(Place *)place;
-(void)mapSearchResultsTableViewController:(MapSearchResultsTableViewController *)tableViewController didSelectSearchAlongRouteForString:(NSString *)searchString;
-(void)mapSearchResultsTableViewController:(MapSearchResultsTableViewController *)tableViewController didSelectSearchNearbyForString:(NSString *)searchString;
@end

@interface MapSearchResultsTableViewController : UITableViewController <UISearchBarDelegate>
@property (nonatomic, weak) id<MapSearchResultsSelectionDelegate> delegate;

@property (nonatomic, strong) NSString *searchString;

@end
