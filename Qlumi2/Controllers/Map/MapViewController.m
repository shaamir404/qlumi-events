//
//  MapViewController.m
//  Qlumi
//
//  Created by Will on 2/2/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import "MapViewController.h"

@interface MapViewController (){
    UIBarButtonItem *closeButton;
    UIBarButtonItem *filterButton;
    
    UINavigationController *destNav;
    UITableViewController *searchResultsVC;
    
    UIBarButtonItem *drawRouteButton;
    UIBarButtonItem *toggleTableViewButton;
    UISlider *zoomSlider;
        
    BOOL firstLoad;
    BOOL drawRoute;
    BOOL searchingAlongRoute;
    
    NSMutableArray *locationsInRoute;
    int completedRouteCalculations;
    
    UIWindow *calloutWindow;
    UIAlertController *noteAlert;
    
    QSearchType currentSearchType;
}

@end

@implementation MapViewController
@synthesize filterController;

-(id)initWithEditingEnabled:(BOOL)editable andSearchingEnabled:(BOOL)searchable{
    if (self = [super init]) {
        self.editingEnabled = editable;
        self.searchingEnabled = searchable;
    }
    return  self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    firstLoad = YES;
    drawRoute = NO;
    searchingAlongRoute = NO;
    
    self.addedPlacesArray = [[NSMutableArray alloc] init];
    self.resultsArray = [[NSMutableArray alloc] init];
    currentSearchType = QSearchTypeNone;
    

    [self setupUI];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.toolbarHidden = NO;
}

#pragma mark - setup

-(void)setupUI{
    self.mapListController = [[MapListTableViewController alloc] initWithEditingEnabled:self.editingEnabled];
    self.mapListController.delegate = self;
    
    self.searchResultsController = [[MapSearchResultsTableViewController alloc] init];
    self.searchResultsController.delegate = self;
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:self.searchResultsController];
    
    self.searchController.searchResultsUpdater = self;
    self.searchController.searchBar.placeholder = nil;
    [self.searchController.searchBar sizeToFit];
    self.navigationItem.titleView = self.searchController.searchBar;
    
    self.searchController.delegate = self;
    self.searchController.dimsBackgroundDuringPresentation = YES;
    self.searchController.hidesNavigationBarDuringPresentation = NO;
    self.searchController.searchBar.delegate = self.searchResultsController;
    self.searchController.searchBar.placeholder = @"Search Interests";
    self.definesPresentationContext = YES;

    
    filterController = [[SearchFilterTableViewController alloc] initWithRootViewController:self];
    filterController.delegate = self;

    
    closeButton = [[UIBarButtonItem alloc] initWithTitle:@"❮ Back" style:UIBarButtonItemStyleDone target:self action:@selector(dismiss:)];
    
    self.navigationItem.leftBarButtonItem = closeButton;
    
    filterButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"filter"] style:UIBarButtonItemStylePlain target:self action:@selector(toggleFilter:)];
    
    self.mapView = [[QMapView alloc] initWithFrame:self.view.bounds andEditingEnabled:self.editingEnabled];
    self.mapView.placesDelegate = self;
    self.mapView.shouldShowResearch = YES;
    [self.view addSubview:self.mapView];
    
    
    drawRouteButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"route"] style:UIBarButtonItemStylePlain target:self action:@selector(drawRoute:)];

    zoomSlider = [[UISlider alloc] initWithFrame:CGRectMake(0,0, self.view.frame.size.width-(80*2), 20)];
    [zoomSlider addTarget:self action:@selector(zoom:) forControlEvents:UIControlEventValueChanged];
    [zoomSlider setMinimumValue:0];
    [zoomSlider setMaximumValue:1];
    zoomSlider.value = 0.5;
    UIBarButtonItem *sliderItem = [[UIBarButtonItem alloc] initWithCustomView:zoomSlider];
    
    
    UIBarButtonItem *flexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    toggleTableViewButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"drag-list"] style:UIBarButtonItemStylePlain target:self action:@selector(displayTableView:)];
    NSArray *items = [NSArray arrayWithObjects:drawRouteButton, flexibleItem, sliderItem, flexibleItem, toggleTableViewButton, nil];
    self.toolbarItems = items;
    
    
    self.searchController.searchBar.hidden = YES;
    self.navigationItem.rightBarButtonItem = nil;
    self.mapView.shouldShowResearch = YES;
    
    if (self.editingEnabled) {
    }
    [self setupEditing];
    
    if (self.searchingEnabled) {
        [self setupSearching];
    }
    [self setupSavedData];
}

-(void)setupSavedData {
    NSMutableDictionary *filters = [[[NSUserDefaults standardUserDefaults] dictionaryForKey:@"lastFilters"] mutableCopy];
    if (filters.allKeys.count > 0){
        [SVProgressHUD show];
        PFQuery *query = [PFQuery queryWithClassName:@"SearchProfile"];
        [query whereKey:@"objectId" containedIn:filters.allKeys];
        [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
            [SVProgressHUD dismiss];
            if (!error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.filterController.selectedFiltersArray = [[NSMutableArray alloc] initWithArray:objects];
                    [self.filterController.tableView reloadData];
                    [self searchFilterTableViewController:self.filterController shouldApplySelectedFilters:objects];
                });
            }
        }];
    }
    
    NSMutableArray *tags = [[[NSUserDefaults standardUserDefaults] objectForKey:@"lastTags"] mutableCopy];
    if (tags.count > 0){
        [self getPlacesByIDs:tags];
    }else{
        tags = [[NSMutableArray alloc] init];
    }
}

-(void)getPlacesByIDs:(NSArray *)ids{
    NSMutableArray *placesArray = [[NSMutableArray alloc] init];
    
    __block int completedRequests = 0;
    
    
    for (NSString *id in ids) {
        NSMutableURLRequest* request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET" URLString:[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/details/json?key=AIzaSyC67qOLCwRi1_6Z8g1zKA6OQkJekYIBDz8&placeid=%@", id] parameters:@{} error:NULL];
        [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        AFHTTPRequestOperation *operation = [manager HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject){
            completedRequests++;
            NSDictionary *result = responseObject[@"result"];
            Place *p = [[Place alloc] initWithDictionary:result];
            [placesArray addObject:p];
            
            if (completedRequests == ids.count) {
                [self fetchDidComplete:placesArray];
            }
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            if (completedRequests == ids.count) {
                [self fetchDidComplete:placesArray];
            }
            completedRequests++;
        }];
        
        [manager.operationQueue addOperation:operation];
    }
}

-(void)fetchDidComplete:(NSMutableArray *)places{
    self.addedPlacesArray = places;
    
    //    for (int i=0; i<self.addedPlacesArray.locations.count; i++) {
    //        NSDictionary *location = self.addedPlacesArray.locations[i];
    //        if (!location[@"placeId"]) {
    //            Place *p = [[Place alloc] init];
    //            p.location = CLLocationCoordinate2DMake([location[@"latitude"] floatValue], [location[@"longitude"] floatValue]);
    //            p.title = location[@"title"];
    //            p.type = GPlaceTypeNote;
    //
    //            [self.addedPlacesArray insertObject:p atIndex:i];
    //        }
    //    }
    
    
    if (places.count) {
        for (Place *p in self.addedPlacesArray) {
            QMapAnnotation *annotation = [[QMapAnnotation alloc] init];
            p.annotation = annotation;
            [annotation setCoordinate:p.location];
            [annotation setTitle:p.title]; //You can set the subtitle too
            [annotation setSubtitle:p.address];
            annotation.place = p;
        }
    }
    
    [self.segmentedControl setTitle:[NSString stringWithFormat:@"Tagged (%lu)", (unsigned long)self.addedPlacesArray.count] forSegmentAtIndex:1];
}


-(IBAction)zoom:(UISlider *)sender{
    MKCoordinateRegion mapRegion;

    CGFloat const inMin = 0; CGFloat const inMax = 1; CGFloat const outMin = 60; CGFloat const outMax = 0.1; CGFloat in = sender.value;
    CGFloat multiplier = outMin + (outMax - outMin) * (in - inMin) / (inMax - inMin);
    
    mapRegion.span.latitudeDelta = multiplier;
    mapRegion.span.longitudeDelta = multiplier;

    
    if (sender.value != 0) {
        mapRegion.center = self.mapView.centerCoordinate;
        [self.mapView setRegion:mapRegion animated: NO];
    }
}


-(void)setupEditing{
    self.segmentedControl = [[UISegmentedControl alloc] initWithItems:@[@"Interests", @"Tagged"]];
    [self.segmentedControl addTarget:self action:@selector(segmentDidChange:) forControlEvents: UIControlEventValueChanged];
    self.segmentedControl.selectedSegmentIndex = 0;
    self.segmentedControl.tintColor = [AppDelegate QOrange];
    [self.mapView addSubview:self.segmentedControl];
}

-(void)setupSearching{    
    self.segmentedControl.hidden = NO;
    self.searchController.searchBar.hidden = NO;
    self.navigationItem.rightBarButtonItem = filterButton;
};




-(void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    
    self.segmentedControl.frame = CGRectMake(8, self.navigationController.navigationBar.frame.size.height+self.navigationController.navigationBar.frame.origin.y+18, self.view.frame.size.width-16, 32);
}

#pragma mark - QMapViewAnnotationDelegate

-(void)mapViewShouldUpdateZoomSlider:(QMapView *)map{
    
    CGFloat const inMin = 0.01; CGFloat const inMax = 60; CGFloat const outMin = 1; CGFloat const outMax = 0; CGFloat in = map.region.span.latitudeDelta;
    CGFloat multiplier = outMin + (outMax - outMin) * (in - inMin) / (inMax - inMin);
    [zoomSlider setValue:multiplier animated:YES];
}


-(void)mapView:(QMapView *)map didSelectAnnotation:(MKAnnotationView *)sender{
    if ([sender isKindOfClass:[QMapAnnotationView class]]){
        Place *place = [(QMapAnnotation *)[sender annotation] place];
        
        
        if (place.type != GPlaceTypeNote) {
            [self showCalloutForPlace:place];
        }if (place.type == GPlaceTypeStartPoint){
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Remove \"Plant Me\"" message:@"Remove the location added by the \"plant me\" button" preferredStyle:UIAlertControllerStyleAlert];
            
            [alert addAction:[UIAlertAction actionWithTitle:@"Remove" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self.addedPlacesArray removeObject:place];
                [self.mapView removeAnnotation:place.annotation];
                [self.segmentedControl setTitle:[NSString stringWithFormat:@"Tagged (%lu)", (unsigned long)self.addedPlacesArray.count] forSegmentAtIndex:1];
            }]];
            [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
            
            [self presentViewController:alert animated:YES completion:nil];
             
        }else{
            [self showCalloutForNote:place];
        }
        [map deselectAnnotation:sender.annotation animated:NO];
    }else{
        Place *place = [[Place alloc] init];
        place.location = sender.annotation.coordinate;
        place.title = @"Current Location";
        place.type = GPlaceTypeEstablishment;
        
        [self showCalloutForPlace:place];
        [map deselectAnnotation:sender.annotation animated:NO];

    }
}

-(void)mapView:(QMapView *)map drawRouteFromLocation:(CLLocationCoordinate2D)location{
    drawRoute = NO;
    
    NSMutableArray *placesToRemove = [[NSMutableArray alloc] init];
    for (Place *p in self.addedPlacesArray) {
        if (p.type == GPlaceTypeStartPoint) {
            [placesToRemove addObject:p];
        }
    }
    [self.addedPlacesArray removeObjectsInArray:placesToRemove];
    
    
    Place *start = [[Place alloc] init];
    start.title = @"Starting Location";
    start.location = location;
    start.type = GPlaceTypeStartPoint;
    
    [self.addedPlacesArray insertObject:start atIndex:0];
    [self.segmentedControl setTitle:[NSString stringWithFormat:@"Tagged (%lu)", (unsigned long)self.addedPlacesArray.count] forSegmentAtIndex:1];

    
    
    [self setAddedPlacesArray:self.addedPlacesArray];
    [self.segmentedControl setSelectedSegmentIndex:1];
    [self segmentDidChange:self.segmentedControl];

    if (self.addedPlacesArray.count > 1) {
        [self drawRoute:drawRouteButton];
    }
}

-(void)mapView:(QMapView *)map shouldAtNoteForLocation:(CLLocationCoordinate2D)location{
    [self addNoteToMap:location];
}

-(void)researchForMapView:(QMapView *)map{
    switch (currentSearchType) {
        case QSearchTypeNearby:
            [self mapSearchResultsTableViewController:self.searchResultsController didSelectSearchNearbyForString:self.searchController.searchBar.text];

            break;
        case QSearchTypeFilters:
            [self searchFilterTableViewController:filterController shouldApplySelectedFilters:filterController.selectedFiltersArray];

            break;
        default:
            break;
    }
}

#pragma mark - Callout Delegate

-(void)calloutController:(CalloutController *)controller shouldRevealOnMap:(Place *)p{
    
    if (self.mapListController) {
        [self.mapListController dismissViewControllerAnimated:YES completion:nil];
    }
    
    MKCoordinateRegion mapRegion;
    mapRegion.center = p.location;
    mapRegion.span.latitudeDelta = 0.1;
    mapRegion.span.longitudeDelta = 0.1;
    [self.mapView setRegion:mapRegion animated: YES];
}

-(void)calloutController:(CalloutController *)controller addedPlace:(Place *)place{
    if (!self.addedPlacesArray) {
        self.addedPlacesArray = [[NSMutableArray alloc] init];
    }
    
    [self.addedPlacesArray addObject:place];
    [self.segmentedControl setTitle:[NSString stringWithFormat:@"Tagged (%lu)", (unsigned long)self.addedPlacesArray.count] forSegmentAtIndex:1];
    
    if (drawRoute) {
        drawRoute = NO;
        [self drawRoute:drawRouteButton];
    }
    
    MBProgressHUD *toast = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    toast.mode = MBProgressHUDModeText;
    toast.label.text = @"Added Place";
    [self cacheAddedPlaces:self.addedPlacesArray];
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [toast hideAnimated:YES];
    });
}

-(void)calloutController:(CalloutController *)controller shouldRemovePlace:(Place *)place{
    UIAlertController *confirmRemove = [UIAlertController alertControllerWithTitle:@"Remove Place?" message:@"Are you sure you would like to remove this place?" preferredStyle:UIAlertControllerStyleAlert];
    [confirmRemove addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [confirmRemove addAction:[UIAlertAction actionWithTitle:@"Remove" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        if ([self place:place isInArray:self.addedPlacesArray]) {
            for (int i=0; i<self.addedPlacesArray.count; i++) {
                Place *p = self.addedPlacesArray[i];
                if ([p.placeId isEqualToString:place.placeId]) {
                    [self.addedPlacesArray removeObjectAtIndex:i];
                    [self didRemovePlace:p];
                    break;
                }
            }
            [controller dismiss:nil];
            [self segmentDidChange:self.segmentedControl];
        }
        calloutWindow = nil;
    }]];
    
    [controller presentViewController:confirmRemove animated:YES completion:^{
    }];
}

-(void)didRemovePlace:(Place *)place{
    [self cacheAddedPlaces:self.addedPlacesArray];
}

-(void)cacheAddedPlaces:(NSArray *)placesArray{
    
    NSMutableArray *placeIds = [[NSMutableArray alloc] init];
    for (Place *p in self.addedPlacesArray) {
        if (p.placeId) {
            [placeIds addObject:p.placeId];
        }
    }
    NSMutableArray *tags = [[[NSUserDefaults standardUserDefaults] objectForKey:@"lastTags"] mutableCopy];
    if (tags == nil) {
        tags = [[NSMutableArray alloc] init];
    }
    for (NSString *s in placeIds) {
        if (![tags containsObject:s]) {
            [tags addObject:s];
        }
    }
    [[NSUserDefaults standardUserDefaults] setObject:tags forKey:@"lastTags"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


-(void)calloutControllerDidDismiss:(CalloutController *)controller{
    calloutWindow = nil;
}


#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    self.navigationItem.leftBarButtonItem = nil;
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    self.navigationItem.leftBarButtonItem = closeButton;
}

#pragma mark - UISearchResultsUpdating

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    [self.searchResultsController setSearchString:searchController.searchBar.text];
}


#pragma mark - IBActions

- (void)addNoteToMap:(CLLocationCoordinate2D)coordinates
{
    noteAlert = [UIAlertController alertControllerWithTitle:@"New Note" message:@"\n\n\n\n\n" preferredStyle:UIAlertControllerStyleAlert];
    
    noteAlert.view.autoresizesSubviews = YES;
    UITextView *textView = [[UITextView alloc] initWithFrame:CGRectZero];
    textView.translatesAutoresizingMaskIntoConstraints = NO;
    textView.editable = YES;
    textView.dataDetectorTypes = UIDataDetectorTypeAll;
    textView.text = @"";
    textView.userInteractionEnabled = YES;
    textView.backgroundColor = [UIColor clearColor];
    textView.scrollEnabled = YES;
    textView.delegate = self;
    NSLayoutConstraint *leadConstraint = [NSLayoutConstraint constraintWithItem:noteAlert.view attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:textView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:-8.0];
    NSLayoutConstraint *trailConstraint = [NSLayoutConstraint constraintWithItem:noteAlert.view attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:textView attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:8.0];
    
    NSLayoutConstraint *topConstraint = [NSLayoutConstraint constraintWithItem:noteAlert.view attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:textView attribute:NSLayoutAttributeTop multiplier:1.0 constant:-64.0];
    NSLayoutConstraint *bottomConstraint = [NSLayoutConstraint constraintWithItem:noteAlert.view attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:textView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:64.0];
    [noteAlert.view addSubview:textView];
    [NSLayoutConstraint activateConstraints:@[leadConstraint, trailConstraint, topConstraint, bottomConstraint]];
    
    
    [noteAlert addAction:[UIAlertAction actionWithTitle:@"Create" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        Place *notePlace = [[Place alloc] init];
        notePlace.title = textView.text;
        notePlace.type  = GPlaceTypeNote;
        notePlace.location = coordinates;
        notePlace.enabled = YES;
        
        QMapAnnotation *annot = [[QMapAnnotation alloc] init];
        annot.coordinate = coordinates;
        annot.title = notePlace.title;
        annot.place = notePlace;
        annot.isNote = YES;
        [self.mapView addAnnotation:annot];
        
        [self.addedPlacesArray addObject:notePlace];
        [self.mapListController.tableView reloadData];
        
        if (drawRoute) {
            drawRoute = NO;
            [self drawRoute:drawRouteButton];
        }
    }]];
    [noteAlert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:noteAlert animated:YES completion:^{
        [textView becomeFirstResponder];
    }];
}

-(void)textViewDidBeginEditing:(UITextView *)textView{
    CGRect frame = noteAlert.view.frame;
    frame.origin.y = self.view.frame.size.height/5;
    noteAlert.view.frame = frame;
}

-(void)textViewDidEndEditing:(UITextView *)textView{
    [noteAlert.view endEditing:YES];
}


-(IBAction)segmentDidChange:(UISegmentedControl *)sender{
    NSArray *array;
    if (sender.selectedSegmentIndex == 0) {
        array = self.resultsArray;
        self.mapListController.editingEnabled = NO;
        self.mapListController.title = @"Interests";
    }else{
        array = self.addedPlacesArray;
        self.mapListController.editingEnabled = YES;
        self.mapListController.title = @"Tagged";
    }
    
    [self.mapView removeAnnotations:self.mapView.annotations];
    for (Place *p in array) {
        if (!p.annotation) {
            QMapAnnotation *annotation = [[QMapAnnotation alloc] init];
            [annotation setCoordinate:p.location];
            [annotation setTitle:p.title]; //You can set the subtitle too
            [annotation setSubtitle:p.address];
            annotation.place = p;
            p.annotation = annotation;
        }
        [self.mapView addAnnotation:p.annotation];
    }

    
    for (Place *p in self.addedPlacesArray) {
        if (p.type == GPlaceTypeStartPoint) {
//            QMapAnnotation *annotation = [[QMapAnnotation alloc] init];
//            [annotation setCoordinate:p.location];
//            [annotation setTitle:p.title]; //You can set the subtitle too
//            [annotation setSubtitle:p.address];
//            annotation.place = p;
//            p.annotation = annotation;
            [self.mapView addAnnotation:p.annotation];
        }
    }
    
    [self.segmentedControl setTitle:[NSString stringWithFormat:@"Tagged (%lu)", (unsigned long)self.addedPlacesArray.count] forSegmentAtIndex:1];

    self.mapListController.passedArray = [array mutableCopy];
    [self.mapListController.tableView reloadData];
}

-(IBAction)drawRoute:(UIBarButtonItem *)sender{
    if (self.addedPlacesArray.count >= 1) {
        drawRoute = !drawRoute;
        
        if (drawRoute == YES) {
            [sender setImage:[UIImage imageNamed:@"route-filled"]];
            if (self.addedPlacesArray.count == 1) {
                Place *start = [[Place alloc] init];
                start.title = @"Starting Location";
                start.location = self.mapView.userLocation.coordinate;
                start.type = GPlaceTypeStartPoint;
                [self.addedPlacesArray insertObject:start atIndex:0];
                [self directionsBetweenPlaces:self.addedPlacesArray];
            }else {
                [self directionsBetweenPlaces:self.addedPlacesArray];
            }
        }else{
            [sender setImage:[UIImage imageNamed:@"route"]];
            [self.mapView removeOverlays:self.mapView.overlays];
        }
    }else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Add Places" message:@"Add more than one location for directions" preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

-(IBAction)displayTableView:(UIBarButtonItem *)sender{
    self.mapListController.modalPresentationStyle = UIModalPresentationPopover;
    self.mapListController.preferredContentSize = CGSizeMake(self.view.frame.size.width-8, self.view.frame.size.width-32);
    
    
    UIPopoverPresentationController *popover;
    destNav = [[UINavigationController alloc] initWithRootViewController:self.mapListController];
    destNav.modalPresentationStyle = UIModalPresentationPopover;
    popover = destNav.popoverPresentationController;
    popover.delegate = self;

    
    popover.delegate = self;
    if (self.segmentedControl) {
        popover.passthroughViews = @[self.segmentedControl];
    }
    popover.sourceView = [sender valueForKey:@"view"];
    popover.sourceRect = CGRectMake(12,0,0,0);
    popover.permittedArrowDirections = UIPopoverArrowDirectionDown;
    
    if (self.segmentedControl.selectedSegmentIndex == 0) {
        self.mapListController.title = @"Interests";
        self.mapListController.editingEnabled = NO;
        self.mapListController.passedArray = self.resultsArray;
        [self.mapListController.tableView reloadData];
    }else{
        self.mapListController.title = @"Tagged";
        self.mapListController.editingEnabled = YES;
        self.mapListController.passedArray = self.addedPlacesArray;
        [self.mapListController.tableView reloadData];
    }
//    if (self.editingEnabled) {
//        if (self.segmentedControl.selectedSegmentIndex == 0) {
//            self.mapListController.title = @"Interests";
//            self.mapListController.editingEnabled = NO;
//            self.mapListController.passedArray = self.resultsArray;
//            [self.mapListController.tableView reloadData];
//        }else{
//            self.mapListController.title = @"Tagged";
//            self.mapListController.editingEnabled = YES;
//            self.mapListController.passedArray = self.addedPlacesArray;
//            [self.mapListController.tableView reloadData];
//        }
//    }else{
//        if (self.addedPlacesArray.count > 0) {
//            self.mapListController.title = @"Interests";
//            self.mapListController.editingEnabled = NO;
//            self.mapListController.passedArray = self.addedPlacesArray;
//            [self.mapListController.tableView reloadData];
//        }else{
//            self.mapListController.title = @"Tagged";
//            self.mapListController.editingEnabled = NO;
//            self.mapListController.passedArray = self.resultsArray;
//            [self.mapListController.tableView reloadData];
//        }
//    }
    
    [self presentViewController:destNav animated:YES completion:nil];
}

-(IBAction)toggleFilter:(UIBarButtonItem *)sender{
    UIPopoverPresentationController *popover;
    destNav = [[UINavigationController alloc] initWithRootViewController:filterController];
    filterController.preferredContentSize = CGSizeMake(300,300);
    destNav.modalPresentationStyle = UIModalPresentationPopover;
    popover = destNav.popoverPresentationController;
    popover.delegate = self;
    popover.sourceView = [self.navigationItem.rightBarButtonItem valueForKey:@"view"];
    popover.sourceRect = CGRectMake(16,40,1,1);
    popover.permittedArrowDirections = UIPopoverArrowDirectionUp;
    [self presentViewController:destNav animated:YES completion:nil];
}

- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller
{
    return UIModalPresentationNone;
}

-(IBAction)dismiss:(id)sender{
    if (self.senderMap) {
        [self.senderMap removeAnnotations:self.senderMap.annotations];
        
        for (Place *p in self.addedPlacesArray) {
            if (p.annotation) {
                [self.senderMap addAnnotation:p.annotation];
            }else{
                QMapAnnotation *annotation = [[QMapAnnotation alloc] init];
                [annotation setCoordinate:p.location];
                [annotation setTitle:p.title]; //You can set the subtitle too
                [annotation setSubtitle:p.address];
                annotation.place = p;
                p.annotation = annotation;
            }
        }
    }
    
    [self dismissViewControllerAnimated:YES completion:^{
        [self.delegate mapViewController:self didDismissWithPlaces:self.addedPlacesArray];
    }];
}

#pragma mark - MapListTableView

-(void)mapList:(MapListTableViewController *)mapListController didDidReorderAddedPlaces:(NSArray *)places{
    
    self.addedPlacesArray = [places mutableCopy];
    
    if (drawRoute) {
        drawRoute = NO;
        [self drawRoute:drawRouteButton];
    }
}

-(void)mapList:(MapListTableViewController *)mapListController didDeletePlace:(Place *)place atIndex:(NSUInteger)index{
    
    [self.addedPlacesArray removeObjectAtIndex:index];
    [self segmentDidChange:self.segmentedControl];
}

-(void)mapList:(MapListTableViewController *)mapListController didSelectPlace:(Place *)place atIndex:(NSUInteger)index{
    if (place.type != GPlaceTypeStartPoint) {
        if (place.type != GPlaceTypeNote) {
            [self showCalloutForPlace:place];
        }else{
            [self showCalloutForNote:place];
        }
    }
}

-(void)showCalloutForPlace:(Place *)place{
    if (place.type != GPlaceTypeStartPoint) {
        if (!calloutWindow) {
            calloutWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
            calloutWindow.rootViewController = [UIViewController new];
            calloutWindow.windowLevel = UIWindowLevelAlert + 1;
        }
        
        CalloutController *callout = [[CalloutController alloc] initWithPlace:place andEditing:self.editingEnabled];
        if ([(Place *)self.addedPlacesArray.firstObject type] == GPlaceTypeStartPoint) {
            callout.sourceLocation = [(Place *)self.addedPlacesArray.firstObject location];
        }else{
            callout.sourceLocation = self.mapView.userLocation.coordinate;;
        }
        
        callout.delegate = self;
        NSMutableArray *tags = [[[NSUserDefaults standardUserDefaults] objectForKey:@"lastTags"] mutableCopy];
        if ([tags containsObject:place.placeId]) {
            [callout setButtonMode:actionButtonTypeRemove];
        }else {
            [callout setButtonMode:actionButtonTypeAdd];
        }
        if ([self place:place isInArray:self.addedPlacesArray]) {
            
        }else{
            
        }
        
        [calloutWindow makeKeyAndVisible];
        [calloutWindow.rootViewController presentViewController:callout animated:YES completion:nil];
    }
}

-(void)showCalloutForNote:(Place *)p{
    if (!calloutWindow) {
        calloutWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        calloutWindow.rootViewController = [UIViewController new];
        calloutWindow.windowLevel = UIWindowLevelAlert + 1;
    }

    
    noteAlert = [UIAlertController alertControllerWithTitle:@"Note" message:@"\n\n\n\n\n\n\n\n" preferredStyle:UIAlertControllerStyleAlert];
    
    noteAlert.view.autoresizesSubviews = YES;
    UITextView *textView = [[UITextView alloc] initWithFrame:CGRectZero];
    textView.translatesAutoresizingMaskIntoConstraints = NO;
    textView.editable = self.editingEnabled;
    textView.dataDetectorTypes = UIDataDetectorTypeAll;
    textView.text = p.title;
    textView.userInteractionEnabled = YES;
    textView.scrollEnabled = YES;
    textView.delegate = self;
    NSLayoutConstraint *leadConstraint = [NSLayoutConstraint constraintWithItem:noteAlert.view attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:textView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:-8.0];
    NSLayoutConstraint *trailConstraint = [NSLayoutConstraint constraintWithItem:noteAlert.view attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:textView attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:8.0];
    
    NSLayoutConstraint *topConstraint = [NSLayoutConstraint constraintWithItem:noteAlert.view attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:textView attribute:NSLayoutAttributeTop multiplier:1.0 constant:-64.0];
    NSLayoutConstraint *bottomConstraint = [NSLayoutConstraint constraintWithItem:noteAlert.view attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:textView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:64.0];
    [noteAlert.view addSubview:textView];
    [NSLayoutConstraint activateConstraints:@[leadConstraint, trailConstraint, topConstraint, bottomConstraint]];
    
    [noteAlert addAction:[UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        if (self.editingEnabled) {
            if ([self place:p isInArray:self.addedPlacesArray]) {
                for (int i=0; i<self.addedPlacesArray.count; i++) {
                    Place *place = self.addedPlacesArray[i];
                    if ([place.title isEqualToString:p.title]) {
                        place.title = textView.text;
                        [self.addedPlacesArray replaceObjectAtIndex:i withObject:place];
                        break;
                    }
                }
                [self segmentDidChange:self.segmentedControl];
            }
        }
        
        
        calloutWindow = nil;
    }]];
    
    if (self.editingEnabled) {
        [noteAlert addAction:[UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            
            if ([self place:p isInArray:self.addedPlacesArray]) {
                for (int i=0; i<self.addedPlacesArray.count; i++) {
                    Place *place = self.addedPlacesArray[i];
                    if ([place.title isEqualToString:p.title]) {
                        [self.addedPlacesArray removeObjectAtIndex:i];
                        [self.mapListController.tableView reloadData];
                        break;
                    }
                }
                [self segmentDidChange:self.segmentedControl];
            }

            
            calloutWindow = nil;
        }]];
    }

    
    
    [calloutWindow makeKeyAndVisible];
    [calloutWindow.rootViewController presentViewController:noteAlert animated:YES completion:^{
        [textView becomeFirstResponder];
    }];
}

#pragma mark - Helper Methods

-(void)directionsBetweenPlaces:(NSArray *)places{
    if (![places containsObject:@"DUMMY"]) {
        for (id overlay in self.mapView.overlays) {
            if ([overlay isKindOfClass:[MKPolyline class]]){
                [self.mapView removeOverlay:overlay];
            }
        }
        
        if (searchingAlongRoute) {
            [self.mapView removeAnnotations:self.mapView.annotations];
        }

        locationsInRoute = [[NSMutableArray alloc] init];
        completedRouteCalculations = 0;


        for (int i=0; i<places.count; i++) {
            if (i+1 < places.count) {
                Place *p = places[i];
                Place *p2 = places[i+1];
                
                [self getPathDirections:p withDestination:p2];
            }
        }
    }
}

-(void)getPathDirections:(Place *)source withDestination:(Place *)destination{
    MKPlacemark *mks = [[MKPlacemark alloc] initWithCoordinate:source.location addressDictionary:nil];
    MKPlacemark *mkd = [[MKPlacemark alloc] initWithCoordinate:destination.location addressDictionary:nil];
    
    MKDirectionsRequest *directionsRequest = [[MKDirectionsRequest alloc] init];
    [directionsRequest setSource:[[MKMapItem alloc] initWithPlacemark:mks]];
    [directionsRequest setDestination:[[MKMapItem alloc] initWithPlacemark:mkd]];
    directionsRequest.transportType = MKDirectionsTransportTypeAutomobile;
    MKDirections *directions = [[MKDirections alloc] initWithRequest:directionsRequest];
    [directions calculateDirectionsWithCompletionHandler:^(MKDirectionsResponse *response, NSError *error) {
        if (!error) {
            for (MKRoute *route in [response routes]) {
                for (MKRouteStep *step in route.steps) {
                    NSMutableDictionary *location = [[NSMutableDictionary alloc] init];
                    [location setObject:[NSNumber numberWithFloat:step.polyline.coordinate.latitude] forKey:@"lat"];
                    [location setObject:[NSNumber numberWithFloat:step.polyline.coordinate.longitude] forKey:@"lon"];
                    [locationsInRoute addObject:location];
                }
                [self.mapView addOverlay:[route polyline] level:MKOverlayLevelAboveRoads];
            }
            completedRouteCalculations++;
            
            if (completedRouteCalculations == self.addedPlacesArray.count-1) {
                if (searchingAlongRoute) {
                    [self getPlacesWithRouteLocations:locationsInRoute ForString:self.searchController.searchBar.text];
                }
            }
        }
    }];
}



-(BOOL)canDrawRouteBasedOnArray:(NSArray *)array{
    if (self.addedPlacesArray.count > 1) {
        drawRouteButton.enabled = YES;
        return YES;
    }else{
        drawRouteButton.enabled = NO;
        return NO;
    }
}

-(BOOL)place:(Place *)place isInArray:(NSArray *)array{
    for (Place *p in array) {
        if (p.type != GPlaceTypeNote) {
            if ([p.placeId isEqualToString:place.placeId]) {
                return YES;
            }
        }else{
            if ([place.title isEqualToString:p.title]) {
                return YES;
            }
        }
    }
    return NO;
}

- (int)visibileRadiusFromMap:(MKMapView *)mapView;
{
    CLLocationCoordinate2D centerCoor = mapView.centerCoordinate;
    CLLocation *centerLocation = [[CLLocation alloc] initWithLatitude:centerCoor.latitude longitude:centerCoor.longitude];
    CLLocationCoordinate2D topCenterCoor = [mapView convertPoint:CGPointMake(mapView.frame.size.width / 2.0f, 0) toCoordinateFromView:mapView];
    CLLocation *topCenterLocation = [[CLLocation alloc] initWithLatitude:topCenterCoor.latitude longitude:topCenterCoor.longitude];
    CLLocationDistance radius = [centerLocation distanceFromLocation:topCenterLocation];
    
    return (int)(radius + 0.5);
}











#pragma mark - SearchSelectionDelegate

-(void)mapSearchResultsTableViewController:(MapSearchResultsTableViewController *)tableViewController didSelectPlace:(Place *)place{
    
    self.mapView.shouldShowResearch = YES;
    currentSearchType = QSearchTypeNone;

    
    self.resultsArray = [[NSMutableArray alloc] initWithObjects:place, nil];
    [self.segmentedControl setSelectedSegmentIndex:0];
    
    [SVProgressHUD show];
    
    NSMutableURLRequest* request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET" URLString:[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/details/json?key=AIzaSyC67qOLCwRi1_6Z8g1zKA6OQkJekYIBDz8&placeid=%@", place.placeId] parameters:@{} error:NULL];
    [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFHTTPRequestOperation *operation = [manager HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject){
        [SVProgressHUD dismiss];
        
        dispatch_async( dispatch_get_main_queue(), ^{
            NSDictionary *result = responseObject[@"result"];
            Place *p = [[Place alloc] initWithDictionary:result];
            
            QMapAnnotation *annotation = [[QMapAnnotation alloc] init]; 
            [annotation setCoordinate:p.location];
            [annotation setTitle:p.title]; //You can set the subtitle too
            [annotation setSubtitle:p.address];
            annotation.place = p;
            p.annotation = annotation;
            
            
            [self.mapView removeAnnotations:self.mapView.annotations];
            [self.mapView addAnnotation:p.annotation];
            
            MKCoordinateRegion mapRegion;
            mapRegion.center = p.location;
            mapRegion.span.latitudeDelta = 0.1;
            mapRegion.span.longitudeDelta = 0.1;
            [self.mapView setRegion:mapRegion animated: YES];
            
            [self showCalloutForPlace:p];
        });
        
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SVProgressHUD dismiss];
    }];
    [manager.operationQueue addOperation:operation];
}

-(void)mapSearchResultsTableViewController:(MapSearchResultsTableViewController *)tableViewController didSelectSearchAlongRouteForString:(NSString *)searchString{
    searchingAlongRoute = YES;
    drawRoute = NO;
    if (drawRoute) {
        [self drawRoute:drawRouteButton];
    }
}


-(void)mapSearchResultsTableViewController:(MapSearchResultsTableViewController *)tableViewController didSelectSearchNearbyForString:(NSString *)searchString{
    
    [SVProgressHUD show];
    
    [self getNearByPlacesForString:searchString callback:^(NSArray *array, NSString *error) {
        if (!error) {
            [SVProgressHUD dismiss];
            self.resultsArray = [array mutableCopy];
            [self.segmentedControl setSelectedSegmentIndex:0];
            
            [self getAdsForKeyword:searchString withLatitude:[NSString stringWithFormat:@"%f", self.mapView.centerCoordinate.latitude] longitude:[NSString stringWithFormat:@"%f", self.mapView.centerCoordinate.longitude] andRadius:[NSString stringWithFormat:@"%i", [self visibileRadiusFromMap:self.mapView]]];
            
            dispatch_async( dispatch_get_main_queue(), ^{
                [self.mapView removeAnnotations:self.mapView.annotations];
                for (Place *p in self.resultsArray) {
                    QMapAnnotation *annotation = [[QMapAnnotation alloc] init];
                    [annotation setCoordinate:p.location];
                    [annotation setTitle:p.title]; //You can set the subtitle too
                    [annotation setSubtitle:p.address];
                    annotation.place = p;
                    p.annotation = annotation;
                    
                    [self.mapView addAnnotation:p.annotation];
                }
                [self segmentDidChange:self.segmentedControl];
            });
        }
    }];
}

#pragma mark - FilterSelectionDelegate

-(void)searchFilterTableViewController:(SearchFilterTableViewController *)viewController shouldApplySelectedFilters:(NSArray *)filtersArray{
    
    NSMutableArray *selectedFilterIds = [[NSMutableArray alloc] init];
    for (PFObject *p in filtersArray) {
        [selectedFilterIds addObject:p.objectId];
    }
    if (filtersArray) {
        NSMutableDictionary *filters = [[NSMutableDictionary alloc] init];
        for (PFObject *filterObj in filtersArray) {
            NSMutableDictionary *filter = [[NSMutableDictionary alloc] init];
            filter[@"keyword"] = filterObj[@"keyword"];
            filter[@"radius"] = filterObj[@"radius"];
            [filters setObject:filter forKey:filterObj.objectId];
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:filters forKey:@"lastFilters"];
    }else{
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"lastFilters"];
    }
    
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self.mapView removeAnnotations:self.mapView.annotations];
    if (filtersArray.count > 0) {
        self.mapView.shouldShowResearch = YES;
        currentSearchType = QSearchTypeFilters;

        [SVProgressHUD show];
        
        
        self.resultsArray = [[NSMutableArray alloc] init];
        [self.segmentedControl setSelectedSegmentIndex:0];
        
        
        for (PFObject *p in filtersArray) {
            [self getAdsForKeyword:p[@"keyword"] withLatitude:[NSString stringWithFormat:@"%f", self.mapView.centerCoordinate.latitude] longitude:[NSString stringWithFormat:@"%f", self.mapView.centerCoordinate.longitude] andRadius:[NSString stringWithFormat:@"%f", [p[@"radius"] floatValue]*1609.344]];
            
            [self getNearByPlacesForProfile:p callback:^(NSArray *array, NSString *error) {
                if (!error) {
                    [self.resultsArray addObjectsFromArray:array];
                    
                    [SVProgressHUD dismiss];
                    dispatch_async( dispatch_get_main_queue(), ^{
                        for (Place *p in array) {
                            QMapAnnotation *annotation = [[QMapAnnotation alloc] init];
                            [annotation setCoordinate:p.location];
                            [annotation setTitle:p.title]; //You can set the subtitle too
                            [annotation setSubtitle:p.address];
                            annotation.place = p;
                            p.annotation = annotation;
                            
                            [self.mapView addAnnotation:p.annotation];
                        }
                        [self segmentDidChange:self.segmentedControl];
                    });
                }
            }];
        }
    }else{
        [self.mapView removeAnnotations:self.mapView.annotations];
    }
}


#pragma mark - Google API

-(void)getPlacesWithRouteLocations:(NSMutableArray *)routeLocations ForString:(NSString *)searchString{
    
    self.mapView.shouldShowResearch = YES;
    currentSearchType = QSearchTypeNearby;
    
    CLLocation *locA = [[CLLocation alloc] initWithLatitude:[routeLocations.firstObject[@"lat"] floatValue] longitude:[routeLocations.firstObject[@"lon"] floatValue]];
    CLLocation *locB = [[CLLocation alloc] initWithLatitude:[routeLocations.lastObject[@"lat"] floatValue] longitude:[routeLocations.lastObject[@"lon"] floatValue]];
    CLLocationDistance distance = [locA distanceFromLocation:locB];
    
    
    float resolution = 3*1609.344;
    if (distance > 32186.88)//20 miles
        resolution = 5*1609.344;
    if (distance > 80467.2)//50 miles
        resolution = 10*1609.344;
    if (distance > 160934.4)//100 miles
        resolution = 20*1609.344;
    if (distance > 804672)//500 miles
        resolution = 30*1609.344;
    if (distance > 1609344)//1000 miles
        resolution = 40*1609.344;
    if (distance > 3218688)//2000 miles
        resolution = 50*1609.344;
    int segments = ceil(fabs(distance/resolution));
    
    
    NSMutableArray *locationsToRemove = [[NSMutableArray alloc] init];
    for (int i=0; i<routeLocations.count-1; i++) {
        CLLocation *a = [[CLLocation alloc] initWithLatitude:[routeLocations[i][@"lat"] floatValue] longitude:[routeLocations[i][@"lon"] floatValue]];
        CLLocation *b = [[CLLocation alloc] initWithLatitude:[routeLocations[i+1][@"lat"] floatValue] longitude:[routeLocations[i+1][@"lon"] floatValue]];
        
        CLLocationDistance d = [b distanceFromLocation:a];
        if (d < resolution) {
            [locationsToRemove addObject:routeLocations[i+1]];
        }
    }
    [routeLocations removeObjectsInArray:locationsToRemove];
    

    
    NSMutableArray *searches = [[NSMutableArray alloc] init];
    searchString = [[searchString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]] mutableCopy];


    for (int i=0; i<routeLocations.count-1; i++) {
        NSDictionary *aDict = routeLocations[i];
        NSDictionary *bDict = routeLocations[i+1];
        
        CLLocation *a = [[CLLocation alloc] initWithLatitude:[aDict[@"lat"] floatValue] longitude:[aDict[@"lon"] floatValue]];
        CLLocation *b = [[CLLocation alloc] initWithLatitude:[bDict[@"lat"] floatValue] longitude:[bDict[@"lon"] floatValue]];
        CLLocationDistance subSegDistance = [a distanceFromLocation:b];
        int subSegments = ceil(fabs(subSegDistance/resolution));

        for (int i=0; i<subSegments; i++) {
            CGFloat const inMin = 0; CGFloat const inMax = subSegments; CGFloat const outMin = 0.0; CGFloat const outMax = 1.0; CGFloat in = i;
            CGFloat percentage = outMin + (outMax - outMin) * (in - inMin) / (inMax - inMin);
            
            float centerLat = INTUInterpolateCGFloat([aDict[@"lat"] floatValue], [bDict[@"lat"] floatValue], percentage);
            float centerLon = INTUInterpolateCGFloat([aDict[@"lon"] floatValue], [bDict[@"lon"] floatValue], percentage);
            
            
            NSMutableDictionary *search = [[NSMutableDictionary alloc] init];
            [search setObject:searchString forKey:@"keyword"];
            [search setObject:[NSNumber numberWithDouble:distance] forKey:@"radius"];
            [search setObject:[NSNumber numberWithDouble:centerLat] forKey:@"lat"];
            [search setObject:[NSNumber numberWithDouble:centerLon] forKey:@"lon"];
            [searches addObject:search];
        }
    }
    

    
    
    self.resultsArray = [[NSMutableArray alloc] init];
    [self.senderMap removeAnnotations:self.senderMap.annotations];
//    [self.segmentedControl setSelectedSegmentIndex:0];

    for (NSDictionary *search in searches) {
    
        [self getAdsForKeyword:searchString withLatitude:search[@"lat"] longitude:search[@"lon"] andRadius:search[@"radius"]];
        
        NSMutableURLRequest* request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET" URLString:[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=%@,%@&radius=%@&keyword=%@&key=AIzaSyC67qOLCwRi1_6Z8g1zKA6OQkJekYIBDz8", search[@"lat"], search[@"lon"], search[@"radius"], searchString] parameters:@{} error:NULL];
        [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        AFHTTPRequestOperation *operation = [manager HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject){
            if (![responseObject[@"status"] isEqualToString:@"Failed"]) {
                NSArray *results = responseObject[@"results"];
                for (NSDictionary *result in results){
                    Place *p = [[Place alloc] initWithDictionary:result];
                    
                    QMapAnnotation *annotation = [[QMapAnnotation alloc] init];
                    [annotation setCoordinate:p.location];
                    [annotation setTitle:p.title];
                    [annotation setSubtitle:p.address];
                    annotation.place = p;
                    p.annotation = annotation;
                    
                    [self.resultsArray addObject:p];
                    [self.mapView addAnnotation:p.annotation];
                }
            }
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {

        }];
        
        [manager.operationQueue addOperation:operation];
    }
}


-(void)getNearByPlacesForString:(NSString *)searchString callback:(void (^)(NSArray *array, NSString *error))callback{
    self.mapView.shouldShowResearch = YES;
    currentSearchType = QSearchTypeNearby;

    searchString = [[searchString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]] mutableCopy];
    
    NSMutableURLRequest* request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET" URLString:[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=%f,%f&radius=%i&keyword=%@&key=AIzaSyC67qOLCwRi1_6Z8g1zKA6OQkJekYIBDz8", self.mapView.centerCoordinate.latitude, self.mapView.centerCoordinate.longitude, [self visibileRadiusFromMap:self.mapView], searchString] parameters:@{} error:NULL];
    [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFHTTPRequestOperation *operation = [manager HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject){
        if (![responseObject[@"status"] isEqualToString:@"Failed"]) {
            NSMutableArray *placesArray = [[NSMutableArray alloc] init];
            NSArray *results = responseObject[@"results"];
            for (NSDictionary *result in results){
                Place *p = [[Place alloc] initWithDictionary:result];
                [placesArray addObject:p];
            }
            callback(placesArray, nil);
        }else{
            callback(nil, responseObject[@"error_message"]);
        }
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(nil, error.localizedDescription);
    }];
    
    [manager.operationQueue addOperation:operation];
}

- (void)getNearByPlacesForProfile:(PFObject *)searchProfile callback:(void (^)(NSArray *array, NSString *error))callback{
    
    
    NSMutableString *url = [NSMutableString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=AIzaSyC67qOLCwRi1_6Z8g1zKA6OQkJekYIBDz8&location=%f,%f&radius=%f", self.mapView.centerCoordinate.latitude, self.mapView.centerCoordinate.longitude, [searchProfile[@"radius"] floatValue]*1609.344];
    
    [url appendString:[NSString stringWithFormat:@"&%@=%@", @"keyword", searchProfile[@"keyword"]]];
    
    url = [[url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]] mutableCopy];
    
    NSMutableURLRequest* request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET" URLString:url parameters:@{} error:NULL];
    
    [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFHTTPRequestOperation *operation = [manager HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject){
        if (![responseObject[@"status"] isEqualToString:@"Failed"]) {
            NSMutableArray *placesArray = [[NSMutableArray alloc] init];
            NSArray *results = responseObject[@"results"];
            for (NSDictionary *result in results){
                Place *p = [[Place alloc] initWithDictionary:result];
                [placesArray addObject:p];
            }
            callback(placesArray, nil);
        }else{
            callback(nil, responseObject[@"error_message"]);
        }
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(nil, error.localizedDescription);
    }];
    
    [manager.operationQueue addOperation:operation];
}

#pragma mark - Ads API

- (void)getAdsForKeyword:(NSString *)keyword withLatitude:(NSString *)lat longitude:(NSString *)lon andRadius:(NSString *)radius
{
    NSURLSessionConfiguration* sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession* session = [NSURLSession sessionWithConfiguration:sessionConfig delegate:nil delegateQueue:nil];
    
    NSURL* URL = [NSURL URLWithString:@"http://qlumi.com/api/GetAds/GetAd"];
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:URL];
    request.HTTPMethod = @"POST";
    
    [request addValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    NSDictionary* bodyObject = @{
                                 @"lat": lat,
                                 @"lon": lon,
                                 @"search": keyword,
                                 @"radius": radius
                                 };
    
    request.HTTPBody = [NSJSONSerialization dataWithJSONObject:bodyObject options:kNilOptions error:NULL];
    
    NSURLSessionDataTask* task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error == nil) {
            NSDictionary *dictFromData = [NSJSONSerialization JSONObjectWithData:data
                                                                         options:NSJSONReadingAllowFragments
                                                                           error:&error];
            NSArray *adsArray = [dictFromData objectForKey:@"ads"];
            
            for (int i=0; i<adsArray.count; i++) {
                
                NSData *data = [adsArray[i][@"place_info"] dataUsingEncoding:NSUTF8StringEncoding];
                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                
                Place *ad = [[Place alloc] initWithDictionary:json[@"result"]];
                
                
                ad.type = GPlaceTypeAd;
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.resultsArray addObject:ad];
                    [self.mapView addAnnotation:ad.annotation];
                });
            }
        }
        else {
            // Failure

        }
    }];
    [task resume];
    [session finishTasksAndInvalidate];
}



@end
