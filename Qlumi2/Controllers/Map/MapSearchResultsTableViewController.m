//
//  MapSearchResultsTableViewController.m
//  Qlumi
//
//  Created by Will on 2/5/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import "MapSearchResultsTableViewController.h"

@interface MapSearchResultsTableViewController (){
    NSArray *placesArray;
}

@end

@implementation MapSearchResultsTableViewController
@synthesize searchString = _searchString;

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)setSearchString:(NSString *)searchString{
    _searchString = searchString;
    [self.tableView reloadData];

    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(search) object:nil];
    [self performSelector:@selector(search) withObject:nil afterDelay:0.5];
}


-(void)search{
    [self getAutoCompleteSuggestionsForString:self.searchString callback:^(NSArray *array) {
        placesArray = [array copy];
        [self.tableView reloadData];
    }];
}


#pragma mark - SearchBarDelegate

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self.delegate mapSearchResultsTableViewController:self didSelectSearchNearbyForString:self.searchString];
    [self dismissViewControllerAnimated:YES completion:nil];
}




#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return placesArray.count+2;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    if (indexPath.row == 0) {
        cell.textLabel.text = [NSString stringWithFormat:@"Search \"%@\" Nearby", self.searchString];
        cell.imageView.image = [UIImage imageNamed:@"magnifying-glass"];
    }else if (indexPath.row == 1){
        cell.textLabel.text = [NSString stringWithFormat:@"Search \"%@\" Along Route", self.searchString];
        cell.imageView.image = [UIImage imageNamed:@"searchRoute"];
    }else{
        Place *p = placesArray[indexPath.row-2];
        cell.textLabel.text = p.title;
        cell.imageView.image = [UIImage imageNamed:@"local-pin"];
    }
    
    cell.imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        [self.delegate mapSearchResultsTableViewController:self didSelectSearchNearbyForString:self.searchString];
    }else if (indexPath.row == 1) {
        [self.delegate mapSearchResultsTableViewController:self didSelectSearchAlongRouteForString:self.searchString];
    }else{
        [self.delegate mapSearchResultsTableViewController:self didSelectPlace:placesArray[indexPath.row-2]];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Google Places API

-(void)getAutoCompleteSuggestionsForString:(NSString *)searchString callback:(void (^)(NSArray *))callback{
    
    searchString = [[searchString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]] mutableCopy];
    
    NSMutableURLRequest* request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET" URLString:[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&key=AIzaSyC67qOLCwRi1_6Z8g1zKA6OQkJekYIBDz8", searchString] parameters:@{} error:NULL];
    [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFHTTPRequestOperation *operation = [manager HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject){
        if ([responseObject[@"status"] isEqualToString:@"OK"]) {
            NSArray *results = responseObject[@"predictions"];
            
            NSMutableArray *returnArray = [[NSMutableArray alloc] init];
            for (NSDictionary *result in results){
                Place *p = [[Place alloc] initWithPredicationDict:result];
                [returnArray addObject:p];
            }
            callback(returnArray);
        }
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {

    }];
    
    [manager.operationQueue addOperation:operation];
}

@end
