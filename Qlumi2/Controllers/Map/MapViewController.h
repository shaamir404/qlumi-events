//
//  MapViewController.h
//  Qlumi
//
//  Created by Will on 2/2/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QMapView.h"
#import "SearchFilterTableViewController.h"
#import "MapSearchResultsTableViewController.h"
#import "AppDelegate.h"
#import "MapListTableViewController.h"
#import <INTUAnimationEngine/INTUInterpolationFunctions.h>
#import <MBProgressHUD/MBProgressHUD.h>


typedef enum : NSUInteger {
    QSearchTypeNearby,
    QSearchTypeFilters,
    QSearchTypeNone,
} QSearchType;

@class MapViewController;
@protocol MapViewControllerDelegate <NSObject>
-(void)mapViewController:(MapViewController *)mapVC didDismissWithPlaces:(NSArray *)places;
@end


@interface MapViewController : UIViewController <UIPopoverPresentationControllerDelegate, SearchFilterSelectionDelegate, MKMapViewDelegate, UISearchControllerDelegate, UISearchBarDelegate, UISearchResultsUpdating, UITableViewDelegate, UITableViewDataSource, MapSearchResultsSelectionDelegate, QMapViewDelegate, CalloutControllerDelegate, MapListDelegate, UITextViewDelegate>
@property (nonatomic, weak) id <MapViewControllerDelegate>delegate;

-(id)initWithEditingEnabled:(BOOL)editable andSearchingEnabled:(BOOL)searching;
-(IBAction)toggleFilter:(UIBarButtonItem *)sender;
-(void)didRemovePlace:(Place *)place;


@property BOOL editingEnabled;
@property BOOL searchingEnabled;

@property QMapView *senderMap;

@property (nonatomic, strong) QMapView *mapView;
@property (strong, nonatomic) UISearchController *searchController;
@property (strong, nonatomic) MapSearchResultsTableViewController  *searchResultsController;
@property (strong, nonatomic) MapListTableViewController *mapListController;
@property (strong, nonatomic) SearchFilterTableViewController *filterController;

@property (strong, nonatomic) NSMutableArray *addedPlacesArray;
@property (strong, nonatomic) NSMutableArray *resultsArray;

@property (strong, nonatomic) UISegmentedControl *segmentedControl;



@end
