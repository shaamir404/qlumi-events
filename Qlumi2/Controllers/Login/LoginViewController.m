//
//  LoginViewController.m
//  Qlumi
//
//  Created by William Smillie on 1/5/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "HomeViewController.h"

@interface LoginViewController (){
    HomeViewController *homeVC;
}

@end

@implementation LoginViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self){
        [self initializeForm];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self){
        [self initializeForm];
    }
    return self;
}

-(instancetype)initWithHomeVC:(HomeViewController *)vc{
    self = [super init];
    if (self){
        homeVC = vc;
        [self initializeForm];
    }
    return self;
}

- (void)initializeForm {
    UIBarButtonItem *loginItem = [[UIBarButtonItem alloc] initWithTitle:@"Sign Up" style:UIBarButtonItemStylePlain target:self action:@selector(showSignUp:)];
    self.navigationItem.leftBarButtonItem = loginItem;
    
    
    XLFormDescriptor * form;
    XLFormSectionDescriptor * section;
    XLFormRowDescriptor * row;
    
    form = [XLFormDescriptor formDescriptorWithTitle:@"Login"];
    
    section = [XLFormSectionDescriptor formSection];
    
    
    [form addFormSection:section];
    
    
    
    
    //923217654321
    //1234567890
    
    row.height = 100;
    
    
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"countryCode" rowType:XLFormRowDescriptorTypeSelectorPickerView title:@"Country code"];
    [row.cellConfig setObject:[UIFont fontWithName:@"JosefinSans-Regular" size:17] forKey:@"textLabel.font"];
    row.selectorOptions = @[@"(+93) - Afghanistan",@"(+355) - Albania",@"(+213) - Algeria",@"(+1684) - American Samoa",@"(+376) - Andorra",@"(+244) - Angola",@"(+1264) - Anguilla",@"(+672) - Antarctica",@"(+1268) - Antigua And Barbuda",@"(+54) - Argentina",@"(+374) - Armenia",@"(+297) - Aruba",@"(+61) - Australia",@"(+43) - Austria",@"(+994) - Azerbaijan",@"(+1242) - Bahamas The",@"(+973) - Bahrain",@"(+880) - Bangladesh",@"(+1246) - Barbados",@"(+375) - Belarus",@"(+32) - Belgium",@"(+501) - Belize",@"(+229) - Benin",@"(+1441) - Bermuda",@"(+975) - Bhutan",@"(+591) - Bolivia",@"(+387) - Bosnia and Herzegovina",@"(+267) - Botswana",@"(+55) - Bouvet Island",@"(+55) - Brazil",@"(+246) - British Indian Ocean Territory",@"(+673) - Brunei",@"(+359) - Bulgaria",@"(+226) - Burkina Faso",@"(+257) - Burundi",@"(+855) - Cambodia",@"(+237) - Cameroon",@"(+1) - Canada",@"(+238) - Cape Verde",@"(+1345) - Cayman Islands",@"(+236) - Central African Republic",@"(+235) - Chad",@"(+56) - Chile",@"(+86) - China",@"(+61) - Christmas Island",@"(+672) - Cocos (Keeling) Islands",@"(+57) - Colombia",@"(+269) - Comoros",@"(+242) - Republic Of The Congo",@"(+242) - Democratic Republic Of The Congo",@"(+682) - Cook Islands",@"(+506) - Costa Rica",@"(+225) - Cote D'Ivoire (Ivory Coast)",@"(+385) - Croatia (Hrvatska)",@"(+53) - Cuba",@"(+357) - Cyprus",@"(+420) - Czech Republic",@"(+45) - Denmark",@"(+253) - Djibouti",@"(+1767) - Dominica",@"(+1809) - Dominican Republic",@"(+670) - East Timor",@"(+593) - Ecuador",@"(+20) - Egypt",@"(+503) - El Salvador",@"(+240) - Equatorial Guinea",@"(+291) - Eritrea",@"(+372) - Estonia",@"(+251) - Ethiopia",@"(+61) - External Territories of Australia",@"(+500) - Falkland Islands",@"(+298) - Faroe Islands",@"(+679) - Fiji Islands",@"(+358) - Finland",@"(+33) - France",@"(+594) - French Guiana",@"(+689) - French Polynesia",@"(+262) - French Southern Territories",@"(+241) - Gabon",@"(+220) - Gambia The",@"(+995) - Georgia",@"(+49) - Germany",@"(+233) - Ghana",@"(+350) - Gibraltar",@"(+30) - Greece",@"(+299) - Greenland",@"(+1473) - Grenada",@"(+590) - Guadeloupe",@"(+1671) - Guam",@"(+502) - Guatemala",@"(+44) - Guernsey and Alderney",@"(+224) - Guinea",@"(+245) - Guinea-Bissau",@"(+592) - Guyana",@"(+509) - Haiti",@"(+0) - Heard and McDonald Islands",@"(+504) - Honduras",@"(+852) - Hong Kong S.A.R.",@"(+36) - Hungary",@"(+354) - Iceland",@"(+91) - India",@"(+62) - Indonesia",@"(+98) - Iran",@"(+964) - Iraq",@"(+353) - Ireland",@"(+972) - Israel",@"(+39) - Italy",@"(+1876) - Jamaica",@"(+81) - Japan",@"(+44) - Jersey",@"(+962) - Jordan",@"(+7) - Kazakhstan",@"(+254) - Kenya",@"(+686) - Kiribati",@"(+850) - Korea North",@"(+82) - Korea South",@"(+965) - Kuwait",@"(+996) - Kyrgyzstan",@"(+856) - Laos",@"(+371) - Latvia",@"(+961) - Lebanon",@"(+266) - Lesotho",@"(+231) - Liberia",@"(+218) - Libya",@"(+423) - Liechtenstein",@"(+370) - Lithuania",@"(+352) - Luxembourg",@"(+853) - Macau S.A.R.",@"(+389) - Macedonia",@"(+261) - Madagascar",@"(+265) - Malawi",@"(+60) - Malaysia",@"(+960) - Maldives",@"(+223) - Mali",@"(+356) - Malta",@"(+44) - Man (Isle of)",@"(+692) - Marshall Islands",@"(+596) - Martinique",@"(+222) - Mauritania",@"(+230) - Mauritius",@"(+269) - Mayotte",@"(+52) - Mexico",@"(+691) - Micronesia",@"(+373) - Moldova",@"(+377) - Monaco",@"(+976) - Mongolia",@"(+1664) - Montserrat",@"(+212) - Morocco",@"(+258) - Mozambique",@"(+95) - Myanmar",@"(+264) - Namibia",@"(+674) - Nauru",@"(+977) - Nepal",@"(+599) - Netherlands Antilles",@"(+31) - Netherlands The",@"(+687) - New Caledonia",@"(+64) - New Zealand",@"(+505) - Nicaragua",@"(+227) - Niger",@"(+234) - Nigeria",@"(+683) - Niue",@"(+672) - Norfolk Island",@"(+1670) - Northern Mariana Islands",@"(+47) - Norway",@"(+968) - Oman",@"(+92) - Pakistan",@"(+680) - Palau",@"(+970) - Palestinian Territory Occupied",@"(+507) - Panama",@"(+675) - Papua new Guinea",@"(+595) - Paraguay",@"(+51) - Peru",@"(+63) - Philippines",@"(+64) - Pitcairn Island",@"(+48) - Poland",@"(+351) - Portugal",@"(+1787) - Puerto Rico",@"(+974) - Qatar",@"(+262) - Reunion",@"(+40) - Romania",@"(+70) - Russia",@"(+250) - Rwanda",@"(+290) - Saint Helena",@"(+1869) - Saint Kitts And Nevis",@"(+1758) - Saint Lucia",@"(+508) - Saint Pierre and Miquelon",@"(+1784) - Saint Vincent And The Grenadines",@"(+684) - Samoa",@"(+378) - San Marino",@"(+239) - Sao Tome and Principe",@"(+966) - Saudi Arabia",@"(+221) - Senegal",@"(+381) - Serbia",@"(+248) - Seychelles",@"(+232) - Sierra Leone",@"(+65) - Singapore",@"(+421) - Slovakia",@"(+386) - Slovenia",@"(+44) - Smaller Territories of the UK",@"(+677) - Solomon Islands",@"(+252) - Somalia",@"(+27) - South Africa",@"(+500) - South Georgia",@"(+211) - South Sudan",@"(+34) - Spain",@"(+94) - Sri Lanka",@"(+249) - Sudan",@"(+597) - Suriname",@"(+47) - Svalbard And Jan Mayen Islands",@"(+268) - Swaziland",@"(+46) - Sweden",@"(+41) - Switzerland",@"(+963) - Syria",@"(+886) - Taiwan",@"(+992) - Tajikistan",@"(+255) - Tanzania",@"(+66) - Thailand",@"(+228) - Togo",@"(+690) - Tokelau",@"(+676) - Tonga",@"(+1868) - Trinidad And Tobago",@"(+216) - Tunisia",@"(+90) - Turkey",@"(+7370) - Turkmenistan",@"(+1649) - Turks And Caicos Islands",@"(+688) - Tuvalu",@"(+256) - Uganda",@"(+380) - Ukraine",@"(+971) - United Arab Emirates",@"(+44) - United Kingdom",@"(+1) - United States",@"(+1) - United States Minor Outlying Islands",@"(+598) - Uruguay",@"(+998) - Uzbekistan",@"(+678) - Vanuatu",@"(+39) - Vatican City State (Holy See)",@"(+58) - Venezuela",@"(+84) - Vietnam",@"(+1284) - Virgin Islands (British)",@"(+1340) - Virgin Islands (US)",@"(+681) - Wallis And Futuna Islands",@"(+212) - Western Sahara",@"(+967) - Yemen",@"(+38) - Yugoslavia",@"(+260) - Zambia",@"(+263) - Zimbabwe"];
    
    row.required = YES;
    
    NSString *numm = [PFUser currentUser][@"phoneNumber"];
    NSString *countryCode = @"";
    
    
    NSLocale *currentLocale = [NSLocale currentLocale];  // get the current locale. //NSLocaleCountryCode
    countryCode = [currentLocale objectForKey:NSLocaleCountryCode];
    NSString *path = [[NSBundle mainBundle] pathForResource:@"DiallingCodes" ofType:@"plist"];
    NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:path];
    for (id key in dict) {
        
        if ([key isEqualToString:countryCode.lowercaseString]){
            countryCode = [dict objectForKey:key];
        }
    }
    
    row.value = [NSString stringWithFormat:@"+%@",countryCode];
    [row.cellConfig setValue:@"15.0" forKey:@"layer.cornerRadius"];
    [row.cellConfig setObject:@(kCALayerMaxXMinYCorner | kCALayerMinXMinYCorner) forKey:@"layer.maskedCorners"];
    
    
    
    
    
    if ([PFUser currentUser])   {
        
        NSLocale *currentLocale = [NSLocale currentLocale];  // get the current locale. //NSLocaleCountryCode
        NSString *countryCode = [currentLocale objectForKey:NSLocaleCountryCode];
        
        NSString *path = [[NSBundle mainBundle] pathForResource:@"DiallingCodes" ofType:@"plist"];
        NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:path];
        
        for (id key in dict) {
            
            if ([key isEqualToString:countryCode.lowercaseString]){
                countryCode = [dict objectForKey:key];
            }
        }
        
        row.value = [NSString stringWithFormat:@"+%@",countryCode];
        numm = [numm stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"+%@",countryCode] withString:@""];
    }
    [section addFormRow:row];
    
    
    
    
    
    
    
    
    // Email
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"phone" rowType:XLFormRowDescriptorTypePhone title:@"Phone Number"];
    [row.cellConfigAtConfigure setObject:@"345678901" forKey:@"textField.placeholder"];
    [row.cellConfig setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    [row.cellConfig setObject:[UIFont fontWithName:@"JosefinSans-Regular" size:17] forKey:@"textLabel.font"];
    [row addValidator:[XLFormRegexValidator formRegexValidatorWithMsg:@"Invalid Phone Number" regex:@"[2345689][0-9]{6}([0-9]{3})?"]];
    row.required = YES;
    [section addFormRow:row];
    
    [section addFormRow:row];
    
    // Password
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"password" rowType:XLFormRowDescriptorTypePassword title:@"Password "];
    [row.cellConfigAtConfigure setObject:@"********" forKey:@"textField.placeholder"];
    [row.cellConfig setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    [row.cellConfig setObject:[UIFont fontWithName:@"JosefinSans-Regular" size:17] forKey:@"textLabel.font"];
    [row.cellConfig setValue:@"15.0" forKey:@"layer.cornerRadius"];
    [row.cellConfig setObject:@(kCALayerMinXMaxYCorner | kCALayerMaxXMaxYCorner) forKey:@"layer.maskedCorners"];
    [section addFormRow:row];
    row.required = YES;
    [section addFormRow:row];
    
    
    // Section
    section = [XLFormSectionDescriptor formSection];
    [form addFormSection:section];
    
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"login" rowType:XLFormRowDescriptorTypeButton title:@"Login"];
    row.action.formSelector = @selector(login:);
    [row.cellConfig setObject:[UIFont fontWithName:@"JosefinSans-Regular" size:17] forKey:@"textLabel.font"];
    [row.cellConfig setValue:@"15.0" forKey:@"layer.cornerRadius"];
    [section addFormRow:row];
    
    // Section
    section = [XLFormSectionDescriptor formSection];
    [form addFormSection:section];
    
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"passwordReset" rowType:XLFormRowDescriptorTypeButton title:@"Forgot Password"];
    [row.cellConfig setObject:[UIFont fontWithName:@"JosefinSans-Regular" size:17] forKey:@"textLabel.font"];
    [row.cellConfig setValue:@"15.0" forKey:@"layer.cornerRadius"];
    row.action.formSelector = @selector(resetPassword:);
    
    
    
    
    
    [section addFormRow:row];
    
    
    
    
    
    for (XLFormSectionDescriptor *section in form.formSections) {
        for (XLFormRowDescriptor *row in section.formRows) {
            row.cellConfigAtConfigure[@"backgroundColor"] = [UIColor colorWithWhite:1.0 alpha:0.9];
            
            [self updateFormRow:row];
            
        }
    }
    
    
    self.form = form;
    
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.tableView.leftAnchor constraintEqualToAnchor:self.view.leftAnchor constant:20.0].active = YES;
    [self.tableView.rightAnchor constraintEqualToAnchor:self.view.rightAnchor constant:-20.0].active = YES;
    [self.tableView.topAnchor constraintEqualToAnchor:self.view.topAnchor constant:0].active = YES;
    [self.tableView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor constant:0].active = YES;
    
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    [self.view setBackgroundColor:[UIColor lightGrayColor]];
}


#pragma mark - IBActions


-(IBAction)showSignUp:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)login:(id)sender{
    NSDictionary *results = [self getFormResults];
    
    
    
    NSString *MobNum = results[@"phone"];
    
    NSString *string= results[@"countryCode"];
    if ([string rangeOfString:@"-"].location == NSNotFound) {
        MobNum = [NSString stringWithFormat:@"%@%@", string, results[@"phone"]];
        
    }
    else{
        NSRange searchFromRange = [string rangeOfString:@"("];
        NSRange searchToRange = [string rangeOfString:@")"];
        NSString *substring = [string substringWithRange:NSMakeRange(searchFromRange.location+searchFromRange.length, searchToRange.location-searchFromRange.location-searchFromRange.length)];
        MobNum = [NSString stringWithFormat:@"%@%@", substring, results[@"phone"]];
        
    }
    
    //(+355) - Albania
    
    
    
    
    
    if(![MobNum hasPrefix:@"+"]) {
        MobNum = [NSString stringWithFormat:@"+%@",MobNum];
    }
    
    [SVProgressHUD show];
    PFQuery *phoneNumberLoginQ = [PFUser query];
    [phoneNumberLoginQ whereKey:@"phoneNumber" equalTo:MobNum];
    [phoneNumberLoginQ getFirstObjectInBackgroundWithBlock:^(PFObject * _Nullable object, NSError * _Nullable error) {
        
        
        
        
        
        if (object) {
            NSMutableDictionary *info = [[NSMutableDictionary alloc] init];
            [info setObject:object[@"username"] forKey:@"username"];
            [info setObject:results[@"password"] forKey:@"password"];
            [self loginWithInfo:info];
        }
        else{
            [SVProgressHUD dismiss];
            UIAlertController *errorAlert = [UIAlertController alertControllerWithTitle:@"Error" message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
            [errorAlert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil]];
            [self presentViewController:errorAlert animated:YES completion:nil];
        }
    }];
    
    
    
}

-(void)loginWithInfo:(NSDictionary *)results{
    [PFUser logInWithUsernameInBackground:results[@"username"] password:results[@"password"] block:^(PFUser * _Nullable user, NSError * error) {
        [SVProgressHUD dismiss];
        if (user) {
            [homeVC refresh];
            [self.view.window.rootViewController dismissViewControllerAnimated:YES completion:^{
            }];
        }else{
            if (error) {
                UIAlertController *errorAlert = [UIAlertController alertControllerWithTitle:@"Error" message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
                [errorAlert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil]];
                [self presentViewController:errorAlert animated:YES completion:nil];
            }
        }
    }];
}

-(IBAction)resetPassword:(id)sender{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CustomAlertView * vc = [storyboard instantiateViewControllerWithIdentifier:@"CustomAlertView"];
    [vc setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    vc.title = @"Forget Password";
    [self presentViewController:vc animated:YES completion:nil];
}


-(void)enterResetCodeForPhoneNumber:(NSString *)number{
    UIAlertController *code = [UIAlertController alertControllerWithTitle:@"Code" message:@"Please enter your reset code" preferredStyle:UIAlertControllerStyleAlert];
    [code addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Reset Code";
        textField.keyboardType = UIKeyboardTypePhonePad;
    }];
    [code addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [code addAction:[UIAlertAction actionWithTitle:@"Submit" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self confirmCode:code.textFields[0].text forPhoneNumber:number];
    }]];
    [self presentViewController:code animated:YES completion:nil];
}

-(void)confirmCode:(NSString *)code forPhoneNumber:(NSString *)number{
    PFQuery *query = [PFUser query];
    [query whereKey:@"phoneNumber" equalTo:number];
    [query getFirstObjectInBackgroundWithBlock:^(PFObject * _Nullable object, NSError * _Nullable error) {
        if ([object[@"resetCode"] isEqualToString:code]) {
            [self resetPasswordForUser:(PFUser *)object];
        }else{
            NSLog(@"Failed try again!");
            [self enterResetCodeForPhoneNumber:number];
        }
    }];
}

-(void)resetPasswordForUser:(PFUser *)user{
    UIAlertController *code = [UIAlertController alertControllerWithTitle:@"New Password" message:@"Please enter your new password" preferredStyle:UIAlertControllerStyleAlert];
    [code addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Password";
        [textField setSecureTextEntry:YES];
        textField.keyboardType = UIKeyboardTypeDefault;
    }];
    
    [code addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [code addAction:[UIAlertAction actionWithTitle:@"Submit" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [PFCloud callFunctionInBackground:@"changeUserPassword" withParameters:@{@"userId": user.objectId, @"password": code.textFields[0].text} block:^(id  _Nullable object, NSError * _Nullable error) {
            if (!error) {
                UIAlertController *complete = [UIAlertController alertControllerWithTitle:@"Reset Complete" message:@"Your password has been changed!" preferredStyle:UIAlertControllerStyleAlert];
                [complete addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
                [self presentViewController:complete animated:YES completion:nil];
            }else{
                NSLog(@"Error: %@", error);
            }
        }];
    }]];
    [self presentViewController:code animated:YES completion:nil];
}

#pragma mark - Validation

-(BOOL)resultsAreValid:(NSDictionary *)results
{
    NSArray * array = [self formValidationErrors];
    [array enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        XLFormValidationStatus * validationStatus = [[obj userInfo] objectForKey:XLValidationStatusErrorKey];
        if ([validationStatus.rowDescriptor.tag isEqualToString:@"email"]){
            UITableViewCell * cell = [self.tableView cellForRowAtIndexPath:[self.form indexPathOfFormRow:validationStatus.rowDescriptor]];
            [self animateCell:cell];
        }else if ([validationStatus.rowDescriptor.tag isEqualToString:@"password"]){
            UITableViewCell * cell = [self.tableView cellForRowAtIndexPath:[self.form indexPathOfFormRow:validationStatus.rowDescriptor]];
            [self animateCell:cell];
        }
        
        UIAlertController *error = [UIAlertController alertControllerWithTitle:@"Invalid Value" message:validationStatus.msg preferredStyle:UIAlertControllerStyleAlert];
        [error addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil]];
        [self presentViewController:error animated:YES completion:nil];
    }];
    
    return (array.count==0)?YES:NO;
}


#pragma mark - helper methods

-(NSMutableDictionary *)getFormResults
{
    NSMutableDictionary * result = [NSMutableDictionary dictionary];
    for (XLFormSectionDescriptor *mysection in self.form.formSections) {
        if (!mysection.isMultivaluedSection){
            for (XLFormRowDescriptor * myrow in mysection.formRows) {
                if (myrow.tag && ![myrow.tag isEqualToString:@""]){
                    [result setObject:(myrow.value ?: [NSNull null]) forKey:myrow.tag];
                }
            }
        }
        else{
            NSMutableArray * multiValuedValuesArray = [NSMutableArray new];
            for (XLFormRowDescriptor * myrow in mysection.formRows) {
                if (myrow.value){
                    [multiValuedValuesArray addObject:myrow.value];
                }
            }
            [result setObject:multiValuedValuesArray forKey:mysection.multivaluedTag];
        }
    }
    return result;
}

-(void)animateCell:(UITableViewCell *)cell
{
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animation];
    animation.keyPath = @"position.x";
    animation.values =  @[ @0, @20, @-20, @10, @0];
    animation.keyTimes = @[@0, @(1 / 6.0), @(3 / 6.0), @(5 / 6.0), @1];
    animation.duration = 0.3;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    animation.additive = YES;
    
    [cell.layer addAnimation:animation forKey:@"shake"];
}


@end
