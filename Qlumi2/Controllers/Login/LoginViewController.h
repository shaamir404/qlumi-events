//
//  LoginViewController.h
//  Qlumi
//
//  Created by William Smillie on 1/5/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import <XLForm/XLForm.h>
#import <Parse/Parse.h>
#import <SVProgressHUD/SVProgressHUD.h>

@interface LoginViewController : XLFormViewController
-(instancetype)initWithHomeVC:(UIViewController *)vc;
@end
