//
//  EventDetailViewController.m
//  Qlumi2
//
//  Created by William Smillie on 12/11/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import "EventDetailViewController.h"

#import "SwipableContainerViewController.h"
#import "NewEventTableViewController.h"

@interface EventDetailViewController (){
    AppDelegate *appdel;
    
    MapViewController *mapViewController;
    
    EventDetailsCell *detailsCell;
    EventPhotosCell *photosCell;

    NSDateFormatter *dateFormatter;
    NSDateFormatter *timeFormatter;
    NSMutableArray *rows;
    
    NSMutableArray *placesArray;
    
    UIButton *editButton;
    UIButton *broadcastButton;
    UIButton *messagesButton;

    LEColorScheme *colorScheme;
    
    PFObject *rsvpObject;
}

@end

@implementation EventDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    appdel = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    mapViewController = [[MapViewController alloc] initWithEditingEnabled:NO andSearchingEnabled:YES];
    
    
    placesArray = [[NSMutableArray alloc] init];
    
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEEE, MMM d, yyyy"];
    timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"hh:mm a"];
    
    [self registerCells];
    [self setupUI];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self createHeaderView];
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self destroyHeaderView];
}



-(void)setPassedEvent:(Event *)passedEvent{
    _passedEvent = passedEvent;
    
    if ([[passedEvent objectForKey:@"featured"] boolValue]) {
        rows = [[NSMutableArray alloc] initWithObjects:@"FeaturedWebCell", nil];
    }else{
        rows = [[NSMutableArray alloc] initWithObjects:@"EventDetailsCell", @"EventLocationsCell", nil];
        
        if (passedEvent.about.length > 0) {
            [rows addObject:@"EventAboutCell"];
        }
        if (passedEvent.schedule.count > 0){
            [rows addObject:@"EventScheduleCell"];
        }
        if (passedEvent.guests.count > 0){
            [rows addObject:@"EventGuestsCell"];
            if ([self.passedEvent.user.objectId isEqualToString:[PFUser currentUser].objectId]) {
                [rows addObject:@"EventSendSMSButtonCell"];
            }
        }
        if (passedEvent.photoCount.intValue > 0) {
            [rows addObject:@"EventPhotosCell"];
        }
        [rows addObject:@"EventAddPhotosCell"];
        [rows addObject:@"EventDeleteCell"];
        
        
        PFQuery *rsvpQ = [PFQuery queryWithClassName:@"Invite"];
        [rsvpQ whereKey:@"phoneNumber" equalTo:[PFUser currentUser][@"phoneNumber"]];
        [rsvpQ whereKey:@"event" equalTo:self.passedEvent];
        [rsvpQ getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError * _Nullable error) {
            if (object) {
                rsvpObject = object;
                
            }else{
                PFObject *object = [PFObject objectWithClassName:@"Invite"];
                [object setObject:[PFUser currentUser][@"phoneNumber"] forKey:@"phoneNumber"];
                [object setObject:self.passedEvent forKey:@"event"];
                [object setObject:@(NO) forKey:@"shouldSendSMS"];
                [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                    if (succeeded) {
                        rsvpObject = object;
                    }
                }];
                
            }
        }];
        
        
        NSMutableArray *placeIds = [[NSMutableArray alloc] init];
        for (NSDictionary *location in passedEvent.locations) {
            if (location[@"placeId"]) {
                [placeIds addObject:location[@"placeId"]];
            }
        }
        [self getPlacesByIDs:placeIds];
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

#pragma mark - UI Setup

-(void)createHeaderView{
    self.headerView = [[EventHeaderView alloc] initWithFrame:[self headerViewFrame]];
    self.headerView.expansionMode = GSKStretchyHeaderViewExpansionModeTopOnly;
    self.headerView.contentAnchor = GSKStretchyHeaderViewContentAnchorBottom;
    self.headerView.stretchDelegate = self;
    self.headerView.maximumContentHeight = 250;
    self.view.clipsToBounds = YES;
    self.headerView.minimumContentHeight = 64;
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone) {
        if ((int)[[UIScreen mainScreen] nativeBounds].size.height == 2436){
            self.headerView.minimumContentHeight = 88;
        }
    }
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 11.0)
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;

    UIButton *closeButton = [[UIButton alloc] initWithFrame:CGRectMake(4, 16, 60, 50)];
    closeButton.layer.masksToBounds = NO;
    closeButton.layer.cornerRadius = 10;
    closeButton.layer.shadowOffset = CGSizeMake(1.5, 1.5);
    closeButton.layer.shadowRadius = 3;
    closeButton.layer.shadowOpacity = .75;
    closeButton.layer.shadowColor = [UIColor blackColor].CGColor;

    [closeButton setTitle:@"❮ Back" forState:UIControlStateNormal];
    
//    [closeButton setImage:[[UIImage imageNamed:@"close"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(dismiss:) forControlEvents:UIControlEventTouchUpInside];
    [closeButton setTintColor:[UIColor whiteColor]];
    [self.headerView addSubview:closeButton];
    
    
    messagesButton = [[UIButton alloc] initWithFrame:CGRectMake((self.view.frame.size.width-50)-4, 16, 32, 32)];
    [messagesButton setBackgroundImage:[[UIImage imageNamed:@"message"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    [messagesButton setTintColor:[UIColor whiteColor]];
    [messagesButton addTarget:self action:@selector(messages:) forControlEvents:UIControlEventTouchUpInside];
    [messagesButton setTitle:[NSString stringWithFormat:@"%i", [self.passedEvent[@"messageCount"] intValue]] forState:UIControlStateNormal];
    messagesButton.layer.masksToBounds = NO;
    messagesButton.layer.cornerRadius = 10;
    messagesButton.layer.shadowOffset = CGSizeMake(1.5, 1.5);
    messagesButton.layer.shadowRadius = 3;
    messagesButton.layer.shadowOpacity = .75;
    messagesButton.layer.shadowColor = [UIColor blackColor].CGColor;
    [self.headerView addSubview:messagesButton];
    
    NSLog(@"event userId: %@, current userId: %@", self.passedEvent.user.objectId, [PFUser currentUser].objectId);
    
    if ([self.passedEvent.user.objectId isEqualToString:[PFUser currentUser].objectId]) {
        editButton = [[UIButton alloc] initWithFrame:CGRectMake((messagesButton.frame.origin.x-45), 16, 50, 50)];
        [editButton setTitle:@"Edit" forState:UIControlStateNormal];
        editButton.layer.masksToBounds = NO;
        editButton.layer.cornerRadius = 10;
        editButton.layer.shadowOffset = CGSizeMake(1.5, 1.5);
        editButton.layer.shadowRadius = 3;
        editButton.layer.shadowOpacity = .75;
        editButton.layer.shadowColor = [UIColor blackColor].CGColor;
        [editButton addTarget:self action:@selector(edit:) forControlEvents:UIControlEventTouchUpInside];
        [editButton setTintColor:[UIColor whiteColor]];
        [self.headerView addSubview:editButton];
    }

    
    
    
    self.tableView.scrollIndicatorInsets = UIEdgeInsetsMake(250, 0, 0, 0);
    self.headerView.contentExpands = YES;
    self.headerView.eventLabel.text = self.passedEvent.title;
    self.headerView.stretchDelegate = (SwipableContainerViewController *)[(UINavigationController *)[self presentingViewController] presentedViewController];
    [self.tableView addSubview:self.headerView];
    
    [self.headerView.imageView sd_setImageWithURL:[NSURL URLWithString:[self.passedEvent image].url] placeholderImage:[UIImage imageNamed:@"photo-placeholder"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        [self updateColorsForImage:image];
        self.headerView.imageView.frame = self.headerView.bounds;
    }];
    [self.tableView setContentOffset:CGPointMake(0, -self.headerView.maximumContentHeight) animated:NO];
    
    self.headerView.imageView.backgroundColor = [UIColor orangeColor];
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    
    messagesButton.frame = CGRectMake((self.view.frame.size.width-40), 24, 30, 30);
    editButton.frame = CGRectMake((messagesButton.frame.origin.x-50), 16, 50, 50);

    self.headerView.imageView.frame = self.headerView.bounds;
}

-(void)destroyHeaderView{
    [self.headerView removeFromSuperview];
    self.headerView = nil;
    
    [editButton removeFromSuperview];
    editButton = nil;
}

-(void)setupUI{
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.allowsSelection = YES;
    
    UIBarButtonItem *closeButton = [[UIBarButtonItem alloc]  initWithTitle:@"❮ Back" style:UIBarButtonItemStyleDone target:self action:@selector(dismiss:)];
    self.navigationItem.leftBarButtonItem = closeButton;
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
}

-(void)registerCells{
    UINib *cellNib = [UINib nibWithNibName:@"cell" bundle:nil];
    [self.tableView registerNib:cellNib forCellReuseIdentifier:@"cell"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"EventLocationsCell" bundle:nil] forCellReuseIdentifier:@"EventLocationsCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"EventDetailsCell" bundle:nil] forCellReuseIdentifier:@"EventDetailsCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"EventAboutCell" bundle:nil] forCellReuseIdentifier:@"EventAboutCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"EventScheduleCell" bundle:nil] forCellReuseIdentifier:@"EventScheduleCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"EventGuestsCell" bundle:nil] forCellReuseIdentifier:@"EventGuestsCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"EventSendSMSButtonCell" bundle:nil] forCellReuseIdentifier:@"EventSendSMSButtonCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"EventPhotosCell" bundle:nil] forCellReuseIdentifier:@"EventPhotosCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"EventAddPhotosCell" bundle:nil] forCellReuseIdentifier:@"EventAddPhotosCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"EventDeleteCell" bundle:nil] forCellReuseIdentifier:@"EventDeleteCell"];
    [self.tableView registerNib:[UINib nibWithNibName:@"FeaturedWebCell" bundle:nil] forCellReuseIdentifier:@"FeaturedWebCell"];
}

-(void)updateColorsForImage:(UIImage *)image{
    LEColorPicker *colorPicker = [[LEColorPicker alloc] init];
    colorScheme = [colorPicker colorSchemeFromImage:image];
    
    [self.tableView reloadData];
}



#pragma mark - UITableView Datasource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return rows.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [NSClassFromString(rows[indexPath.row]) sectionHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    NSString *cellIdentifier = [NSString stringWithFormat:@"%@", rows[indexPath.row]];
    
    EventTableViewBaseCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    [cell setTint:colorScheme.backgroundColor];
    
    if ([cellIdentifier isEqualToString:@"FeaturedWebCell"]) {
        [(FeaturedWebCell *)cell setSectionTitle:self.passedEvent.featureTitle];
        NSString * htmlString = [NSString stringWithFormat:@"<span style=\"font-family: %@; font-size: %i\">%@</span>",
                                              @"JosefinSans-Regular",
                                              16,
                                              self.passedEvent[@"featureBody"]];
        [[(FeaturedWebCell *)cell webView] loadHTMLString:htmlString baseURL:nil];
        
        messagesButton.hidden = YES;

    }else if ([cellIdentifier isEqualToString:@"EventLocationsCell"]) {
        EventLocationsCell *mapCell = (EventLocationsCell *)cell;
        
        for (Place *p in placesArray) {
            QMapAnnotation *annotation = [[QMapAnnotation alloc] init];
            p.annotation = annotation;
            [annotation setCoordinate:p.location];
            [annotation setTitle:p.title]; //You can set the subtitle too
            [annotation setSubtitle:p.address];
            annotation.place = p;
            [mapCell.mapView addAnnotation:annotation];
        }
        
        [mapCell.mapView zoomToPins];

        
    }else if ([cellIdentifier isEqualToString:@"EventDetailsCell"]){
        detailsCell = (EventDetailsCell *)cell;
        detailsCell.RSVPDelegate = self;
        
        NSString *rsvp = rsvpObject[@"rsvp"];
        if ([rsvp isEqualToString:@"yes"]) {
            detailsCell.yesButton.backgroundColor = [UIColor colorWithRed:0.33 green:0.71 blue:0.64 alpha:1.0];
            detailsCell.maybeButton.backgroundColor = [UIColor whiteColor];
            detailsCell.noButton.backgroundColor = [UIColor whiteColor];
            
        }else if ([rsvp isEqualToString:@"no"]) {
            detailsCell.noButton.backgroundColor = [UIColor colorWithRed:0.88 green:0.44 blue:0.34 alpha:1.0];
            detailsCell.maybeButton.backgroundColor = [UIColor whiteColor];
            detailsCell.yesButton.backgroundColor = [UIColor whiteColor];
            
        }else if ([rsvp isEqualToString:@"maybe"]) {
            detailsCell.maybeButton.backgroundColor = [UIColor colorWithRed:0.97 green:0.66 blue:0.33 alpha:1.0];
            detailsCell.noButton.backgroundColor = [UIColor whiteColor];
            detailsCell.yesButton.backgroundColor = [UIColor whiteColor];
            
        }else{
            detailsCell.maybeButton.backgroundColor =  [UIColor whiteColor];
            detailsCell.noButton.backgroundColor = [UIColor whiteColor];
            detailsCell.yesButton.backgroundColor = [UIColor whiteColor];
        }
    
        
        if ([[PFUser currentUser].objectId isEqualToString:self.passedEvent.user.objectId]) {
            detailsCell.maybeButton.enabled = NO;
            detailsCell.noButton.enabled = NO;
            detailsCell.yesButton.enabled = NO;
            
            detailsCell.maybeButton.alpha = 0.5;
            detailsCell.noButton.alpha = 0.5;
            detailsCell.yesButton.alpha = 0.5;
            detailsCell.yesButton.backgroundColor = [UIColor colorWithRed:0.33 green:0.71 blue:0.64 alpha:1.0];
        }

        
        
        detailsCell.clipsToBounds = NO;
//        [detailsCell.profilePictureImageView sd_setImageWithURL:[NSURL URLWithString:[(PFFile *) self.passedEvent.user[@"profilePicture"] url]]];
        detailsCell.profilePictureImageView.alpha = 0;
        detailsCell.dateLabel.text = [dateFormatter stringFromDate:self.passedEvent.date];
        detailsCell.timeLabel.text = [timeFormatter stringFromDate:self.passedEvent.date];
        if (self.passedEvent.locations.count > 0) {
            if ([self.passedEvent.locations[0] objectForKey:@"title"] && [self.passedEvent.locations[0] objectForKey:@"address"]) {
//                detailsCell.venueLabel.text = [self.passedEvent.locations[0] objectForKey:@"title"];
                if (![self.passedEvent.locations[0][@"address"] isKindOfClass:[NSNull class]]){
                    detailsCell.addressLabel.text = [self.passedEvent.locations[0] objectForKey:@"address"];
                }
            }
        }
    }else if ([cellIdentifier isEqualToString:@"EventAboutCell"]){
        EventAboutCell *aboutCell = (EventAboutCell *)cell;
        aboutCell.textView.text = self.passedEvent.about;
        
    }else if ([cellIdentifier isEqualToString:@"EventScheduleCell"]){
        if ([(EventScheduleCell *)cell scheduleArray].count == 0) {
            NSMutableArray *objects = [[NSMutableArray alloc] init];
            for (NSDictionary *i in self.passedEvent.schedule) {
                ScheduledObject *o = [[ScheduledObject alloc] init];
                o.title = i[@"title"];
                o.startTime = i[@"startTime"];
                o.endTime = i[@"endTime"];
                [objects addObject:o];
            }
            
            [(EventScheduleCell *)cell setScheduleArray:objects];
            [[(EventScheduleCell *)cell collectionView] reloadData];
        }
    }else if ([cellIdentifier isEqualToString:@"EventGuestsCell"]){
        [(EventGuestsCell *)cell setEditingEnabled:NO];
        [(EventGuestsCell *)cell setGuestsArray:[self.passedEvent.guests mutableCopy]];
        [[(EventGuestsCell *)cell collectionView] reloadData];
    }else if ([cellIdentifier isEqualToString:@"EventSendSMSButtonCell"]){
        [(EventSendSMSButtonCell *)cell setGuestsArray:[self.passedEvent.guests mutableCopy]];

//        [(EventSendSMSButtonCell *)cell setEditingEnabled:NO];
//        [(EventSendSMSButtonCell *)cell setGuestsArray:[self.passedEvent.guests mutableCopy]];
//        [[(EventGuestsCell *)cell collectionView] reloadData];
    }else if ([cellIdentifier isEqualToString:@"EventPhotosCell"]){
        photosCell = (EventPhotosCell *)cell;
        
        [(EventPhotosCell *)cell setEditingEnabled:NO];
        [(EventPhotosCell *)cell setSelectionDelegate:self];
        [(EventPhotosCell *)cell setPhotosArray:self.passedEvent.photos];
    }else if ([cellIdentifier isEqualToString:@"EventAddPhotosCell"]){
        [(EventAddPhotosCell *)cell setPhotoSelectionDelegate:self];
        [(EventAddPhotosCell *)cell setShouldDisplaySectionHeader:(self.passedEvent.photoCount == 0)];
        [cell.sectionTitleLabel setText:[(EventAddPhotosCell *)cell sectionTitle]];
        if ((self.passedEvent.photoCount == 0)) {
            cell.line.hidden = NO;
        }else{
            cell.line.hidden = YES;
        }
    }else if ([cellIdentifier isEqualToString:@"EventDeleteCell"]){
        [(EventDeleteCell *)cell setDelegate:self];
    }

    return cell;
}

#pragma mark - UITableView Delegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 1) {
        EventLocationsCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:mapViewController];
        [self presentViewController:nav animated:YES completion:^{
            mapViewController.addedPlacesArray = placesArray;
            [mapViewController.mapView addAnnotations:cell.mapView.annotations];
            [mapViewController.mapView setRegion:cell.mapView.region];
            [mapViewController.mapListController.tableView reloadData];
            
            if (mapViewController.addedPlacesArray.count > 0) {
                [mapViewController.segmentedControl setSelectedSegmentIndex:1];
                [mapViewController.segmentedControl setTitle:[NSString stringWithFormat:@"Tagged (%lu)", (unsigned long)mapViewController.addedPlacesArray.count] forSegmentAtIndex:1];
                
                if ([mapViewController respondsToSelector:@selector(segmentDidChange:)]){
                    [mapViewController performSelector:@selector(segmentDidChange:) withObject:mapViewController.segmentedControl];
                };
            }else{
                [mapViewController.segmentedControl setSelectedSegmentIndex:0];
            }
            
//            mapViewController.delegate = self;
        }];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark - eventDeleteCell

-(void)eventDeleteCellShouldDelete:(EventDeleteCell *)cell{
    UIAlertController *deleteAlert = [UIAlertController alertControllerWithTitle:@"Delete Event" message:@"Are you sure you would like to delete this event? You will no longer see it in your feed." preferredStyle:UIAlertControllerStyleAlert];
    [deleteAlert addAction:[UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        NSMutableArray *archivedForUsers = self.passedEvent[@"archivedForUsers"];
        if (!archivedForUsers) {
            archivedForUsers = [[NSMutableArray alloc] init];
        }
        if (![archivedForUsers containsObject:[PFUser currentUser][@"phoneNumber"]]) {
            [archivedForUsers addObject:[PFUser currentUser][@"phoneNumber"]];
        }
        
        [self.passedEvent setObject:archivedForUsers forKey:@"archivedForUsers"];
        [self.passedEvent saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
    }]];
    [deleteAlert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:deleteAlert animated:YES completion:nil];
}

#pragma mark - RSVPDelegate
-(void)detailCell:(EventDetailsCell *)cell didRSVP:(NSString *)rsvp{
    [rsvpObject setObject:rsvp forKey:@"rsvp"];
    
    [rsvpObject saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
        if (succeeded) {
            [self.tableView reloadData];
        }
    }];
}

#pragma mark - PhotoSelectionDelegate

-(void)eventPhotosCell:(EventPhotosCell *)cell didSelectPhotoAtIndex:(NSInteger)index{
    LPPhotoViewer *photosViewer = [[LPPhotoViewer alloc] init];
    photosViewer.imgArr = cell.photoURLs;
    photosViewer.currentIndex = index;
    [self presentViewController:photosViewer animated:YES completion:nil];
}

-(void)eventPhotosCell:(EventPhotosCell *)cell didDeletePhotoAtIndex:(NSInteger)index{
    self.passedEvent.photoCount = [NSNumber numberWithInt:[self.passedEvent.photoCount intValue]-1];
    [self setPassedEvent:self.passedEvent];
}

#pragma mark - uploadPhotos

-(void)AddPhotosCell:(EventAddPhotosCell *)cell didSelectPhoto:(UIImage *)image{
    PFObject *photo = [PFObject objectWithClassName:@"Photo"];
    photo[@"image"] = [PFFile fileWithName:@"photo.png" data:UIImageJPEGRepresentation(image, 0.5)];
    photo[@"event"] = self.passedEvent;
    photo[@"user"] = [PFUser currentUser];
    [SVProgressHUD show];
    [photo saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
        [SVProgressHUD dismiss];
        if (!error && succeeded) {
            PFRelation *relation = [self.passedEvent relationForKey:@"photos"];
            [relation addObject:photo];
            [self.passedEvent saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
                [SVProgressHUD dismiss];
                if (succeeded) {
                    self.passedEvent.photoCount = [NSNumber numberWithInteger:[self.passedEvent.photoCount integerValue]+1];
                    [self setPassedEvent:self.passedEvent];
                    [self.tableView reloadData];
                }else{
                    [self.passedEvent saveEventually];
                }
            }];
        }
    }];
}

#pragma mark - Transition Animiation <RMPZoomTransitionAnimating>

-(CGRect)headerViewFrame{
    
    CGRect frame = self.headerView.frame;
    frame.origin.x = 0;
    frame.origin.y = 0;
    
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone && ((int)[[UIScreen mainScreen] nativeBounds].size.height == 2436)) {
        frame.size.height += 88;
    }else{
        frame.size.height += 64;
    }
    
    
    return frame;
}

- (UIImageView *)transitionSourceImageView
{
    UIImageView *imageView = [[UIImageView alloc] initWithImage:self.headerView.imageView.image];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;
    imageView.userInteractionEnabled = NO;
    imageView.frame = self.headerView.bounds;
    
    return imageView;
}

- (UIColor *)transitionSourceBackgroundColor
{
    return self.view.backgroundColor;
}

- (CGRect)transitionDestinationImageViewFrame
{
    return [self headerViewFrame];
}

#pragma mark - <RMPZoomTransitionDelegate>

- (void)zoomTransitionAnimator:(RMPZoomTransitionAnimator *)animator
         didCompleteTransition:(BOOL)didComplete
      animatingSourceImageView:(UIImageView *)imageView
{
    self.headerView.imageView.image = imageView.image;
    [self updateColorsForImage:self.headerView.imageView.image];
}


#pragma mark - StretchyHeaderDelegate

-(void)stretchyHeaderView:(GSKStretchyHeaderView *)headerView didChangeStretchFactor:(CGFloat)stretchFactor{
    if (stretchFactor > 1.65) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}


#pragma mark - IBActions

-(IBAction)dismiss:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)broadcast:(id)sender{
    
}

-(IBAction)edit:(id)sender{
    NewEventViewController *eventVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"NewEventViewController"];
    [eventVC setDelegate:self];
    [eventVC setPassedEvent: self.passedEvent];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:eventVC];
    nav.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    
    if (self.parentViewController){
        [self.parentViewController presentViewController:nav animated:YES completion:nil];
    }else{
        [self presentViewController:nav animated:YES completion:nil];
    }
}

-(IBAction)messages:(id)sender{
    MessagesViewController *messagesVC = [[MessagesViewController alloc] init];
    messagesVC.passedEvent = self.passedEvent;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:messagesVC];
    
    UIBarButtonItem *closeButton = [[UIBarButtonItem alloc]  initWithTitle:@"❮ Back" style:UIBarButtonItemStyleDone target:self action:@selector(dismiss:)];
    messagesVC.navigationItem.leftBarButtonItem = closeButton;

    
    [self presentViewController:nav animated:YES completion:nil];
}


#pragma mark - New Event Delegate

-(void)newEventTableViewController:(NewEventTableViewController *)vc didSaveWithEvent:(Event *)event{
    self.passedEvent = event;
    [self viewDidLoad];
}

#pragma mark - New Event (swift) Delegate

-(void)didAddEventWithEvent:(Event *)event {
    self.passedEvent = event;
    [self viewDidLoad];
}


#pragma mark - Google Places helper

-(void)getPlacesByIDs:(NSArray *)ids{
    
    __block int completedRequests = 0;
    
    
    for (NSString *id in ids) {
        
        NSMutableURLRequest* request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET" URLString:[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/details/json?key=AIzaSyC67qOLCwRi1_6Z8g1zKA6OQkJekYIBDz8&placeid=%@", id] parameters:@{} error:NULL];
        [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        AFHTTPRequestOperation *operation = [manager HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject){
            completedRequests++;
            NSDictionary *result = responseObject[@"result"];
            Place *p = [[Place alloc] initWithDictionary:result];
            [placesArray addObject:p];
            
            if (completedRequests == ids.count) {
                [self fetchDidComplete:placesArray];
            }
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            if (completedRequests == ids.count) {
                [self fetchDidComplete:placesArray];
            }
            completedRequests++;
        }];
        
        [manager.operationQueue addOperation:operation];
    }
}

-(void)fetchDidComplete:(NSMutableArray *)places{
    
    NSMutableArray *origOrderArray = [[NSMutableArray alloc] init];
    for (NSDictionary *location in self.passedEvent.locations) {
        if (location[@"placeId"]) {
            [origOrderArray addObject:location[@"placeId"]];
        }
    }
    
    
    NSMutableArray *newOrderArray = [[NSMutableArray alloc] init];
    for (NSString *id in origOrderArray) {
        for (Place *p in places) {
            if ([p.placeId isEqualToString:id]) {
                [newOrderArray addObject:p];
                break;
            }
        }
    }
    
    places = newOrderArray;

    
    
    for (int i=0; i<self.passedEvent.locations.count; i++) {
        NSDictionary *location = self.passedEvent.locations[i];
        if (!location[@"placeId"]) {
            Place *p = [[Place alloc] init];
            p.location = CLLocationCoordinate2DMake([location[@"latitude"] floatValue], [location[@"longitude"] floatValue]);
            p.title = location[@"title"];
            p.type = GPlaceTypeNote;
            
            [places insertObject:p atIndex:i];
        }
    }
    
    
    placesArray = places;
    EventLocationsCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    [cell.mapView removeAnnotations:cell.mapView.annotations];


    [self.tableView reloadData];
}


@end
