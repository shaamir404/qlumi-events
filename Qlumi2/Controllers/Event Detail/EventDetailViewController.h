//
//  EventDetailViewController.h
//  Qlumi2
//
//  Created by William Smillie on 12/11/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import <UIKit/UIKit.h>

// My Classes
#import "Event.h"
#import "EventHeaderView.h"
#import "MapViewController.h"
#import "MessagesViewController.h"
#import "NewEventTableViewController.h"

#import "FeaturedWebCell.h"
#import "EventLocationsCell.h"
#import "EventDetailsCell.h"
#import "EventAboutCell.h"
#import "EventScheduleCell.h"
#import "EventGuestsCell.h"
#import "EventSendSMSButtonCell.h"
#import "EventPhotosCell.h"
#import "EventAddPhotosCell.h"
#import "EventDeleteCell.h"

//3rd Party
#import "RMPZoomTransitionAnimator.h"
#import <LEColorPicker/LEColorPicker.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIView+WebCache.h>
#import <LPPhotoViewer/LPPhotoViewer.h>
#import "UIBarButtonItem+Badge.h"


@interface EventDetailViewController : UITableViewController <RMPZoomTransitionAnimating, RMPZoomTransitionDelegate, GSKStretchyHeaderViewStretchDelegate, EventAddPhotosCellDelegate, EventDetailsRSVPDelegate, EventPhotosSelectionDelegate, NewEventDelegate, UIPopoverPresentationControllerDelegate, UIPopoverControllerDelegate, NewEventViewControllerDelegate>

@property (nonatomic) Event *passedEvent;
@property NSInteger index;

@property EventHeaderView *headerView;

-(void)setupUI;
-(void)updateColorsForImage:(UIImage *)image;

@end
