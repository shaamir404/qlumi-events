//
//  ScheduleTableViewController.h
//  Qlumi
//
//  Created by William Smillie on 11/3/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

#import <Parse/Parse.h>
#import "ScheduleTableViewCell.h"

@interface ScheduleTableViewController : UITableViewController <ScheduleCellDelegate>

@end
