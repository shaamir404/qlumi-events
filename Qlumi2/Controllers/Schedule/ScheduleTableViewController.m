//
//  ScheduleTableViewController.m
//  Qlumi
//
//  Created by William Smillie on 11/3/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import "ScheduleTableViewController.h"
#import "ScheduleTableViewCell.h"
#import "ScheduleCollectionViewCell.h"
#import "Event.h"
#import "AppDelegate.h"

#import "EventDetailViewController.h"

@interface ScheduleTableViewController (){
    AppDelegate *appdel;
    
    NSArray *scheduledEventsArray;

    NSDateFormatter *dateFormatter;
    NSDateFormatter *timeFormatter;
}

@end

@implementation ScheduleTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    appdel = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM d"];

    timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"hh :mm a"];

    [self refresh];
}


#pragma mark - backend

-(void)refresh{
    PFQuery *invitedEventIdsQ = [PFQuery queryWithClassName:@"Invite"];
    [invitedEventIdsQ whereKey:@"phoneNumber" equalTo:[PFUser currentUser][@"phoneNumber"]];
    [invitedEventIdsQ setLimit:100];
    [invitedEventIdsQ findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        if (objects && !error) {
            NSMutableArray *invitedEventIds = [[NSMutableArray alloc] init];
            for (PFObject *object in objects) {
                [invitedEventIds addObject: [object[@"event"] objectId]];
            }
            
            
            PFQuery *myEventsQ = [PFQuery queryWithClassName:@"Event"];
            [myEventsQ whereKey:@"user" equalTo:[PFUser currentUser]];
            
            PFQuery *invitedEventsQ = [PFQuery queryWithClassName:@"Event"];
            [invitedEventsQ whereKey:@"objectId" containedIn:invitedEventIds];
            
            PFQuery *aggregateQ = [PFQuery orQueryWithSubqueries:@[myEventsQ, invitedEventsQ]];
            [aggregateQ orderByAscending:@"date"];
            [aggregateQ includeKey:@"user"];
//            [aggregateQ whereKey:@"date" greaterThan:[NSDate new]];
            [aggregateQ whereKey:@"archived" notEqualTo:@(YES)];
            [aggregateQ whereKey:@"archivedForUsers" notContainedIn:@[[PFUser currentUser][@"phoneNumber"]]];
            [aggregateQ findObjectsInBackgroundWithBlock:^(NSArray * _Nullable eventsArray, NSError * _Nullable error) {
                if (!error && eventsArray) {
                    scheduledEventsArray = eventsArray;
                    [self.tableView reloadData];
                }
            }];
        }
    }];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return scheduledEventsArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ScheduleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    Event *event = (Event *)scheduledEventsArray[indexPath.row];
 
    NSMutableArray *objects = [[NSMutableArray alloc] init];
    if (event.schedule) {
        for (NSDictionary *i in event.schedule) {
            ScheduledObject *o = [[ScheduledObject alloc] init];
            o.title = i[@"title"];
            o.startTime = i[@"startTime"];
            o.endTime = i[@"endTime"];
            [objects addObject:o];
        }
    }
    
    ScheduledObject *o = [[ScheduledObject alloc] init];
    o.title = event[@"title"];
    o.startTime = [timeFormatter stringFromDate:event[@"date"]];
    [objects insertObject:o atIndex:0];
    
    cell.dateLabel.text = [dateFormatter stringFromDate:event.date];
    cell.eventImageURL = [NSURL URLWithString:event.image.url];
    cell.delegate = self;
    
    cell.scheduledObjectsArray = objects;
    [cell.scheduleCollectionView reloadData];
    
    return cell;
}


-(void)scheduleTableViewCell:(id)cell shouldPresentEventForIndexPath:(NSIndexPath *)indexPath{
    NewEventViewController *eventVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"NewEventViewController"];
    [eventVC setPassedEvent:scheduledEventsArray[indexPath.row]];
    [eventVC setHierarchy:NewEventHierarchyDETAILS];
    UINavigationController * navVC = [[UINavigationController alloc] initWithRootViewController:eventVC];
    [self presentViewController:navVC animated:true completion:nil];

}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(IBAction)dismiss:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
