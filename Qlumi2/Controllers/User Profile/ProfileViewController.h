//
//  ProfileViewController.h
//  Qlumi
//
//  Created by Will on 1/7/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import <XLForm/XLForm.h>
#import <Parse/Parse.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import "ProfilePictureFormCell.h"
#import "CountryPicker.h"

@interface ProfileViewController :  XLFormViewController

-(instancetype)initWithHomeVC:(UIViewController *)vc;

@end
