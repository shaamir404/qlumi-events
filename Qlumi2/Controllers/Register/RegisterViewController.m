//
//  RegisterViewController.m
//  Qlumi
//
//  Created by William Smillie on 1/5/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import "RegisterViewController.h"
#import "LoginViewController.h"
#import <SDWebImage/SDWebImageDownloader.h>
#import "HomeViewController.h"

#import <SafariServices/SafariServices.h>

@interface RegisterViewController (){
    HomeViewController *homeVC;
    XLFormRowDescriptor *profPicRow;
}
@end

@implementation RegisterViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self){
        self.title = @"Sign Up";
        [self initializeForm];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self){
        self.title = @"Sign Up";
        [self initializeForm];
    }
    return self;
}

-(instancetype)initWithHomeVC:(HomeViewController *)vc{
    self = [super init];
    if (self){
        homeVC = vc;
        self.title = @"Sign Up";
        [self initializeForm];
    }
    return self;
}


- (void)initializeForm {
    [[XLFormViewController cellClassesForRowDescriptorTypes] setObject:[ProfilePictureFormCell class] forKey:@"profilePictureCell"];

    
    UIBarButtonItem *loginItem = [[UIBarButtonItem alloc] initWithTitle:@"Login" style:UIBarButtonItemStylePlain target:self action:@selector(showLogin:)];
    self.navigationItem.leftBarButtonItem = loginItem;
    
    
    XLFormDescriptor * form;
    XLFormSectionDescriptor * section;
    XLFormRowDescriptor * row;
    
    form = [XLFormDescriptor formDescriptor];
    
    // Section
    section = [XLFormSectionDescriptor formSection];
    [form addFormSection:section];

    // Profpic
    profPicRow = [XLFormRowDescriptor formRowDescriptorWithTag:@"profilePicture" rowType:@"profilePictureCell"];
    [profPicRow.cellConfig setValue:@"15.0" forKey:@"layer.cornerRadius"];
    if ([PFUser currentUser]) {
        PFFile *profPicFile = [PFUser currentUser][@"profilePicture"];
        if (profPicFile.url.length > 0) {
            [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:profPicFile.url] options:SDWebImageDownloaderLowPriority progress:nil completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, BOOL finished) {
                if (image) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        profPicRow.value = image;
                        [self reloadFormRow:profPicRow];
                    });
                }
            }];
        }
    }
    
    [section addFormRow:profPicRow];
    
    
    // Section
    section = [XLFormSectionDescriptor formSection];
    
    [section setFooterTitle:@"Standard message and data rates may apply. Qlumi events are sent by SMS text. We do not share this information with any other parties.\n\nWe use your mobile number to send you invitations and for password recovery"];
    
    [form addFormSection:section];
    
    // First
    [UIFont fontWithName:@"" size:17.0];
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"first" rowType:XLFormRowDescriptorTypeText title:@"First Name"];
    [row.cellConfigAtConfigure setObject:@"Jane" forKey:@"textField.placeholder"];
    [row.cellConfig setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    [row.cellConfig setObject:[UIFont fontWithName:@"JosefinSans-Regular" size:17] forKey:@"textLabel.font"];
    row.required = YES;
    [row.cellConfig setValue:@"15.0" forKey:@"layer.cornerRadius"];
    [row.cellConfig setObject:@(kCALayerMaxXMinYCorner | kCALayerMinXMinYCorner) forKey:@"layer.maskedCorners"];
    
    if ([PFUser currentUser]) {row.value = [PFUser currentUser][@"firstName"];}
    [section addFormRow:row];
    
    // Last
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"last" rowType:XLFormRowDescriptorTypeText title:@"Last Name"];
    [row.cellConfigAtConfigure setObject:@"Doe" forKey:@"textField.placeholder"];
    [row.cellConfig setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    row.required = YES;
    [row.cellConfig setObject:[UIFont fontWithName:@"JosefinSans-Regular" size:17] forKey:@"textLabel.font"];
    if ([PFUser currentUser]) {row.value = [PFUser currentUser][@"lastName"];}
    [section addFormRow:row];
    
    
    
    
    
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"countryCode" rowType:XLFormRowDescriptorTypeSelectorPickerView title:@"Country code"];
    [row.cellConfig setObject:[UIFont fontWithName:@"JosefinSans-Regular" size:17] forKey:@"textLabel.font"];
    row.selectorOptions = @[@"(+93) - Afghanistan",@"(+355) - Albania",@"(+213) - Algeria",@"(+1684) - American Samoa",@"(+376) - Andorra",@"(+244) - Angola",@"(+1264) - Anguilla",@"(+672) - Antarctica",@"(+1268) - Antigua And Barbuda",@"(+54) - Argentina",@"(+374) - Armenia",@"(+297) - Aruba",@"(+61) - Australia",@"(+43) - Austria",@"(+994) - Azerbaijan",@"(+1242) - Bahamas The",@"(+973) - Bahrain",@"(+880) - Bangladesh",@"(+1246) - Barbados",@"(+375) - Belarus",@"(+32) - Belgium",@"(+501) - Belize",@"(+229) - Benin",@"(+1441) - Bermuda",@"(+975) - Bhutan",@"(+591) - Bolivia",@"(+387) - Bosnia and Herzegovina",@"(+267) - Botswana",@"(+55) - Bouvet Island",@"(+55) - Brazil",@"(+246) - British Indian Ocean Territory",@"(+673) - Brunei",@"(+359) - Bulgaria",@"(+226) - Burkina Faso",@"(+257) - Burundi",@"(+855) - Cambodia",@"(+237) - Cameroon",@"(+1) - Canada",@"(+238) - Cape Verde",@"(+1345) - Cayman Islands",@"(+236) - Central African Republic",@"(+235) - Chad",@"(+56) - Chile",@"(+86) - China",@"(+61) - Christmas Island",@"(+672) - Cocos (Keeling) Islands",@"(+57) - Colombia",@"(+269) - Comoros",@"(+242) - Republic Of The Congo",@"(+242) - Democratic Republic Of The Congo",@"(+682) - Cook Islands",@"(+506) - Costa Rica",@"(+225) - Cote D'Ivoire (Ivory Coast)",@"(+385) - Croatia (Hrvatska)",@"(+53) - Cuba",@"(+357) - Cyprus",@"(+420) - Czech Republic",@"(+45) - Denmark",@"(+253) - Djibouti",@"(+1767) - Dominica",@"(+1809) - Dominican Republic",@"(+670) - East Timor",@"(+593) - Ecuador",@"(+20) - Egypt",@"(+503) - El Salvador",@"(+240) - Equatorial Guinea",@"(+291) - Eritrea",@"(+372) - Estonia",@"(+251) - Ethiopia",@"(+61) - External Territories of Australia",@"(+500) - Falkland Islands",@"(+298) - Faroe Islands",@"(+679) - Fiji Islands",@"(+358) - Finland",@"(+33) - France",@"(+594) - French Guiana",@"(+689) - French Polynesia",@"(+262) - French Southern Territories",@"(+241) - Gabon",@"(+220) - Gambia The",@"(+995) - Georgia",@"(+49) - Germany",@"(+233) - Ghana",@"(+350) - Gibraltar",@"(+30) - Greece",@"(+299) - Greenland",@"(+1473) - Grenada",@"(+590) - Guadeloupe",@"(+1671) - Guam",@"(+502) - Guatemala",@"(+44) - Guernsey and Alderney",@"(+224) - Guinea",@"(+245) - Guinea-Bissau",@"(+592) - Guyana",@"(+509) - Haiti",@"(+0) - Heard and McDonald Islands",@"(+504) - Honduras",@"(+852) - Hong Kong S.A.R.",@"(+36) - Hungary",@"(+354) - Iceland",@"(+91) - India",@"(+62) - Indonesia",@"(+98) - Iran",@"(+964) - Iraq",@"(+353) - Ireland",@"(+972) - Israel",@"(+39) - Italy",@"(+1876) - Jamaica",@"(+81) - Japan",@"(+44) - Jersey",@"(+962) - Jordan",@"(+7) - Kazakhstan",@"(+254) - Kenya",@"(+686) - Kiribati",@"(+850) - Korea North",@"(+82) - Korea South",@"(+965) - Kuwait",@"(+996) - Kyrgyzstan",@"(+856) - Laos",@"(+371) - Latvia",@"(+961) - Lebanon",@"(+266) - Lesotho",@"(+231) - Liberia",@"(+218) - Libya",@"(+423) - Liechtenstein",@"(+370) - Lithuania",@"(+352) - Luxembourg",@"(+853) - Macau S.A.R.",@"(+389) - Macedonia",@"(+261) - Madagascar",@"(+265) - Malawi",@"(+60) - Malaysia",@"(+960) - Maldives",@"(+223) - Mali",@"(+356) - Malta",@"(+44) - Man (Isle of)",@"(+692) - Marshall Islands",@"(+596) - Martinique",@"(+222) - Mauritania",@"(+230) - Mauritius",@"(+269) - Mayotte",@"(+52) - Mexico",@"(+691) - Micronesia",@"(+373) - Moldova",@"(+377) - Monaco",@"(+976) - Mongolia",@"(+1664) - Montserrat",@"(+212) - Morocco",@"(+258) - Mozambique",@"(+95) - Myanmar",@"(+264) - Namibia",@"(+674) - Nauru",@"(+977) - Nepal",@"(+599) - Netherlands Antilles",@"(+31) - Netherlands The",@"(+687) - New Caledonia",@"(+64) - New Zealand",@"(+505) - Nicaragua",@"(+227) - Niger",@"(+234) - Nigeria",@"(+683) - Niue",@"(+672) - Norfolk Island",@"(+1670) - Northern Mariana Islands",@"(+47) - Norway",@"(+968) - Oman",@"(+92) - Pakistan",@"(+680) - Palau",@"(+970) - Palestinian Territory Occupied",@"(+507) - Panama",@"(+675) - Papua new Guinea",@"(+595) - Paraguay",@"(+51) - Peru",@"(+63) - Philippines",@"(+64) - Pitcairn Island",@"(+48) - Poland",@"(+351) - Portugal",@"(+1787) - Puerto Rico",@"(+974) - Qatar",@"(+262) - Reunion",@"(+40) - Romania",@"(+70) - Russia",@"(+250) - Rwanda",@"(+290) - Saint Helena",@"(+1869) - Saint Kitts And Nevis",@"(+1758) - Saint Lucia",@"(+508) - Saint Pierre and Miquelon",@"(+1784) - Saint Vincent And The Grenadines",@"(+684) - Samoa",@"(+378) - San Marino",@"(+239) - Sao Tome and Principe",@"(+966) - Saudi Arabia",@"(+221) - Senegal",@"(+381) - Serbia",@"(+248) - Seychelles",@"(+232) - Sierra Leone",@"(+65) - Singapore",@"(+421) - Slovakia",@"(+386) - Slovenia",@"(+44) - Smaller Territories of the UK",@"(+677) - Solomon Islands",@"(+252) - Somalia",@"(+27) - South Africa",@"(+500) - South Georgia",@"(+211) - South Sudan",@"(+34) - Spain",@"(+94) - Sri Lanka",@"(+249) - Sudan",@"(+597) - Suriname",@"(+47) - Svalbard And Jan Mayen Islands",@"(+268) - Swaziland",@"(+46) - Sweden",@"(+41) - Switzerland",@"(+963) - Syria",@"(+886) - Taiwan",@"(+992) - Tajikistan",@"(+255) - Tanzania",@"(+66) - Thailand",@"(+228) - Togo",@"(+690) - Tokelau",@"(+676) - Tonga",@"(+1868) - Trinidad And Tobago",@"(+216) - Tunisia",@"(+90) - Turkey",@"(+7370) - Turkmenistan",@"(+1649) - Turks And Caicos Islands",@"(+688) - Tuvalu",@"(+256) - Uganda",@"(+380) - Ukraine",@"(+971) - United Arab Emirates",@"(+44) - United Kingdom",@"(+1) - United States",@"(+1) - United States Minor Outlying Islands",@"(+598) - Uruguay",@"(+998) - Uzbekistan",@"(+678) - Vanuatu",@"(+39) - Vatican City State (Holy See)",@"(+58) - Venezuela",@"(+84) - Vietnam",@"(+1284) - Virgin Islands (British)",@"(+1340) - Virgin Islands (US)",@"(+681) - Wallis And Futuna Islands",@"(+212) - Western Sahara",@"(+967) - Yemen",@"(+38) - Yugoslavia",@"(+260) - Zambia",@"(+263) - Zimbabwe"];
    
    row.required = YES;
    
    NSString *numm = [PFUser currentUser][@"phoneNumber"];
    NSString *countryCode = @"";
    
    
    NSLocale *currentLocale = [NSLocale currentLocale];  // get the current locale. //NSLocaleCountryCode
    countryCode = [currentLocale objectForKey:NSLocaleCountryCode];
    NSString *path = [[NSBundle mainBundle] pathForResource:@"DiallingCodes" ofType:@"plist"];
    NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:path];
    for (id key in dict) {
        
        if ([key isEqualToString:countryCode.lowercaseString]){
            countryCode = [dict objectForKey:key];
        }
    }
    
    row.value = [NSString stringWithFormat:@"+%@",countryCode];
    
    
    
    
    
    if ([PFUser currentUser])   {
        
        NSLocale *currentLocale = [NSLocale currentLocale];  // get the current locale. //NSLocaleCountryCode
        NSString *countryCode = [currentLocale objectForKey:NSLocaleCountryCode];
        
        NSString *path = [[NSBundle mainBundle] pathForResource:@"DiallingCodes" ofType:@"plist"];
        NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:path];
        
        for (id key in dict) {
            
            if ([key isEqualToString:countryCode.lowercaseString]){
                countryCode = [dict objectForKey:key];
            }
        }
        
        row.value = [NSString stringWithFormat:@"+%@",countryCode];
        numm = [numm stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"+%@",countryCode] withString:@""];
    }
    [section addFormRow:row];
    
    
    
    
    
    
    
    
    
    
    
    
    // Phone
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"phone" rowType:XLFormRowDescriptorTypePhone title:@"Phone Number"];
    [row.cellConfigAtConfigure setObject:@"123-456-7890" forKey:@"textField.placeholder"];
    [row.cellConfig setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
    [row.cellConfig setObject:[UIFont fontWithName:@"JosefinSans-Regular" size:17] forKey:@"textLabel.font"];
    row.required = YES;
    if ([PFUser currentUser]) {
        
        
        row.value =   numm; //[PFUser currentUser][@"phoneNumber"];
        
    }
    [section addFormRow:row];
    
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"smsNotifications" rowType:XLFormRowDescriptorTypeBooleanSwitch title:@"SMS Notifications"];
    if ([PFUser currentUser]) {row.value = @([[PFUser currentUser][@"smsNotifications"] boolValue]);}else{
        row.value = @(YES);
    }
    [row.cellConfig setObject:[UIFont fontWithName:@"JosefinSans-Regular" size:17] forKey:@"textLabel.font"];
    [row.cellConfig setValue:@"15.0" forKey:@"layer.cornerRadius"];
    [row.cellConfig setObject:@(kCALayerMinXMaxYCorner | kCALayerMaxXMaxYCorner) forKey:@"layer.maskedCorners"];
    [section addFormRow:row];
    
    
    
    // Section
    section = [XLFormSectionDescriptor formSection];
    if (![PFUser currentUser]) {
        [section setFooterTitle:@"By signing up to Qlumi, you accept our Terms and Conditions and agree to our Privacy Policy"];
    }
    [form addFormSection:section];
    
    
    // Password
    if (![PFUser currentUser]) {
        row = [XLFormRowDescriptor formRowDescriptorWithTag:@"password" rowType:XLFormRowDescriptorTypePassword title:@"Password "];
        [row.cellConfigAtConfigure setObject:@"********" forKey:@"textField.placeholder"];
        [row.cellConfig setObject:@(NSTextAlignmentRight) forKey:@"textField.textAlignment"];
        [row.cellConfig setValue:@"15.0" forKey:@"layer.cornerRadius"];
        [row.cellConfig setObject:[UIFont fontWithName:@"JosefinSans-Regular" size:17] forKey:@"textLabel.font"];
        row.required = YES;
        [section addFormRow:row];
    }
    
    
    section = [XLFormSectionDescriptor formSection];
    [form addFormSection:section];

    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"tos" rowType:XLFormRowDescriptorTypeButton title:@"Privacy Policy"];
    [row.cellConfig setValue:@"15.0" forKey:@"layer.cornerRadius"];
    [row.cellConfig setObject:@(kCALayerMaxXMinYCorner | kCALayerMinXMinYCorner) forKey:@"layer.maskedCorners"];
    [row.cellConfig setObject:[UIFont fontWithName:@"JosefinSans-Regular" size:17] forKey:@"textLabel.font"];
    row.action.formSelector = @selector(showPrivacy:);
    [section addFormRow:row];
    
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"tos" rowType:XLFormRowDescriptorTypeButton title:@"Terms of Service"];
    row.action.formSelector = @selector(showTerms:);
    [row.cellConfig setValue:@"15.0" forKey:@"layer.cornerRadius"];
    [row.cellConfig setObject:[UIFont fontWithName:@"JosefinSans-Regular" size:17] forKey:@"textLabel.font"];
    [row.cellConfig setObject:@(kCALayerMinXMaxYCorner | kCALayerMaxXMaxYCorner) forKey:@"layer.maskedCorners"];
    
    [section addFormRow:row];

    // Section
    section = [XLFormSectionDescriptor formSection];
    [form addFormSection:section];
    
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"signUp" rowType:XLFormRowDescriptorTypeButton title:@"Sign Up"];
    row.action.formSelector = @selector(signUp:);
    if ([PFUser currentUser]) {
        row.title = @"Save Changes";
        row.action.formSelector = @selector(update:);
    }
    [row.cellConfig setObject:[UIFont fontWithName:@"JosefinSans-Regular" size:17] forKey:@"textLabel.font"];
    [row.cellConfig setValue:@"15.0" forKey:@"layer.cornerRadius"];
    [section addFormRow:row];
    
    
    if ([PFUser currentUser]) {
        section = [XLFormSectionDescriptor formSection];
        [form addFormSection:section];
        
        row = [XLFormRowDescriptor formRowDescriptorWithTag:@"showTut" rowType:XLFormRowDescriptorTypeButton title:@"Show Tutorial"];
        [row.cellConfig setObject:[UIFont fontWithName:@"JosefinSans-Regular" size:17] forKey:@"textLabel.font"];
        row.action.formSelector = @selector(showTut:);
        [section addFormRow:row];
        
        
        section = [XLFormSectionDescriptor formSection];
        [form addFormSection:section];
        
        row = [XLFormRowDescriptor formRowDescriptorWithTag:@"reset" rowType:XLFormRowDescriptorTypeButton title:@"Reset Password"];
        row.action.formSelector = @selector(resetPassword:);
        [row.cellConfig setObject:[UIFont fontWithName:@"JosefinSans-Regular" size:17] forKey:@"textLabel.font"];
        [row.cellConfig setObject:[UIColor redColor] forKey:@"textLabel.textColor"];
        [section addFormRow:row];
        
//        row = [XLFormRowDescriptor formRowDescriptorWithTag:@"reset" rowType:XLFormRowDescriptorTypeButton title:@"Reset Cache"];
//        row.action.formSelector = @selector(resetCache:);
//        [row.cellConfig setObject:[UIColor redColor] forKey:@"textLabel.textColor"];
//        [section addFormRow:row];
    }
    
    section.footerTitle = [NSString stringWithFormat:@"Qlumi App Build %@", [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
    
    for (XLFormSectionDescriptor *section in form.formSections) {
        for (XLFormRowDescriptor *row in section.formRows) {
            row.cellConfigAtConfigure[@"backgroundColor"] = [UIColor colorWithWhite:1.0 alpha:0.9];
            [self updateFormRow:row];
            
        }
    }

    
    self.form = form;
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.tableView.leftAnchor constraintEqualToAnchor:self.view.leftAnchor constant:20.0].active = YES;
    [self.tableView.rightAnchor constraintEqualToAnchor:self.view.rightAnchor constant:-20.0].active = YES;
    [self.tableView.topAnchor constraintEqualToAnchor:self.view.topAnchor constant:0].active = YES;
    [self.tableView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor constant:0].active = YES;
    
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    [self.view setBackgroundColor:[UIColor lightGrayColor]];
    
    if (![PFUser currentUser]) {
        [self getImagesForKeyword:@"Travel" withCallback:^(NSArray * imagesArray) {
            NSMutableArray *imageURLsArray = [[NSMutableArray alloc] init];
            for (NSDictionary *imageObj in imagesArray) {
                [imageURLsArray addObject:imageObj[@"urls"][@"regular"]];
            }
            
            [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:[imageURLsArray objectAtIndex:(arc4random() % [imageURLsArray count])]] options:SDWebImageDownloaderHighPriority progress:nil completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, BOOL finished) {
                UIImageView *imageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
                imageView.contentMode = UIViewContentModeScaleAspectFill;
//                self.tableView.backgroundView = imageView;
                [self.view insertSubview:imageView belowSubview:self.tableView];
                [self.view setBackgroundColor:[UIColor whiteColor]];
//                [self.view addSubview:imageView];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [UIView transitionWithView:imageView
                                      duration:.5f
                                       options:UIViewAnimationOptionTransitionCrossDissolve
                                    animations:^{
                                        imageView.image = image;
                                        imageView.alpha = 0.2;
                                    } completion:nil];
                });

            }];
        }];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayFooterView:(UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView * footer = (UITableViewHeaderFooterView *)view;
    footer.textLabel.textColor = [UIColor colorWithWhite:0.0 alpha:1.0];
}

//-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
//    [cell.contentView setLayoutMargins:UIEdgeInsetsMake(0, 20, 0, 20)];
////    cell.contentView.layoutMargins.left = 20;
////    cell.contentView.layoutMargins.right = 20;
//}

- (void)getImagesForKeyword:(NSString *)keyword withCallback:(void (^)(NSArray * imagesArray))completionHandler {
    
    keyword = [keyword stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    NSString *urlString = [NSString stringWithFormat:@"https://api.unsplash.com/search/photos?client_id=5d4592e505f01a9c0a941a1eebb68b5d1e838ee5c3fae3e4b98d984e1334be2e&page=1&query=%@", keyword];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (!error) {
            NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            if (completionHandler) {
                completionHandler(dictionary[@"results"]);
            }
        }
    }];
    [dataTask resume];
}

-(void) signUpMethod{
    NSDictionary *results = [self getFormResults];
    if ([self resultsAreValid:results]) {
        
        PFUser *newUser = [PFUser user];
        newUser[@"username"] = results[@"phone"];
        newUser[@"password"] = results[@"password"];
        newUser[@"firstName"] = results[@"first"];
        newUser[@"lastName"] = results[@"last"];
        
        
        NSString *string= results[@"countryCode"];
        
        if ([string rangeOfString:@"-"].location == NSNotFound) {
            newUser[@"phoneNumber"] = [NSString stringWithFormat:@"%@%@", string, results[@"phone"]];
            
        }
        else{
            NSRange searchFromRange = [string rangeOfString:@"("];
            NSRange searchToRange = [string rangeOfString:@")"];
            NSString *substring = [string substringWithRange:NSMakeRange(searchFromRange.location+searchFromRange.length, searchToRange.location-searchFromRange.location-searchFromRange.length)];
            newUser[@"phoneNumber"] = [NSString stringWithFormat:@"%@%@", substring, results[@"phone"]];
            
        }
        
        
        newUser[@"smsNotifications"] = results[@"smsNotifications"];
        if (![results[@"profilePicture"] isKindOfClass:[NSNull class]]) {
            PFFile *file = [PFFile fileWithName:@"profPic.jpg" data:UIImageJPEGRepresentation(results[@"profilePicture"], 0.5)];
            newUser[@"profilePicture"] = file;
        }
        [SVProgressHUD show];
        [newUser signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            [SVProgressHUD dismiss];
            if (succeeded) {
                [homeVC refresh];
                [self.view.window.rootViewController dismissViewControllerAnimated:YES completion:^{
                }];
            }else{
                if (error) {
                    UIAlertController *errorAlert = [UIAlertController alertControllerWithTitle:@"Error" message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
                    [errorAlert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil]];
                    [self presentViewController:errorAlert animated:YES completion:nil];
                }
            }
        }];
        
    }//
}



-(void) upDateMethod{
    NSDictionary *results = [self getFormResults];
    if ([self resultsAreValid:results]) {
        PFUser *newUser = [PFUser currentUser];
        newUser[@"username"] = results[@"phone"];
        newUser[@"firstName"] = results[@"first"];
        newUser[@"lastName"] = results[@"last"];
       
        NSString *string= results[@"countryCode"];
        
        if ([string rangeOfString:@"-"].location == NSNotFound) {
            newUser[@"phoneNumber"] = [NSString stringWithFormat:@"%@%@", string, results[@"phone"]];
            
        }
        else{
            NSRange searchFromRange = [string rangeOfString:@"("];
            NSRange searchToRange = [string rangeOfString:@")"];
            NSString *substring = [string substringWithRange:NSMakeRange(searchFromRange.location+searchFromRange.length, searchToRange.location-searchFromRange.location-searchFromRange.length)];
            newUser[@"phoneNumber"] = [NSString stringWithFormat:@"%@%@", substring, results[@"phone"]];
            
        }
        
        NSString *imgURL = results[@"profilePicture"];
        newUser[@"smsNotifications"] = results[@"smsNotifications"];
        if ([imgURL isKindOfClass:[NSNull class]]) {}
        else{
            PFFile *file = [PFFile fileWithName:@"profPic.jpg" data:UIImageJPEGRepresentation(results[@"profilePicture"], 0.5)];
            newUser[@"profilePicture"] = file;
        }
        
        [SVProgressHUD show];
        [newUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
            [SVProgressHUD dismiss];
            if (succeeded) {
                [self dismissViewControllerAnimated:YES completion:nil];
            }else{
                if (error) {
                    UIAlertController *errorAlert = [UIAlertController alertControllerWithTitle:@"Error" message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
                    [errorAlert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil]];
                    [self presentViewController:errorAlert animated:YES completion:nil];
                }
            }
        }];
    }
}




-(NSString*) VerifyMobNumber:(NSString *)code forPhoneNumber:(NSString *)number comingFrom:(NSString *)landing{
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://qlumi-server.herokuapp.com/parse/functions/validatePhoneNumber"]];
    __block NSString *abcc = @"";
    
    [SVProgressHUD show];
    
    NSString *userUpdate =[NSString stringWithFormat:@"PhoneNo=%@&CountryCode=%@&_ApplicationId=%@&_ClientVersion=%@&_MasterKey=%@&_RevocableSession=%@",number,code,@"Qlumi",@"js1.6.9",@"6A0B698B8BDA5BAA9D347F9978FA3BF4",@"1", nil];
    
    //create the Method "GET" or "POST"
    [urlRequest setHTTPMethod:@"POST"];
    
    //Convert the String to Data
    NSData *data1 = [userUpdate dataUsingEncoding:NSUTF8StringEncoding];
    
    //Apply the data to the body
    [urlRequest setHTTPBody:data1];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        if(httpResponse.statusCode == 200)
        {
            [SVProgressHUD dismiss];
            NSError *parseError = nil;
            NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
            NSLog(@"The response is - %@",responseDictionary);
            NSDictionary *responseDate = [responseDictionary objectForKey:@"result"];
            NSInteger success = [[responseDate objectForKey:@"Status"] integerValue];
            if(success == 1)
            {
                if ([landing isEqualToString:@"signup"]){
                    [self signUpMethod];
                }
                else{
                    [self upDateMethod];
                }
            }
            else
            {
                UIAlertController *complete = [UIAlertController alertControllerWithTitle:@"Error" message:@"Mobile Number is invalid" preferredStyle:UIAlertControllerStyleAlert];
                [complete addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
                [self presentViewController:complete animated:YES completion:nil];
            }
            //return NO;
        }
        else
        {
            NSLog(@"Error");
        }
    }];
    
    [dataTask resume];
    return abcc;
}


#pragma mark - IBActions

-(IBAction)showPrivacy:(id)sender{
    SFSafariViewController *safari = [[SFSafariViewController alloc] initWithURL:[NSURL URLWithString:@"http://www.qlumi.com/privacy.aspx"]];
    [self presentViewController:safari animated:YES completion:nil];
}

-(IBAction)showTerms:(id)sender{
    SFSafariViewController *safari = [[SFSafariViewController alloc] initWithURL:[NSURL URLWithString:@"http://www.qlumi.com/TOS.aspx"]];
    [self presentViewController:safari animated:YES completion:nil];
}

-(IBAction)showTut:(id)sender{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"completedWalkthrough"];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)showLogin:(id)sender{
    LoginViewController *loginVC = [[LoginViewController alloc] initWithHomeVC:homeVC];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:loginVC];
    nav.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    //[self.parentViewController presentViewController:nav animated:true completion:nil];
    [self.parentViewController presentViewController:nav animated:YES completion:^{
        [self getImagesForKeyword:@"Travel" withCallback:^(NSArray * imagesArray) {
            NSMutableArray *imageURLsArray = [[NSMutableArray alloc] init];
            for (NSDictionary *imageObj in imagesArray) {
                [imageURLsArray addObject:imageObj[@"urls"][@"regular"]];
            }
            
            [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:[imageURLsArray objectAtIndex:(arc4random() % [imageURLsArray count])]] options:SDWebImageDownloaderHighPriority progress:nil completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, BOOL finished) {
                UIImageView *imageView = [[UIImageView alloc] initWithFrame:loginVC.view.bounds];
                imageView.contentMode = UIViewContentModeScaleAspectFill;
//                loginVC.tableView.backgroundView = imageView;
                [loginVC.view insertSubview:imageView belowSubview:loginVC.tableView];
                [loginVC.view setBackgroundColor:[UIColor whiteColor]];
                dispatch_async(dispatch_get_main_queue(), ^{

                    [UIView transitionWithView:imageView
                                      duration:.5f
                                       options:UIViewAnimationOptionTransitionCrossDissolve
                                    animations:^{
                                        imageView.image = image;
                                        imageView.alpha = 0.5;
                                        for (XLFormSectionDescriptor *section in loginVC.form.formSections) {
                                            for (XLFormRowDescriptor *row in section.formRows) {
                                                row.cellConfigAtConfigure[@"backgroundColor"] = [UIColor colorWithWhite:1.0 alpha:.90];
                                                [loginVC updateFormRow:row];
                                            }
                                        }
                                    } completion:nil];
                });
            }];
        }];
    }];
}

-(IBAction)signUp:(id)sender{
    [self VerifyMobNumber:@"+92" forPhoneNumber:@"232424234" comingFrom:@"signup"];
}

-(IBAction)update:(id)sender{
    [self VerifyMobNumber:@"+92" forPhoneNumber:@"232424234" comingFrom:@"update"];
}

-(IBAction)resetPassword:(id)sender{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CustomAlertView * vc = [storyboard instantiateViewControllerWithIdentifier:@"CustomAlertView"];
    //vc.comingFrom = @"fdfd";
    [vc setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    
    vc.title = @"Forget Password";
    [self presentViewController:vc animated:YES completion:nil];
    
    
    
}


-(void)enterResetCodeForPhoneNumber:(NSString *)number{
    UIAlertController *code = [UIAlertController alertControllerWithTitle:@"Code" message:@"Please enter your reset code" preferredStyle:UIAlertControllerStyleAlert];
    [code addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Reset Code";
        textField.keyboardType = UIKeyboardTypePhonePad;
    }];
    [code addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [code addAction:[UIAlertAction actionWithTitle:@"Submit" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self confirmCode:code.textFields[0].text forPhoneNumber:number];
    }]];
    [self presentViewController:code animated:YES completion:nil];
}

-(void)confirmCode:(NSString *)code forPhoneNumber:(NSString *)number{
    PFQuery *query = [PFUser query];
    [query whereKey:@"phoneNumber" equalTo:number];
    [query getFirstObjectInBackgroundWithBlock:^(PFObject * _Nullable object, NSError * _Nullable error) {
        if ([object[@"resetCode"] isEqualToString:code]) {
            [self resetPassword];
        }else{
            NSLog(@"Failed try again!");
            [self enterResetCodeForPhoneNumber:number];
        }
    }];
}

-(void)resetPassword{
    UIAlertController *code = [UIAlertController alertControllerWithTitle:@"New Password" message:@"Please enter your new password" preferredStyle:UIAlertControllerStyleAlert];
    [code addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Password";
        [textField setSecureTextEntry:YES];
        textField.keyboardType = UIKeyboardTypeDefault;
    }];
    
    [code addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [code addAction:[UIAlertAction actionWithTitle:@"Submit" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [[PFUser currentUser] setPassword:code.textFields[0].text];
        [[PFUser currentUser] removeObjectForKey:@"resetCode"];
        [[PFUser currentUser] saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
            UIAlertController *complete = [UIAlertController alertControllerWithTitle:@"Reset Complete" message:@"Your password has been changed!" preferredStyle:UIAlertControllerStyleAlert];
            [complete addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:complete animated:YES completion:nil];
        }];
    }]];
    [self presentViewController:code animated:YES completion:nil];
}



-(IBAction)resetCache:(id)sender{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"lastFilters"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"openInAppleMaps"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"openInGoogleMaps"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


#pragma mark - Validation

-(BOOL)resultsAreValid:(NSDictionary *)results
{
    NSArray * array = [self formValidationErrors];
    [array enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        XLFormValidationStatus * validationStatus = [[obj userInfo] objectForKey:XLValidationStatusErrorKey];
        if ([validationStatus.rowDescriptor.tag isEqualToString:@"first"]){
            UITableViewCell * cell = [self.tableView cellForRowAtIndexPath:[self.form indexPathOfFormRow:validationStatus.rowDescriptor]];
            [self animateCell:cell];
        }else if ([validationStatus.rowDescriptor.tag isEqualToString:@"last"]){
            UITableViewCell * cell = [self.tableView cellForRowAtIndexPath:[self.form indexPathOfFormRow:validationStatus.rowDescriptor]];
            [self animateCell:cell];
        }else if ([validationStatus.rowDescriptor.tag isEqualToString:@"phone"]){
            UITableViewCell * cell = [self.tableView cellForRowAtIndexPath:[self.form indexPathOfFormRow:validationStatus.rowDescriptor]];
            [self animateCell:cell];
        }else if ([validationStatus.rowDescriptor.tag isEqualToString:@"password"]){
            UITableViewCell * cell = [self.tableView cellForRowAtIndexPath:[self.form indexPathOfFormRow:validationStatus.rowDescriptor]];
            [self animateCell:cell];
        }
        
        UIAlertController *error = [UIAlertController alertControllerWithTitle:@"Invalid Value" message:validationStatus.msg preferredStyle:UIAlertControllerStyleAlert];
        [error addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil]];
        [self presentViewController:error animated:YES completion:nil];
    }];
    
    return (array.count==0)?YES:NO;
}


#pragma mark - helper methods

-(NSMutableDictionary *)getFormResults
{
    NSMutableDictionary * result = [NSMutableDictionary dictionary];
    for (XLFormSectionDescriptor *mysection in self.form.formSections) {
        if (!mysection.isMultivaluedSection){
            for (XLFormRowDescriptor * myrow in mysection.formRows) {
                if (myrow.tag && ![myrow.tag isEqualToString:@""]){
                    [result setObject:(myrow.value ?: [NSNull null]) forKey:myrow.tag];
                }
            }
        }
        else{
            NSMutableArray * multiValuedValuesArray = [NSMutableArray new];
            for (XLFormRowDescriptor * myrow in mysection.formRows) {
                if (myrow.value){
                    [multiValuedValuesArray addObject:myrow.value];
                }
            }
            [result setObject:multiValuedValuesArray forKey:mysection.multivaluedTag];
        }
    }
    return result;
}


-(void)animateCell:(UITableViewCell *)cell
{
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animation];
    animation.keyPath = @"position.x";
    animation.values =  @[ @0, @20, @-20, @10, @0];
    animation.keyTimes = @[@0, @(1 / 6.0), @(3 / 6.0), @(5 / 6.0), @1];
    animation.duration = 0.3;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    animation.additive = YES;
    
    [cell.layer addAnimation:animation forKey:@"shake"];
}


@end
