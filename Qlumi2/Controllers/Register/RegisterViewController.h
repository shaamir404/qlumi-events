//
//  RegisterViewController.h
//  Qlumi
//
//  Created by William Smillie on 1/5/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import <XLForm/XLForm.h>
#import <Parse/Parse.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import "ProfilePictureFormCell.h"
#import "CountryPicker.h"
@interface RegisterViewController : XLFormViewController <CountryPickerDelegate>

-(instancetype)initWithHomeVC:(UIViewController *)vc;

@end
