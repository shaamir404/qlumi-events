//
//  NearMeViewController.m
//  Qlumi
//
//  Created by Will on 2/23/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import "NearMeViewController.h"

@interface NearMeViewController ()

@end

@implementation NearMeViewController

-(instancetype)init{
    if (self = [super initWithEditingEnabled:YES andSearchingEnabled:YES]) {
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSMutableDictionary *filters = [[[NSUserDefaults standardUserDefaults] dictionaryForKey:@"lastFilters"] mutableCopy];
    if (filters.allKeys.count > 0){
        [SVProgressHUD show];
        PFQuery *query = [PFQuery queryWithClassName:@"SearchProfile"];
        [query whereKey:@"objectId" containedIn:filters.allKeys];
        [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
            [SVProgressHUD dismiss];
            if (!error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.filterController.selectedFiltersArray = [[NSMutableArray alloc] initWithArray:objects];
                    [self.filterController.tableView reloadData];
                    [self searchFilterTableViewController:self.filterController shouldApplySelectedFilters:objects];
                });
            }
        }];
    }
    
    NSMutableArray *tags = [[[NSUserDefaults standardUserDefaults] objectForKey:@"lastTags"] mutableCopy];
    if (tags.count > 0){
        [self getPlacesByIDs:tags];
    }else{
        tags = [[NSMutableArray alloc] init];
    }
    
    self.mapView.canAddNote = NO;
    self.mapView.showsUserLocation = YES;
}

-(void)searchFilterTableViewController:(SearchFilterTableViewController *)viewController shouldApplySelectedFilters:(NSArray *)filtersArray{
    [super searchFilterTableViewController:viewController shouldApplySelectedFilters:filtersArray];

    if (filtersArray) {
        NSMutableDictionary *filters = [[NSMutableDictionary alloc] init];
        for (PFObject *filterObj in filtersArray) {
            NSMutableDictionary *filter = [[NSMutableDictionary alloc] init];
            filter[@"keyword"] = filterObj[@"keyword"];
            filter[@"radius"] = filterObj[@"radius"];
            [filters setObject:filter forKey:filterObj.objectId];
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:filters forKey:@"lastFilters"];
    }else{
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"lastFilters"];
    }
        

    [[NSUserDefaults standardUserDefaults] synchronize];

}

-(void)calloutController:(CalloutController *)controller addedPlace:(Place *)place{
    [super calloutController:controller addedPlace:place];
    
    [self cacheAddedPlaces:self.addedPlacesArray];
}

-(void)didRemovePlace:(Place *)place{
    [super didRemovePlace:place];
    [self cacheAddedPlaces:self.addedPlacesArray];
}

-(void)cacheAddedPlaces:(NSArray *)placesArray{

    NSMutableArray *placeIds = [[NSMutableArray alloc] init];
    for (Place *p in self.addedPlacesArray) {
        if (p.placeId) {
            [placeIds addObject:p.placeId];
        }
    }
        
    [[NSUserDefaults standardUserDefaults] setObject:placeIds forKey:@"lastTags"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}




-(void)getPlacesByIDs:(NSArray *)ids{
    NSMutableArray *placesArray = [[NSMutableArray alloc] init];
    
    __block int completedRequests = 0;
    
    
    for (NSString *id in ids) {
        NSMutableURLRequest* request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET" URLString:[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/details/json?key=AIzaSyC67qOLCwRi1_6Z8g1zKA6OQkJekYIBDz8&placeid=%@", id] parameters:@{} error:NULL];
        [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        AFHTTPRequestOperation *operation = [manager HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject){
            completedRequests++;
            NSDictionary *result = responseObject[@"result"];
            Place *p = [[Place alloc] initWithDictionary:result];
            [placesArray addObject:p];
            
            if (completedRequests == ids.count) {
                [self fetchDidComplete:placesArray];
            }
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            if (completedRequests == ids.count) {
                [self fetchDidComplete:placesArray];
            }
            completedRequests++;
        }];
        
        [manager.operationQueue addOperation:operation];
    }
}

-(void)fetchDidComplete:(NSMutableArray *)places{
    self.addedPlacesArray = places;
    
//    for (int i=0; i<self.addedPlacesArray.locations.count; i++) {
//        NSDictionary *location = self.addedPlacesArray.locations[i];
//        if (!location[@"placeId"]) {
//            Place *p = [[Place alloc] init];
//            p.location = CLLocationCoordinate2DMake([location[@"latitude"] floatValue], [location[@"longitude"] floatValue]);
//            p.title = location[@"title"];
//            p.type = GPlaceTypeNote;
//
//            [self.addedPlacesArray insertObject:p atIndex:i];
//        }
//    }
    

    if (places.count) {
        for (Place *p in self.addedPlacesArray) {
            QMapAnnotation *annotation = [[QMapAnnotation alloc] init];
            p.annotation = annotation;
            [annotation setCoordinate:p.location];
            [annotation setTitle:p.title]; //You can set the subtitle too
            [annotation setSubtitle:p.address];
            annotation.place = p;
        }
    }
    
    [self.segmentedControl setTitle:[NSString stringWithFormat:@"Tagged (%lu)", (unsigned long)self.addedPlacesArray.count] forSegmentAtIndex:1];
}


@end
