//
//  MessagesViewController.h
//  MyPrice
//
//  Created by William Smillie on 8/2/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import "AppDelegate.h"

#import "MessageModelData.h"
#import "Event.h"
#import <SVProgressHUD/SVProgressHUD.h>

#import <JSQMessagesViewController/JSQMessagesViewController.h>
#import <JSQMessagesViewController/JSQMessage.h>
#import <JSQMessagesViewController/JSQMessagesTimestampFormatter.h>
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>

@interface MessagesViewController : JSQMessagesViewController
@property Event *passedEvent;
@property MessageModelData *dataModel;

@end
