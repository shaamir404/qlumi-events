//
//  Created by Jesse Squires
//  http://www.jessesquires.com
//
//
//  Documentation
//  http://cocoadocs.org/docsets/JSQMessagesViewController
//
//
//  GitHub
//  https://github.com/jessesquires/JSQMessagesViewController
//
//
//  License
//  Copyright (c) 2014 Jesse Squires
//  Released under an MIT license: http://opensource.org/licenses/MIT
//

#import "MessageModelData.h"
#import "Event.h"


/**
 *  This is for demo/testing purposes only.
 *  This object sets up some fake model data.
 *  Do not actually do anything like this.
 */

@implementation MessageModelData

- (instancetype)initWithEvent:(Event *)event
{
    self = [super init];
    if (self) {
        
        self.avatars = [[NSMutableDictionary alloc] init];
        self.otherUsers = [[NSMutableArray alloc] init];
        self.users = [[NSMutableDictionary alloc] init];
        
        NSMutableArray *phoneNumbers = [[NSMutableArray alloc] init];
        [phoneNumbers addObject:event[@"user"][@"phoneNumber"]];
        for (NSDictionary *guest in event[@"guests"]){
            [phoneNumbers addObject:guest[@"phoneNumber"]];
        }
        
        
        [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:[(PFFile *)[PFUser currentUser][@"profilePicture"] url]] options:SDWebImageDownloaderLowPriority progress:nil completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, BOOL finished) {
            if (!image){
                image = [UIImage imageNamed:@"photo-placeholder"];
            }
            JSQMessagesAvatarImage *currentUserAvatar = [JSQMessagesAvatarImageFactory avatarImageWithImage:image diameter:30];
            [self.avatars setObject:currentUserAvatar forKey:[PFUser currentUser].objectId];
            [self.users setObject:[PFUser currentUser][@"firstName"] forKey:[PFUser currentUser].objectId];
        }];
        
        PFQuery *guestsQuery = [PFUser query];
        [guestsQuery whereKey:@"phoneNumber" containedIn:phoneNumbers];
        [guestsQuery findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
            self.otherUsers = [objects mutableCopy];
            for (PFUser *user in self.otherUsers){
                [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:[(PFFile *)user[@"profilePicture"] url]] options:SDWebImageDownloaderLowPriority progress:nil completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, BOOL finished) {
                    if (!image){
                        image = [UIImage imageNamed:@"photo-placeholder"];
                    }

                    JSQMessagesAvatarImage *othertUserAvatar = [JSQMessagesAvatarImageFactory avatarImageWithImage:image diameter:30];
                    [self.avatars setObject:othertUserAvatar forKey:user.objectId];
                    [self.users setObject:user[@"firstName"] forKey:user.objectId];
                 }];

            }
        }];


        self.messages = [[NSMutableArray alloc] init];
        
        JSQMessagesBubbleImageFactory *bubbleFactory = [[JSQMessagesBubbleImageFactory alloc] init];
        self.outgoingBubbleImageData = [bubbleFactory outgoingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleLightGrayColor]];
        self.incomingBubbleImageData = [bubbleFactory incomingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleGreenColor]];
    }
    
    return self;
}

//- (void)loadFakeMessages
//{
//    /**
//     *  Load some fake messages for demo.
//     *
//     *  You should have a mutable array or orderedSet, or something.
//     */
//    self.messages = [[NSMutableArray alloc] initWithObjects:
//                     [[JSQMessage alloc] initWithSenderId:self.otherUser.objectId
//                                        senderDisplayName:self.otherUser[@"first"]
//                                                     date:[NSDate distantPast]
//                                                     text:NSLocalizedString(@"Hey! Thanks for accepting my bid! Looking forward to working with you!", nil)],
//
//                     [[JSQMessage alloc] initWithSenderId:self.currentUser.objectId
//                                        senderDisplayName:self.currentUser[@"first"]
//                                                     date:[NSDate distantPast]
//                                                     text:NSLocalizedString(@"Hi there! Right back at ya! What does your availibility look like?", nil)],
//
//                     [[JSQMessage alloc] initWithSenderId:self.otherUser.objectId
//                                        senderDisplayName:self.otherUser[@"first"]
//                                                     date:[NSDate distantPast]
//                                                     text:NSLocalizedString(@"This weekend shouldn't be a problem. How's Saturday around 2:00?", nil)],
//
//                     [[JSQMessage alloc] initWithSenderId:self.currentUser.objectId
//                                        senderDisplayName:self.currentUser[@"first"]
//                                                     date:[NSDate date]
//                                                     text:NSLocalizedString(@"Sounds Great! See you then!", nil)],
//
//                     [[JSQMessage alloc] initWithSenderId:self.otherUser.objectId
//                                        senderDisplayName:self.otherUser[@"first"]
//                                                     date:[NSDate date]
//                                                     text:NSLocalizedString(@"Perfect!", nil)],
//                     nil];
//
//}
//
//
//
//
//- (void)addAudioMediaMessage
//{
//    NSString * sample = [[NSBundle mainBundle] pathForResource:@"jsq_messages_sample" ofType:@"m4a"];
//    NSData * audioData = [NSData dataWithContentsOfFile:sample];
//    JSQAudioMediaItem *audioItem = [[JSQAudioMediaItem alloc] initWithData:audioData];
//    JSQMessage *audioMessage = [JSQMessage messageWithSenderId:self.otherUser.objectId
//                                                   displayName:self.otherUser[@"first"]
//                                                         media:audioItem];
//    [self.messages addObject:audioMessage];
//}
//
//- (void)addPhotoMediaMessage
//{
//    JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImage:[UIImage imageNamed:@"placeholder"]];
//    JSQMessage *photoMessage = [JSQMessage messageWithSenderId:self.otherUser.objectId
//                                                   displayName:self.otherUser[@"first"]
//                                                         media:photoItem];
//    [self.messages addObject:photoMessage];
//}
//
//- (void)addLocationMediaMessageCompletion:(JSQLocationMediaItemCompletionBlock)completion
//{
//    CLLocation *ferryBuildingInSF = [[CLLocation alloc] initWithLatitude:37.795313 longitude:-122.393757];
//
//    JSQLocationMediaItem *locationItem = [[JSQLocationMediaItem alloc] init];
//    [locationItem setLocation:ferryBuildingInSF withCompletionHandler:completion];
//
//    JSQMessage *locationMessage = [JSQMessage messageWithSenderId:self.otherUser.objectId
//                                                      displayName:self.otherUser[@"first"]
//                                                            media:locationItem];
//    [self.messages addObject:locationMessage];
//}
//
//- (void)addVideoMediaMessage
//{
//    // don't have a real video, just pretending
//    NSURL *videoURL = [NSURL URLWithString:@"file://"];
//
//    JSQVideoMediaItem *videoItem = [[JSQVideoMediaItem alloc] initWithFileURL:videoURL isReadyToPlay:YES];
//    JSQMessage *videoMessage = [JSQMessage messageWithSenderId:self.otherUser.objectId
//                                                   displayName:self.otherUser[@"first"]
//                                                         media:videoItem];
//    [self.messages addObject:videoMessage];
//}
//
//- (void)addVideoMediaMessageWithThumbnail
//{
//    // don't have a real video, just pretending
//    NSURL *videoURL = [NSURL URLWithString:@"file://"];
//
//    JSQVideoMediaItem *videoItem = [[JSQVideoMediaItem alloc] initWithFileURL:videoURL isReadyToPlay:YES];
//    JSQMessage *videoMessage = [JSQMessage messageWithSenderId:self.otherUser.objectId
//                                                   displayName:self.otherUser[@"first"]
//                                                         media:videoItem];
//    [self.messages addObject:videoMessage];
//}

@end
