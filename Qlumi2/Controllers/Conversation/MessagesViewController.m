//
//  MessagesViewController.m
//  MyPrice
//
//  Created by William Smillie on 8/2/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import "MessagesViewController.h"

@interface MessagesViewController ()<DZNEmptyDataSetDelegate, DZNEmptyDataSetSource> {
    NSTimer *refreshTimer;
}

@end

@implementation MessagesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.dataModel = [[MessageModelData alloc] initWithEvent:self.passedEvent];
    [self refresh];

    self.senderId = [PFUser currentUser].objectId;
    self.senderDisplayName = [PFUser currentUser][@"firstName"];
    
    self.title = [NSString stringWithFormat:@"%@ Group Chat", self.passedEvent[@"title"]];
    
    [self.collectionView reloadData];
    self.collectionView.emptyDataSetSource = self;
    self.collectionView.emptyDataSetDelegate = self;
    
    self.inputToolbar.contentView.leftBarButtonItem = nil;
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    refreshTimer = [NSTimer scheduledTimerWithTimeInterval:5.0f target:self selector:@selector(refresh) userInfo:nil repeats:YES];
}


- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"No Messages...";
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:18.0f],
                                 NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView {
    
    return YES;
}

-(void)refresh{
    
    self.dataModel.messages = [[NSMutableArray alloc] init];
    
    PFQuery *messagesQuery = [PFQuery queryWithClassName:@"Message"];
    [messagesQuery whereKey:@"event" equalTo:self.passedEvent];
    [messagesQuery includeKey:@"createdAt"];
    [messagesQuery orderByAscending:@"createdAt"];
    [messagesQuery includeKey:@"sender"];
    [messagesQuery findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        if (objects && !error) {
            for (PFObject *object in objects) {
                JSQMessage *message = [[JSQMessage alloc] initWithSenderId:[object[@"sender"] objectId] senderDisplayName:[object[@"sender"] objectForKey:@"firstName"] date:object.createdAt text:object[@"message"]];
                [self.dataModel.messages addObject:message];
                [self finishReceivingMessageAnimated:YES];
            }
            [self.collectionView reloadData];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - JSQMessagesViewController method overrides

- (void)didPressSendButton:(UIButton *)button withMessageText:(NSString *)text senderId:(NSString *)senderId senderDisplayName:(NSString *)senderDisplayName date:(NSDate *)date
{
    PFObject *PFMessage = [PFObject objectWithClassName:@"Message"];
    [PFMessage setValue:[PFUser currentUser] forKey:@"sender"];
    [PFMessage setValue:text forKey:@"message"];
    [PFMessage setValue:self.passedEvent forKey:@"event"];
    
    [PFMessage saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
        if (succeeded && !error) {
            JSQMessage *message = [[JSQMessage alloc] initWithSenderId:senderId senderDisplayName:senderDisplayName date:date text:text];
            [self.dataModel.messages addObject:message];
            [self finishSendingMessageAnimated:YES];
        }
    }];
}

- (void)didPressAccessoryButton:(UIButton *)sender
{

}


#pragma mark - JSQMessages CollectionView DataSource


- (id<JSQMessageData>)collectionView:(JSQMessagesCollectionView *)collectionView messageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.dataModel.messages objectAtIndex:indexPath.item];
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didDeleteMessageAtIndexPath:(NSIndexPath *)indexPath
{
    [self.dataModel.messages removeObjectAtIndex:indexPath.item];
}

- (id<JSQMessageBubbleImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    JSQMessage *message = [self.dataModel.messages objectAtIndex:indexPath.item];
    
    if ([message.senderId isEqualToString:self.senderId]) {
        return self.dataModel.outgoingBubbleImageData;
    }
    
    return self.dataModel.incomingBubbleImageData;
}

-(CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath{
    return 20;
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    JSQMessage *message = [self.dataModel.messages objectAtIndex:indexPath.item];
    return [[NSAttributedString alloc] initWithString:message.senderDisplayName];
}

- (id<JSQMessageAvatarImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView avatarImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    JSQMessage *message = [self.dataModel.messages objectAtIndex:indexPath.item];
    
    return [self.dataModel.avatars objectForKey:message.senderId];
}

#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.dataModel.messages count];
}

- (UICollectionViewCell *)collectionView:(JSQMessagesCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    JSQMessagesCollectionViewCell *cell = (JSQMessagesCollectionViewCell *)[super collectionView:collectionView cellForItemAtIndexPath:indexPath];

    
    JSQMessage *msg = [self.dataModel.messages objectAtIndex:indexPath.item];
    
    if (!msg.isMediaMessage) {
        
        if ([msg.senderId isEqualToString:self.senderId]) {
            cell.textView.textColor = [UIColor blackColor];
        }
        else {
            cell.textView.textColor = [UIColor whiteColor];
        }
        
        cell.textView.linkTextAttributes = @{ NSForegroundColorAttributeName : cell.textView.textColor,
                                              NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid) };
    }
    
    return cell;
}




-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [refreshTimer invalidate];
}

-(void)dismiss:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}



@end
