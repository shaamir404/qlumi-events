//
//  ConversationsTableViewController.m
//  Qlumi
//
//  Created by Will on 2/16/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import "ConversationsTableViewController.h"

@interface ConversationsTableViewController (){
    NSMutableArray *conversationsArray;
    UIRefreshControl *refreshControl;
}

@end

@implementation ConversationsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Messages";
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor blackColor];
    [refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    [self.tableView setRefreshControl:refreshControl];
    [self refresh];
    
    UIBarButtonItem *closeButton = [[UIBarButtonItem alloc]  initWithTitle:@"❮ Back" style:UIBarButtonItemStyleDone target:self action:@selector(dismiss:)];
    self.navigationItem.leftBarButtonItem = closeButton;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ConversationTableViewCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
}

-(void)refresh{
    conversationsArray = [[NSMutableArray alloc] init];
    
    __block NSMutableArray *invitedEventIds = [[NSMutableArray alloc] init];
    
    PFQuery *invitedEventIdsQ = [PFQuery queryWithClassName:@"Invite"];
    [invitedEventIdsQ whereKey:@"phoneNumber" equalTo:[PFUser currentUser][@"phoneNumber"]];
    [invitedEventIdsQ findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        if (objects && !error) {
            for (PFObject *object in objects) {
                [invitedEventIds addObject: [object[@"event"] objectId]];
            }
            [self refreshAllQueriesWithArray:invitedEventIds];
        }else{
            [self refreshAllQueriesWithArray:nil];
        }
    }];
}

-(void)refreshAllQueriesWithArray:(NSArray *)invitedEventIds{
    PFQuery *myEvents = [PFQuery queryWithClassName:@"Event"];
    [myEvents whereKey:@"user" equalTo:[PFUser currentUser]];
    
    PFQuery *invitedEvents = [PFQuery queryWithClassName:@"Event"];
    [invitedEvents whereKey:@"objectId" containedIn:invitedEventIds];
    
    PFQuery *query = [PFQuery orQueryWithSubqueries:@[myEvents, invitedEvents]];
    [query orderByAscending:@"date"];
    [query includeKey:@"user"];
    [query whereKey:@"archived" notEqualTo:@(YES)];
    [query whereKey:@"archivedForUsers" notContainedIn:@[[PFUser currentUser][@"phoneNumber"]]];
    [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable events, NSError * _Nullable error) {
        [refreshControl endRefreshing];
        if (!error && events) {
            conversationsArray = [events mutableCopy];
            [self.tableView reloadData];
        }
    }];
}


#pragma mark - Table view data source

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 72;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return conversationsArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    ConversationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[ConversationTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    [cell.thumbnailImageView sd_setImageWithURL:[NSURL URLWithString:[(PFFile *)conversationsArray[indexPath.row][@"image"] url]] placeholderImage:[UIImage imageNamed:@"photo-placeholder"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        [cell.imageView setNeedsDisplay];
    }];

    cell.groupLabel.text = [NSString stringWithFormat:@"%@ Group Chat", conversationsArray[indexPath.row][@"title"]];
    cell.messagePreview.text = conversationsArray[indexPath.row][@"last_message"];
//    cell.detailTextLabel.text = @"The latest message goes here and is just a preview of what's going on.";
    

    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    MessagesViewController *vc = [[MessagesViewController alloc] init];
    vc.passedEvent = conversationsArray[indexPath.row];
    
    [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark - IBActions

-(IBAction)dismiss:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
