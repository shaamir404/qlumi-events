//
//  NewSearchProfileViewController.h
//  Qlumi
//
//  Created by Will on 2/2/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import "QMapView.h"

@class NewSearchProfileViewController;
@protocol NewSearchProfileDelegate <NSObject>
- (void)newSearchProfileViewController:(NewSearchProfileViewController *)viewController didCreateSearchProfile:(PFObject *)newProfile;
@end


@interface NewSearchProfileViewController : UIViewController <MKMapViewDelegate>
@property (nonatomic, weak) id <NewSearchProfileDelegate> delegate;

@end
