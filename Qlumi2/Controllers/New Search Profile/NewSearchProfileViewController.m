//
//  NewSearchProfileViewController.m
//  Qlumi
//
//  Created by Will on 2/2/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import "NewSearchProfileViewController.h"

@interface NewSearchProfileViewController (){
    
    UITextField *keywordTextField;
    UIView *underline;
    
    QMapView *mapView;
    MKCircle *circle;
    
    UIView *radiusView;
    UILabel *radiusLabel;
}

@end

@implementation NewSearchProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"New Search Interest";

    self.view.backgroundColor = [UIColor whiteColor];
    
    UIBarButtonItem *closeButton = [[UIBarButtonItem alloc]  initWithTitle:@"❮ Back" style:UIBarButtonItemStyleDone target:self action:@selector(dismiss:)];
    self.navigationItem.leftBarButtonItem = closeButton;
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(create:)];
    self.navigationItem.rightBarButtonItem = doneButton;
    
    keywordTextField = [[UITextField alloc] initWithFrame:CGRectZero];
    keywordTextField.placeholder = @"Keyword";
    keywordTextField.font = [UIFont systemFontOfSize:24];
    [self.view addSubview:keywordTextField];
    
    [keywordTextField becomeFirstResponder];
    
    underline = [[UIView alloc] initWithFrame:CGRectZero];
    underline.backgroundColor = [UIColor lightGrayColor];
    [self.view addSubview:underline];
    
    mapView = [[QMapView alloc] initWithFrame:CGRectZero andEditingEnabled:NO];
    mapView.showsUserLocation = YES;
    mapView.delegate = self;
    mapView.shouldShowResearch = YES;
    [self.view addSubview:mapView];
    
    radiusView = [[UIView alloc] initWithFrame:CGRectZero];
    radiusView.userInteractionEnabled = NO;
    [self.view addSubview:radiusView];
    
    radiusLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    radiusLabel.font = [UIFont systemFontOfSize:24];
    [self.view addSubview:radiusLabel];
}

-(void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    keywordTextField.frame = CGRectMake(16, self.navigationController.navigationBar.frame.size.height+self.navigationController.navigationBar.frame.origin.y+16, self.view.frame.size.width-32, 44);
    
    underline.frame = CGRectMake(16, keywordTextField.frame.origin.y+keywordTextField.frame.size.height, keywordTextField.frame.size.width, 1);

    mapView.frame = CGRectMake(16, keywordTextField.frame.size.height+keywordTextField.frame.origin.y+16, self.view.frame.size.width-32, (self.view.frame.size.width/2)-32);
    
    radiusView.frame = CGRectMake(0,0, mapView.frame.size.height, mapView.frame.size.height);
    radiusView.center = mapView.center;
    radiusView.layer.cornerRadius = radiusView.frame.size.height/2;
    radiusView.backgroundColor = [UIColor colorWithRed:0.33 green:0.63 blue:0.86 alpha:1.0];
    radiusView.layer.borderColor = [UIColor colorWithRed:0.08 green:0.27 blue:0.76 alpha:1.0].CGColor;
    radiusView.layer.borderWidth = 2;
    radiusView.alpha = 0.5;
    
    radiusLabel.frame = CGRectMake(0, 0, 200, 100);
    radiusLabel.layer.shadowColor = [UIColor whiteColor].CGColor;
    radiusLabel.layer.shadowRadius = 10;
    radiusLabel.layer.shadowOpacity = 1;
    radiusLabel.center = mapView.center;
    radiusLabel.textAlignment = NSTextAlignmentCenter;
}

-(float)radiusFromMap:(MKMapView *)map{
    CLLocationCoordinate2D centerCoor = map.centerCoordinate;
    CLLocation *centerLocation = [[CLLocation alloc] initWithLatitude:centerCoor.latitude longitude:centerCoor.longitude];
    CLLocationCoordinate2D topCenterCoor = [map convertPoint:CGPointMake(map.frame.size.width / 2.0f, 0) toCoordinateFromView:map];
    CLLocation *topCenterLocation = [[CLLocation alloc] initWithLatitude:topCenterCoor.latitude longitude:topCenterCoor.longitude];
    CLLocationDistance radius = [centerLocation distanceFromLocation:topCenterLocation];
    
    return radius/1609.344;
}
#pragma mark - MKMapViewDelegate

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated{
    radiusLabel.text = [NSString stringWithFormat:@"%.1fmi",([self radiusFromMap:mapView])];
}

#pragma mark - IBActions

-(IBAction)create:(id)sender{
    
    if (keywordTextField.text.length == 0) {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Please enter a keyword" preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
        
        return;
    }
    
    [SVProgressHUD show];
    
    PFObject *searchProfile = [PFObject objectWithClassName:@"SearchProfile"];
    searchProfile[@"radius"] = [NSNumber numberWithFloat: [self radiusFromMap:mapView]];
    searchProfile[@"keyword"] = keywordTextField.text;
    searchProfile[@"user"] = [PFUser currentUser];
    [searchProfile saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
        [SVProgressHUD dismiss];
        if (succeeded) {
            [self.delegate newSearchProfileViewController:self didCreateSearchProfile:searchProfile];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }];
    
}

-(IBAction)dismiss:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
