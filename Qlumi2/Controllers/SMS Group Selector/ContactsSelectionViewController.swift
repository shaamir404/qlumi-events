//
//  ContactsSelectionViewController.swift
//  Qlumi
//
//  Created by iMEDHealth_Dev on 20/11/2019.
//  Copyright © 2019 Red Door Endeavors. All rights reserved.
//

import UIKit
import MessageUI

class ContactsSelectionViewController: UIViewController {

    @IBOutlet weak var allGuestsCollectionView: UICollectionView!
    @IBOutlet weak var goingGuestsCollectionView: UICollectionView!
    @IBOutlet weak var notGoingGuestsCoilectionView: UICollectionView!
    @IBOutlet weak var maybeGoingGuestsCollectionView: UICollectionView!
    @IBOutlet weak var noRSVPGuestsCollectionView: UICollectionView!
    
    @IBOutlet weak var allGuestsView_height: NSLayoutConstraint!
    @IBOutlet weak var goingGuestsView_height: NSLayoutConstraint!
    @IBOutlet weak var notGoingGuestsView_height: NSLayoutConstraint!
    @IBOutlet weak var maybeGoingGuestsView_height: NSLayoutConstraint!
    @IBOutlet weak var noRSVPGuestsView_height: NSLayoutConstraint!
    
    @IBOutlet weak var selectionButton1: UIButton!
    @IBOutlet weak var selectionButton2: UIButton!
    @IBOutlet weak var selectionButton3: UIButton!
    @IBOutlet weak var selectionButton4: UIButton!
    @IBOutlet weak var selectionButton5: UIButton!
    
    var guestsArray: [[String: Any]] = []
    var goingGuestsArray: [[String: Any]] = []
    var notGoingGuestsArray: [[String: Any]] = []
    var maybeGoingGuestsArray: [[String: Any]] = []
    var noRSVPGuestsArray: [[String: Any]] = []
    
    var selectedGuests: [String] = []
    var profilePics: [String: Any] = [:]
    var colorsArray: [UIColor] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        title = "Send SMS"
        let nextButton = UIBarButtonItem(title: "Next", style: .plain, target: self, action: #selector(showNextVC))
        self.navigationItem.rightBarButtonItem = nextButton
        let closeButton = UIBarButtonItem(title: "❮ Back", style: .done, target: self, action: #selector(navigateBack))
        self.navigationItem.leftBarButtonItem = closeButton
        colorsArray = EPGlobalConstants.colorsArray() as! [UIColor]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getGuestsProfilePics()
        filterGuests()
    }
    
    @IBAction func guestsSelectionAction(_ sender: UIButton) {
        var isSelected = false
        if sender.titleLabel?.text == "Select All" {
            isSelected = true
            sender.setTitle("Deselect All", for: .normal)
        }else {
            isSelected = false
            sender.setTitle("Select All", for: .normal)
        }
        var sourceArray: [[String: Any]] = []
        if sender == selectionButton1 {
            sourceArray = guestsArray
        }else if sender == selectionButton2 {
            sourceArray = goingGuestsArray
        }else if sender == selectionButton3 {
            sourceArray = notGoingGuestsArray
        }else if sender == selectionButton4 {
            sourceArray = maybeGoingGuestsArray
        }else {
            sourceArray = noRSVPGuestsArray
        }
        if isSelected {
            for guest in sourceArray {
                let phoneNumber = guest["phoneNumber"] as? String ?? ""
                if !selectedGuests.contains(phoneNumber) {
                    selectedGuests.append(phoneNumber)
                }
            }
        }else {
            for guest in sourceArray {
                let phoneNumber = guest["phoneNumber"] as? String ?? ""
                if selectedGuests.contains(phoneNumber) {
                    selectedGuests.removeAll(where: { return $0 == phoneNumber })
                }
            }
        }
        
        allGuestsCollectionView.reloadData()
        goingGuestsCollectionView.reloadData()
        notGoingGuestsCoilectionView.reloadData()
        maybeGoingGuestsCollectionView.reloadData()
        noRSVPGuestsCollectionView.reloadData()
    }
    
    @objc func showNextVC() {
        if selectedGuests.count == 0 {
            let titleString = NSAttributedString(string: "Error", attributes: [.font: UIFont(name: "JosefinSans-Bold", size: 18.0)!])
            let messageString = NSAttributedString(string: "Please select contacts to send Message.", attributes: [.font: UIFont(name: "JosefinSans-Regular", size: 15.0)!])
            let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
            alert.setValue(titleString, forKey: "attributedTitle")
            alert.setValue(messageString, forKey: "attributedMessage")
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        let composeVC = MFMessageComposeViewController()
        composeVC.messageComposeDelegate = self
        composeVC.recipients = selectedGuests
        self.present(composeVC, animated: true, completion: nil)
    }
    
    @objc func navigateBack() {
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ContactsSelectionViewController {
    func filterGuests() {
        goingGuestsArray = []
        notGoingGuestsArray = []
        maybeGoingGuestsArray = []
        noRSVPGuestsArray = []
        for guest in guestsArray {
            let rsvp = guest["rsvp"] as? String ?? ""
            if rsvp == "yes" {
                goingGuestsArray.append(guest)
            }else if rsvp == "no" {
                notGoingGuestsArray.append(guest)
            }else if rsvp == "maybe" {
                maybeGoingGuestsArray.append(guest)
            }else if rsvp == "none" {
                noRSVPGuestsArray.append(guest)
            }
        }
        goingGuestsView_height.constant = goingGuestsArray.count == 0 ? 0:120
        notGoingGuestsView_height.constant = notGoingGuestsArray.count == 0 ? 0:120
        maybeGoingGuestsView_height.constant = maybeGoingGuestsArray.count == 0 ? 0:120
        noRSVPGuestsView_height.constant = noRSVPGuestsArray.count == 0 ? 0:120
        allGuestsCollectionView.reloadData()
        goingGuestsCollectionView.reloadData()
        notGoingGuestsCoilectionView.reloadData()
        maybeGoingGuestsCollectionView.reloadData()
        noRSVPGuestsCollectionView.reloadData()
    }
    
    func getGuestsProfilePics() {
        let usersQuery = PFUser.query()
        var phoneNumbers: [String] = []
        for guest in self.guestsArray {
            let phoneNumber = guest["phoneNumber"] as? String ?? ""
            phoneNumbers.append(phoneNumber)
        }
        usersQuery?.whereKey("phoneNumber", containedIn: phoneNumbers)
        usersQuery?.findObjectsInBackground(block: { objects, error in
            if error == nil {
                for profile in objects ?? [] {
                    guard let profile = profile as? PFUser else {
                        continue
                    }
                    let picFile = profile["profilePicture"] as? PFFile
                    if picFile != nil {
                        let phone = profile["PhoneNumber"] as? String ?? ""
                        self.profilePics[phone] = picFile?.url
                    }
                }
                self.allGuestsCollectionView.reloadData()
                self.goingGuestsCollectionView.reloadData()
                self.notGoingGuestsCoilectionView.reloadData()
                self.maybeGoingGuestsCollectionView.reloadData()
                self.noRSVPGuestsCollectionView.reloadData()
            }
        })
    }
    
    func color(forRSVP rsvp: String?) -> UIColor? {
        if (rsvp == "yes") {
            return UIColor(red: 0.33, green: 0.71, blue: 0.64, alpha: 1.0)
        } else if (rsvp == "no") {
            return UIColor(red: 0.88, green: 0.44, blue: 0.34, alpha: 1.0)
        } else if (rsvp == "maybe") {
            return UIColor(red: 0.97, green: 0.66, blue: 0.33, alpha: 1.0)
        } else {
            return UIColor.clear
        }
    }
}

extension ContactsSelectionViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == allGuestsCollectionView {
            return guestsArray.count
        }else if collectionView == goingGuestsCollectionView {
            return goingGuestsArray.count
        }else if collectionView == notGoingGuestsCoilectionView {
            return notGoingGuestsArray.count
        }else if collectionView == maybeGoingGuestsCollectionView {
            return maybeGoingGuestsArray.count
        }else {
            return noRSVPGuestsArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "guestCollectionCell", for: indexPath) as! GuestCollectionCell
        var guest: [String: Any] = [:]
        if collectionView == allGuestsCollectionView {
            guest = guestsArray[indexPath.row]
        }else if collectionView == goingGuestsCollectionView {
            guest = goingGuestsArray[indexPath.row]
        }else if collectionView == notGoingGuestsCoilectionView {
            guest = notGoingGuestsArray[indexPath.row]
        }else if collectionView == maybeGoingGuestsCollectionView {
            guest = maybeGoingGuestsArray[indexPath.row]
        }else {
            guest = noRSVPGuestsArray[indexPath.row]
        }
        cell.nameLabel.text = guest["name"] as? String ?? ""
        let phoneNumber = guest["phoneNumber"] as? String ?? ""
        var isProfilePic = false
        if let profilePic = profilePics[phoneNumber] as? String {
            isProfilePic = true
            cell.profileImageView.sd_setImage(with: URL(string: profilePic), completed: nil)
        }else {
            cell.profileImageView.image = nil
        }
        let rsvp = guest["rsvp"] as? String ?? ""
        cell.rsvpImageView.image = UIImage(named: "rsvp-]\(rsvp)")
        cell.rsvpImageView.backgroundColor = self.color(forRSVP: rsvp)
        cell.containerView.backgroundColor = colorsArray[indexPath.row % colorsArray.count]
        if selectedGuests.contains(phoneNumber) {
            cell.checkImageView.isHidden = false
        }else {
            cell.checkImageView.isHidden = true
        }
        if !isProfilePic {
            cell.profileImageView.isHidden = true
            let guestName = guest["name"] as? String ?? ""
            let components = guestName.components(separatedBy: " ")
            if components.count > 1 {
                let firstInitial: String = components[0].count > 0 ? String(components[0].prefix(1)): ""
                let secondInitial: String = components[1].count > 0 ? String(components[1].prefix(1)): ""
                cell.userInitialsLAbel.text = "\(firstInitial)\(secondInitial)"
            }else {
                cell.userInitialsLAbel.text = components[0].count > 0 ? String(components[0].prefix(1)): ""
            }
        }else {
            cell.profileImageView.isHidden = false
            cell.userInitialsLAbel.text = ""
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var guest: [String: Any] = [:]
        if collectionView == allGuestsCollectionView {
            guest = guestsArray[indexPath.row]
        }else if collectionView == goingGuestsCollectionView {
            guest = goingGuestsArray[indexPath.row]
        }else if collectionView == notGoingGuestsCoilectionView {
            guest = notGoingGuestsArray[indexPath.row]
        }else if collectionView == maybeGoingGuestsCollectionView {
            guest = maybeGoingGuestsArray[indexPath.row]
        }else {
            guest = noRSVPGuestsArray[indexPath.row]
        }
        let phoneNumber = guest["phoneNumber"] as? String ?? ""
        if selectedGuests.contains(phoneNumber) {
            selectedGuests.removeAll {
                return $0 == phoneNumber
            }
        }else {
            selectedGuests.append(phoneNumber)
        }
        self.allGuestsCollectionView.reloadData()
        self.goingGuestsCollectionView.reloadData()
        self.notGoingGuestsCoilectionView.reloadData()
        self.maybeGoingGuestsCollectionView.reloadData()
        self.noRSVPGuestsCollectionView.reloadData()
    }
}

extension ContactsSelectionViewController: MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true, completion: nil)
    }
}

