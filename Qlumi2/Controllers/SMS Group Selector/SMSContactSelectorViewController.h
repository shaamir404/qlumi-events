//
//  SMSContactSelectorViewController.h
//  Qlumi
//
//  Created by Will Smillie on 11/13/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventGuestsCell.h"
#import <MessageUI/MessageUI.h>

NS_ASSUME_NONNULL_BEGIN

typedef enum : NSUInteger {
    RSVPStatusYES,
    RSVPStatusNo,
    RSVPStatusMaybe,
    RSVPStatusMissing,
} RSVPStatus;

@interface SMSContactSelectorViewController : UITableViewController <GuestSelectionDelegate, MFMessageComposeViewControllerDelegate>

@property (nonatomic, strong) NSMutableArray *guestsArray;
@property (nonatomic, strong) NSMutableArray *selectedGuests;
-(NSMutableArray *)guestsWithRSVPStatus:(RSVPStatus)status;

@end

NS_ASSUME_NONNULL_END
