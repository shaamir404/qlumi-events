//
//  SMSContactSelectorViewController.m
//  Qlumi
//
//  Created by Will Smillie on 11/13/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import "SMSContactSelectorViewController.h"
#import <Parse/Parse.h>
#import <SDWebImage/SDWebImageDownloader.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface SMSContactSelectorViewController (){
    NSMutableDictionary *profilePictureURLs;
}

@end

@implementation SMSContactSelectorViewController

static NSString * const reuseIdentifier = @"guestsCell";


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"EventGuestsCell" bundle:nil] forCellReuseIdentifier:@"EventGuestsCell"];

    self.title = @"Send SMS";
    
    self.selectedGuests = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *closeButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(dismiss)];
    self.navigationItem.leftBarButtonItem = closeButton;
    
    UIBarButtonItem *nextButton = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(next)];
    self.navigationItem.rightBarButtonItem = nextButton;

}

-(void)setGuestsArray:(NSMutableArray *)guests{
    _guestsArray = [guests mutableCopy];
    
    profilePictureURLs = [[NSMutableDictionary alloc] init];
    NSMutableArray *phoneNumbers = [[NSMutableArray alloc] init];
    for (NSMutableDictionary *guest in self.guestsArray) {
        [phoneNumbers addObject:guest[@"phoneNumber"]];
    }
    
    PFQuery *usersQuery = [PFUser query];
    [usersQuery whereKey:@"phoneNumber" containedIn:phoneNumbers];
    [usersQuery findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        if (!error) {
            for (PFUser *profile in objects) {
                PFFile *picFile = profile[@"profilePicture"];
                if (picFile) {
                    [profilePictureURLs setObject:picFile.url forKey:profile[@"phoneNumber"]];
                }
            }
            [self.tableView reloadData];
        }
    }];
    [self.tableView reloadData];
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 5;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    EventGuestsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EventGuestsCell"];
    cell.delegate = self;

    
    switch (indexPath.row) {
        case 0:
            cell.customHeader = @"All";
            [cell setGuestsArray:self.guestsArray];

            break;
        case 1:
            cell.customHeader =  @"Going";
            [cell setGuestsArray:[self guestsWithRSVPStatus:RSVPStatusYES]];

            break;
        case 2:
            cell.customHeader =  @"Not Going";
            [cell setGuestsArray:[self guestsWithRSVPStatus:RSVPStatusNo]];

            break;
        case 3:
            cell.customHeader =  @"Maybe";
            [cell setGuestsArray:[self guestsWithRSVPStatus:RSVPStatusMaybe]];

            break;
        case 4:
            cell.customHeader =  @"Not RSVP'd";
            [cell setGuestsArray:[self guestsWithRSVPStatus:RSVPStatusMissing]];

            break;
    }
    
    [cell setSelectedGuests:self.selectedGuests];
    [cell setSelectionEnabled:YES];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:
            if (self.guestsArray.count == 0) {return 0;}
            break;
        case 1:
            if ([self guestsWithRSVPStatus:RSVPStatusYES].count == 0) {return 0;}
            break;
        case 2:
            if ([self guestsWithRSVPStatus:RSVPStatusNo].count == 0) {return 0;}
            break;
        case 3:
            if ([self guestsWithRSVPStatus:RSVPStatusMaybe].count == 0) {return 0;}
            break;
        case 4:
            if ([self guestsWithRSVPStatus:RSVPStatusMissing].count == 0) {return 0;}
            break;
    }
    return [EventGuestsCell sectionHeight];
}

-(void)guestsCell:(EventGuestsCell *)guestsCell didSetGuestsArray:(NSArray *)guestsArray{
}

-(void)guestsCell:(EventGuestsCell *)guestsCell didAddGuest:(NSDictionary *)guest{
    [self.selectedGuests addObject:guest];
    [self.tableView reloadData];
}
-(void)guestsCell:(EventGuestsCell *)guestsCell didRemoveGuest:(NSDictionary *)guest{
    [self.selectedGuests removeObject:guest];
    [self.tableView reloadData];
}


-(void)dismiss{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)next{
    MFMessageComposeViewController* composeVC = [[MFMessageComposeViewController alloc] init];
    composeVC.messageComposeDelegate = self;
    
    NSMutableArray *numbers = [[NSMutableArray alloc] init];
    for (NSDictionary *guest in self.selectedGuests) {
        [numbers addObject:guest[@"phoneNumber"]];
    }
    
    // Configure the fields of the interface.
    composeVC.recipients = numbers;
//    composeVC.body = ;
    
    // Present the view controller modally.
    [self presentViewController:composeVC animated:YES completion:nil];
}

-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result{
    [controller dismissViewControllerAnimated:YES completion:nil];
}


-(NSMutableArray *)guestsWithRSVPStatus:(RSVPStatus)status{
    NSString *statusString = [self statusToString:status];
    NSMutableArray *results = [[NSMutableArray alloc] init];
    for (NSDictionary *guest in self.guestsArray) {
        if ([guest[@"rsvp"] isEqualToString:statusString]) {
            [results addObject:guest];
        }
    }
    
    
    NSSortDescriptor *brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:brandDescriptor];
    return [[results sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
}

-(NSString *)statusToString:(RSVPStatus)status{
    switch (status) {
        case RSVPStatusMissing:
            return @"none";
            break;
        case RSVPStatusYES:
            return @"yes";
            break;
        case RSVPStatusNo:
            return @"no";
            break;
        case RSVPStatusMaybe:
            return @"maybe";
            break;

        default:
            break;
    }
}


@end
