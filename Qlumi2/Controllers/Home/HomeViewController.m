//
//  ViewController.m
//  Qlumi2
//
//  Created by William Smillie on 12/11/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import "HomeViewController.h"


@interface HomeViewController (){
    NSIndexPath *selectedIndex;
    UIRefreshControl *refreshControl;
        
    QButton *button;
    UIView *mapFooterView;
    
    SwipableContainerViewController *detailVC;
    NotificationsViewController *notificationsController;
    NearMeViewController *mapViewController;
    
    int walkthroughIndex;
    WVWalkthroughView *walkthrough;
}

@end

@implementation HomeViewController
@synthesize eventsArray, notificationsButton;

- (void)viewDidLoad {
    [super viewDidLoad];

    [self setupUI];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refresh) name:@"reloadEvents" object:nil];
    if ([PFUser currentUser]) {
        [self refresh];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    

    if (![PFUser currentUser]) {
        RegisterViewController *registerVC = [[RegisterViewController alloc] initWithHomeVC:self];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:registerVC];
        [self presentViewController:nav animated:YES completion:nil];
    }else{
        if (![[NSUserDefaults standardUserDefaults] boolForKey:@"completedWalkthrough"]) {
            UIAlertController *welcomeAlert = [UIAlertController alertControllerWithTitle:@"Welcome to Qlumi!" message:@"Would you like a quick tutorial?" preferredStyle:UIAlertControllerStyleAlert];
            [welcomeAlert addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                walkthroughIndex = 0;
                [self stepThroughWalkthrough];
            }]];
            [welcomeAlert addAction:[UIAlertAction actionWithTitle:@"Skip" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"completedWalkthrough"];
            }]];
            [self presentViewController:welcomeAlert animated:YES completion:nil];
        }
    }
}

-(void)stepThroughWalkthrough{
    walkthrough = [[WVWalkthroughView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [walkthrough setDelegate:self];

    
    [walkthrough setHideOnTappingEmptyArea:YES];
    [walkthrough setHideOnTappingTooltip:YES];
    [walkthrough setHideOnTappingMaskedArea:YES];
    [walkthrough setPassTouchThroughEmptyArea:NO];
    
    CGRect firstCellFrame = [walkthrough convertRect:[[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]] frame] fromView:self.view];
    firstCellFrame.origin.y = self.navigationController.navigationBar.frame.size.height+self.navigationController.navigationBar.frame.origin.y;
    
    UIImageView *pressAndHoldImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 246, 246)];
    [pressAndHoldImgView setImage:[UIImage imageNamed:@"holdExample"]];

    switch (walkthroughIndex) {
        case 0:
            [walkthrough setIconFont:[UIFont systemFontOfSize:19]];
            [walkthrough setIconText:[NSString stringWithFormat:@"Q"]];
            [walkthrough addMaskWithRect:CGRectMake(0, 0, 0, 0) andCornerRadius:4.0f];
            [walkthrough setText:@"Welcome to Qlumi! Let's show you around. Tap anywhere to begin."];

            break;
        case 1:
            [walkthrough setIconFont:[UIFont systemFontOfSize:19]];
            [walkthrough setIconText:[NSString stringWithFormat:@"Q"]];
            [walkthrough addMaskWithRect:firstCellFrame andCornerRadius:4.0f];
            [walkthrough setShowTouchPointer:NO];
            [walkthrough setText:@"This is an event. Events that you have created or have been invited to will show up here."];
            break;
        case 2:
            [walkthrough setIconFont:[UIFont systemFontOfSize:19]];
            [walkthrough setIconText:[NSString stringWithFormat:@"Q"]];
            [walkthrough addMaskWithRect:CGRectMake(self.view.frame.size.width-75, self.view.frame.size.height-75, 75, 75) andCornerRadius:4.0f];
            [walkthrough setShowTouchPointer:YES];
            [walkthrough setText:@"Tap the \"Q\" To open the menu. You can create an event by tapping on the \"New Event +\" icon. Tap anywhere to close the Menu."];
            break;
        case 3:
            [walkthrough setIconFont:[UIFont systemFontOfSize:19]];
            [walkthrough setIconText:[NSString stringWithFormat:@"Q"]];
            [walkthrough addMaskWithRect:CGRectMake(0,200,self.view.frame.size.width, 200) andCornerRadius:4.0f];
            [walkthrough setShowTouchPointer:YES];
            [walkthrough setText:@"Tap anywhere to dismiss"];

            break;
        
        case 4:
            [walkthrough setIconFont:[UIFont systemFontOfSize:19]];
            [walkthrough setIconText:[NSString stringWithFormat:@"Q"]];
            [walkthrough addMaskWithRect:CGRectMake(self.view.frame.size.width-51, 26, 40, 40) andCornerRadius:4.0f];
            [walkthrough setShowTouchPointer:YES];
            [walkthrough setText:@"Tap the marker button to filter/search by your interests. You can tap on items to toggle them on/off. You can also add new interests by tapping the \"+\""];
            
            break;
        case 5:
            [walkthrough setIconFont:[UIFont systemFontOfSize:19]];
            [walkthrough setIconText:[NSString stringWithFormat:@"Q"]];
            [walkthrough setShowTouchPointer:YES];
            [walkthrough addMaskWithRect:CGRectMake(0, mapViewController.filterController.tableView.frame.size.height+mapViewController.filterController.tableView.frame.origin.y+80, self.view.frame.size.width, 100) andCornerRadius:4.0f];
            [walkthrough setText:@"Touch and hold a place on the map to enable options to place yourself or navigate \"home\""];
            
            break;

        case 6:
            [walkthrough setPassTouchThroughEmptyArea:YES];
            [walkthrough setIconFont:[UIFont systemFontOfSize:19]];
            [walkthrough setIconText:[NSString stringWithFormat:@"Q"]];
            [walkthrough setShowTouchPointer:YES];
            [walkthrough addMaskWithRect:CGRectMake(0, mapViewController.filterController.tableView.frame.size.height+mapViewController.filterController.tableView.frame.origin.y, self.view.frame.size.width, 180) andCornerRadius:4.0f];
            [walkthrough setText:@"Touch and hold on the map to show more options"];
            
            [mapViewController.mapView.contextualMenu presentFromPoint:CGPointMake((self.view.frame.size.width/2), mapViewController.filterController.tableView.frame.size.height+mapViewController.filterController.tableView.frame.origin.y+120)];

            break;

        case 7:
            [mapViewController.mapView.contextualMenu dismiss];
            [walkthrough setIconFont:[UIFont systemFontOfSize:19]];
            [walkthrough setIconText:[NSString stringWithFormat:@"Q"]];
            [walkthrough setShowTouchPointer:YES];
            [walkthrough addMaskWithRect:CGRectMake(8, 26, 60, 40) andCornerRadius:4.0f];
            [walkthrough setText:@"That's it! Enjoy using Qlumi!"];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"completedWalkthrough"];
            
            break;
    }
    

    
    if (walkthroughIndex <= 8) {
        [[UIApplication sharedApplication].keyWindow addSubview:walkthrough];
        [walkthrough show];
    }
}


#pragma mark - WVWalkthroughViewDelegate methods

- (void)didTapEmptyAreaForWalkthrough:(WVWalkthroughView *)walkView {
    [self nextAfterDelay];
}

- (void)didTapMaskedAreaForWalkthrough:(WVWalkthroughView *)walkView {
    [self nextAfterDelay];
}

- (void)didTapTooltipForWalkthrough:(WVWalkthroughView *)walkView {
    [self nextAfterDelay];
}

-(void)nextAfterDelay{
    switch (walkthroughIndex) {
        case 2:
            [button showButtonsAnimated:YES completionHandler:nil];
            break;
        case 3:
            [button hideButtonsAnimated:YES completionHandler:nil];
            break;
        case 4:
            [self showMap:nil];
            break;
        case 5:
            [mapViewController toggleFilter:nil];
            break;
        case 6:
            [mapViewController.filterController dismissViewControllerAnimated:YES completion:nil];
            break;
        case 7:

            break;
        case 8:
            [mapViewController dismissViewControllerAnimated:YES completion:nil];
            break;

        default:
            break;
    }
    
    [walkthrough hide];

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        walkthroughIndex++;
        [self stepThroughWalkthrough];
    });
}



#pragma mark - UICollectionViewDelegate
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return eventsArray.count;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((self.view.frame.size.width/2)-(12), (self.view.frame.size.width/2)-(3*20));
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    EventCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"eventCell" forIndexPath:indexPath];
    
    if (indexPath.row <= eventsArray.count) {
        Event *event = [eventsArray objectAtIndex:indexPath.row];
        cell.contentView.layer.cornerRadius = 10;
        [cell.titleLabel setText:event.title];
        [cell.bgImageView sd_setImageWithURL:[NSURL URLWithString:event.image.url] placeholderImage:[UIImage imageNamed:@"photo-placeholder"]];
        
        NSDate *yesterday = [[NSDate new] dateByAddingTimeInterval: -86400.0];


        if([yesterday timeIntervalSinceDate:event.date] > 0) {
            cell.alpha = 0.5;
        }else{
            cell.alpha = 1.0;
        }
    }
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    selectedIndex = indexPath;
    
    self.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    if (indexPath.row <= eventsArray.count) {
        Event *event = [eventsArray objectAtIndex:indexPath.row];
        if ([[event objectForKey:@"featured"] boolValue]) {
            EventDetailViewController *vc = [[EventDetailViewController alloc] init];
//            vc.view.frame = pageViewController.view.bounds;
            vc.tableView.layer.shadowRadius = 12;
            vc.tableView.layer.shadowOpacity = 1;
            [vc setPassedEvent:event];
            [self presentViewController:vc animated:true completion:nil];
        }else {
            NewEventViewController *eventVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"NewEventViewController"];
            [eventVC setPassedEvent:event];
            [eventVC setHierarchy:NewEventHierarchyDETAILS];
            UINavigationController * navVC = [[UINavigationController alloc] initWithRootViewController:eventVC];
            [self presentViewController:navVC animated:true completion:nil];
        }
        
        
    }
//    detailVC = [[SwipableContainerViewController alloc] init];
//    detailVC.transitioningDelegate = self;
//    detailVC.events = self.eventsArray;
//    detailVC.selectedIndex = selectedIndex.row;
//    [self presentViewController:detailVC animated:YES completion:nil];
}



#pragma mark - backend

-(void)refresh{
    if ([PFUser currentUser] == nil) {
        return;
    }

    [SVProgressHUD show];
    eventsArray = [[NSMutableArray alloc] init];

    __block NSMutableArray *invitedEventIds = [[NSMutableArray alloc] init];
    
    if ([notificationsController respondsToSelector:@selector(refresh)]){
        [notificationsController performSelector:@selector(refresh)];
    }
    
    PFQuery *invitedEventIdsQ = [PFQuery queryWithClassName:@"Invite"];
    [invitedEventIdsQ whereKey:@"phoneNumber" equalTo:[PFUser currentUser][@"phoneNumber"]];
    [invitedEventIdsQ setLimit:100];
    [invitedEventIdsQ findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        [SVProgressHUD dismiss];
        if (objects && !error) {
            for (PFObject *object in objects) {
                [invitedEventIds addObject: [object[@"event"] objectId]];
            }
            [self refreshAllQueriesWithArray:invitedEventIds];
        }else{
            [self refreshAllQueriesWithArray:nil];
        }
    }];
}

-(void)refreshAllQueriesWithArray:(NSArray *)invitedEventIds{
    PFQuery *myEvents = [PFQuery queryWithClassName:@"Event"];
    [myEvents whereKey:@"user" equalTo:[PFUser currentUser]];
    
    PFQuery *invitedEvents = [PFQuery queryWithClassName:@"Event"];
    [invitedEvents whereKey:@"objectId" containedIn:invitedEventIds];
    
    PFQuery *featuredEvents = [PFQuery queryWithClassName:@"Event"];
    [featuredEvents whereKey:@"featured" equalTo:@(YES)];
    [featuredEvents whereKey:@"default" equalTo:@(YES)];

    PFQuery *query = [PFQuery orQueryWithSubqueries:@[featuredEvents, myEvents, invitedEvents]];
    [query orderByDescending:@"date"];
    [query includeKey:@"user"];
    [query whereKey:@"archivedForUsers" notContainedIn:@[[PFUser currentUser][@"phoneNumber"]]];
    [query whereKey:@"archived" notEqualTo:@(YES)];
    [query setLimit:100];
    [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable events, NSError * _Nullable error) {
        [refreshControl endRefreshing];
        if (!error && events.count > 0) {
            eventsArray = [events mutableCopy];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.collectionView reloadData];
                
                /*for (int i=0; i<eventsArray.count; i++){
                    Event *e = eventsArray[i];
                    
                    if([e.date timeIntervalSinceDate:[NSDate new]] > 0) {
                        [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0] atScrollPosition:UICollectionViewScrollPositionTop animated:YES];
                        break;
                    }
                }*/
            });
        }else{
            PFQuery *invitedEvents = [PFQuery queryWithClassName:@"Event"];
            [invitedEvents whereKey:@"default" equalTo:@(YES)];
            [invitedEvents findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
                if (objects.count > 0) {
                    eventsArray = [objects mutableCopy];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.collectionView reloadData];
                        
                        for (int i=0; i<eventsArray.count; i++){
                            Event *e = eventsArray[i];
                            
                            if([e.date timeIntervalSinceDate:[NSDate new]] > 0) {
                                [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0] atScrollPosition:UICollectionViewScrollPositionTop animated:YES];
                                break;
                            }
                        }
                    });
                }
            }];
        }
    }];
}

#pragma mark - setup

-(void)setupUI{
    notificationsController = [[NotificationsViewController alloc] initWithRootViewController:self];
    
    self.collectionView.emptyDataSetSource = self;
    self.collectionView.emptyDataSetDelegate = self;

    notificationsButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"bell"] style:UIBarButtonItemStylePlain target:self action:@selector(showNotifications:)];
    notificationsButton.tintColor = [UIColor blackColor];
    self.navigationItem.rightBarButtonItem = notificationsButton;
    
//    UIImage *image = [UIImage imageNamed:@"header"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"header"]];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.frame = CGRectMake(0, 0, 140, self.navigationController.navigationBar.frame.size.height);
    imageView.center = self.navigationController.navigationBar.center;
    [self.navigationController.view addSubview:imageView];
    
    //Register Cells
    [self.collectionView registerNib:[UINib nibWithNibName:@"EventCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"eventCell"];
    
    //CollectionView Setup
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    [layout setSectionInset:UIEdgeInsetsMake(4, 8, 4, 8)];
    layout.itemSize = CGSizeMake((self.view.frame.size.width/2)-(2*8), 137);
    layout.minimumInteritemSpacing = 8;
    layout.minimumLineSpacing = 8;

    
    self.collectionView.collectionViewLayout = layout;
    self.collectionView.contentInset = UIEdgeInsetsMake(self.collectionView.contentInset.top, self.collectionView.contentInset.left, 200, self.collectionView.contentInset.right);
    
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor blackColor];
    [refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    [self.collectionView setRefreshControl:refreshControl];
    
    //Map Footer setup
    mapFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 200)];
    mapFooterView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:mapFooterView];
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(8, 16, mapFooterView.frame.size.width-16, 1)];
    line.backgroundColor = [UIColor lightGrayColor];
    [mapFooterView addSubview:line];
    
    UILabel *footerHeaderLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 4, 103, 21)];
    footerHeaderLabel.center = mapFooterView.center;
    footerHeaderLabel.frame = CGRectMake(footerHeaderLabel.frame.origin.x, 4, 90, 21);
    footerHeaderLabel.font = [UIFont boldSystemFontOfSize:17];
    footerHeaderLabel.text = @"Where To?";
    footerHeaderLabel.backgroundColor = [UIColor whiteColor];
    footerHeaderLabel.textAlignment = NSTextAlignmentCenter;
    [mapFooterView addSubview:footerHeaderLabel];
    
    mapFooterView.layer.masksToBounds = NO;
    mapFooterView.layer.shadowOpacity = 0.5f;
    mapFooterView.layer.shadowRadius = 5;
    mapFooterView.layer.shadowOffset = CGSizeZero;
    mapFooterView.layer.shadowColor = [UIColor blackColor].CGColor;
    mapFooterView.layer.shadowPath = [UIBezierPath bezierPathWithRect:mapFooterView.bounds].CGPath;
    
    QMapView *map = [[QMapView alloc] initWithFrame:CGRectMake(8, 28, mapFooterView.frame.size.width-16, mapFooterView.frame.size.height-36) andEditingEnabled:NO];
    [mapFooterView addSubview:map];
    map.showsUserLocation = YES;
    map.delegate = self;
    map.showsCompass = YES;
    map.showsScale = YES;
    map.layer.cornerRadius = 5;
    
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone) {
        if ((int)[[UIScreen mainScreen] nativeBounds].size.height == 2436){
            [self roundBottomCornersForView:map];
        }
    }
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showMap:)];
    [map addGestureRecognizer:tap];

    
    //Q Button
    button = [[QButton alloc] initWithNumberOfButtons:6 firstButtonIsPlusButton:YES showAfterInit:YES];
    [self.view addSubview:button];
    [button showAnimated:YES completionHandler:nil];
}

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"No Events";
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:18.0f],
                                 NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView {
    return YES;
}

#pragma mark - IBActions

-(IBAction)showMap:(UITapGestureRecognizer *)sender{
    mapViewController = [[NearMeViewController alloc] init];

    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:mapViewController];
    
    [self presentViewController:nav animated:YES completion:^{
    }];
}

-(IBAction)showNotifications:(UIBarButtonItem *)sender{
    notificationsController.modalPresentationStyle = UIModalPresentationPopover;
    notificationsController.preferredContentSize = CGSizeMake(300, 300);
    
    UIPopoverPresentationController *popover = notificationsController.popoverPresentationController;
    popover.delegate = self;
    popover.sourceView = [self.navigationItem.rightBarButtonItem valueForKey:@"view"];
    popover.sourceRect = CGRectMake(16,40,1,1);
    popover.permittedArrowDirections = UIPopoverArrowDirectionUp;

    [self presentViewController:notificationsController animated:YES completion:nil];
}

- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller
{
    return UIModalPresentationNone;
}


#pragma mark - UI Helpers

-(void)roundBottomCornersForView:(UIView *)view{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:view.bounds byRoundingCorners:UIRectCornerBottomLeft| UIRectCornerBottomRight cornerRadii:CGSizeMake(34, 34)];
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = view.bounds;
    maskLayer.path = maskPath.CGPath;
    view.layer.mask = maskLayer;
}


#pragma mark - RMPZoomTransitionAnimating

- (UIImageView *)transitionSourceImageView
{
    EventCollectionViewCell *cell = (EventCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:selectedIndex];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:cell.bgImageView.image];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;
    imageView.userInteractionEnabled = NO;
    imageView.frame = [cell.bgImageView convertRect:cell.bgImageView.frame toView:self.collectionView.superview];
    return imageView;
}

- (UIColor *)transitionSourceBackgroundColor{
    return self.collectionView.backgroundColor;
}

- (CGRect)transitionDestinationImageViewFrame
{
    EventCollectionViewCell *cell = (EventCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:detailVC.selectedIndex inSection:0]];
    CGRect cellFrameInSuperview = [cell.bgImageView convertRect:cell.bgImageView.frame toView:self.collectionView.superview];
    return cellFrameInSuperview;
}


#pragma mark - MKMapKitDelegate

-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation{
    MKCoordinateRegion mapRegion;
    mapRegion.center = mapView.userLocation.coordinate;
    mapRegion.span.latitudeDelta = 0.2;
    mapRegion.span.longitudeDelta = 0.2;
    [mapView setRegion:mapRegion animated: YES];
}



#pragma mark - <UIViewControllerTransitioningDelegate>

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
                                                                  presentingController:(UIViewController *)presenting
                                                                      sourceController:(UIViewController *)source
{
    
//    presented = [(UINavigationController *)presented viewControllers][0];
    id <RMPZoomTransitionAnimating, RMPZoomTransitionDelegate> sourceTransition = (id<RMPZoomTransitionAnimating, RMPZoomTransitionDelegate>)source;
    id <RMPZoomTransitionAnimating, RMPZoomTransitionDelegate> destinationTransition = (id<RMPZoomTransitionAnimating, RMPZoomTransitionDelegate>)presented;
    if ([sourceTransition conformsToProtocol:@protocol(RMPZoomTransitionAnimating)] &&
        [destinationTransition conformsToProtocol:@protocol(RMPZoomTransitionAnimating)]) {
        RMPZoomTransitionAnimator *animator = [[RMPZoomTransitionAnimator alloc] init];
        animator.goingForward = YES;
        animator.sourceTransition = sourceTransition;
        animator.destinationTransition = destinationTransition;
        return animator;
    }
    return nil;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
    id <RMPZoomTransitionAnimating, RMPZoomTransitionDelegate> sourceTransition = (id<RMPZoomTransitionAnimating, RMPZoomTransitionDelegate>)dismissed;
    id <RMPZoomTransitionAnimating, RMPZoomTransitionDelegate> destinationTransition = (id<RMPZoomTransitionAnimating, RMPZoomTransitionDelegate>)self;
    if ([sourceTransition conformsToProtocol:@protocol(RMPZoomTransitionAnimating)] &&
        [destinationTransition conformsToProtocol:@protocol(RMPZoomTransitionAnimating)]) {
        RMPZoomTransitionAnimator *animator = [[RMPZoomTransitionAnimator alloc] init];
        animator.goingForward = NO;
        animator.sourceTransition = sourceTransition;
        animator.destinationTransition = destinationTransition;
        return animator;
    }
    return nil;
}



@end
