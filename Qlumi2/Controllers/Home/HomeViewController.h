//
//  ViewController.h
//  Qlumi2
//
//  Created by William Smillie on 12/11/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

// Apple
#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

//3rd Party
#import <Parse/Parse.h>
#import "RMPZoomTransitionAnimator.h"

#import "WVWalkthroughView.h"
#import "UIFont+FontAwesome.h"
#import "NSString+FontAwesome.h"

//My Classes
#import "EventCollectionViewCell.h"
#import "SwipableContainerViewController.h"
#import "RegisterViewController.h"
#import "QButton.h"
#import "NotificationsViewController.h"
#import "NearMeViewController.h"



@interface HomeViewController : UICollectionViewController <UIViewControllerTransitioningDelegate, RMPZoomTransitionAnimating, MKMapViewDelegate, DZNEmptyDataSetDelegate, DZNEmptyDataSetSource, UIPopoverPresentationControllerDelegate, WVWalkthroughViewDelegate>

@property UIBarButtonItem *notificationsButton;
@property NSMutableArray *eventsArray;
-(void)refresh;


@end

