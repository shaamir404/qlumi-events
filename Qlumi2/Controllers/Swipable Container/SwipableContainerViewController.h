//
//  SwipableContainerViewController.h
//  Qlumi
//
//  Created by William Smillie on 12/29/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <INTUAnimationEngine/INTUAnimationEngine.h>

#import "SCPageViewController.h"

#import "SCParallaxPageLayouter.h"
#import "SCCardsPageLayouter.h"
#import "SCSlidingPageLayouter.h"

#import "EventDetailViewController.h"
#import "RMPZoomTransitionAnimator.h"

@interface SwipableContainerViewController : UIViewController <SCPageViewControllerDelegate, SCPageViewControllerDataSource, UIPageViewControllerDelegate, RMPZoomTransitionAnimating, RMPZoomTransitionDelegate, GSKStretchyHeaderViewStretchDelegate>

@property (nonatomic, strong)SCPageViewController *pageController;
@property (nonatomic, strong)NSArray *events;
@property NSInteger selectedIndex;


@end
