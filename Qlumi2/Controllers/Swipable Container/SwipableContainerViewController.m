//
//  SwipableContainerViewController.m
//  Qlumi
//
//  Created by William Smillie on 12/29/17.
//  Copyright © 2017 Red Door Endeavors. All rights reserved.
//

#import "SwipableContainerViewController.h"
#import "HomeViewController.h"

#import "Event.h"


@interface PassthroughView : UIView
@property (readonly) id target;
@property (readonly) SEL selector;
@end
@implementation PassthroughView
- (void)setTarget:(id)target selector:(SEL)selector {
    _target = target;
    _selector = selector;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [super touchesBegan:touches withEvent:event];

    UITouch *touch;
    touch=[touches anyObject];
    CGPoint point=[touch locationInView:self];
}
@end


@interface SwipableContainerViewController (){
    RMPZoomTransitionAnimator *animator;
    
    NSMutableArray *viewControllers;
    EventDetailViewController *currentViewController;
    
    PassthroughView *anytouchView;
}

@end

@implementation SwipableContainerViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.superview.backgroundColor = [UIColor redColor];

//    UIBarButtonItem *closeButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"close"] style:UIBarButtonItemStylePlain target:self action:@selector(dismiss:)];
//    self.navigationItem.leftBarButtonItem = closeButton;
    
    
    SCParallaxPageLayouter *layouter = [[SCParallaxPageLayouter alloc] init];
    self.pageController = [[SCPageViewController alloc] init];
    [self.pageController setLayouter:layouter animated:NO completion:nil];
    self.pageController.delegate = self;
    self.pageController.dataSource = self;
    [[self.pageController view] setFrame:[[self view] bounds]];
    [self addChildViewController:self.pageController];
    [[self view] addSubview:[self.pageController view]];
    [self.pageController didMoveToParentViewController:self];


    anytouchView = [[PassthroughView alloc] initWithFrame:self.view.bounds];
    [anytouchView setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
    [anytouchView setHidden:YES];
    [self.view addSubview:anytouchView];
}


-(void)viewDidLayoutSubviews{
    self.pageController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
}


- (NSUInteger)numberOfPagesInPageViewController:(SCPageViewController *)pageViewController{
    return self.events.count;
}

- (UIViewController *)pageViewController:(SCPageViewController *)pageViewController viewControllerForPageAtIndex:(NSUInteger)pageIndex{
    
    Event *currentEvent = self.events[pageIndex];
    if ([[currentEvent objectForKey:@"featured"] boolValue]) {
        EventDetailViewController *vc = [[EventDetailViewController alloc] init];
        vc.view.frame = pageViewController.view.bounds;
        vc.tableView.layer.shadowRadius = 12;
        vc.tableView.layer.shadowOpacity = 1;
        [vc setPassedEvent:self.events[pageIndex]];
        [viewControllers addObject:vc];
        return vc;
    }else {
        NewEventViewController *eventVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"NewEventViewController"];
        [eventVC setPassedEvent:self.events[pageIndex]];
        [viewControllers addObject:eventVC];
        return eventVC;
    }
//    EventDetailViewController *vc = [[EventDetailViewController alloc] init];
//    vc.view.frame = pageViewController.view.bounds;
//    vc.tableView.layer.shadowRadius = 12;
//    vc.tableView.layer.shadowOpacity = 1;
//    [vc setPassedEvent:self.events[pageIndex]];
//    [viewControllers addObject:vc];
    
//    return vc;
}

- (NSUInteger)initialPageInPageViewController:(SCPageViewController *)pageViewController{
    return self.selectedIndex;
}

-(void)pageViewController:(SCPageViewController *)pageViewController didNavigateToOffset:(CGPoint)offset{

//    for (EventDetailViewController *controller in self.pageController.visibleViewControllers) {
//        controller.view.alpha = 1;
//    }
}

-(void)pageViewController:(SCPageViewController *)pageViewController didNavigateToPageAtIndex:(NSUInteger)pageIndex{
    self.selectedIndex = pageIndex;
}



#pragma mark — Transition Animiation <RMPZoomTransitionAnimating>

-(CGRect)headerViewFrame{
    CGRect frame;// = [currentViewController headerView].frame;
//    frame = [[self.pageController.visibleViewControllers[0] headerView] convertRect:[self.pageController.visibleViewControllers[0] headerView].bounds toView:self.view.superview];
    return frame;
}

- (UIImageView *)transitionSourceImageView
{
//    UIImageView *imageView = [[UIImageView alloc] initWithImage:[self.pageController.visibleViewControllers[0] eventImageView].image];
//    imageView.contentMode = UIViewContentModeScaleAspectFill;
//    imageView.clipsToBounds = YES;
//    imageView.userInteractionEnabled = NO;
//    imageView.frame = [self headerViewFrame];
    UIImageView * imageView = [[UIImageView alloc] init];
    return imageView;
}

- (UIColor *)transitionSourceBackgroundColor
{
    return self.view.backgroundColor;
}

- (CGRect)transitionDestinationImageViewFrame
{
    return [self headerViewFrame];
}

#pragma mark - <RMPZoomTransitionDelegate>

- (void)zoomTransitionAnimator:(RMPZoomTransitionAnimator *)animator
         didCompleteTransition:(BOOL)didComplete
      animatingSourceImageView:(UIImageView *)imageView
{
//    [self.pageController.visibleViewControllers[0] eventImageView].image = imageView.image;
//    [self.pageController.visibleViewControllers[0] updateColorsForImage:[self.pageController.visibleViewControllers[0] eventImageView].image];
}


#pragma mark - StretchyHeaderDelegate

-(void)stretchyHeaderView:(GSKStretchyHeaderView *)headerView didChangeStretchFactor:(CGFloat)stretchFactor{
    
    HomeViewController *homeVC = (HomeViewController*)[(UINavigationController *)self.presentingViewController viewControllers][0];
    UICollectionViewCell *selectedCell = [homeVC.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:self.selectedIndex inSection:0]];
    CGRect frame = selectedCell.frame;
    
    UIView *view = [self.pageController.visibleViewControllers[0] view];
    
    
    if (stretchFactor >= 1) {
        CGFloat const inMin = 1;
        CGFloat const inMax = 1.6;
        CGFloat outMin = 0;
        CGFloat outMax = 1;
        CGFloat in = stretchFactor;
        CGFloat out = outMin + (outMax - outMin) * (in - inMin) / (inMax - inMin);


        float finalScale = frame.size.width/headerView.frame.size.width;
        float currentScale = INTUInterpolateCGFloat(1, finalScale, (out > 1)?1:out);

        float finalX = frame.origin.x;//(frame.origin.x-(view.frame.size.width/2))-12;
        float finalY = frame.origin.y-homeVC.collectionView.contentOffset.y;//(frame.origin.y-(view.frame.size.height/2))-(homeVC.collectionView.contentOffset.y+20);

        float currentX = INTUInterpolateCGFloat(0, finalX, (out > 1)?1:out);
        float currentY = INTUInterpolateCGFloat(0, finalY, (out > 1)?1:out);
        
//        CGAffineTransform scale = CGAffineTransformMakeScale(currentScale, currentScale);
//        CGAffineTransform translate = CGAffineTransformMakeTranslation(currentX, currentY);
//        CGAffineTransform transform = CGAffineTransformConcat(translate, scale);
//        CATransform3D transform3d = CATransform3DMakeAffineTransform(translate);
        
//        [(EventDetailViewController*)self.pageController.visibleViewControllers[0] tableView.];
        
        view.layer.shadowOpacity = 1.0;
        view.layer.shadowRadius = 10.0;
        
//        [self.pageController _setAnimatableSublayerTransform:transform3d forViewController:self.pageController];
        
        if (out >= 1) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
    
//    for (EventDetailViewController *controller in self.pageController.loadedViewControllers) {
//        if (controller != self.pageController.visibleViewControllers[0]) {
//            controller.view.alpha = INTUInterpolateCGFloat(1, 0, (stretchFactor > 1)?1:stretchFactor);
//            [controller.view setNeedsDisplay];
//        }else{
//            controller.view.alpha = 1;
//        }
//    }
}

- (CGFloat)xscale:(CGAffineTransform)transform {
    CGAffineTransform t = transform;
    return sqrt(t.a * t.a + t.c * t.c);
}

- (CGFloat)yscale:(CGAffineTransform)transform{
    CGAffineTransform t = transform;
    return sqrt(t.b * t.b + t.d * t.d);
}



#pragma mark - IBActions

-(IBAction)dismiss:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
