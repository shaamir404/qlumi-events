//
//  CalloutController.h
//  Qlumi
//
//  Created by Will on 2/6/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Place.h"
#import "QCalloutView.h"

#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIView+WebCache.h>


typedef enum {
    actionButtonTypeAdd,
    actionButtonTypeRemove
} actionButtonType;


@class CalloutController;
@protocol CalloutControllerDelegate <NSObject>
-(void)calloutController:(CalloutController *)controller shouldRevealOnMap:(Place *)place;
-(void)calloutController:(CalloutController *)controller addedPlace:(Place *)place;
-(void)calloutController:(CalloutController *)controller shouldRemovePlace:(Place *)place;
-(void)calloutControllerDidDismiss:(CalloutController *)controller;
@end

@interface CalloutController : UIViewController <UIPopoverPresentationControllerDelegate, UIPopoverControllerDelegate>
@property (nonatomic, weak) id <CalloutControllerDelegate> delegate;

@property BOOL editing;
@property Place *place;
@property CLLocationCoordinate2D sourceLocation;

-(id)initWithPlace:(Place *)p andEditing:(BOOL)editing;
-(void)setButtonMode:(actionButtonType)buttonType;
-(IBAction)dismiss:(id)sender;

@end
