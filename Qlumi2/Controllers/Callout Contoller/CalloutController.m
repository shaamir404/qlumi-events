//
//  CalloutController.m
//  Qlumi
//
//  Created by Will on 2/6/18.
//  Copyright © 2018 Red Door Endeavors. All rights reserved.
//

#import "CalloutController.h"
#import <SafariServices/SafariServices.h>
#import <AFNetworking/AFNetworking.h>


@interface CalloutController ()<SFSafariViewControllerDelegate>{
    QCalloutView *calloutView;
}

@end

@implementation CalloutController


-(id)initWithPlace:(Place *)p andEditing:(BOOL)editing{
    if (self = [super init]) {
        self.place = p;
        self.editing = editing;
        
        self.view.backgroundColor = [UIColor colorWithWhite:0.25 alpha:0.25];
        self.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        calloutView = [[QCalloutView alloc] init];
        
        [calloutView.actionButton removeFromSuperview];
        calloutView.actionButton.layer.cornerRadius = calloutView.actionButton.frame.size.height/2;
        calloutView.actionButton.clipsToBounds = YES;
        [self.view addSubview:calloutView.actionButton];
        [self.view addSubview:calloutView];
        
        [self reloadData];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss:)];
        [self.view addGestureRecognizer:tap];
        
        NSMutableURLRequest* request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET" URLString:[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/details/json?key=AIzaSyC67qOLCwRi1_6Z8g1zKA6OQkJekYIBDz8&placeid=%@", self.place.placeId] parameters:@{} error:NULL];
        [request setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        AFHTTPRequestOperation *operation = [manager HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject){
            NSDictionary *result = responseObject[@"result"];
            if (result) {
                self.place = [[Place alloc] initWithDictionary:result];
                [self reloadData];
            }
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        }];
        
        [manager.operationQueue addOperation:operation];

    }
    return self;
}


-(void)setButtonMode:(actionButtonType)buttonType{
    switch (buttonType) {
        case actionButtonTypeRemove:
            [calloutView.actionButton setBackgroundImage:[UIImage imageNamed:@"remove"] forState:UIControlStateNormal];
            [calloutView.actionButton addTarget:self action:@selector(removePlace:) forControlEvents:UIControlEventTouchUpInside];
            break;
        default:
            [calloutView.actionButton setBackgroundImage:[UIImage imageNamed:@"plus-circle"] forState:UIControlStateNormal];
            [calloutView.actionButton addTarget:self action:@selector(addPlace:) forControlEvents:UIControlEventTouchUpInside];
            break;
    }
}


-(void)reloadData{
    [calloutView.imageView sd_setShowActivityIndicatorView:YES];
    [calloutView.imageView sd_setImageWithURL:self.place.photoURL placeholderImage:[UIImage imageNamed:@"photo-placeholder"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        self.place.thumbnail = image;
    }];
    calloutView.nameLabel.text = self.place.title;
    calloutView.addressLabel.text = self.place.address;

    
    [calloutView.revealOnMapButton addTarget:self action:@selector(revealOnMap:) forControlEvents:UIControlEventTouchUpInside];
    
    if (self.editing) {
        calloutView.actionButton.hidden = NO;
    }else{
        calloutView.actionButton.hidden = NO;
    }

    if (self.place.rating){
        [calloutView.ratingView setValue:self.place.rating];
    }
    if (!self.place.phoneNumber.absoluteString){
        calloutView.callButton.enabled = NO;
    }else{
        UIButton *button = (UIButton *)calloutView.callButton.customView;
        [button addTarget:self action:@selector(call:) forControlEvents:UIControlEventTouchUpInside];
    }
    if (self.place.website.absoluteString.length == 0){
        calloutView.websiteButton.enabled = NO;
    }else{
        calloutView.websiteButton.enabled = YES;
        UIButton *button = (UIButton *)calloutView.websiteButton.customView;
        [button addTarget:self action:@selector(openWebsite:) forControlEvents:UIControlEventTouchUpInside];
    }
    if (!self.place.address){
        calloutView.addressLabel.enabled = NO;
    }else{
        UIButton *button = (UIButton *)calloutView.directionsButton.customView;
        [button addTarget:self action:@selector(getDirections:) forControlEvents:UIControlEventTouchUpInside];
    }
    UIButton *button = (UIButton *)calloutView.shareButton.customView;
    [button addTarget:self action:@selector(share:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    
    calloutView.center = self.view.center;
    
    CGRect frame = calloutView.actionButton.frame;
    if (self.view.frame.size.width == 320) {
        frame.size.width = calloutView.revealOnMapButton.frame.size.height;
        frame.size.height = calloutView.revealOnMapButton.frame.size.height;
        frame.origin.x = (calloutView.frame.origin.x+calloutView.frame.size.width)-(frame.size.width+4);
        frame.origin.y = (calloutView.frame.origin.y)+calloutView.revealOnMapButton.frame.origin.y;
    }else{
        frame.origin.x = (calloutView.frame.origin.x+calloutView.frame.size.width)-((calloutView.actionButton.frame.size.width/2)+15);
        frame.origin.y = (calloutView.frame.origin.y)-((calloutView.actionButton.frame.size.width/2)-15);
    }
    
    calloutView.actionButton.layer.cornerRadius = frame.size.width/2;
    calloutView.actionButton.frame = frame;
    [self.view bringSubviewToFront:calloutView.actionButton];
}


#pragma mark - IBActions

-(IBAction)call:(id)sender{
    UIAlertController *callAlert = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"Call %@", self.place.title] message:[NSString stringWithFormat:@"Are you sure you'd like to call %@", self.place.title] preferredStyle:UIAlertControllerStyleAlert];
    
    [callAlert addAction:[UIAlertAction actionWithTitle:@"Call" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [[UIApplication sharedApplication] openURL:self.place.phoneNumber options:@{} completionHandler:nil];
    }]];
    [callAlert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];

    
    [self presentViewController:callAlert animated:YES completion:nil];
}

-(IBAction)revealOnMap:(id)sender{
    [self dismiss:nil];
    
    [self.delegate calloutController:self shouldRevealOnMap:self.place];
}

-(IBAction)openWebsite:(id)sender{
    SFSafariViewController *svc = [[SFSafariViewController alloc] initWithURL:self.place.website];
    [self presentViewController:svc animated:YES completion:nil];
}

-(IBAction)getDirections:(UIButton *)sender{
    
    BOOL googleMapsDefault = [[NSUserDefaults standardUserDefaults] boolForKey:@"openInGoogleMaps"];
    BOOL appleMapsDefault = [[NSUserDefaults standardUserDefaults] boolForKey:@"openInAppleMaps"];
    
    NSString *saddr = [NSString stringWithFormat:@"%f,%f", self.sourceLocation.latitude, self.sourceLocation.longitude];
    
    if (googleMapsDefault | appleMapsDefault) {
        if (googleMapsDefault) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"comgooglemaps://?saddr=%@&daddr=%@", saddr, [self urlReadyString:self.place.address]]] options:@{} completionHandler:nil];
        }
        if (appleMapsDefault) {
            NSString* directionsURL = [NSString stringWithFormat:@"http://maps.apple.com/?saddr=%@&daddr=%@", saddr, [self urlReadyString:self.place.address]];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString: directionsURL] options:@{} completionHandler:nil];
        }
    }else{
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"comgooglemaps://"]]) {
            UIAlertController *callAlert = [UIAlertController alertControllerWithTitle:@"Get Directions In..." message:nil preferredStyle:UIAlertControllerStyleActionSheet];
            [callAlert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
            [callAlert addAction:[UIAlertAction actionWithTitle:@"Google Maps" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"comgooglemaps://?saddr=%@", [self urlReadyString:self.place.address]]] options:@{} completionHandler:nil];
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"openInGoogleMaps"];
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"openInAppleMaps"];
            }]];
            [callAlert addAction:[UIAlertAction actionWithTitle:@"Apple Maps" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                NSString* directionsURL = [NSString stringWithFormat:@"http://maps.apple.com/?saddr=%@&daddr=%@",saddr,[self urlReadyString:self.place.address]];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString: directionsURL] options:@{} completionHandler:nil];
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"openInAppleMaps"];
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"openInGoogleMaps"];
            }]];
            
            UIPopoverPresentationController *popPresenter = [callAlert popoverPresentationController];
            [popPresenter setPermittedArrowDirections:UIPopoverArrowDirectionUp];
            popPresenter.sourceView = sender;
            popPresenter.sourceRect = sender.bounds;
            popPresenter.delegate = self;

        
            
            [self presentViewController:callAlert animated:YES completion:nil];
        }else{
            NSString* directionsURL = [NSString stringWithFormat:@"http://maps.apple.com/?saddr=%@&daddr=%@",saddr,[self urlReadyString:self.place.address]];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString: directionsURL] options:@{} completionHandler:nil];
        }
    }
}

-(IBAction)share:(id)sender{
    
    NSArray *shareItemArray = [[NSArray alloc] initWithObjects:self.place.thumbnail, self.place.title, @"\n", self.place.address, nil];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:shareItemArray applicationActivities:nil];
    activityVC.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll, UIActivityTypeAirDrop];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
            activityVC.popoverPresentationController.sourceView = sender;
        }
        [self presentViewController:activityVC animated:YES completion:nil];
    });
}

-(IBAction)addPlace:(id)sender{
    [self.delegate calloutController:self addedPlace:self.place];
    [self dismiss:nil];
}

-(IBAction)removePlace:(id)sender{
    [self.delegate calloutController:self shouldRemovePlace:self.place];
//    [self dismiss:nil];
}

-(IBAction)dismiss:(id)sender{
    [self dismissViewControllerAnimated:YES completion:^{
        [self.delegate calloutControllerDidDismiss:self];
    }];
}

#pragma mark - helpers

- (NSString *)urlReadyString:(NSString *)string {
    return [string stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
}


-(BOOL)popoverPresentationControllerShouldDismissPopover:(UIPopoverPresentationController *)popoverPresentationController{
    return YES;
}

@end
